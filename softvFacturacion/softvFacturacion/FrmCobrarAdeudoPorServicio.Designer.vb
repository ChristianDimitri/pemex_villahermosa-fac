﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCobrarAdeudoPorServicio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_total_a_pagar = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_enviar_a_Izquierda = New System.Windows.Forms.Button()
        Me.btn_agregar_todos_a_derecha = New System.Windows.Forms.Button()
        Me.btn_agregar_todos_a_izquierda = New System.Windows.Forms.Button()
        Me.btn_enviar_a_derecha = New System.Windows.Forms.Button()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lbl_MsgAyudaDobleClic = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkRed
        Me.Button6.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.White
        Me.Button6.Location = New System.Drawing.Point(567, 577)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(199, 61)
        Me.Button6.TabIndex = 31
        Me.Button6.Text = "Salir"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.BackColor = System.Drawing.Color.DarkRed
        Me.btn_Aceptar.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Aceptar.ForeColor = System.Drawing.Color.White
        Me.btn_Aceptar.Location = New System.Drawing.Point(798, 577)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(206, 61)
        Me.btn_Aceptar.TabIndex = 30
        Me.btn_Aceptar.Text = "Generar su cobro de adeudo"
        Me.btn_Aceptar.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(12, 577)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(141, 29)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Total a Pagar"
        Me.Label7.Visible = False
        '
        'txt_total_a_pagar
        '
        Me.txt_total_a_pagar.BackColor = System.Drawing.Color.White
        Me.txt_total_a_pagar.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_total_a_pagar.ForeColor = System.Drawing.Color.Maroon
        Me.txt_total_a_pagar.Location = New System.Drawing.Point(17, 609)
        Me.txt_total_a_pagar.Multiline = True
        Me.txt_total_a_pagar.Name = "txt_total_a_pagar"
        Me.txt_total_a_pagar.ReadOnly = True
        Me.txt_total_a_pagar.Size = New System.Drawing.Size(58, 22)
        Me.txt_total_a_pagar.TabIndex = 28
        Me.txt_total_a_pagar.Text = "$0.00"
        Me.txt_total_a_pagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txt_total_a_pagar.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(677, 63)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(212, 23)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "Servicios a cobrar adeudo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(222, -31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(461, 36)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Selecciona los Servicios a Adelanatar"
        '
        'btn_enviar_a_Izquierda
        '
        Me.btn_enviar_a_Izquierda.BackColor = System.Drawing.Color.DarkRed
        Me.btn_enviar_a_Izquierda.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_enviar_a_Izquierda.ForeColor = System.Drawing.Color.White
        Me.btn_enviar_a_Izquierda.Location = New System.Drawing.Point(424, 313)
        Me.btn_enviar_a_Izquierda.Name = "btn_enviar_a_Izquierda"
        Me.btn_enviar_a_Izquierda.Size = New System.Drawing.Size(125, 57)
        Me.btn_enviar_a_Izquierda.TabIndex = 25
        Me.btn_enviar_a_Izquierda.Text = "<"
        Me.btn_enviar_a_Izquierda.UseVisualStyleBackColor = False
        '
        'btn_agregar_todos_a_derecha
        '
        Me.btn_agregar_todos_a_derecha.BackColor = System.Drawing.Color.DarkRed
        Me.btn_agregar_todos_a_derecha.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_agregar_todos_a_derecha.ForeColor = System.Drawing.Color.White
        Me.btn_agregar_todos_a_derecha.Location = New System.Drawing.Point(424, 197)
        Me.btn_agregar_todos_a_derecha.Name = "btn_agregar_todos_a_derecha"
        Me.btn_agregar_todos_a_derecha.Size = New System.Drawing.Size(125, 59)
        Me.btn_agregar_todos_a_derecha.TabIndex = 24
        Me.btn_agregar_todos_a_derecha.Text = ">>"
        Me.btn_agregar_todos_a_derecha.UseVisualStyleBackColor = False
        '
        'btn_agregar_todos_a_izquierda
        '
        Me.btn_agregar_todos_a_izquierda.BackColor = System.Drawing.Color.DarkRed
        Me.btn_agregar_todos_a_izquierda.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_agregar_todos_a_izquierda.ForeColor = System.Drawing.Color.White
        Me.btn_agregar_todos_a_izquierda.Location = New System.Drawing.Point(424, 434)
        Me.btn_agregar_todos_a_izquierda.Name = "btn_agregar_todos_a_izquierda"
        Me.btn_agregar_todos_a_izquierda.Size = New System.Drawing.Size(125, 55)
        Me.btn_agregar_todos_a_izquierda.TabIndex = 23
        Me.btn_agregar_todos_a_izquierda.Text = "<<"
        Me.btn_agregar_todos_a_izquierda.UseVisualStyleBackColor = False
        '
        'btn_enviar_a_derecha
        '
        Me.btn_enviar_a_derecha.BackColor = System.Drawing.Color.DarkRed
        Me.btn_enviar_a_derecha.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_enviar_a_derecha.ForeColor = System.Drawing.Color.White
        Me.btn_enviar_a_derecha.Location = New System.Drawing.Point(424, 89)
        Me.btn_enviar_a_derecha.Name = "btn_enviar_a_derecha"
        Me.btn_enviar_a_derecha.Size = New System.Drawing.Size(125, 55)
        Me.btn_enviar_a_derecha.TabIndex = 22
        Me.btn_enviar_a_derecha.Text = ">"
        Me.btn_enviar_a_derecha.UseVisualStyleBackColor = False
        '
        'ListBox2
        '
        Me.ListBox2.BackColor = System.Drawing.Color.White
        Me.ListBox2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox2.ForeColor = System.Drawing.Color.Red
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 18
        Me.ListBox2.Location = New System.Drawing.Point(567, 89)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(405, 400)
        Me.ListBox2.TabIndex = 21
        '
        'ListBox1
        '
        Me.ListBox1.BackColor = System.Drawing.Color.White
        Me.ListBox1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.ForeColor = System.Drawing.Color.Black
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 18
        Me.ListBox1.Location = New System.Drawing.Point(15, 89)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(384, 400)
        Me.ListBox1.TabIndex = 20
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(120, 63)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(171, 23)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Servicios del Cliente"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(240, 2)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(500, 33)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "Selecciona los Servicios a Cobrar el Adeudo"
        '
        'lbl_MsgAyudaDobleClic
        '
        Me.lbl_MsgAyudaDobleClic.AutoSize = True
        Me.lbl_MsgAyudaDobleClic.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_MsgAyudaDobleClic.Location = New System.Drawing.Point(12, 501)
        Me.lbl_MsgAyudaDobleClic.Name = "lbl_MsgAyudaDobleClic"
        Me.lbl_MsgAyudaDobleClic.Size = New System.Drawing.Size(458, 18)
        Me.lbl_MsgAyudaDobleClic.TabIndex = 550
        Me.lbl_MsgAyudaDobleClic.Text = "Coloca los servicios a los cuales quieres cobrar el Adeudo en la lista derecha."
        '
        'FrmCobrarAdeudoPorServicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 650)
        Me.Controls.Add(Me.lbl_MsgAyudaDobleClic)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.btn_Aceptar)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_total_a_pagar)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btn_enviar_a_Izquierda)
        Me.Controls.Add(Me.btn_agregar_todos_a_derecha)
        Me.Controls.Add(Me.btn_agregar_todos_a_izquierda)
        Me.Controls.Add(Me.btn_enviar_a_derecha)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.Name = "FrmCobrarAdeudoPorServicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Cobro de Adeudo por Servicios"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents btn_Aceptar As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_total_a_pagar As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btn_enviar_a_Izquierda As System.Windows.Forms.Button
    Friend WithEvents btn_agregar_todos_a_derecha As System.Windows.Forms.Button
    Friend WithEvents btn_agregar_todos_a_izquierda As System.Windows.Forms.Button
    Friend WithEvents btn_enviar_a_derecha As System.Windows.Forms.Button
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lbl_MsgAyudaDobleClic As System.Windows.Forms.Label
End Class
