﻿Imports System.Data.SqlClient

Public Class classRoboDeSeñal

    Public Function buscaDT(ByVal prmContrato As Long) As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(prmContrato))
            buscaDT = BaseII.ConsultaDT("ConRoboDeSeñal")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub Actualiza(ByVal prmContrato As Long, ByVal prmDescripcion As String, ByVal Monto As Decimal, ByVal Cobrado As Boolean)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(prmContrato))
            BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, CStr(prmDescripcion))
            BaseII.CreateMyParameter("@Monto", SqlDbType.Money, CDec(Monto))
            BaseII.CreateMyParameter("@Cobrado", SqlDbType.Bit, CByte(Cobrado))

            BaseII.Inserta("NueRoboDeSeñal")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ChecaRoboDeSeñal(ByVal prmContrato As Long) As Integer

        Dim CON As New SqlConnection(MiConexion)
        Dim Comando As SqlClient.SqlCommand
        CON.Open()
        Try
            Comando = New SqlClient.SqlCommand
            With Comando
                .Connection = CON
                .CommandText = "ChecaRoboDeSeñal"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@Res", SqlDbType.Int)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Output

                prm.Value = prmContrato
                prm1.Value = 0
                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                Dim i As Integer = Comando.ExecuteNonQuery()
                ChecaRoboDeSeñal = prm1.Value
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
