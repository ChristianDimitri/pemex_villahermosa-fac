Imports System.Data.SqlClient
Public Class FrmCancela_NotaCredito

    Private Sub FrmCancela_NotaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.MuestraMotivoCancelacion' Puede moverla o quitarla seg�n sea necesario.
        Dim con As New SqlClient.SqlConnection(MiConexion)
        con.Open()
        Me.MuestraMotivoCancelacionTableAdapter.Connection = con
        Me.MuestraMotivoCancelacionTableAdapter.Fill(Me.DataSetLydia.MuestraMotivoCancelacion)
        con.Close()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.ComboBox1.Text.Trim.Length > 0 Then
            gloMotivoCan = Me.ComboBox1.SelectedValue
            BrwNotasdeCredito.Cancelacion(gloMotivoCan)
            refrescar = True
            Me.Close()
        Else
            MsgBox("Debes de Elegir un Motivo de Cancelaci�n")
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        gloMotivoCan = 0
        Me.Close()
    End Sub
End Class