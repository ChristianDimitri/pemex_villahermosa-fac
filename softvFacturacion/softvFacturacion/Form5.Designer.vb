<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form5
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form5))
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet
        Me.CONDETFACTURASBANCOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONDETFACTURASBANCOSTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.CONDETFACTURASBANCOSTableAdapter
        Me.CONDETFACTURASBANCOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.CONDETFACTURASBANCOSDataGridView = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONDETFACTURASBANCOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONDETFACTURASBANCOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONDETFACTURASBANCOSBindingNavigator.SuspendLayout()
        CType(Me.CONDETFACTURASBANCOSDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONDETFACTURASBANCOSBindingSource
        '
        Me.CONDETFACTURASBANCOSBindingSource.DataMember = "CONDETFACTURASBANCOS"
        Me.CONDETFACTURASBANCOSBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'CONDETFACTURASBANCOSTableAdapter
        '
        Me.CONDETFACTURASBANCOSTableAdapter.ClearBeforeFill = True
        '
        'CONDETFACTURASBANCOSBindingNavigator
        '
        Me.CONDETFACTURASBANCOSBindingNavigator.AddNewItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.BindingSource = Me.CONDETFACTURASBANCOSBindingSource
        Me.CONDETFACTURASBANCOSBindingNavigator.CountItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.DeleteItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem})
        Me.CONDETFACTURASBANCOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONDETFACTURASBANCOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.MoveLastItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.MoveNextItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.Name = "CONDETFACTURASBANCOSBindingNavigator"
        Me.CONDETFACTURASBANCOSBindingNavigator.PositionItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.Size = New System.Drawing.Size(743, 25)
        Me.CONDETFACTURASBANCOSBindingNavigator.TabIndex = 0
        Me.CONDETFACTURASBANCOSBindingNavigator.Text = "BindingNavigator1"
        '
        'CONDETFACTURASBANCOSBindingNavigatorSaveItem
        '
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONDETFACTURASBANCOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.Name = "CONDETFACTURASBANCOSBindingNavigatorSaveItem"
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'CONDETFACTURASBANCOSDataGridView
        '
        Me.CONDETFACTURASBANCOSDataGridView.AllowUserToAddRows = False
        Me.CONDETFACTURASBANCOSDataGridView.AllowUserToDeleteRows = False
        Me.CONDETFACTURASBANCOSDataGridView.AutoGenerateColumns = False
        Me.CONDETFACTURASBANCOSDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DataGridViewTextBoxColumn21, Me.DataGridViewTextBoxColumn22})
        Me.CONDETFACTURASBANCOSDataGridView.DataSource = Me.CONDETFACTURASBANCOSBindingSource
        Me.CONDETFACTURASBANCOSDataGridView.Location = New System.Drawing.Point(22, 75)
        Me.CONDETFACTURASBANCOSDataGridView.Name = "CONDETFACTURASBANCOSDataGridView"
        Me.CONDETFACTURASBANCOSDataGridView.Size = New System.Drawing.Size(474, 220)
        Me.CONDETFACTURASBANCOSDataGridView.TabIndex = 2
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clv_SessionBancos"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clv_SessionBancos"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clv_Id"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clv_Id"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "FECHA"
        Me.DataGridViewTextBoxColumn3.HeaderText = "FECHA"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "IMPORTE_TOTAL"
        Me.DataGridViewTextBoxColumn4.HeaderText = "IMPORTE_TOTAL"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "PROCESO_CANCELADO"
        Me.DataGridViewTextBoxColumn5.HeaderText = "PROCESO_CANCELADO"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "FECHA_GRAL"
        Me.DataGridViewTextBoxColumn6.HeaderText = "FECHA_GRAL"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "IMPORTE"
        Me.DataGridViewTextBoxColumn7.HeaderText = "IMPORTE"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "REALIZADO"
        Me.DataGridViewTextBoxColumn8.HeaderText = "REALIZADO"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "CANCELADA"
        Me.DataGridViewTextBoxColumn9.HeaderText = "CANCELADA"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "PAGADO"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "PAGADO"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "NOMBRE"
        Me.DataGridViewTextBoxColumn10.HeaderText = "NOMBRE"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "TELEFONO"
        Me.DataGridViewTextBoxColumn11.HeaderText = "TELEFONO"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "CONTRATO"
        Me.DataGridViewTextBoxColumn12.HeaderText = "CONTRATO"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "CUENTA_BANCO"
        Me.DataGridViewTextBoxColumn13.HeaderText = "CUENTA_BANCO"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "TIPO_CUENTA"
        Me.DataGridViewTextBoxColumn14.HeaderText = "TIPO_CUENTA"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "VENCIMIENTO"
        Me.DataGridViewTextBoxColumn15.HeaderText = "VENCIMIENTO"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "CODIGOSEGURIDAD"
        Me.DataGridViewTextBoxColumn16.HeaderText = "CODIGOSEGURIDAD"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "NOMTARJETA"
        Me.DataGridViewTextBoxColumn17.HeaderText = "NOMTARJETA"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "BANCO"
        Me.DataGridViewTextBoxColumn18.HeaderText = "BANCO"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.DataPropertyName = "SUCURSAL"
        Me.DataGridViewTextBoxColumn19.HeaderText = "SUCURSAL"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.DataPropertyName = "CAJERA"
        Me.DataGridViewTextBoxColumn20.HeaderText = "CAJERA"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.DataPropertyName = "CAJA"
        Me.DataGridViewTextBoxColumn21.HeaderText = "CAJA"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.DataPropertyName = "Error"
        Me.DataGridViewTextBoxColumn22.HeaderText = "Error"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        '
        'Form5
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(743, 445)
        Me.Controls.Add(Me.CONDETFACTURASBANCOSDataGridView)
        Me.Controls.Add(Me.CONDETFACTURASBANCOSBindingNavigator)
        Me.Name = "Form5"
        Me.Text = "Form5"
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONDETFACTURASBANCOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONDETFACTURASBANCOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONDETFACTURASBANCOSBindingNavigator.ResumeLayout(False)
        Me.CONDETFACTURASBANCOSBindingNavigator.PerformLayout()
        CType(Me.CONDETFACTURASBANCOSDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents CONDETFACTURASBANCOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONDETFACTURASBANCOSTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.CONDETFACTURASBANCOSTableAdapter
    Friend WithEvents CONDETFACTURASBANCOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents CONDETFACTURASBANCOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONDETFACTURASBANCOSDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
