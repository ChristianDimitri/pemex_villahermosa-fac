﻿
Imports System.Data.SqlClient
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Public Class FrmFacCobroABajas

    Dim eCont As Integer = 0
    Dim identi As Integer = 0
    Dim eActTickets As Boolean = False
    Dim eMsjTickets As String = ""
    Private customersByCityReport As ReportDocument


    Private Sub CobraABajas(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("CobraABajas", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Contrato
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Res", SqlDbType.Int)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par4.Direction = ParameterDirection.Output
        comando.Parameters.Add(par4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eRes = par3.Value
            If eRes = 1 Then
                labelError.Text = ""
                labelError.Text = par4.Value
                panelError.Visible = True
                bnPagar.Enabled = False
            Else
                panelError.Visible = False
                bnPagar.Enabled = True
                gloClv_Session = par2.Value
                DameDetalleCobraABajas(gloClv_Session)
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub DameDetalleCobraABajas(ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC DameDetalleCobraABajas " + Clv_Session.ToString)
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            dGDetalle.DataSource = bindingSource
            dGDetalle.Columns(0).Visible = False
            dGDetalle.Columns(1).Visible = False
            dGDetalle.Columns(2).Visible = False
            dGDetalle.Columns(3).Visible = False
            dGDetalle.Columns(7).Visible = False
            dGDetalle.Columns(4).Width = 120
            dGDetalle.Columns(5).Width = 300

            labelTotal.Text = "$" + dGDetalle(7, 0).Value.ToString

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub BUSCACLIENTES(ByVal OP As Integer)

        Try

            Dim conexion As New SqlConnection(MiConexion)

            If IsNumeric(gloClv_Session) = True Then
                conexion.Open()
                Me.BorraClv_SessionTableAdapter.Connection = conexion
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(gloClv_Session, Long)))
                conexion.Close()

            End If

            'If IsNumeric(GloContrato) = True And GloContrato > 0 Then

            conexion.Open()
            Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = conexion
            Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, GloContrato, 0)
            Me.DAMETIPOSCLIENTESTableAdapter.Connection = conexion
            Me.DAMETIPOSCLIENTESTableAdapter.Fill(Me.DataSetEdgar.DAMETIPOSCLIENTES, GloContrato)
            conexion.Close()

            Dime_Si_DatosFiscales(GloContrato)

            CREAARBOL()
            CobraABajas(GloContrato)

            ' End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "BUSCACLIENTES()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)
        End Try


    End Sub

    Private Sub CREAARBOL()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next
            Dim CON15 As New SqlConnection(MiConexion)
            CON15.Open()

            If IsNumeric(GloContrato) = True Then
                Me.DameSerDELCliFACTableAdapter.Connection = CON15
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(GloContrato, Long)))
            Else
                Me.DameSerDELCliFACTableAdapter.Connection = CON15
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            'CON15.Dispose()
            'CON15.Close()
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewsoftvDataSet.DameSerDELCliFAC.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)


                    pasa = True

                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisión Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Teléfonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    'Nostramos los servicios del Cliente de Telefonía

                    pasa = True

                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            CON15.Dispose()
            CON15.Close()

            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "CREARARBOL()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)
        End Try

    End Sub

    Private Sub Dime_Si_DatosFiscales(ByVal Contrato As Long)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As SqlCommand
        Dim parcial As Integer
        Dim Liperiodo As String

        comando = New SqlClient.SqlCommand
        With comando
            .Connection = conexion
            .CommandText = "Dime_Si_DatosFiscales "
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            ' Create a SqlParameter for each parameter in the stored procedure.
            Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
            Dim prm1 As New SqlParameter("@cont", SqlDbType.Int)
            Dim prm2 As New SqlParameter("@Periodo", SqlDbType.VarChar, 50)
            Dim prm3 As New SqlParameter("@al_corriente", SqlDbType.VarChar, 100)
            prm.Direction = ParameterDirection.Input
            prm1.Direction = ParameterDirection.Output
            prm2.Direction = ParameterDirection.Output
            prm3.Direction = ParameterDirection.Output
            prm.Value = Contrato
            prm1.Value = 0
            prm2.Value = 0
            prm3.Value = ""
            .Parameters.Add(prm)
            .Parameters.Add(prm1)
            .Parameters.Add(prm2)
            'If IdSistema = "VA" Then
            .Parameters.Add(prm3)
            Try
                conexion.Open()
                comando.ExecuteNonQuery()

                parcial = prm1.Value
                Liperiodo = prm2.Value


                If parcial > 0 Then
                    Me.REDLabel25.Visible = True
                    Me.REDLabel25.Text = " EL CLIENTE CUENTA CON DATOS FISCALES "
                Else
                    Me.REDLabel25.Visible = False
                End If

            Catch ex As Exception
                conexion.Close()
                conexion.Dispose()
            End Try



        End With
    End Sub

    Private Sub bnBuscarCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnBuscarCliente.Click
        Glocontratosel = 0
        GloContrato = 0
        FrmSelCliente.Show()
    End Sub

    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)
        Try

            Dim ba As Boolean = False
            Dim customersByCityReport As New ReportDocument

            Dim connectionInfo As New ConnectionInfo
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BusFacFiscalTableAdapter.Connection = CON
            Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
            CON.Dispose()
            CON.Close()

            eActTickets = False
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
            Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
            CON2.Close()

            'Si es una Factura Fiscal
            If IdSistema = "SA" And facnormal = True And identi > 0 Then
                reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
                ba = True
            Else
                ConfigureCrystalReports_tickets(Clv_Factura)
                Exit Sub
            End If


            'End If

            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '@Clv_Factura 
            customersByCityReport.SetParameterValue(0, Clv_Factura)
            '@Clv_Factura_Ini
            customersByCityReport.SetParameterValue(1, "0")
            '@Clv_Factura_Fin
            customersByCityReport.SetParameterValue(2, "0")
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(3, "01/01/1900")
            '@Fecha_Fin
            customersByCityReport.SetParameterValue(4, "01/01/1900")
            '@op
            customersByCityReport.SetParameterValue(5, "0")

            If ba = False Then
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
                If eActTickets = True Then
                    customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
                End If
            End If

            'If facticket = 1 Then
            '    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"
            If (IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "AG" Or IdSistema = "VA") And facnormal = True And identi > 0 Then

                customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            Else
                customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
            End If

            If IdSistema = "AG" And facnormal = True And identi > 0 Then
                eCont = 1
                eRes = 0
                Do
                    customersByCityReport.PrintToPrinter(1, True, 1, 1)
                    eRes = MsgBox("La Impresión de la Factura " + CStr(eCont) + "/3, ¿Fué Correcta?", MsgBoxStyle.YesNo, "Atención")
                    '6=Yes;7=No
                    If eRes = 6 Then eCont = eCont + 1

                Loop While eCont <= 3
            Else
                If IdSistema = "SA" Then
                    MsgBox("Se va a imprimir una Factura Fiscal. ¿Está lista la impresora?", MsgBoxStyle.OkOnly)
                    customersByCityReport.PrintToPrinter(1, True, 1, 1)
                    MsgBox("Se va a imprimir la copia de la Factura Fiscal anterior. ¿Está lista la impresora?", MsgBoxStyle.OkOnly)
                    customersByCityReport.PrintToPrinter(1, True, 1, 1)
                Else
                    customersByCityReport.PrintToPrinter(1, True, 1, 1)
                End If

            End If


            customersByCityReport = Nothing

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long)
        Try


            Dim ba As Boolean = False
            Select Case IdSistema
                Case "VA"
                    customersByCityReport = New ReporteCajasTickets_2VA
                Case "LO"
                    customersByCityReport = New ReporteCajasTickets_2Log
                Case "AG"
                    customersByCityReport = New ReporteCajasTickets_2AG
                Case "SA"
                    customersByCityReport = New ReporteCajasTickets_2SA
                Case "TO"
                    customersByCityReport = New ReporteCajasTickets_2TOM
                Case Else
                    customersByCityReport = New ReporteCajasTickets_2OLD
            End Select


            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing

            '        If GloImprimeTickets = False Then
            'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
            'Else
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BusFacFiscalTableAdapter.Connection = CON
            Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
            CON.Close()
            CON.Dispose()

            eActTickets = False
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
            Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
            CON2.Close()
            CON2.Dispose()

            'If IdSistema = "VA" Then
            '    'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
            '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
            'Else
            '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
            'End If

            'End If

            'customersByCityReport.Load(reportPath)
            'If GloImprimeTickets = False Then
            '    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            'End If
            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '@Clv_Factura 
            customersByCityReport.SetParameterValue(0, Clv_Factura)
            '@Clv_Factura_Ini
            customersByCityReport.SetParameterValue(1, "0")
            '@Clv_Factura_Fin
            customersByCityReport.SetParameterValue(2, "0")
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(3, "01/01/1900")
            '@Fecha_Fin
            customersByCityReport.SetParameterValue(4, "01/01/1900")
            '@op
            customersByCityReport.SetParameterValue(5, "0")

            If ba = False Then
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
                If eActTickets = True Then
                    customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
                End If
            End If

            'If facticket = 1 Then
            '    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"

            customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets


            customersByCityReport.PrintToPrinter(1, True, 1, 1)





            'CrystalReportViewer1.ReportSource = customersByCityReport

            'If GloOpFacturas = 3 Then
            'CrystalReportViewer1.ShowExportButton = False
            'CrystalReportViewer1.ShowPrintButton = False
            'CrystalReportViewer1.ShowRefreshButton = False
            'End If
            'SetDBLogonForReport2(connectionInfo)
            customersByCityReport.Dispose()



            'Dim ba As Boolean = False
            ''Select Case IdSistema
            ''    Case "LO"
            'If Ticket_Fac_Normal = False And Ticket_Fac_EstadoCuenta = True Then
            '    customersByCityReport = New ReporteCajasTickets_2Log
            'End If

            ''End Select

            'Dim connectionInfo As New ConnectionInfo
            ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            ''    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            'Dim reportPath As String = Nothing

            'eActTickets = False
            'Dim CON2 As New SqlConnection(MiConexion)
            'CON2.Open()
            'Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
            'Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
            'CON2.Close()
            'CON2.Dispose()

            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'customersByCityReport.SetParameterValue(0, Clv_Factura)
            'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
            'customersByCityReport.PrintToPrinter(1, True, 1, 1)
            'customersByCityReport.Dispose()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub FrmFacCobroABajas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Glocontratosel > 0 Then
            GloContrato = Glocontratosel
            ContratoTextBox.Text = GloContrato
            Glocontratosel = 0
            BUSCACLIENTES(0)

        End If


        If GLOSIPAGO = 1 Then 'SI YA PAGO PASA Y SI NO AL CHORIZO

            GLOSIPAGO = 0
            If IsNumeric(GLOEFECTIVO) = False Then GLOEFECTIVO = 0
            If IsNumeric(GLOCHEQUE) = False Then GLOCHEQUE = 0
            If IsNumeric(GLOCLV_BANCOCHEQUE) = False Then GLOCLV_BANCOCHEQUE = 0
            If Len(NUMEROCHEQUE) = 0 Then NUMEROCHEQUE = ""
            If IsNumeric(GLOTARJETA) = False Then GLOTARJETA = 0
            If IsNumeric(GLOCLV_BANCOTARJETA) = False Then GLOCLV_BANCOTARJETA = 0
            If Len(NUMEROTARJETA) = 0 Then NUMEROTARJETA = ""
            If Len(TARJETAAUTORIZACION) = 0 Then TARJETAAUTORIZACION = ""
            'GloOpCobro = 0
            'If PanelNrm.Visible = True Then
            '    GloOpCobro = 1
            'End If

            Dim CON As New SqlConnection(MiConexion)

            'Aquí hay que checar si el cobro va por Estado de Cuenta o por Pago por Facturación Normal.
            'Si quieren realizar pagos adelantados a un cliente con Estados de Cuenta se facturará utilizando Facturación Normal 



            '-----------------------------------------------------------------------------------------------------------------
            'Grabamos los Clientes de Facturación Normal

            CON.Open()
            Dim comando As SqlClient.SqlCommand
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CON
                .CommandText = "GrabaFacturas "
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Me.ContratoTextBox.Text
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = gloClv_Session  'Me.Clv_Session.Text
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Cajera", SqlDbType.VarChar, 11)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = GloUsuario
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@Sucursal", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = GloSucursal
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@Caja", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = GloCaja
                .Parameters.Add(prm4)

                Dim prm5 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
                prm5.Direction = ParameterDirection.Input
                'If Me.SplitContainer1.Panel1Collapsed = False Then
                '    GloTipo = "V"
                '    prm5.Value = "V"
                'Else
                GloTipo = "C"
                prm5.Value = "C"
                'End If

                .Parameters.Add(prm5)

                Dim prm6 As New SqlParameter("@Serie_V", SqlDbType.VarChar, 5)
                prm6.Direction = ParameterDirection.Input
                'If Len(Loc_Serie) = 0 Then
                'Loc_Serie = ""
                'End If
                prm6.Value = ""
                .Parameters.Add(prm6)


                Dim prm7 As New SqlParameter("@Folio_V", SqlDbType.BigInt)
                prm7.Direction = ParameterDirection.Input
                'If IsNumeric(Loc_Folio) = False Then
                'Loc_Folio = 0
                'End If
                prm7.Value = 0
                .Parameters.Add(prm7)

                Dim prm8 As New SqlParameter("@Clv_Vendedor", SqlDbType.Int)
                prm8.Direction = ParameterDirection.Input
                prm8.Value = 0
                .Parameters.Add(prm8)

                Dim prm9 As New SqlParameter("@BndError", SqlDbType.Int)
                prm9.Direction = ParameterDirection.Input
                prm9.Value = 0
                .Parameters.Add(prm9)

                Dim prm10 As New SqlParameter("@Msg", SqlDbType.VarChar, 250)
                prm10.Direction = ParameterDirection.Input
                prm10.Value = ""
                .Parameters.Add(prm10)

                Dim prm17 As New SqlParameter("@Clv_FacturaSalida", SqlDbType.BigInt)
                prm17.Direction = ParameterDirection.Output
                prm17.Value = 0
                .Parameters.Add(prm17)


                Dim i As Integer = comando.ExecuteNonQuery()
                GloClv_Factura = prm17.Value

                'mensaje = prm10.Value

                Ticket_Fac_Normal = True
                Ticket_Fac_EstadoCuenta = False

            End With
            CON.Close()

            Guarda_Tipo_Tarjeta(GloClv_Factura, GloTipoTarjeta, GLOTARJETA)

            '-----------------------------------------------------------------------------------------------------------------

            CON.Open()
            Dim comando2 As SqlClient.SqlCommand
            comando2 = New SqlClient.SqlCommand
            With comando2
                .Connection = CON
                .CommandText = "GUARDATIPOPAGO "
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = GloClv_Factura
                .Parameters.Add(prm)

                Dim prm2 As New SqlParameter("@GLOEFECTIVO", SqlDbType.Money)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = GLOEFECTIVO
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@GLOCHEQUE", SqlDbType.Money)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = GLOCHEQUE
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@GLOCLV_BANCOCHEQUE", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = GLOCLV_BANCOCHEQUE
                .Parameters.Add(prm4)

                Dim prm5 As New SqlParameter("@NUMEROCHEQUE", SqlDbType.VarChar)
                prm5.Direction = ParameterDirection.Input
                prm5.Value = NUMEROCHEQUE
                .Parameters.Add(prm5)

                Dim prm6 As New SqlParameter("@GLOTARJETA", SqlDbType.Money)
                prm6.Direction = ParameterDirection.Input
                prm6.Value = GLOTARJETA
                .Parameters.Add(prm6)

                Dim prm7 As New SqlParameter("@GLOCLV_BANCOTARJETA", SqlDbType.Int)
                prm7.Direction = ParameterDirection.Input
                prm7.Value = GLOCLV_BANCOTARJETA
                .Parameters.Add(prm7)

                Dim prm8 As New SqlParameter("@NUMEROTARJETA", SqlDbType.VarChar)
                prm8.Direction = ParameterDirection.Input
                prm8.Value = NUMEROTARJETA
                .Parameters.Add(prm8)

                Dim prm9 As New SqlParameter("@TARJETAAUTORIZACION", SqlDbType.VarChar)
                prm9.Direction = ParameterDirection.Input
                prm9.Value = TARJETAAUTORIZACION
                .Parameters.Add(prm9)

                Dim j As Integer = comando2.ExecuteNonQuery()

            End With



            'Fin Guarda si el tipo de Pago es con Nota de Credito
            If LocImpresoraTickets = "" Then
                MsgBox("No Se Ha Asignado Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)




            Else
                ':: IMPRESIÓN DE TICKETS :: // Diferenciando entre Ticket Normal y Ticket por Estado de Cuenta
                ConfigureCrystalReports(GloClv_Factura)


            End If

            GLOSIPAGO = 0
            Me.ContratoTextBox.Text = 0
            GloContrato = 0
            Glocontratosel = 0
            gloClv_Session = 0
            BUSCACLIENTES(0)

        End If

    End Sub

    Private Sub Guarda_Tipo_Tarjeta(ByVal Clv_factura As Long, ByVal Tipo As Integer, ByVal Monto As Decimal)
        Dim CON100 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()

        Try
            SQL = New SqlCommand()
            CON100.Open()
            With SQL
                .CommandText = "Nuevo_Rel_Pago_Tarjeta_Factura"
                .Connection = CON100
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@Clv_factura bigint,@Tipo int,@Monto money
                Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Clv_factura
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Tipo", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = Tipo
                .Parameters.Add(prm1)

                prm = New SqlParameter("@Monto", SqlDbType.Money)
                prm.Direction = ParameterDirection.Input
                prm.Value = Monto
                .Parameters.Add(prm)

                Dim ia As Integer = .ExecuteNonQuery()
            End With
            CON100.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmFacCobroABajas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        GloContrato = 0
        Glocontratosel = 0
        gloClv_Session = 0

        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        Me.DamedatosUsuarioTableAdapter.Connection = conexion
        Me.DamedatosUsuarioTableAdapter.Fill(Me.NewsoftvDataSet.DamedatosUsuario, GloUsuario)
        Me.DAMENOMBRESUCURSALTableAdapter.Connection = conexion
        Me.DAMENOMBRESUCURSALTableAdapter.Fill(Me.NewsoftvDataSet.DAMENOMBRESUCURSAL, GloSucursal)
        Me.DameDatosGeneralesTableAdapter.Connection = conexion
        Me.DameDatosGeneralesTableAdapter.Fill(Me.NewsoftvDataSet.DameDatosGenerales)
        conexion.Close()

        Me.LblNomCaja.Text = GlonOMCaja
        Me.LblVersion.Text = My.Application.Info.Version.ToString

    End Sub

    Private Sub ContratoTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ContratoTextBox.KeyDown
        If e.KeyValue = 13 And IsNumeric(ContratoTextBox.Text) = True Then
            GloContrato = ContratoTextBox.Text
            BUSCACLIENTES(0)
        Else
            GloContrato = 0
            BUSCACLIENTES(0)
        End If
    End Sub

    Private Sub bnPagar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnPagar.Click
        If dGDetalle.Rows.Count = 0 Then
            MsgBox("No hay adeudos que cobrar.", MsgBoxStyle.Information)
            Exit Sub
        End If
        GLOIMPTOTAL = 0
        GLOIMPTOTAL = dGDetalle(7, 0).Value
        FrmPago.Show()
    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged
        Limpiar()
    End Sub

    Private Sub Limpiar()
        If Glocontratosel = 0 Then
            'Dim bindingSource As New BindingSource
            GloContrato = 0
            BUSCACLIENTES(0)
            dGDetalle.DataSource = Nothing
            labelTotal.Text = "$0.00"
            labelError.Text = ""
            panelError.Visible = False
        End If
    End Sub

    Private Sub lblSumaCargos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBlblSumaCargos.Click

    End Sub
End Class