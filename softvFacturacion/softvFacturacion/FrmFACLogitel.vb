Imports System.Net
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
'------------------------------------------------------
Imports System.Text
Imports System.Xml
Imports System.IO


Public Class FrmFACLogitel
    Private customersByCityReport As ReportDocument
    Dim BndError As Integer = 0
    Dim Loc_Clv_Vendedor As Integer = 0
    Dim Loc_Folio As Long = 0
    Dim Loc_Serie As String = Nothing
    Dim Msg As String = Nothing
    Dim bloqueado, identi As Integer
    Dim Bnd As Integer = 0
    Dim CuantasTv As Integer = 0
    Private LocNomImpresora_Contratos As String = Nothing
    Private LocNomImpresora_Tarjetas As String = Nothing
    Dim SiPagos As Integer = 0
    Private eMsjTickets As String = Nothing
    Private eActTickets As Boolean = False
    Private eCont As Integer = 1
    Private eRes As Integer = 0
    Private ePideAparato As Integer = 0
    Private eClv_Detalle As Long = 0
    Private Checa_recurrente As Integer = Nothing
    Private es_recurrente As Boolean = False
    Private ya_entre As Integer = 0
    Private Tipo_Cargos_a_facturar As Integer
    Private EsEdoCta As Boolean
    '------------------------------------------------
    Private PuedoQuitarDeLaLista As Boolean = True
    Private XML_DetCargos As String = ""
    Private IDEdoCta As Integer
    Private FacturacionNormal_EstadoDeCuentaNoGeneradoAun As Boolean = False
    Private PrimeraVezSelectedRow As Boolean = True

    Private Glo_TotalAbonadoACuenta As Double = 0

    'Variables Auxiliares para la Promoci�n
    Private GloClaveDelDetalle_UNO As Integer = 0
    Private GloClaveDelDetalle_DOS As Integer = 0

    'Variables para checar si se cobra por Estado de Cuenta o No
    Dim GloBndEdoCta As Boolean = True
    Dim GloYaFueGenerado As Boolean = True

    'SAUL ROBO SE�AL
    Dim RoboSe�al As New classRoboDeSe�al
    Dim eRespuesta As Integer
    'SAUL FIN

    'Direccion Sucursal
    Dim RCalleSucur As String = Nothing
    Dim RNumSucur As String = Nothing
    Dim RColSucur As String = Nothing
    Dim RMuniSucur As String = Nothing
    Dim RCiudadSucur As String = Nothing
    Dim RCPSucur As String = Nothing
    Dim RTelSucur As String = Nothing

    Private Sub Guarda_Cobro_Rel_Supervisor_cobrosRec(ByVal clv_factura As Long, ByVal clv_supervisor As String, ByVal contrato As Long)
        Dim CON90 As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand()
        Try
            CMD = New SqlCommand()
            CON90.Open()
            With CMD
                .CommandText = "Guarda_Cobro_Rel_Supervisor_cobrosRec"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON90
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@clv_factura", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_factura
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@clv_supervisor", SqlDbType.VarChar, 5)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = clv_supervisor
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = contrato
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery()
            End With
            CON90.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Guarda_Cobro_Rel_Supervisor_cobrosRec"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try
    End Sub

    Private Sub HazFacturaDigital(ByVal LocClv_Factura As Long)
        Try
            FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
            Dim identi As Integer = 0
            FacturacionDigitalSoftv.ClassCFDI.EsTimbrePrueba = False
            identi = 0
            identi = FacturacionDigitalSoftv.ClassCFDI.BusFacFiscalOledb(LocClv_Factura, MiConexion)
            'fin Facturacion Digital
            FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
            FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""

            If CInt(identi) > 0 Then
                FacturacionDigitalSoftv.ClassCFDI.Locop = 0
                FacturacionDigitalSoftv.ClassCFDI.Dime_Aque_Compania_Facturarle(LocClv_Factura, MiConexion)
                FacturacionDigitalSoftv.ClassCFDI.Graba_Factura_Digital(LocClv_Factura, identi, MiConexion)
                Try
                    If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                        Dim frm As New FacturacionDigitalSoftv.FrmImprimir
                        frm.ShowDialog()
                    Else
                        MsgBox("No se genero la factura digital cancele y vuelva a intentar por favor")
                    End If
                    'FormPruebaDigital.Show()
                Catch ex As Exception
                End Try

                Dim r As New Globalization.CultureInfo("es-MX")
                r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
                System.Threading.Thread.CurrentThread.CurrentCulture = r

                FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
                FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Checa_es_recurrente()
        Dim CON80 As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand()
        Try
            CMD = New SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Checa_si_es_pago_cargo_recurrente"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = CLng(Me.ContratoTextBox.Text)
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@error", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim i As Integer = .ExecuteNonQuery()
                Checa_recurrente = prm1.Value
            End With
            CON80.Close()
            If Checa_recurrente = 1 Then
                locband_pant = 8
                es_recurrente = True
                If eAccesoAdmin = False Then
                    Bloque(False)
                    FrmSupervisor.Show()
                Else
                    es_recurrente = False
                    Bloque(True)
                End If
                bndGraboClienteRecurrente = True
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Checa_si_es_pago_cargo_recurrente"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    'Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
    ' Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
    '     For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
    '         myTableLogOnInfo.ConnectionInfo = myConnectionInfo
    '     Next
    ' End Sub

    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)
        Try
            'Comentado 18/01/2018
            'Dim ba As Boolean = False
            'customersByCityReport = New ReportDocument
            'Dim reportPath As String = Nothing

            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Me.BusFacFiscalTableAdapter.Connection = CON
            'Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
            'CON.Dispose()
            'CON.Close()

            'eActTickets = False
            'Dim CON2 As New SqlConnection(MiConexion)
            'CON2.Open()
            'Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
            'Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
            'CON2.Close()
            'Comentado 18/01/2018

            'If IdSistema = "SA" And facnormal = True And identi > 0 Then
            '    reportPath = RutaReportes + "\ReportesFacturasXsd.rpt"
            '    ba = True
            'ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
            '    reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
            '    ba = True
            'ElseIf IdSistema = "AG" And facnormal = True And identi > 0 Then
            '    reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
            '    ba = True
            'ElseIf IdSistema = "VA" And facnormal = True And identi > 0 Then
            '    reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
            '    ba = True
            'Else
            If IdSistema = "VA" Then
                'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
                ConfigureCrystalReports_tickets(Clv_Factura, "Original")
                Exit Sub
            Else
                ConfigureCrystalReports_tickets(Clv_Factura, "Original")
                Exit Sub
            End If
            'End If

            'Comentado 18/01/2018
            '    ReportesFacturasXsd(Clv_Factura, 0, 0, "01-01-1900", "01-01-1900", 0, reportPath)


            '    If ba = False Then
            '        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            '        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            '        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            '        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            '        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            '        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            '        If eActTickets = True Then
            '            customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            '        End If
            '    End If

            '    'If facticket = 1 Then
            '    '    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"
            '    If (IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "AG" Or IdSistema = "VA") And facnormal = True And identi > 0 Then

            '        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            '    Else
            '        customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
            '    End If

            '    If IdSistema = "AG" And facnormal = True And identi > 0 Then
            '        eCont = 1
            '        eRes = 0
            '        Do
            '            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '            eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont) + "/3, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
            '            '6=Yes;7=No
            '            If eRes = 6 Then eCont = eCont + 1

            '        Loop While eCont <= 3
            '    Else
            '        If IdSistema = "SA" Then
            '            MsgBox("Se va a imprimir una Factura Fiscal. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
            '            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '            MsgBox("Se va a imprimir la copia de la Factura Fiscal anterior. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
            '            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        Else
            '            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        End If
            '    End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        'Comentado 18/01/2018
    End Sub

    Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long, ByVal oMsj As String)
        customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        Dim cnn As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("ReportesFacturas", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        Dim reportPath As String = Nothing

        reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"


        'customersByCityReport.Load(reportPath)
        ''If IdSistema <> "TO" Then
        ''    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ''End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, GloClv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = GloClv_Factura
        cmd.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Clv_Factura_Ini", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = 0
        cmd.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Clv_Factura_Fin", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = 0
        cmd.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Fecha_Ini", SqlDbType.DateTime)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = "01/01/1900"
        cmd.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Fecha_Fin", SqlDbType.DateTime)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = "01/01/1900"
        cmd.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@op", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = 0
        cmd.Parameters.Add(parametro5)

        Dim da As New SqlDataAdapter(cmd)

        Dim ds As New DataSet()


        da.Fill(ds)
        ds.Tables(0).TableName = "ReportesFacturas"
        ds.Tables(1).TableName = "CALLES"
        ds.Tables(2).TableName = "CatalogoCajas"
        ds.Tables(3).TableName = "CIUDADES"
        ds.Tables(4).TableName = "CLIENTES"
        ds.Tables(5).TableName = "COLONIAS"
        ds.Tables(6).TableName = "DatosFiscales"
        ds.Tables(7).TableName = "DetFacturas"
        ds.Tables(8).TableName = "DetFacturasImpuestos"
        ds.Tables(9).TableName = "Facturas"
        ds.Tables(10).TableName = "GeneralDesconexion"
        ds.Tables(11).TableName = "SUCURSALES"
        ds.Tables(12).TableName = "Usuarios"
        ds.Tables(13).TableName = "General"

        'DamePerido(GloContrato)
        'DamePeridoTicket(Clv_Factura)
        'If GloFechaPeridoPagado = "Periodo : Periodo 5" Then
        '    GloFechaPeridoPagado = "5"
        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 10" Then
        '    GloFechaPeridoPagado = "10"
        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 15" Then
        '    GloFechaPeridoPagado = "15"
        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 20" Then
        '    GloFechaPeridoPagado = "20"
        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 25" Then
        '    GloFechaPeridoPagado = "25"
        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 30" Then
        '    GloFechaPeridoPagado = "1"
        'ElseIf GloFechaPeridoPagado = "Periodo : " Then
        '    GloFechaPeridoPagado = " "
        'End If

        consultaDatosGeneralesSucursal(0, GloClv_Factura)

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(ds)

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"

        customersByCityReport.DataDefinition.FormulaFields("DireccionSucursal").Text = "'" & RCalleSucur & " - #" & RNumSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpSucursal").Text = "'" & RColSucur & ", C.P." & RCPSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadSucursal").Text = "'" & RCiudadSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoSucursal").Text = "'" & RTelSucur & "'"

        'customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'" & oMsj & "'"
        customersByCityReport.DataDefinition.FormulaFields("Periodo").Text = "'" & GloFechaPeridoPagado & "'"
        customersByCityReport.DataDefinition.FormulaFields("PeriodoMes").Text = "'" & GloFechaPeriodoPagadoMes & "'"
        customersByCityReport.DataDefinition.FormulaFields("PeriodoFin").Text = "'" & GloFechaPeriodoFinal & "'"
        customersByCityReport.DataDefinition.FormulaFields("PagoProximo").Text = "'" & GloFechaProximoPago & "'"

        customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets

        'Dim oPrintLayout As New CrystalDecisions.Shared.PrintLayoutSettings
        'Dim oPrinterSettings As New System.Drawing.Printing.PrinterSettings
        'Dim oPageSettings As New System.Drawing.Printing.PageSettings
        'oPrinterSettings.PrinterName = LocImpresoraTickets
        'oPageSettings.PaperSize = New System.Drawing.Printing.PaperSize("Carta", 850, 1400)
        'oPrintLayout.Scaling = PrintLayoutSettings.PrintScaling.DoNotScale
        'oPrintLayout.Centered = True
        'customersByCityReport.PrintOptions.PrinterDuplex = PrinterDuplex.Simplex
        ''customersByCityReport.PrintOptions.DissociatePageSizeAndPrinterPaperSize = True
        'customersByCityReport.PrintToPrinter(oPrinterSettings, oPageSettings, False, oPrintLayout)
        customersByCityReport.PrintToPrinter(1, True, 1, 1)

        customersByCityReport.Dispose()

        'CrystalReportViewer1.ReportSource = customersByCityReport

        'If GloOpFacturas = 3 Then
        '    CrystalReportViewer1.ShowExportButton = False
        '    CrystalReportViewer1.ShowPrintButton = False
        '    CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub


    'Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long, ByVal oMsj As String)
    '    'Comentado version anterior----------------
    '    'Try


    '    '    Dim ba As Boolean = False
    '    '    Select Case IdSistema
    '    '        Case "VA"
    '    '            customersByCityReport = New ReporteCajasTickets_2VA
    '    '        Case "LO"
    '    '            customersByCityReport = New ReporteCajasTickets_2Log
    '    '        Case "AG"
    '    '            customersByCityReport = New ReporteCajasTickets_2AG
    '    '        Case "SA"
    '    '            customersByCityReport = New ReporteCajasTickets_2SA
    '    '        Case "TO"
    '    '            customersByCityReport = New ReporteCajasTickets_2TOM
    '    '        Case Else
    '    '            customersByCityReport = New ReporteCajasTickets_2OLD
    '    '    End Select


    '    '    Dim connectionInfo As New ConnectionInfo
    '    '    '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
    '    '    '    "=True;User ID=DeSistema;Password=1975huli")
    '    '    connectionInfo.ServerName = GloServerName
    '    '    connectionInfo.DatabaseName = GloDatabaseName
    '    '    connectionInfo.UserID = GloUserID
    '    '    connectionInfo.Password = GloPassword

    '    '    Dim reportPath As String = Nothing

    '    '    '        If GloImprimeTickets = False Then
    '    '    'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
    '    '    'Else
    '    '    Dim CON As New SqlConnection(MiConexion)
    '    '    CON.Open()
    '    '    Me.BusFacFiscalTableAdapter.Connection = CON
    '    '    Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
    '    '    CON.Close()
    '    '    CON.Dispose()

    '    '    eActTickets = False
    '    '    Dim CON2 As New SqlConnection(MiConexion)
    '    '    CON2.Open()
    '    '    Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
    '    '    Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
    '    '    CON2.Close()
    '    '    CON2.Dispose()

    '    '    'If IdSistema = "VA" Then
    '    '    '    'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
    '    '    '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
    '    '    'Else
    '    '    '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
    '    '    'End If

    '    '    'End If

    '    '    'customersByCityReport.Load(reportPath)
    '    '    'If GloImprimeTickets = False Then
    '    '    '    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
    '    '    'End If
    '    '    SetDBLogonForReport(connectionInfo, customersByCityReport)


    '    '    '@Clv_Factura 
    '    '    customersByCityReport.SetParameterValue(0, Clv_Factura)
    '    '    '@Clv_Factura_Ini
    '    '    customersByCityReport.SetParameterValue(1, "0")
    '    '    '@Clv_Factura_Fin
    '    '    customersByCityReport.SetParameterValue(2, "0")
    '    '    '@Fecha_Ini
    '    '    customersByCityReport.SetParameterValue(3, "01/01/1900")
    '    '    '@Fecha_Fin
    '    '    customersByCityReport.SetParameterValue(4, "01/01/1900")
    '    '    '@op
    '    '    customersByCityReport.SetParameterValue(5, "0")

    '    '    If ba = False Then
    '    '        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
    '    '        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
    '    '        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
    '    '        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
    '    '        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
    '    '        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
    '    '        Dim emsj As String = Nothing
    '    '        emsj = upsmanamensajeticketSuspendidos(Clv_Factura)
    '    '        If eActTickets = True Or emsj.Length > 0 Then
    '    '            eMsjTickets = emsj
    '    '            customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
    '    '        End If
    '    '    End If

    '    '    'If facticket = 1 Then
    '    '    '    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"

    '    '    customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets


    '    '    customersByCityReport.PrintToPrinter(1, True, 1, 1)





    '    '    'CrystalReportViewer1.ReportSource = customersByCityReport

    '    '    'If GloOpFacturas = 3 Then
    '    '    'CrystalReportViewer1.ShowExportButton = False
    '    '    'CrystalReportViewer1.ShowPrintButton = False
    '    '    'CrystalReportViewer1.ShowRefreshButton = False
    '    '    'End If
    '    '    'SetDBLogonForReport2(connectionInfo)
    '    '    customersByCityReport.Dispose()



    '    '    'Dim ba As Boolean = False
    '    '    ''Select Case IdSistema
    '    '    ''    Case "LO"
    '    '    'If Ticket_Fac_Normal = False And Ticket_Fac_EstadoCuenta = True Then
    '    '    '    customersByCityReport = New ReporteCajasTickets_2Log
    '    '    'End If

    '    '    ''End Select

    '    '    'Dim connectionInfo As New ConnectionInfo
    '    '    ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
    '    '    ''    "=True;User ID=DeSistema;Password=1975huli")
    '    '    'connectionInfo.ServerName = GloServerName
    '    '    'connectionInfo.DatabaseName = GloDatabaseName
    '    '    'connectionInfo.UserID = GloUserID
    '    '    'connectionInfo.Password = GloPassword

    '    '    'Dim reportPath As String = Nothing

    '    '    'eActTickets = False
    '    '    'Dim CON2 As New SqlConnection(MiConexion)
    '    '    'CON2.Open()
    '    '    'Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
    '    '    'Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
    '    '    'CON2.Close()
    '    '    'CON2.Dispose()

    '    '    'SetDBLogonForReport(connectionInfo, customersByCityReport)
    '    '    'customersByCityReport.SetParameterValue(0, Clv_Factura)
    '    '    'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
    '    '    'customersByCityReport.PrintToPrinter(1, True, 1, 1)
    '    '    'customersByCityReport.Dispose()

    '    'Catch ex As Exception
    '    '    System.Windows.Forms.MessageBox.Show(ex.Message)
    '    'End Try
    '    'Fin version anterior---------------

    '    Try
    '        'customersByCityReport.Close()
    '        'customersByCityReport.Dispose()
    '        customersByCityReport = New ReportDocument
    '        'Dim connectionInfo As New ConnectionInfo
    '        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
    '        ''    "=True;User ID=DeSistema;Password=1975huli")
    '        'connectionInfo.ServerName = GloServerName
    '        'connectionInfo.DatabaseName = GloDatabaseName
    '        'connectionInfo.UserID = GloUserID
    '        'connectionInfo.Password = GloPassword

    '        Dim cnn As New SqlConnection(MiConexion)
    '        Dim cmd As New SqlCommand("ReportesFacturas", cnn)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.CommandTimeout = 0

    '        Dim reportPath As String = Nothing

    '        reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"


    '        'customersByCityReport.Load(reportPath)
    '        ''If IdSistema <> "TO" Then
    '        ''    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
    '        ''End If
    '        'SetDBLogonForReport(connectionInfo, customersByCityReport)

    '        ''@Clv_Factura 
    '        'customersByCityReport.SetParameterValue(0, GloClv_Factura)
    '        ''@Clv_Factura_Ini
    '        'customersByCityReport.SetParameterValue(1, "0")
    '        ''@Clv_Factura_Fin
    '        'customersByCityReport.SetParameterValue(2, "0")
    '        ''@Fecha_Ini
    '        'customersByCityReport.SetParameterValue(3, "01/01/1900")
    '        ''@Fecha_Fin
    '        'customersByCityReport.SetParameterValue(4, "01/01/1900")
    '        ''@op
    '        'customersByCityReport.SetParameterValue(5, "0")

    '        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
    '        parametro.Direction = ParameterDirection.Input
    '        parametro.Value = GloClv_Factura
    '        cmd.Parameters.Add(parametro)

    '        Dim parametro1 As New SqlParameter("@Clv_Factura_Ini", SqlDbType.BigInt)
    '        parametro1.Direction = ParameterDirection.Input
    '        parametro1.Value = 0
    '        cmd.Parameters.Add(parametro1)

    '        Dim parametro2 As New SqlParameter("@Clv_Factura_Fin", SqlDbType.BigInt)
    '        parametro2.Direction = ParameterDirection.Input
    '        parametro2.Value = 0
    '        cmd.Parameters.Add(parametro2)

    '        Dim parametro3 As New SqlParameter("@Fecha_Ini", SqlDbType.DateTime)
    '        parametro3.Direction = ParameterDirection.Input
    '        parametro3.Value = "01/01/1900"
    '        cmd.Parameters.Add(parametro3)

    '        Dim parametro4 As New SqlParameter("@Fecha_Fin", SqlDbType.DateTime)
    '        parametro4.Direction = ParameterDirection.Input
    '        parametro4.Value = "01/01/1900"
    '        cmd.Parameters.Add(parametro4)

    '        Dim parametro5 As New SqlParameter("@op", SqlDbType.Int)
    '        parametro5.Direction = ParameterDirection.Input
    '        parametro5.Value = 0
    '        cmd.Parameters.Add(parametro5)

    '        Dim da As New SqlDataAdapter(cmd)

    '        Dim ds As New DataSet()


    '        da.Fill(ds)
    '        ds.Tables(0).TableName = "ReportesFacturas;1"
    '        ds.Tables(1).TableName = "CALLES"
    '        ds.Tables(2).TableName = "CatalogoCajas"
    '        ds.Tables(3).TableName = "CIUDADES"
    '        ds.Tables(4).TableName = "CLIENTES"
    '        ds.Tables(5).TableName = "COLONIAS"
    '        ds.Tables(6).TableName = "DatosFiscales"
    '        ds.Tables(7).TableName = "DetFacturas"
    '        ds.Tables(8).TableName = "DetFacturasImpuestos"
    '        ds.Tables(9).TableName = "Facturas"
    '        ds.Tables(10).TableName = "GeneralDesconexion"
    '        ds.Tables(11).TableName = "SUCURSALES"
    '        ds.Tables(12).TableName = "Usuarios"
    '        ds.Tables(13).TableName = "General"

    '        'DamePerido(GloContrato)
    '        'DamePeridoTicket(Clv_Factura)
    '        'If GloFechaPeridoPagado = "Periodo : Periodo 5" Then
    '        '    GloFechaPeridoPagado = "5"
    '        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 10" Then
    '        '    GloFechaPeridoPagado = "10"
    '        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 15" Then
    '        '    GloFechaPeridoPagado = "15"
    '        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 20" Then
    '        '    GloFechaPeridoPagado = "20"
    '        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 25" Then
    '        '    GloFechaPeridoPagado = "25"
    '        'ElseIf GloFechaPeridoPagado = "Periodo : Periodo 30" Then
    '        '    GloFechaPeridoPagado = "1"
    '        'ElseIf GloFechaPeridoPagado = "Periodo : " Then
    '        '    GloFechaPeridoPagado = " "
    '        'End If

    '        consultaDatosGeneralesSucursal(0, GloClv_Factura)

    '        customersByCityReport.Load(reportPath)
    '        customersByCityReport.SetDataSource(ds)

    '        'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
    '        'customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
    '        'customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
    '        'customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
    '        'customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
    '        'customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"

    '        'customersByCityReport.DataDefinition.FormulaFields("DireccionSucursal").Text = "'" & RCalleSucur & " - #" & RNumSucur & "'"
    '        'customersByCityReport.DataDefinition.FormulaFields("Colonia_CpSucursal").Text = "'" & RColSucur & ", C.P." & RCPSucur & "'"
    '        'customersByCityReport.DataDefinition.FormulaFields("CiudadSucursal").Text = "'" & RCiudadSucur & "'"
    '        'customersByCityReport.DataDefinition.FormulaFields("TelefonoSucursal").Text = "'" & RTelSucur & "'"

    '        'GloFechaPeridoPagado = "Periodo : 1"
    '        'GloFechaPeriodoPagadoMes = "Ene 2018"
    '        'GloFechaPeriodoFinal = "Feb 2018"
    '        'GloFechaProximoPago = "Proximo pago antes del : 11/02/2018"

    '        ''customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
    '        'customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'" & oMsj & "'"
    '        'customersByCityReport.DataDefinition.FormulaFields("Periodo").Text = "'" & GloFechaPeridoPagado & "'"
    '        'customersByCityReport.DataDefinition.FormulaFields("PeriodoMes").Text = "'" & GloFechaPeriodoPagadoMes & "'"
    '        'customersByCityReport.DataDefinition.FormulaFields("PeriodoFin").Text = "'" & GloFechaPeriodoFinal & "'"
    '        'customersByCityReport.DataDefinition.FormulaFields("PagoProximo").Text = "'" & GloFechaProximoPago & "'"

    '        customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets

    '        'Dim oPrintLayout As New CrystalDecisions.Shared.PrintLayoutSettings
    '        'Dim oPrinterSettings As New System.Drawing.Printing.PrinterSettings
    '        'Dim oPageSettings As New System.Drawing.Printing.PageSettings
    '        'oPrinterSettings.PrinterName = LocImpresoraTickets
    '        'oPageSettings.PaperSize = New System.Drawing.Printing.PaperSize("Carta", 850, 1400)
    '        'oPrintLayout.Scaling = PrintLayoutSettings.PrintScaling.DoNotScale
    '        'oPrintLayout.Centered = True
    '        'customersByCityReport.PrintOptions.PrinterDuplex = PrinterDuplex.Simplex
    '        ''customersByCityReport.PrintOptions.DissociatePageSizeAndPrinterPaperSize = True
    '        'customersByCityReport.PrintToPrinter(oPrinterSettings, oPageSettings, False, oPrintLayout)
    '        customersByCityReport.PrintToPrinter(1, True, 1, 2)

    '        'CrystalReportViewer1.ReportSource = customersByCityReport

    '        'If GloOpFacturas = 3 Then
    '        '    CrystalReportViewer1.ShowExportButton = False
    '        '    CrystalReportViewer1.ShowPrintButton = False
    '        '    CrystalReportViewer1.ShowRefreshButton = False
    '        'End If
    '        'SetDBLogonForReport2(connectionInfo)

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    Finally
    '        customersByCityReport.Close()
    '        customersByCityReport.Dispose()
    '        customersByCityReport = Nothing
    '    End Try

    'End Sub

    Private Sub DamePeridoTicket(ByVal contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("SP_InformacionTicket", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = contrato
        comando.Parameters.Add(par1)
        Dim par2 As New SqlParameter("@clv_factura", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = GloClv_Factura
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                GloFechaPeridoPagado = reader(0).ToString()
                Label26.Text = reader(1).ToString()
                GloFechaPeriodoFinal = reader(2).ToString()
                GloFechaPeriodoPagadoMes = Label26.Text
                GloFechaProximoPago = reader(3).ToString()
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub consultaDatosGeneralesSucursal(ByVal prmClvSucursal As Integer, ByVal prmClvFactura As Integer)

        Dim dtDatosGenerales As New DataTable

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvSucursal", SqlDbType.Int, prmClvSucursal)
        BaseII.CreateMyParameter("@clvFactura", SqlDbType.Int, prmClvFactura)
        dtDatosGenerales = BaseII.ConsultaDT("uspConsultaTblRelSucursalDatosGenerales")


        If dtDatosGenerales.Rows.Count > 0 Then
            Me.RCalleSucur = dtDatosGenerales.Rows(0)("calle").ToString
            Me.RNumSucur = dtDatosGenerales.Rows(0)("numero").ToString
            Me.RColSucur = dtDatosGenerales.Rows(0)("colonia").ToString
            Me.RCPSucur = CInt(dtDatosGenerales.Rows(0)("cp").ToString)
            Me.RMuniSucur = dtDatosGenerales.Rows(0)("municipio").ToString
            Me.RCiudadSucur = dtDatosGenerales.Rows(0)("ciudad").ToString
            Me.RTelSucur = dtDatosGenerales.Rows(0)("telefono").ToString
        End If
    End Sub

    Private Sub borracambiosdeposito(ByVal clv_session As Long, ByVal contrato As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand

        Try

            If locbndborracabiosdep = True Then
                locbndborracabiosdep = False
                cmd = New SqlClient.SqlCommand
                con.Open()
                With cmd
                    .Connection = con
                    .CommandText = "Deshacerdepositocancela"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    '@clv_session bigint,@contrato bigint
                    Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)

                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Input

                    prm.Value = CLng(clv_session)
                    prm1.Value = CLng(contrato)

                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    Dim i As Integer = cmd.ExecuteNonQuery()
                End With
                con.Close()
                con.Dispose()
            End If

        Catch ex As Exception
            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Checa_si_es_pago_cargo_recurrente"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)
        End Try

       
    End Sub
    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Dim clv_Session1 As Long = 0
        Dim parcial As Integer
        Dim Liperiodo As String

        Try

            SiPagos = 0
            Bnd = 0
            CuantasTv = 0

            'Limpiamos los totales de cargos
            Me.LabelSubTotal.Text = 0
            Me.LabelIva.Text = 0
            Me.LabelIEPS.Text = 0
            Me.LabelTotal.Text = 0
            Me.LblCredito_Apagar.Text = 0
            Me.LblImporte_Total.Text = 0
            Me.LabelGRan_Total.Text = 0
            Me.LabelSaldoAnterior.Text = 0
            'Limpiamos Puntos Aplicados (Facturaci�n Normal)
            Me.ImportePuntosAplicados.Text = 0
            Me.ImporteTotalBonificado.Text = 0
            '
            Me.TextImporte_Adic.Text = 0
            'Mostramos inicialmente el Panel de Telefon�a
            Me.PanelTel.Visible = True
            Me.PanelNrm.Visible = False
            Me.Panel5.Visible = False
            Me.CMBPanel6.Visible = False
            BndError = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            'Verificamos que la clave de sesi�n sea num�rica
            'y limpiamos las sesiones
            If IsNumeric(Me.Clv_Session.Text) = True Then
                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)))
            End If
            If IsNumeric(GloTelClv_Session) = True Then
                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)))
            End If
            If IsNumeric(gloClv_Session) = True Then
                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(gloClv_Session, Long)))
            End If
            If IsNumeric(clv_Session1) = True Then
                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(clv_Session1, Long)))
            End If

            gloClv_Session = 0
            clv_Session1 = 0

            'Sies un contrato v�lido entramos
            If IsNumeric(GloContrato) = True And GloContrato > 0 Then

                ''Checamos si es que el Cliente se facturar� por Estado de Cuenta o por Facturaci�n Normal
                'If Dime_si_cobro_EdoCta(GloContrato) And Me.PanelNrm.Visible = False And FacturacionNormal_EstadoDeCuentaNoGeneradoAun = False Then

                '    'Cobro por Estado de Cuenta
                '    PanelTel.Visible = True
                '    PanelNrm.Visible = False
                '    ButtonPagoAbono.Visible = True
                '    Button6.Visible = False
                '    Me.lblEsEdoCta.Visible = True
                '    Me.btnVerEdoCta.Visible = True
                '    Me.btnVerEdoCta.Enabled = True
                '    Me.lbl_MsgAyudaDobleClic.Visible = False

                '    'Mostramos los Saldos de Estado de Cuenta
                '    Me.Label29.Visible = True
                '    LabelSaldoAnterior.Visible = True
                '    CMBLabel29.Visible = True
                '    LblCredito_Apagar.Visible = True

                '    Label31.Visible = False
                '    ImportePuntosAplicados.Visible = False
                '    Label32.Visible = False
                '    ImporteTotalBonificado.Visible = False

                '    Me.MuestraDetalleCargos_EdoCta()

                'Else

                '    'De lo contrario es Facturaci�n Normal
                '    PanelTel.Visible = False
                '    PanelNrm.Visible = True
                '    EsEdoCta = False
                '    ButtonPagoAbono.Visible = False
                '    Button6.Visible = True
                '    Me.lblEsEdoCta.Visible = False
                '    Me.btnVerEdoCta.Visible = False
                '    Me.btnVerEdoCta.Enabled = False
                '    Me.lbl_MsgAyudaDobleClic.Visible = True

                '    'Ocultamos los Saldos de Estado de Cuenta
                '    Me.Label29.Visible = False
                '    LabelSaldoAnterior.Visible = False
                '    CMBLabel29.Visible = False
                '    LblCredito_Apagar.Visible = False

                '    Label31.Visible = True
                '    ImportePuntosAplicados.Visible = True
                '    Label32.Visible = True
                '    ImporteTotalBonificado.Visible = True
                '    Me.btnVerDetalle.Visible = True

                'End If

                DAmeClv_Session()

                'Ocultamos los detalle de servicios
                Me.panelBasico.Visible = True
                Dim comando As SqlClient.SqlCommand

                Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, GloContrato, 0)
                Me.BuscaBloqueadoTableAdapter.Connection = CON
                Me.BuscaBloqueadoTableAdapter.Fill(Me.NewsoftvDataSet2.BuscaBloqueado, GloContrato, num, num2)
                'Me.Dime_Si_DatosFiscalesTableAdapter.Connection = CON
                'Me.Dime_Si_DatosFiscalesTableAdapter.Fill(Me.NewsoftvDataSet2.Dime_Si_DatosFiscales, GloContrato, parcial)
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "Dime_Si_DatosFiscales "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@cont", SqlDbType.Int)
                    Dim prm2 As New SqlParameter("@Periodo", SqlDbType.VarChar, 50)
                    Dim prm3 As New SqlParameter("@al_corriente", SqlDbType.VarChar, 100)
                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Output
                    prm2.Direction = ParameterDirection.Output
                    prm3.Direction = ParameterDirection.Output
                    prm.Value = GloContrato
                    prm1.Value = 0
                    prm2.Value = 0
                    prm3.Value = ""
                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    'If IdSistema = "VA" Then
                    .Parameters.Add(prm3)

                    Dim i As Integer = comando.ExecuteNonQuery()
                    parcial = prm1.Value
                    Liperiodo = prm2.Value

                    Me.REDLabel26.Text = prm3.Value

                End With

                If parcial > 0 Then
                    Me.REDLabel25.Visible = True
                    Me.REDLabel25.Text = " EL CLIENTE CUENTA CON DATOS FISCALES "
                Else
                    Me.REDLabel25.Visible = False
                End If
                If Liperiodo <> "" Then
                    Me.Label25.Text = Liperiodo
                Else
                    Me.Label25.Text = ""
                End If
                If IdSistema = "VA" And Me.REDLabel26.Text.Trim <> "SN" Then
                    Me.REDLabel26.Visible = True
                ElseIf IdSistema <> "VA" Then
                    Me.REDLabel26.Visible = False
                End If
                'CON2.Close()
                If num = 0 Or num2 = 0 Then
                    CREAARBOL()
                Else
                    bloqueado = 1
                    clibloqueado()
                    MsgBox("El Cliente " + Me.ContratoTextBox.Text + " Ha Sido Bloqueado por lo que no se Podr� Llevar a cabo la Queja ", MsgBoxStyle.Exclamation)
                    Me.ContratoTextBox.Text = 0
                    GloContrato = 0
                    Glocontratosel = 0
                    Me.Clv_Session.Text = 0
                    BUSCACLIENTES(0)
                    Bloque(False)
                    'Glocontratosel = 0
                End If

                'SE COMENT� EL 17 DE ENERO 2009
                If IdSistema = "SA" Then
                    If GloTipo = "V" Then
                        If IsDate(Fecha_Venta.Text) = True Then
                            Me.Cobra_VentasTableAdapter.Connection = CON
                            Me.Cobra_VentasTableAdapter.Fill(Me.DataSetEdgar.Cobra_Ventas, New System.Nullable(Of Long)(CType(GloContrato, Long)), IdSistema, 0, clv_Session1, BndError, Msg, Fecha_Venta.Value, "V")
                        End If
                    Else
                        Me.CobraTableAdapter.Connection = CON
                        Me.CobraTableAdapter.Fill(Me.NewsoftvDataSet.Cobra, New System.Nullable(Of Long)(CType(GloContrato, Long)), IdSistema, 0, clv_Session1, BndError, Msg)
                    End If
                Else
                    Me.CobraTableAdapter.Connection = CON
                    Me.CobraTableAdapter.Fill(Me.NewsoftvDataSet.Cobra, New System.Nullable(Of Long)(CType(GloContrato, Long)), IdSistema, 0, clv_Session1, BndError, Msg)
                End If

                'Me.Clv_Session.Text = clv_Session1
                gloClv_Session = clv_Session1

                If IsNumeric(BndError) = False Then BndError = 1
                If BndError = 1 Then
                    Me.LABEL19.Text = Msg
                    Me.Panel5.Visible = True
                    Me.Bloque(False)
                    Me.Button2.Enabled = False
                    'Si existe alg�n error bloqueamos todo
                    Exit Sub
                ElseIf bloqueado <> 1 Then
                    Me.Bloque(True)
                    bloqueado = 0
                End If

              

                'Dim CON6 As New SqlConnection(MiConexion)
                'CON6.Open()
                Me.Dime_Si_ProcedePagoParcialTableAdapter.Connection = CON
                Me.Dime_Si_ProcedePagoParcialTableAdapter.Fill(Me.DataSetEdgar.Dime_Si_ProcedePagoParcial, New System.Nullable(Of Long)(CType(Clv_Session.Text, Long)), New System.Nullable(Of Long)(CType(GloContrato, Long)), Bnd, CuantasTv)
                'CON6.Close()
                If Bnd = 1 And CuantasTv > 0 Then
                    CMBPanel6.Visible = True
                End If

                If Loccontratopardep <> CLng(GloContrato) Then
                    Dim cmdarn As New SqlClient.SqlCommand
                    cmdarn = New SqlCommand
                    With cmdarn
                        .Connection = CON
                        .CommandText = "Dime_si_procedepagoparcialdeposito"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        '@clv_session bigint,@contrato bigint,@bnd1 int output,@bnd2 int output
                        Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@bnd1", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@bnd2", SqlDbType.Int)
                        prm.Direction = ParameterDirection.Input
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Output
                        prm3.Direction = ParameterDirection.Output
                        prm.Value = CLng(Clv_Session.Text)
                        prm1.Value = CLng(GloContrato)
                        prm2.Value = 0
                        prm3.Value = 0
                        .Parameters.Add(prm)
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        Dim i As Integer = cmdarn.ExecuteNonQuery()
                        bnd1pardep1 = prm2.Value
                        bnd1pardep2 = prm3.Value
                    End With
                    locclvsessionpardep = CLng(Clv_Session.Text)
                    Loccontratopardep = CLng(GloContrato)
                    If bnd1pardep1 = 1 Then
                        bnd2pardep = True
                        locbndborracabiosdep = True
                        FrmPagosNet.Show()
                    End If
                    If bnd1pardep2 = 1 And bnd2pardep = False Then
                        bnd2pardep = True
                        locbndborracabiosdep = True
                        FrmPagosNet.Show()
                    End If
                End If
            Else
                GloContrato = 0
                'Dim CON7 As New SqlConnection(MiConexion)
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, 0, 0)
                Me.CobraTableAdapter.Connection = CON
                Me.CobraTableAdapter.Fill(Me.NewsoftvDataSet.Cobra, New System.Nullable(Of Long)(CType(0, Long)), "", 0, Me.Clv_Session.Text, BndError, Msg)
                'CON7.Close()
                Me.Clv_Session.Text = 0
                Me.TreeView1.Nodes.Clear()
                Me.Bloque(False)
            End If
            CON.Dispose()
            CON.Close()
            DAMETIPOSCLIENTEDAME()
            bloqueado = 0


            ''Cobrar por Facturaci�n Normal mientras se genera el Primer Estado de Cuenta
            'If FacturacionNormal_EstadoDeCuentaNoGeneradoAun = True Then

            '    'De lo contrario es Facturaci�n Normal
            '    PanelTel.Visible = False
            '    PanelNrm.Visible = True
            '    EsEdoCta = False
            '    ButtonPagoAbono.Visible = False
            '    Button6.Visible = True
            '    Me.lblEsEdoCta.Visible = False
            '    Me.btnVerEdoCta.Visible = False
            '    Me.btnVerEdoCta.Enabled = False
            '    Me.lbl_MsgAyudaDobleClic.Visible = True

            '    'Ocultamos los Saldos de Estado de Cuenta
            '    Me.Label29.Visible = False
            '    LabelSaldoAnterior.Visible = False
            '    CMBLabel29.Visible = False
            '    LblCredito_Apagar.Visible = False

            '    Label31.Visible = True
            '    ImportePuntosAplicados.Visible = True
            '    Label32.Visible = True
            '    ImporteTotalBonificado.Visible = True
            '    Me.btnVerDetalle.Visible = True

            'Else


            '    'Aqu� hay que checar si el cobro va por Estado de Cuenta o por Pago por Facturaci�n Normal.
            '    If EsEdoCta = True And Me.PanelNrm.Visible = False Then

            '        PanelTel.Visible = True
            '        PanelNrm.Visible = False
            '        Me.TelMuestraDetalleCargos(GloContrato)
            '        ButtonPagoAbono.Visible = True
            '        Button6.Visible = False

            '        Me.lblEsEdoCta.Visible = True
            '        Me.btnVerEdoCta.Visible = True
            '        Me.btnVerEdoCta.Enabled = True
            '        Me.lbl_MsgAyudaDobleClic.Visible = False

            '        'Como el Cobro se va a realizar por Estado de Cuenta, la sesi�n a tomar ser� la de Sesi�n-Tel
            '        gloClv_Session = GloTelClv_Session
            '        Me.btnAdelantarPagos.Enabled = False

            '        Me.MuestraDetalleCargos_EdoCta()

            '    Else

            '        PanelTel.Visible = False
            '        PanelNrm.Visible = True
            '        EsEdoCta = False
            '        ButtonPagoAbono.Visible = False
            '        Button6.Visible = True

            '        Me.lblEsEdoCta.Visible = False
            '        Me.btnVerEdoCta.Visible = False
            '        Me.btnVerEdoCta.Enabled = False
            '        Me.lbl_MsgAyudaDobleClic.Visible = True

            '        'Como el Cobro se va a realizar por facturaci�n Normal la sesi�n a tomar es la regresada por el cobra
            '        gloClv_Session = clv_Session1 'Me.Clv_Session.Text
            '        'gloClv_Session = Me.Clv_Session.Text
            '        Me.Clv_Session.Text = gloClv_Session
            '        GloTelClv_Session = gloClv_Session


            '        'Mostramos la Leyenda de la Promoci�n en que aplica el pago (si es que aplica promoci�n) para Sahuayo (Mayo 2010)
            '        lblLeyendaPromocion.Text = MuestraPromocionAplica()
            '        If lblLeyendaPromocion.Text <> "" Then
            '            lblLeyendaPromocion.Visible = True
            '        Else
            '            lblLeyendaPromocion.Visible = False
            '        End If

            '    End If


            'End If

            '----------------------------------------------------------------------------------------------------------------------------------------------------------------------

            'Verificamos el Tipo de Interfaz a mostrar
            DimeSiCobroPorEstadoDeCuenta()
            Me.lblEsEdoCta.Visible = False

            'Aplicamos la Interfaz requerida para el Cliente
            If GloBndEdoCta = True Then
                If GloYaFueGenerado = True Then

                    'MsgBox("El Cliente Cuenta con Estado de Cuenta y ya fu� Generado, se debe de cobrar por Fac 2")

                    PanelTel.Visible = True
                    PanelNrm.Visible = False
                    'Me.TelMuestraDetalleCargos(GloContrato)
                    ButtonPagoAbono.Visible = True
                    Button6.Visible = False

                    Me.lblEsEdoCta.Visible = True
                    Me.btnVerEdoCta.Visible = True
                    Me.btnVerEdoCta.Enabled = True
                    Me.lbl_MsgAyudaDobleClic.Visible = False

                    'Como el Cobro se va a realizar por Estado de Cuenta, la sesi�n a tomar ser� la de Sesi�n-Tel
                    gloClv_Session = GloTelClv_Session
                    Me.btnAdelantarPagos.Enabled = False

                    Me.MuestraDetalleCargos_EdoCta()


                Else
                    'MsgBox("El Cliente se cobra por Fac 1 por que no ha sido generado su Estado de Cuenta.")


                    PanelTel.Visible = False
                    PanelNrm.Visible = True
                    EsEdoCta = False
                    ButtonPagoAbono.Visible = False
                    Button6.Visible = True

                    Me.lblEsEdoCta.Visible = False
                    Me.btnVerEdoCta.Visible = False
                    Me.btnVerEdoCta.Enabled = False
                    Me.lbl_MsgAyudaDobleClic.Visible = True

                    'Como el Cobro se va a realizar por facturaci�n Normal la sesi�n a tomar es la regresada por el cobra
                    gloClv_Session = clv_Session1 'Me.Clv_Session.Text
                    'gloClv_Session = Me.Clv_Session.Text
                    Me.Clv_Session.Text = gloClv_Session
                    GloTelClv_Session = gloClv_Session

                    '-----------------------------------
                    GloTelClv_Session = gloClv_Session
                    Dim CON20 As New SqlConnection(MiConexion)
                    CON20.Open()

                    Me.DameDetalleTableAdapter.Connection = CON20
                    Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)

                    Me.SumaDetalleTableAdapter.Connection = CON20
                    Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
                    '
                    RecalculaTotales_FacturacionNormal()
                    CON20.Close()
                    CON20.Dispose()
                    '-----------------------------------

                    Me.lblEsEdoCta.Text = "Estado de Cuenta a�n no se ha generado."
                    Me.lblEsEdoCta.Visible = True

                End If
            Else
                'MsgBox("El Cliente NO  tiene estado de Cuenta.")

                PanelTel.Visible = False
                PanelNrm.Visible = True
                EsEdoCta = False
                ButtonPagoAbono.Visible = False
                Button6.Visible = True

                Me.lblEsEdoCta.Visible = False
                Me.btnVerEdoCta.Visible = False
                Me.btnVerEdoCta.Enabled = False
                Me.lbl_MsgAyudaDobleClic.Visible = True

                'Como el Cobro se va a realizar por facturaci�n Normal la sesi�n a tomar es la regresada por el cobra
                gloClv_Session = clv_Session1 'Me.Clv_Session.Text
                Me.Clv_Session.Text = gloClv_Session
                GloTelClv_Session = gloClv_Session


                'Mostramos la Leyenda de la Promoci�n en que aplica el pago (si es que aplica promoci�n) para Sahuayo (Mayo 2010)
                lblLeyendaPromocion.Text = MuestraPromocionAplica()
                If lblLeyendaPromocion.Text <> "" Then
                    lblLeyendaPromocion.Visible = True
                Else
                    lblLeyendaPromocion.Visible = False
                End If


            End If
            locPregunta = ""
            locPregunta = UspHaz_Pregunta(GloContrato, 0)
            If locPregunta.ToString.Length > 0 Then
                Dim selpregunta As New FrmPregunta()
                selpregunta.TextBox1.Text = locPregunta
                selpregunta.ShowDialog()
            End If

            '----------------------------------------------------------------------------------------------------------------------------------------------------------------------


        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "BUSCACLIENTES()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try


    End Sub

    Private Sub CREAARBOL()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next
            Dim CON15 As New SqlConnection(MiConexion)
            CON15.Open()

            If IsNumeric(GloContrato) = True Then
                Me.DameSerDELCliFACTableAdapter.Connection = CON15
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(GloContrato, Long)))
            Else
                Me.DameSerDELCliFACTableAdapter.Connection = CON15
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            'CON15.Dispose()
            'CON15.Close()
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewsoftvDataSet.DameSerDELCliFAC.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)


                    pasa = True

                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Tel�fonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    'Nostramos los servicios del Cliente de Telefon�a

                    pasa = True

                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            CON15.Dispose()
            CON15.Close()

            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "CREARARBOL()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)


        End Try

    End Sub

    Private Sub ContratoTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ContratoTextBox.KeyDown

        '-----------------------------------------------------------------------

        'LIMPIANDO LA GUI

        Me.FacturacionNormal_EstadoDeCuentaNoGeneradoAun = False
        Me.btnHabilitaFacNormal.Visible = False

        Me.btnVerDetalle.Visible = False
        Me.btnAdelantarPagos.Enabled = False

        '---

        PanelTel.Visible = False
        PanelNrm.Visible = True
        EsEdoCta = False
        ButtonPagoAbono.Visible = False
        Button6.Visible = True
        Me.lblEsEdoCta.Visible = False
        Me.btnVerEdoCta.Visible = False
        Me.btnVerEdoCta.Enabled = False
        Me.lbl_MsgAyudaDobleClic.Visible = True

        'Ocultamos los Saldos de Estado de Cuenta
        Me.Label29.Visible = False
        LabelSaldoAnterior.Visible = False
        CMBLabel29.Visible = False
        LblCredito_Apagar.Visible = False

        Label31.Visible = True
        ImportePuntosAplicados.Visible = True
        Label32.Visible = True
        ImporteTotalBonificado.Visible = True
        Me.btnVerDetalle.Visible = True

        '------------------------------------------------------------------------

        'Limpiamos los datos y GUI antes de mostrar los cargos del nuevo Contrato
        Me.Clv_Session.Text = 0
        GloContrato = 0
        BUSCACLIENTES(0)
        Me.Bloque(False)
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If

        If Me.DataGridView1.Rows.Count > 0 Then
            limpiaGridFacturaNormal()
            If Me.DataGridView2.Rows.Count > 0 Then
                Me.DataGridView2.Rows.Clear()
            End If
        End If

        If e.KeyValue = Keys.Enter Then
            If bloqueado <> 1 Then
                If (Me.ContratoTextBox.Text) = "" Then
                    LiContrato = 0
                    Me.Label25.Text = ""
                    Me.REDLabel26.Visible = False
                ElseIf IsNumeric(Me.ContratoTextBox.Text) = True Then
                    LiContrato = Me.ContratoTextBox.Text
                    GloContrato = CLng(Me.ContratoTextBox.Text)
                    es_recurrente = False
                    BndSupervisorFac = False
                    BUSCACLIENTES(0)
                    Me.Bloque(True)
                    'Checa_es_recurrente()
                End If
                'BUSCACLIENTES(0)
                Me.Bloque(True)
            End If

        End If


        'Fijo hasta que se libere el primer Periodo de Cobro
        'Me.btnVerEdoCta.Enabled = False

        'SAUL ROBO DE SE�AL---------------------------------------------------------------------------
        If Me.ContratoTextBox.TextLength > 0 Then
            eGloContrato = Me.ContratoTextBox.Text
            eRespuesta = 0
            eRespuesta = RoboSe�al.ChecaRoboDeSe�al(eGloContrato)
            If eRespuesta = 1 Then
                btnRoboSe�al.BackColor = Color.Red
                btnRoboSe�al.Enabled = True
            Else
                btnRoboSe�al.BackColor = Button5.BackColor
                btnRoboSe�al.Enabled = False
            End If
        End If
        'SAUL ROBO DE SE�AL-FIN--------------------------------------------------------------------------

    End Sub

    Private Sub DAmeClv_Session()


        Try

            Dim CON2 As New SqlConnection(MiConexion)
            If IsNumeric(GloTelClv_Session) = True Then
                If GloTelClv_Session > 0 Then
                    CON2.Open()
                    Me.BorraClv_SessionTableAdapter.Connection = CON2
                    Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)))
                    CON2.Dispose()
                    CON2.Close()
                End If
            End If

            GloTelClv_Session = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim comando As SqlClient.SqlCommand
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CON
                .CommandText = "DameClv_Session "
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Output
                prm.Value = 0
                .Parameters.Add(prm)
                Dim i As Integer = comando.ExecuteNonQuery()
                GloTelClv_Session = prm.Value
                Me.Clv_SessionTel.Text = GloTelClv_Session
            End With
            CON.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, ":: Adevertencia :: ")

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "DameClv_Session"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try


    End Sub
    Private Sub FrmFacturaci�n_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.btnHabilitaFacNormal.Enabled = True


        Dim valida As Integer = 0
        'Dim MSG As String
        Dim bndtodos As Integer = 0
        Dim coneLidia As New SqlClient.SqlConnection(MiConexion)
        Dim Cmd As New SqlClient.SqlCommand
        Dim BndAdelantoPagos As Boolean
        BndAdelantoPagos = False


        Try
            'Limpiamos Mensaje de Error
            LABEL19.BackColor = Me.BackColor
            Me.LABEL19.Text = ""
            Me.Panel5.Visible = False

            'Verificamos si es que el cobro va a ser un Cobro Normal o un Cobro por Estado a Cuenta
            'If Me.PanelTel.Visible = False Then
            '    HabilitaBotonesFacNormal(True)
            'Else
            '    HabilitaBotonesFacNormal(False)
            'End If

            If bnd2pardep1 = True Then
                bnd2pardep1 = False
                Dim CON20 As New SqlConnection(MiConexion)
                CON20.Open()
                Me.DameDetalleTableAdapter.Connection = CON20
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)
                '  , Loccontratopardep
                'GUARDE 
                Me.SumaDetalleTableAdapter.Connection = CON20
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
                '
                RecalculaTotales_FacturacionNormal()
                '
                CON20.Dispose()
                CON20.Close()
            End If
            If Glocontratosel > 0 Then
                Me.ContratoTextBox.Text = 0
                Me.ContratoTextBox.Text = Glocontratosel
                GloContrato = Glocontratosel
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                If locband_pant <> 8 Then
                    Me.BUSCACLIENTES(0)
                End If
            End If
            If GloBnd = True Then


              


                BndAdelantoPagos = True
                GloBnd = False
                If Glo_Apli_Pnt_Ade = True Then
                    bndtodos = 1
                    Glo_Apli_Pnt_Ade = False
                End If
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.PagosAdelantadosTableAdapter.Connection = CON
                Me.PagosAdelantadosTableAdapter.Fill(Me.NewsoftvDataSet.PagosAdelantados, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), New System.Nullable(Of Long)(CType(gloClv_Servicio, Long)), New System.Nullable(Of Long)(CType(gloClv_llave, Long)), New System.Nullable(Of Long)(CType(gloClv_UnicaNet, Long)), New System.Nullable(Of Long)(CType(gloClave, Long)), IdSistema, New System.Nullable(Of Integer)(CType(GloAdelantados, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, bndtodos, Me.ContratoTextBox.Text, BndError, Msg)
                CON.Dispose()
                CON.Close()
                Me.Clv_Session.Text = 0
                Me.Clv_Session.Text = gloClv_Session
                If IsNumeric(BndError) = False Then BndError = 1
                If BndError = 1 Then
                    Me.LABEL19.Text = Msg
                    Me.Panel5.Visible = True
                    Me.Bloque(False)
                ElseIf BndError = 2 Then
                    MsgBox(Msg)
                Else
                    Me.Bloque(True)
                End If
            End If

            If GloBndExt = True Then
                GloBndExt = False

                If GloClv_Txt = "CEXTV" Then

                    gloClv_Session = Me.Clv_Session.Text

                    Dim Error_1 As Long = 0
                    Dim CON8 As New SqlConnection(MiConexion)
                    CON8.Open()

                    Me.DimesiahiConexTableAdapter.Connection = CON8
                    Me.DimesiahiConexTableAdapter.Fill(Me.DataSetEdgar.DimesiahiConex, New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)), Error_1)

                    If Error_1 = 0 Then
                        Me.AgregarServicioAdicionalesTableAdapter.Connection = CON8
                        Me.AgregarServicioAdicionalesTableAdapter.Fill(Me.NewsoftvDataSet.AgregarServicioAdicionales, New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)), GloClv_Txt, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), New System.Nullable(Of Long)(CType(1, Long)), IdSistema, New System.Nullable(Of Integer)(CType(0, Integer)), New System.Nullable(Of Integer)(CType(GloExt, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)
                        bitsist(GloUsuario, LiContrato, GloSistema, Me.Name, "Agregar Servicios", "Se Agrego un Servicio Adicional", "Servicio Agregado: " + CStr(GloClv_Txt) + " Tv Adicionales: " + CStr(GloExt), LocClv_Ciudad)
                    Else
                        MsgBox("Primero cobre la contrataci�n tvs. adicional que ya esta en la lista")
                    End If

                    CON8.Dispose()
                    CON8.Close()

                    Me.Clv_Session.Text = 0
                    Me.Clv_Session.Text = GloTelClv_Session

                Else

                    'Borramos los Totales de los Estados de Cuenta
                    'Me.LabelSubTotal.Text = 0
                    'Me.LabelIva.Text = 0
                    'Me.LabelIEPS.Text = 0
                    'Me.LabelTotal.Text = 0
                    'Me.LblCredito_Apagar.Text = 0
                    'Me.LblImporte_Total.Text = 0
                    'Me.LabelGRan_Total.Text = 0
                    'Me.LabelSaldoAnterior.Text = 0
                    'Me.TextImporte_Adic.Text = 0

                    gloClv_Session = GloTelClv_Session

                    Dim CON9 As New SqlConnection(MiConexion)
                    CON9.Open()
                    If eBndPPE = True Then
                        eBndPPE = False
                        Me.AgregarServicioAdicionales_PPETableAdapter1.Connection = CON9
                        Me.AgregarServicioAdicionales_PPETableAdapter1.Fill(Me.DataSetEdgar.AgregarServicioAdicionales_PPE, GloTelClv_Session, eClv_Progra, eClv_Txt)
                    Else
                        Me.AgregarServicioAdicionalesTableAdapter.Connection = CON9
                        Me.AgregarServicioAdicionalesTableAdapter.Fill(Me.NewsoftvDataSet.AgregarServicioAdicionales, New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)), GloClv_Txt, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), New System.Nullable(Of Long)(CType(0, Long)), IdSistema, New System.Nullable(Of Integer)(CType(1, Integer)), New System.Nullable(Of Integer)(CType(0, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)
                    End If
                    CON9.Dispose()
                    CON9.Close()

                    Me.Clv_Session.Text = 0
                    Me.Clv_Session.Text = GloTelClv_Session
                End If
                If IsNumeric(BndError) = False Then BndError = 1
                If BndError = 1 Then
                    Me.LABEL19.Text = Msg
                    Me.Panel5.Visible = True
                    Me.Bloque(False)
                ElseIf BndError = 2 Then
                    MsgBox(Msg)
                Else
                    Me.Bloque(True)
                End If
            End If

            'Agragamos la Revista a los conceptos 
            If Revista = True Then
                Revista = False
                Dim CON9 As New SqlConnection(MiConexion)
                CON9.Open()

                Me.AgregarServicioAdicionalesTableAdapter.Connection = CON9
                Me.AgregarServicioAdicionalesTableAdapter.Fill(Me.NewsoftvDataSet.AgregarServicioAdicionales, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)), GloClv_Txt, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), New System.Nullable(Of Long)(CType(0, Long)), IdSistema, New System.Nullable(Of Integer)(CType(NoRevistas, Integer)), New System.Nullable(Of Integer)(CType(0, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)

                Me.DameDetalleTableAdapter.Connection = CON9
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, CType(Me.Clv_Session.Text, Long), 0)

                Me.SumaDetalleTableAdapter.Connection = CON9
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, CType(Me.Clv_Session.Text, Long), False, 0)

                '
                RecalculaTotales_FacturacionNormal()
                '

                CON9.Dispose()
                CON9.Close()

                If (MsgBox("�Desea Continuar Con El Pago?", MsgBoxStyle.YesNo)) = 6 Then
                    BotonGrabar()
                Else
                    Exit Sub
                End If
            End If


            'Verificar si se esta facturando por Conceptos Normales o por Estado de Cuenta - Ya esta agregada la bandera de Revista de Juanjo
            If GloServiciosCobroAdeudo = True Or BndAdelantoPagos = True Or GloBonif = 1 Or Revista = True Or GloBndExt = True Or GloBnd = True Or GloAbonoACuenta = True Or GloAbonoACuenta = True Or GloAdelantarCargos = True Or GloAgregar_a_lista = True Then

                'Cualquiera de estas opciones modifica el pago por Estado de Cuenta
                'Mostramos los cargos de Facturaci�n Normal
                'Verificamos si es que el cobro va a ser un Cobro Normal o un Cobro por Estado a Cuenta
                Me.PanelTel.Visible = False
                HabilitaBotonesFacNormal(True)
                'Me.PanelTel.Visible = False
                Me.PanelNrm.Visible = True


                If BndAdelantoPagos = True Then

                    'Como desde el m�dulo de adelantar pagos se pudo haber dado una mensualidad con el servicio como contratado tambien debemos actualizar la leyenda
                    'Mostramos la Leyenda de la Promoci�n en que aplica el pago (si es que aplica promoci�n) para Sahuayo (Mayo 2010)
                    lblLeyendaPromocion.Text = MuestraPromocionAplica()
                    If lblLeyendaPromocion.Text <> "" Then
                        lblLeyendaPromocion.Visible = True
                    Else
                        lblLeyendaPromocion.Visible = False
                    End If
                    'Fin de Actualizar la Leyenda de la Promoci�n
                    '----------------------------------------------------

                End If


                BndAdelantoPagos = False
                GloBonif = 0
                Revista = False
                GloBndExt = False
                GloBnd = False
                GloAbonoACuenta = False
                GloAbonoACuenta = False
                GloAdelantarCargos = False
                GloAgregar_a_lista = False

                GloServiciosCobroAdeudo = False

                Dim CON5000 As New SqlConnection(MiConexion)
                CON5000.Open()

                Me.DameDetalleTableAdapter.Connection = CON5000
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)

                Me.SumaDetalleTableAdapter.Connection = CON5000
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
                '
                RecalculaTotales_FacturacionNormal()
                '
                CON5000.Dispose()
                CON5000.Close()

                'Como Agregamos a la lista estamos cobrando por Facturaci�n NORMAL y se habilita esa opci�n
                Me.Label29.Visible = True
                Me.CMBLabel29.Visible = True
                Me.LabelSaldoAnterior.Visible = True
                Me.LblCredito_Apagar.Visible = True

                Me.Label31.Visible = False
                Me.Label32.Visible = False
                Me.ImportePuntosAplicados.Visible = False
                Me.ImporteTotalBonificado.Visible = False

                GloYaFueGenerado = False

            End If

            Dim mensaje As String = ""
            Dim bndmensaje As Integer = 0

            If GLOSIPAGO = 1 Then 'SI YA PAGO PASA Y SI NO AL CHORIZO

                GLOSIPAGO = 0
                If IsNumeric(GLOEFECTIVO) = False Then GLOEFECTIVO = 0
                If IsNumeric(GLOCHEQUE) = False Then GLOCHEQUE = 0
                If IsNumeric(GLOCLV_BANCOCHEQUE) = False Then GLOCLV_BANCOCHEQUE = 0
                If Len(NUMEROCHEQUE) = 0 Then NUMEROCHEQUE = ""
                If IsNumeric(GLOTARJETA) = False Then GLOTARJETA = 0
                If IsNumeric(GLOCLV_BANCOTARJETA) = False Then GLOCLV_BANCOTARJETA = 0
                If Len(NUMEROTARJETA) = 0 Then NUMEROTARJETA = ""
                If Len(TARJETAAUTORIZACION) = 0 Then TARJETAAUTORIZACION = ""
                GloOpCobro = 0
                If PanelNrm.Visible = True Then
                    GloOpCobro = 1
                End If

                Dim CON As New SqlConnection(MiConexion)

                'Aqu� hay que checar si el cobro va por Estado de Cuenta o por Pago por Facturaci�n Normal.
                'Si quieren realizar pagos adelantados a un cliente con Estados de Cuenta se facturar� utilizando Facturaci�n Normal 

                If Dime_si_cobro_EdoCta(GloContrato) = False Or Me.PanelNrm.Visible = True Then 'Or Me.PanelTel.Visible = False Then

                    '-----------------------------------------------------------------------------------------------------------------
                    'Grabamos los Clientes de Facturaci�n Normal

                    CON.Open()
                    Dim comando As SqlClient.SqlCommand
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "GrabaFacturas "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = Me.ContratoTextBox.Text
                        .Parameters.Add(prm)

                        Dim prm1 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                        prm1.Direction = ParameterDirection.Input
                        prm1.Value = GloTelClv_Session 'Me.Clv_Session.Text
                        .Parameters.Add(prm1)

                        Dim prm2 As New SqlParameter("@Cajera", SqlDbType.VarChar, 11)
                        prm2.Direction = ParameterDirection.Input
                        prm2.Value = GloUsuario
                        .Parameters.Add(prm2)

                        Dim prm3 As New SqlParameter("@Sucursal", SqlDbType.Int)
                        prm3.Direction = ParameterDirection.Input
                        prm3.Value = GloSucursal
                        .Parameters.Add(prm3)

                        Dim prm4 As New SqlParameter("@Caja", SqlDbType.Int)
                        prm4.Direction = ParameterDirection.Input
                        prm4.Value = GloCaja
                        .Parameters.Add(prm4)

                        Dim prm5 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
                        prm5.Direction = ParameterDirection.Input
                        If Me.SplitContainer1.Panel1Collapsed = False Then
                            GloTipo = "V"
                            prm5.Value = "V"
                        Else
                            GloTipo = "C"
                            prm5.Value = "C"
                        End If

                        .Parameters.Add(prm5)

                        Dim prm6 As New SqlParameter("@Serie_V", SqlDbType.VarChar, 5)
                        prm6.Direction = ParameterDirection.Input
                        If Len(Loc_Serie) = 0 Then
                            Loc_Serie = ""
                        End If
                        prm6.Value = Loc_Serie
                        .Parameters.Add(prm6)


                        Dim prm7 As New SqlParameter("@Folio_V", SqlDbType.BigInt)
                        prm7.Direction = ParameterDirection.Input
                        If IsNumeric(Loc_Folio) = False Then
                            Loc_Folio = 0
                        End If
                        prm7.Value = Loc_Folio
                        .Parameters.Add(prm7)

                        Dim prm8 As New SqlParameter("@Clv_Vendedor", SqlDbType.Int)
                        prm8.Direction = ParameterDirection.Input
                        prm8.Value = Loc_Clv_Vendedor
                        .Parameters.Add(prm8)

                        Dim prm9 As New SqlParameter("@BndError", SqlDbType.Int)
                        prm9.Direction = ParameterDirection.Input
                        prm9.Value = BndError
                        .Parameters.Add(prm9)

                        Dim prm10 As New SqlParameter("@Msg", SqlDbType.VarChar, 250)
                        prm10.Direction = ParameterDirection.Input
                        prm10.Value = ""
                        .Parameters.Add(prm10)

                        Dim prm17 As New SqlParameter("@Clv_FacturaSalida", SqlDbType.BigInt)
                        prm17.Direction = ParameterDirection.Output
                        prm17.Value = 0
                        .Parameters.Add(prm17)


                        Dim i As Integer = comando.ExecuteNonQuery()
                        GloClv_Factura = prm17.Value

                        mensaje = prm10.Value

                        Ticket_Fac_Normal = True
                        Ticket_Fac_EstadoCuenta = False

                    End With
                    CON.Close()

                    Guarda_Tipo_Tarjeta(GloClv_Factura, GloTipoTarjeta, GLOTARJETA)

                    '-----------------------------------------------------------------------------------------------------------------

                Else

                    '-----------------------------------------------------------------------------------------------------------------

                    'Grabamos Factura por Estado de Cuenta o por Pago Abono a Cuenta

                    CON.Open()
                    Dim comando As SqlClient.SqlCommand
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "Tel_GrabaFacturas "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = Me.ContratoTextBox.Text
                        .Parameters.Add(prm)

                        Dim prm1 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                        prm1.Direction = ParameterDirection.Input
                        prm1.Value = GloTelClv_Session 'Me.Clv_Session.Text
                        .Parameters.Add(prm1)

                        Dim prm2 As New SqlParameter("@Cajera", SqlDbType.VarChar, 11)
                        prm2.Direction = ParameterDirection.Input
                        prm2.Value = GloUsuario
                        .Parameters.Add(prm2)

                        Dim prm3 As New SqlParameter("@Sucursal", SqlDbType.Int)
                        prm3.Direction = ParameterDirection.Input
                        prm3.Value = GloSucursal
                        .Parameters.Add(prm3)

                        Dim prm4 As New SqlParameter("@Caja", SqlDbType.Int)
                        prm4.Direction = ParameterDirection.Input
                        prm4.Value = GloCaja
                        .Parameters.Add(prm4)

                        Dim prm5 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
                        prm5.Direction = ParameterDirection.Input
                        If Me.SplitContainer1.Panel1Collapsed = False Then
                            GloTipo = "V"
                            prm5.Value = "V"
                        Else
                            GloTipo = "C"
                            prm5.Value = "C"
                        End If

                        .Parameters.Add(prm5)

                        Dim prm6 As New SqlParameter("@Serie_V", SqlDbType.VarChar, 5)
                        prm6.Direction = ParameterDirection.Input
                        If Len(Loc_Serie) = 0 Then
                            Loc_Serie = ""
                        End If
                        prm6.Value = Loc_Serie
                        .Parameters.Add(prm6)


                        Dim prm7 As New SqlParameter("@Folio_V", SqlDbType.BigInt)
                        prm7.Direction = ParameterDirection.Input
                        If IsNumeric(Loc_Folio) = False Then
                            Loc_Folio = 0
                        End If
                        prm7.Value = Loc_Folio
                        .Parameters.Add(prm7)

                        Dim prm8 As New SqlParameter("@Clv_Vendedor", SqlDbType.Int)
                        prm8.Direction = ParameterDirection.Input
                        prm8.Value = Loc_Clv_Vendedor
                        .Parameters.Add(prm8)

                        Dim prm9 As New SqlParameter("@BndError", SqlDbType.Int)
                        prm9.Direction = ParameterDirection.Input
                        prm9.Value = BndError
                        .Parameters.Add(prm9)

                        Dim prm10 As New SqlParameter("@Msg", SqlDbType.VarChar, 250)
                        prm10.Direction = ParameterDirection.Input
                        prm10.Value = ""
                        .Parameters.Add(prm10)

                        Dim prm17 As New SqlParameter("@Clv_FacturaSalida", SqlDbType.BigInt)
                        prm17.Direction = ParameterDirection.Output
                        prm17.Value = 0
                        .Parameters.Add(prm17)

                        Dim prmEsEstadiDeCuenta As New SqlParameter("@EsEstadoDeCuenta", SqlDbType.Bit)
                        prmEsEstadiDeCuenta.Value = True
                        .Parameters.Add(prmEsEstadiDeCuenta)

                        Dim prmXML As New SqlParameter("@XML", SqlDbType.Xml)
                        prmXML.Value = XML_DetCargos
                        .Parameters.Add(prmXML)

                        Dim prmIDEdoCta As New SqlParameter("@ID_EDOCTA", SqlDbType.BigInt)
                        prmIDEdoCta.Value = IDEdoCta
                        .Parameters.Add(prmIDEdoCta)

                        Dim prmGranTotal As New SqlParameter("@GRANTOTAL", SqlDbType.Money)
                        prmGranTotal.Value = CType(LabelGRan_Total.Text, Double)
                        .Parameters.Add(prmGranTotal)



                        Dim i As Integer = comando.ExecuteNonQuery()
                        GloClv_Factura = prm17.Value

                        mensaje = prm10.Value

                        Ticket_Fac_Normal = False
                        Ticket_Fac_EstadoCuenta = True


                    End With
                    CON.Close()


                    '-----------------------------------------------------------------------------------------------------------------
                End If

                If bndmensaje = 1 Then

                    'Limpiamos los conceptos a facturar
                    limpiaGridFacturaNormal()
                    limpiaGridFacturaEstadoCta()

                    MsgBox("Ocurri� un Error: " & mensaje.ToString, MsgBoxStyle.Exclamation, ":: Error al Completar la Factura ::")
                    Exit Sub

                End If

                'Aqui va arnold
                If bndGraboClienteRecurrente = True Then
                    bndGraboClienteRecurrente = False
                    If GloClvSupAuto <> "" And GloClvSupAuto <> Nothing Then
                        Guarda_Cobro_Rel_Supervisor_cobrosRec(GloClv_Factura, GloClvSupAuto, CLng(Me.ContratoTextBox.Text))
                    Else
                        Guarda_Cobro_Rel_Supervisor_cobrosRec(GloClv_Factura, GloUsuario, CLng(Me.ContratoTextBox.Text))
                    End If
                    GloClvSupAuto = ""
                End If

                GloPagorecibido = 0

                'Dim CON2 As New SqlConnection(MiConexion)
                If bndmensaje = 0 Then

                    Try

                        CON.Open()
                        Dim comando2 As SqlClient.SqlCommand
                        comando2 = New SqlClient.SqlCommand
                        With comando2
                            .Connection = CON
                            .CommandText = "GUARDATIPOPAGO "
                            .CommandType = CommandType.StoredProcedure
                            .CommandTimeout = 0

                            Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                            prm.Direction = ParameterDirection.Input
                            prm.Value = GloClv_Factura
                            .Parameters.Add(prm)

                            Dim prm2 As New SqlParameter("@GLOEFECTIVO", SqlDbType.Money)
                            prm2.Direction = ParameterDirection.Input
                            prm2.Value = GLOEFECTIVO
                            .Parameters.Add(prm2)

                            Dim prm3 As New SqlParameter("@GLOCHEQUE", SqlDbType.Money)
                            prm3.Direction = ParameterDirection.Input
                            prm3.Value = GLOCHEQUE
                            .Parameters.Add(prm3)

                            Dim prm4 As New SqlParameter("@GLOCLV_BANCOCHEQUE", SqlDbType.Int)
                            prm4.Direction = ParameterDirection.Input
                            prm4.Value = GLOCLV_BANCOCHEQUE
                            .Parameters.Add(prm4)

                            Dim prm5 As New SqlParameter("@NUMEROCHEQUE", SqlDbType.VarChar)
                            prm5.Direction = ParameterDirection.Input
                            prm5.Value = NUMEROCHEQUE
                            .Parameters.Add(prm5)

                            Dim prm6 As New SqlParameter("@GLOTARJETA", SqlDbType.Money)
                            prm6.Direction = ParameterDirection.Input
                            prm6.Value = GLOTARJETA
                            .Parameters.Add(prm6)

                            Dim prm7 As New SqlParameter("@GLOCLV_BANCOTARJETA", SqlDbType.Int)
                            prm7.Direction = ParameterDirection.Input
                            prm7.Value = GLOCLV_BANCOTARJETA
                            .Parameters.Add(prm7)

                            Dim prm8 As New SqlParameter("@NUMEROTARJETA", SqlDbType.VarChar)
                            prm8.Direction = ParameterDirection.Input
                            prm8.Value = NUMEROTARJETA
                            .Parameters.Add(prm8)

                            Dim prm9 As New SqlParameter("@TARJETAAUTORIZACION", SqlDbType.VarChar)
                            prm9.Direction = ParameterDirection.Input
                            prm9.Value = TARJETAAUTORIZACION
                            .Parameters.Add(prm9)

                            Dim j As Integer = comando2.ExecuteNonQuery()

                        End With
                        CON.Close()

                    Catch ex As Exception

                        MsgBox(ex.Message, MsgBoxStyle.Critical, ":: Advertencia ::")

                        Log_Descripcion = ex.Message.ToString
                        Log_Formulario = Me.Name.ToString
                        Log_ProcedimientoAlmacenado = "GUARDATIPOPAGO"
                        GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

                    End Try


                    ''Inica Guarda Si el Tipo de Pago fue con una Nota de Credito
                    If GLOCLV_NOTA > 0 And GloClv_Factura > 0 And GLONOTA > 0 Then
                        'Try
                        CON.Open()
                        Dim comando3 As SqlClient.SqlCommand
                        comando3 = New SqlClient.SqlCommand
                        With comando3
                            .Connection = CON
                            .CommandText = "GUARDATIPOPAGO_Nota_Credito "
                            .CommandType = CommandType.StoredProcedure
                            .CommandTimeout = 0

                            Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                            prm.Direction = ParameterDirection.Input
                            prm.Value = GloClv_Factura
                            .Parameters.Add(prm)

                            Dim prm2 As New SqlParameter("@CLV_Nota", SqlDbType.BigInt)
                            prm2.Direction = ParameterDirection.Input
                            prm2.Value = GLOCLV_NOTA
                            .Parameters.Add(prm2)

                            Dim prm3 As New SqlParameter("@GLONOTA", SqlDbType.Money)
                            prm3.Direction = ParameterDirection.Input
                            prm3.Value = GLONOTA
                            .Parameters.Add(prm3)

                            Dim j As Integer = comando3.ExecuteNonQuery()

                        End With
                        CON.Close()

                 

                    End If

                    If GLOTRANSFERENCIA > 0 Then
                        GUARDAPagoTransferencia(GloClv_Factura, GLOTRANSFERENCIA, GLOBANCO, GLONUMEROTRANSFERENCIA, GLOAUTORIZACION)
                    End If

                    GLOTRANSFERENCIA = 0
                    GLOBANCO = 0
                    GLONUMEROTRANSFERENCIA = ""
                    GLOAUTORIZACION = ""

                    If IsNumeric(GLOEFECTIVO) = False Then GLOEFECTIVO = 0
                    If IsNumeric(GLOCHEQUE) = False Then GLOCHEQUE = 0
                    If IsNumeric(GLOCLV_BANCOCHEQUE) = False Then GLOCLV_BANCOCHEQUE = 0
                    If Len(NUMEROCHEQUE) = 0 Then NUMEROCHEQUE = ""
                    If IsNumeric(GLOTARJETA) = False Then GLOTARJETA = 0
                    If IsNumeric(GLOCLV_BANCOTARJETA) = False Then GLOCLV_BANCOTARJETA = 0
                    If Len(NUMEROTARJETA) = 0 Then NUMEROTARJETA = ""
                    If Len(TARJETAAUTORIZACION) = 0 Then TARJETAAUTORIZACION = ""

                    If GLOTOTALEFECTIVO > 0 Then
                        NUEPago_En_EfectivoDet(GloClv_Factura, GLOTOTALEFECTIVO, GLOCAMBIO)
                    End If

                    '------------------------------- BONIFICACIONES

                    Dim CON11 As New SqlConnection(MiConexion)
                    CON11.Open()

                    Me.Dime_ContratacionTableAdapter.Connection = CON11
                    Me.Dime_ContratacionTableAdapter.Fill(Me.NewsoftvDataSet2.Dime_Contratacion, GloClv_Factura, res)

                    'If eMotivoBonificacion.Length > 0 Then
                    '    Me.GuardaMotivosBonificacionTableAdapter.Connection = CON11
                    '    Me.GuardaMotivosBonificacionTableAdapter.Fill(Me.DataSetEdgar.GuardaMotivosBonificacion, GloClv_Factura, eMotivoBonificacion)
                    'End If
                    'graba si hay bonificacion

                    If locBndBon1 = True Then
                        If GloClv_Factura > 0 Then
                            Me.Inserta_Bonificacion_SupervisorTableAdapter.Connection = CON11
                            Me.Inserta_Bonificacion_SupervisorTableAdapter.Fill(Me.Procedimientos_arnoldo.Inserta_Bonificacion_Supervisor, GloClv_Factura, LocSupBon)
                            bitsist(GloUsuario, Me.ContratoTextBox.Text, GloSistema, Me.Name, "Se Realizo Una Bonificaci�n", "", "Factura:" + CStr(GloClv_Factura), SubCiudad)
                        End If
                    End If

                    'CON11.Dispose()
                    CON11.Close()
                    'MsgBox(Msg)

                    '------------------------------- FIN DE BONIFICACIONES

                    'identi = 0
                    'identi = BusFacFiscalOledb(GloClv_Factura)
                    ''fin Facturacion Digital
                    'locID_Compania_Mizart = ""
                    'locID_Sucursal_Mizart = ""
                    ''Fin Guarda si el tipo de Pago es con Nota de Credito
                    ' ''FIN FACTURACION DIGITAL EDGAR 4/FEB/2012

                    ''   FIN FACTURACION DIGITAL EDGAR 4/FEB/2012

                    'If CInt(identi) > 0 Then
                    '    ''SAUL IMPRIMIR FACTURA FISCAL
                    '    ''LiTipo = 2
                    '    ''GloOpFacturas = 1
                    '    ''FrmImprimir.ShowDialog()
                    '    ''SAUL IMPRIMIR FACTURA FISCAL(FIN)

                    '    ' ''Inicio Facturacion Digital
                    '    'Locop = 0
                    '    'Dime_Aque_Compania_Facturarle(GloClv_Factura)
                    '    'Graba_Factura_Digital(GloClv_Factura, identi)
                    '    ' ''FrmImprimirFacturaDigital.Show()
                    '    ''FormPruebaDigital.Show()
                    '    'locID_Compania_Mizart = ""
                    '    'locID_Sucursal_Mizart = ""
                    '    ' ''fin Facturacion Digital
                    '    Locop = 0
                    '    Dime_Aque_Compania_Facturarle(GloClv_Factura)
                    '    Graba_Factura_Digital(GloClv_Factura, identi)
                    '    'FrmImprimirFacturaDigital.Show()
                    '    Try
                    '        FormPruebaDigital.Show()
                    '    Catch ex As Exception

                    '    End Try

                    '    locID_Compania_Mizart = ""
                    '    locID_Sucursal_Mizart = ""
                    'Else


                    'Fin Guarda si el tipo de Pago es con Nota de Credito
                    If LocImpresoraTickets = "" Then
                        MsgBox("No Se Ha Asignado Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
                        GLOSIPAGO = 0
                        Me.ContratoTextBox.Text = 0
                        GloContrato = 0
                        Glocontratosel = 0
                        Me.Clv_Session.Text = 0
                        BUSCACLIENTES(0)

                        Ticket_Fac_Normal = False
                        Ticket_Fac_EstadoCuenta = False

                        'Limpiamos los valores antes de hacer clualquier moviento
                        Me.limpiaGridFacturaEstadoCta()
                        Me.limpiaGridFacturaNormal()

                    Else
                        ':: IMPRESI�N DE TICKETS :: // Diferenciando entre Ticket Normal y Ticket por Estado de Cuenta
                        ConfigureCrystalReports(GloClv_Factura)
                        'If Ticket_Fac_Normal = False And Ticket_Fac_EstadoCuenta = True Then
                        '    ConfigureCrystalReports_tickets(GloClv_Factura)
                        'Else
                        '    ConfigureCrystalReports_tickets_Normal(GloClv_Factura)
                        'End If

                        'LiTipo = 6
                        'FrmImprimir.Show()

                        Me.limpiaGridFacturaEstadoCta()
                        Me.limpiaGridFacturaNormal()

                        'Limpiamos las banderas de Tockets
                        Ticket_Fac_Normal = False
                        Ticket_Fac_EstadoCuenta = False
                        PrimeraVezSelectedRow = True
                        btnHabilitaFacNormal.Visible = False
                        FacturacionNormal_EstadoDeCuentaNoGeneradoAun = False
                        Me.lblEsEdoCta.Visible = False

                    End If
                    'End If

                    If GloActivarCFD = 1 Then
                        Try
                            HazFacturaDigital(GloClv_Factura)
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try

                    End If
                    Try
                        If checaSiImprimeOrden() = True Then
                            Mana_ImprimirOrdenes(GloClv_Factura)
                        End If
                    Catch ex As Exception

                    End Try

                    'If GloTipo = "V" Then
                    '    Me.Dame_UltimoFolio()
                    'End If

                    'ConfigureCrystalReports_tickets(GloClv_Factura)

                    'FrmImprimir.Show()
                    If res = 1 Then
                        If LocNomImpresora_Contratos = "" Then
                            MsgBox("No se ha asignado una Impresora de Contratos a esta Sucursal", MsgBoxStyle.Information)
                            Me.ContratoTextBox.Text = 0
                            GloContrato = 0
                            Glocontratosel = 0
                            Me.Clv_Session.Text = 0
                            BUSCACLIENTES(0)
                        Else
                            If IdSistema = "TO" Then
                                '' FrmHorasInst.Show()
                                horaini = "0"
                                horafin = "0"
                                bndcontt = True
                                If bndcontt = True Then
                                    bndcontt = False
                                    ConfigureCrystalReportsContratoTomatlan("TO")
                                    valida = MsgBox("Voltee la hoja Para Continuar la Impresi�n", MsgBoxStyle.YesNo, "Pausa")
                                    If valida = 6 Then
                                        ConfigureCrystalReportsContratoTomatlan2("TO")
                                    ElseIf valida = 7 Then
                                        MsgBox("No se continuo con la Impresion", MsgBoxStyle.Information)
                                    End If
                                    Me.ContratoTextBox.Text = 0
                                    GloContrato = 0
                                    Glocontratosel = 0
                                    Me.Clv_Session.Text = 0
                                    BUSCACLIENTES(0)
                                    Ticket_Fac_Normal = False
                                    Ticket_Fac_EstadoCuenta = False
                                End If
                            End If
                        End If
                    End If
                End If

                Me.ContratoTextBox.Text = 0
                GloContrato = 0
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                BUSCACLIENTES(0)

                Ticket_Fac_Normal = False
                Ticket_Fac_EstadoCuenta = False

                'Limpiamos el GridView despues de pagar
                Me.limpiaGridFacturaEstadoCta()
                Me.limpiaGridFacturaNormal()
                PrimeraVezSelectedRow = True

                btnHabilitaFacNormal.Visible = False
                FacturacionNormal_EstadoDeCuentaNoGeneradoAun = False
                Me.lblEsEdoCta.Visible = False

                '------------------------------------------------------------------------------------------------------------------

                Me.REDLabel25.Visible = False

            ElseIf GLOSIPAGO = 2 Then 'CANCELO EL PAGO

                GLOSIPAGO = 0
                Me.ContratoTextBox.Text = 0
                GloContrato = 0
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                BUSCACLIENTES(0)

                Ticket_Fac_Normal = False
                Ticket_Fac_EstadoCuenta = False

                'Limpiamos los valores de los conceptos/cargos
                limpiaGridFacturaNormal()
                limpiaGridFacturaEstadoCta()

                btnHabilitaFacNormal.Visible = False
                FacturacionNormal_EstadoDeCuentaNoGeneradoAun = False
                Me.lblEsEdoCta.Visible = False

            End If
            If BndSupervisorFac = True Then '--->Checar si es cargo Recurrente

                BndSupervisorFac = False
                es_recurrente = False
                'bndGraboClienteRecurrente = True
                BUSCACLIENTES(0)

            End If
        Catch ex As System.Exception

            'Limpiamos los valores de los conceptos/cargos
            limpiaGridFacturaNormal()
            limpiaGridFacturaEstadoCta()

            Ticket_Fac_Normal = False
            Ticket_Fac_EstadoCuenta = False
            PrimeraVezSelectedRow = True

            btnHabilitaFacNormal.Visible = False
            FacturacionNormal_EstadoDeCuentaNoGeneradoAun = False
            Me.lblEsEdoCta.Visible = False

            MsgBox("Ocurri� un Error: " & ex.Message.ToString, MsgBoxStyle.Critical, ":: Aviso Importante :: ")
            GLOSIPAGO = 0
            Me.ContratoTextBox.Text = 0
            GloContrato = 0
            Glocontratosel = 0
            Me.Clv_Session.Text = 0
            BUSCACLIENTES(0)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = ""
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try
        If eBotonGuardar = True Then
            Bloque(False)
        End If

        'Hacesmos el Source
        Me.DataGridView3.DataSource = DataGridView1.DataSource


        'If Me.PanelNrm.Visible = True Then

        '    'Ocultamos los Saldos de Estado de Cuenta
        '    Me.Label29.Visible = False
        '    LabelSaldoAnterior.Visible = False
        '    CMBLabel29.Visible = False
        '    LblCredito_Apagar.Visible = False

        '    Label31.Visible = True
        '    ImportePuntosAplicados.Visible = True
        '    Label32.Visible = True
        '    ImporteTotalBonificado.Visible = True
        '    Me.btnVerDetalle.Visible = True

        'End If

        'Aplicamos la Interfaz requerida para el Cliente
        If GloBndEdoCta = True Then
            If GloYaFueGenerado = True Then

                Me.Label29.Visible = True
                Me.CMBLabel29.Visible = True
                Me.LabelSaldoAnterior.Visible = True
                Me.LblCredito_Apagar.Visible = True

                Me.Label31.Visible = False
                Me.Label32.Visible = False
                Me.ImportePuntosAplicados.Visible = False
                Me.ImporteTotalBonificado.Visible = False

                Me.ButtonPagoAbono.Visible = True
                Me.btnVerEdoCta.Enabled = True

            Else

                Me.Label29.Visible = False
                Me.CMBLabel29.Visible = False
                Me.LabelSaldoAnterior.Visible = False
                Me.LblCredito_Apagar.Visible = False
                Me.ButtonPagoAbono.Visible = False
                Me.btnVerEdoCta.Enabled = False

                Me.Label31.Visible = True
                Me.Label32.Visible = True
                Me.ImportePuntosAplicados.Visible = True
                Me.ImporteTotalBonificado.Visible = True



            End If
        Else

            Me.Label29.Visible = False
            Me.CMBLabel29.Visible = False
            Me.LabelSaldoAnterior.Visible = False
            Me.LblCredito_Apagar.Visible = False
            Me.ButtonPagoAbono.Visible = False
            Me.btnVerEdoCta.Enabled = False

            Me.Label31.Visible = True
            Me.Label32.Visible = True
            Me.ImportePuntosAplicados.Visible = True
            Me.ImporteTotalBonificado.Visible = True



        End If



        'SAUL ROBO DE SE�AL---------------------------------------------------------------------------
        If Me.ContratoTextBox.TextLength > 0 Then
            eGloContrato = Me.ContratoTextBox.Text
            eRespuesta = 0
            eRespuesta = RoboSe�al.ChecaRoboDeSe�al(eGloContrato)
            If eRespuesta = 1 Then
                btnRoboSe�al.BackColor = Color.Red
                btnRoboSe�al.Enabled = True
            Else
                btnRoboSe�al.Enabled = False
            End If
        End If
        'SAUL ROBO DE SE�AL-FIN--------------------------------------------------------------------------

    End Sub

    Private Sub GUARDAPagoTransferencia(ByVal Clv_Factura As Integer, ByVal Importe As Double, ByVal Banco As Integer, ByVal NumeroTransferencia As String, ByVal Autorizacion As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Factura", SqlDbType.Int, Clv_Factura)
        BaseII.CreateMyParameter("@Importe", SqlDbType.Decimal, Importe)
        BaseII.CreateMyParameter("@Banco", SqlDbType.Int, Banco)
        BaseII.CreateMyParameter("@NumeroTransferencia", SqlDbType.VarChar, NumeroTransferencia, 50)
        BaseII.CreateMyParameter("@Autorizacion", SqlDbType.VarChar, Autorizacion, 50)
        BaseII.Inserta("GUARDAPagoTransferencia")
    End Sub

    Private Sub NUEPago_En_EfectivoDet(ByVal Clv_Factura As Integer, ByVal Efectivo As Decimal, ByVal Cambio As Decimal)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Factura", SqlDbType.Int, Clv_Factura)
        BaseII.CreateMyParameter("@Efectivo", SqlDbType.Decimal, Efectivo)
        BaseII.CreateMyParameter("@Cambio", SqlDbType.Decimal, Cambio)
        BaseII.Inserta("NUEPago_En_EfectivoDet")
    End Sub

    Private Function checaSiImprimeOrden() As Boolean
        Dim imprimeQueja As New classValidaciones

        imprimeQueja.sistema = "FAC"
        imprimeQueja.ordenQueja = "ORDEN"
        checaSiImprimeOrden = imprimeQueja.uspChecaSiImprimeOrdenQueja()
    End Function

    Private Sub ConfigureCrystalReportsContratoTomatlan(ByVal clv_empresa As String)
        Try
            Dim impresora As String = Nothing
            'Dim horaini As String = nothing
            'Dim horafin As String = nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\ContratoTomatlan.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, GloContrato)
            '@clv_empresa
            customersByCityReport.SetParameterValue(1, clv_empresa)


            'horaini = InputBox("Apartir de ", "Captura Hora")
            'horafin = InputBox("Capture la hora de la Instalaci�n Final", "Captura Hora")



            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
            'If horaini = "" Then horaini = "0"
            'If horafin = "" Then horafin = "0"

            customersByCityReport.DataDefinition.FormulaFields("horaini").Text = "'" & horaini & "'"
            customersByCityReport.DataDefinition.FormulaFields("horafin").Text = "'" & horafin & "'"


            ' customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(2, True, 1, 1)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Hora_insTableAdapter.Connection = CON
            Me.Hora_insTableAdapter.Fill(Me.NewsoftvDataSet2.Hora_ins, horaini, horafin, GloContrato)
            Me.Inserta_Comentario2TableAdapter.Connection = CON
            Me.Inserta_Comentario2TableAdapter.Fill(Me.NewsoftvDataSet2.Inserta_Comentario2, GloContrato)
            CON.Dispose()
            CON.Close()
            'customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub ConfigureCrystalReportsContratoTomatlan2(ByVal clv_empresa As String)
        Try
            Dim impresora As String = Nothing
            Dim horaini As String = Nothing
            Dim horafin As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\TomatlanAtras.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, GloContrato)
            '@clv_empresa
            customersByCityReport.SetParameterValue(1, clv_empresa)

            'customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(2, True, 1, 1)
            'customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

   


    Private Sub FrmFacturaci�n_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If IsNumeric(Me.Clv_Session.Text) = True Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BorraClv_SessionTableAdapter.Connection = CON
            Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)))
            CON.Dispose()
            CON.Close()
        End If
        If IsNumeric(GloTelClv_Session) = True Then
            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()
            Me.BorraClv_SessionTableAdapter.Connection = CON3
            Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)))
            CON3.Dispose()
            CON3.Close()
        End If

        'Liberamos el Form
        Me.Dispose()

    End Sub



    Private Sub FrmFac_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        EsEdoCta = False

        'Opciones de Impresi�n de Tickets
        Ticket_Fac_Normal = False
        Ticket_Fac_EstadoCuenta = False

        colorea(Me)

        'Limpiamos Mensaje de Error
        LABEL19.BackColor = Me.BackColor
        Me.LABEL19.Text = ""
        Me.Panel5.Visible = False

        'Ocultamos estos totales para Sahuayo
        '------------------------------------------------
        Me.Label26.ForeColor = Me.BackColor
        Me.Label27.ForeColor = Me.BackColor
        Me.Label30.ForeColor = Me.BackColor
        Me.LabelIva.ForeColor = Me.BackColor
        Me.LabelIEPS.ForeColor = Me.BackColor
        Me.LabelSubTotal.ForeColor = Me.BackColor
        '------------------------------------------------

        lblSumaCargos.ForeColor = Color.Maroon
        Me.Label28.ForeColor = Color.Black
        Me.LabelTotal.ForeColor = Color.Black
        Me.LblCredito_Apagar.ForeColor = Color.Black
        Me.LblImporte_Total.ForeColor = Color.Black
        Me.Label29.ForeColor = Color.Black
        Me.LabelSaldoAnterior.ForeColor = Color.Black
        '
        Me.Label32.ForeColor = Color.Black
        Me.Label31.ForeColor = Color.Black
        Me.ImportePuntosAplicados.ForeColor = Color.Black
        Me.ImporteTotalBonificado.ForeColor = Color.Black
        Me.lblEsEdoCta.ForeColor = Color.Red
        Me.lblEsEdoCta.BackColor = Color.Yellow
        Me.lbl_MsgAyudaDobleClic.ForeColor = Color.Black
        '
        Me.lblEsEdoCta.Visible = False
        Me.btnVerDetalle.Visible = True

        If bndGraboClienteRecurrente = True Then
            bndGraboClienteRecurrente = False
        End If

        'Colocamos la GUI por default al Iniciar
        Me.btnVerEdoCta.Visible = False
        Me.btnAdelantarPagos.Enabled = False
        Me.ButtonPagoAbono.Visible = True
        Me.btnVerDetalle.Visible = True

        'Etiqueta de la Leyenda de la Promoci�n
        Me.lblLeyendaPromocion.ForeColor = Color.Red
        Me.lblLeyendaPromocion.BackColor = Color.Yellow
        Me.lblLeyendaPromocion.Text = ""

        Try

            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Me.DamedatosUsuarioTableAdapter.Connection = CON
            Me.DamedatosUsuarioTableAdapter.Fill(Me.NewsoftvDataSet.DamedatosUsuario, GloUsuario)

            Me.DAMENOMBRESUCURSALTableAdapter.Connection = CON
            Me.DAMENOMBRESUCURSALTableAdapter.Fill(Me.NewsoftvDataSet.DAMENOMBRESUCURSAL, GloSucursal)

            Me.DameDatosGeneralesTableAdapter.Connection = CON
            Me.DameDatosGeneralesTableAdapter.Fill(Me.NewsoftvDataSet.DameDatosGenerales)

            CON.Dispose()
            CON.Close()

            Me.REDLabel25.Visible = False
            Me.LblNomCaja.Text = GlonOMCaja
            Me.LblVersion.Text = My.Application.Info.Version.ToString

            Me.lblEsEdoCta.Visible = False

            If GloTipo = "V" Then

                Me.SplitContainer1.Panel1Collapsed = False
                Dim CON1 As New SqlConnection(MiConexion)
                CON1.Open()

                Me.MUESTRAVENDEDORES_2TableAdapter.Connection = CON1
                Me.MUESTRAVENDEDORES_2TableAdapter.Fill(Me.DataSetEdgar.MUESTRAVENDEDORES_2)

                CON1.Dispose()
                CON1.Close()

                Me.ComboBox1.Text = ""
                Me.ComboBox1.SelectedValue = 0

            Else

                Me.SplitContainer1.Panel1Collapsed = True
                Me.ComboBox1.TabStop = False
                Me.ComboBox2.TabStop = False

            End If

            Label22.Visible = False
            Fecha_Venta.Visible = False

            If IdSistema = "SA" Then
                Me.Button12.Visible = True
                'Me.Button9.Visible = False

                If GloTipo = "V" Then
                    Label22.Visible = True
                    Fecha_Venta.Visible = True
                End If

            End If

            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()

            Me.Selecciona_Impresora_SucursalTableAdapter.Connection = CON2
            Me.Selecciona_Impresora_SucursalTableAdapter.Fill(Me.NewsoftvDataSet2.Selecciona_Impresora_Sucursal, GloSucursal, LocNomImpresora_Tarjetas, LocNomImpresora_Contratos)

            CON2.Dispose()
            CON2.Close()

            Bloque(False)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, ":: Advertencia :: ")

            Log_Descripcion = ex.Message.ToString & " - Al cargar el Formulario"
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Private Sub FrmFac_Load()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try

        If GloTipo = "C" Then

            Me.Text = "Facturaci�n Por Cajas y Pagos de Estados de Cuenta"

        ElseIf GloTipo = "V" Then

            Me.Text = "Facturaci�n Por Ventas y Pagos de Estados de Cuenta"

        End If

        Button5.Enabled = False
        Button7.Enabled = False
        Button12.Enabled = False
        btnAdelantarPagos.Enabled = False
        btnVerDetalle.Enabled = False
        Button6.Enabled = False

        Me.REDLabel25.Visible = False

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Glocontratosel = 0
        GloContrato = 0
        eBotonGuardar = False
        'If IdSistema = "VA" Or IdSistema = "LO" Then
        '    FrmSelCliente2.Show()
        'ElseIf IdSistema <> "VA" And IdSistema <> "LO" Then
        FrmSelCliente.Show()
        'End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'If locbndborracabiosdep = True Then
        '    'locbndborracabiosdep = False
        '    borracambiosdeposito(locclvsessionpardep, Loccontratopardep)
        'End If

        Dim CON As New SqlConnection(MiConexion)
        Try

            CON.Open()


            'Verificamos que la clave de sesi�n sea num�rica
            'y limpiamos las sesiones

            If IsNumeric(Me.Clv_Session.Text) = True Then
                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)))
            End If

            If IsNumeric(GloTelClv_Session) = True Then
                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)))
            End If

            If IsNumeric(gloClv_Session) = True Then
                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(gloClv_Session, Long)))
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()

            Me.Close()
            Me.Dispose()

        End Try

       

    End Sub


    Private Sub Clv_Session_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_Session.TextChanged
        If IsNumeric(Me.Clv_Session.Text) = True And Me.Clv_Session.Text > 0 Then
            Dime_si_cobro_EdoCta(GloContrato)

            If EsEdoCta = True Then
                Exit Sub
            Else

                Me.Panel5.Visible = False
                locBndBon1 = False
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.DameDetalleTableAdapter.Connection = CON
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, CType(Me.Clv_Session.Text, Integer), 0)
                Me.SumaDetalleTableAdapter.Connection = CON
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, CType(Me.Clv_Session.Text, Integer), False, 0)
                '
                RecalculaTotales_FacturacionNormal()
                '
                CON.Dispose()
                CON.Close()

            End If
            ''CREAARBOL1()
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If e.ColumnIndex = 7 Then
            If SiPagos = 1 Then
                MsgBox("No se puede adelantar pagos con la promoci�n que se le esta aplicando")
                Exit Sub
            End If
            If DataGridView1.SelectedCells(7).Value = "Adelantar Pagos" Then
                If IsNumeric(DataGridView1.SelectedCells(0).Value) = True And IsNumeric(DataGridView1.SelectedCells(1).Value) = True And IsNumeric(DataGridView1.SelectedCells(2).Value) = True And IsNumeric(Me.ContratoTextBox.Text) = True Then
                    gloClv_Session = DataGridView1.SelectedCells(0).Value
                    gloClv_Servicio = DataGridView1.SelectedCells(1).Value
                    gloClv_llave = DataGridView1.SelectedCells(2).Value
                    gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                    gloClave = DataGridView1.SelectedCells(4).Value
                    Dim ERROR_1 As Integer = 0
                    Dim MSGERROR_1 As String = Nothing
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.Pregunta_Si_Puedo_AdelantarTableAdapter.Connection = CON
                    Me.Pregunta_Si_Puedo_AdelantarTableAdapter.Fill(Me.DataSetEdgar.Pregunta_Si_Puedo_Adelantar, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), ERROR_1, MSGERROR_1)
                    CON.Dispose()
                    CON.Close()
                    If ERROR_1 = 0 Then
                        My.Forms.FrmPagosAdelantados.Show()
                    ElseIf ERROR_1 = 2 Then
                        MsgBox(MSGERROR_1)
                    End If
                ElseIf DataGridView1.SelectedCells(7).Value = "Ext. Adicionales" Then
                    GloClv_Txt = "CEXTV"
                    My.Forms.FrmExtecionesTv.Show()
                End If
            ElseIf DataGridView1.SelectedCells(7).Value = "Ver Detalle" Then
                gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                FrmDetCobroDesc.Show()
            End If
        End If
    End Sub

    Private Sub clibloqueado()
        Me.Button7.Enabled = False
        'Me.Button8.Enabled = False
        Me.Button2.Enabled = False
        Me.Button6.Enabled = False
        Me.Button5.Enabled = False
        Me.NOMBRELabel1.Text = ""
        Me.CALLELabel1.Text = ""
        Me.NUMEROLabel1.Text = ""
        Me.COLONIALabel1.Text = ""
        Me.CIUDADLabel1.Text = ""
    End Sub

    Private Sub Bloque(ByVal bnd As Boolean)
        Me.Button5.Enabled = bnd
        If IsNumeric(Me.ContratoTextBox.Text) = True Then
            If Me.ContratoTextBox.Text > 0 Then
                Me.Button5.Enabled = True
            End If
        End If
        'Me.Button6.Enabled = bnd
        Me.Button7.Enabled = bnd
        'Me.Button8.Enabled = bnd
        Me.Button2.Enabled = bnd
        Me.Button6.Enabled = bnd

        'DATAGRIDVIEW1 ES PARA EL FAC NORMAL
        'If Me.DataGridView1.RowCount > 0 And bnd = False Then
        '    DataGridView1.DataSource = ""
        'End If

        If IsNumeric(GloContrato) = False Then GloContrato = 0
        If GloContrato = 0 Or es_recurrente = True Then

            Me.Button7.Enabled = False
            Me.Button6.Enabled = False
            Me.Button2.Enabled = False
            Me.Button5.Enabled = False
            Me.Button12.Enabled = False
            Me.ButtonVentaEq.Enabled = False
            ButtonPagoAbono.Enabled = False
            Me.btnVerDetalle.Enabled = False

            'Adelantar Pagos
            Me.btnAdelantarPagos.Enabled = False



        Else
            Me.Button7.Enabled = True
            Me.Button6.Enabled = True
            Me.Button2.Enabled = True
            Me.Button8.Enabled = True
            Me.Button5.Enabled = True
            Me.Button12.Enabled = True
            Me.ButtonVentaEq.Enabled = True
            ButtonPagoAbono.Enabled = True
            Me.btnVerDetalle.Enabled = True


            'Adelantar Pagos
            Me.btnAdelantarPagos.Enabled = True
        End If
        '--------------------------------------
        'If Me.DataGridView1.RowCount = 0 Then
        '    Me.Button7.Enabled = False
        '    Me.Button6.Enabled = False
        '    Me.Button2.Enabled = False
        '    Me.Button8.Enabled = False
        '    Me.Button5.Enabled = False
        'Else
        '    Me.Button7.Enabled = True
        '    Me.Button6.Enabled = True
        '    Me.Button2.Enabled = True
        '    Me.Button8.Enabled = True
        '    Me.Button5.Enabled = True
        'End If
        '--------------------------------------
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Me.LabelSubTotal.Text = 0
        'Me.LabelIva.Text = 0
        'Me.LabelIEPS.Text = 0
        'Me.LabelTotal.Text = 0
        'Me.LblCredito_Apagar.Text = 0
        'Me.LblImporte_Total.Text = 0
        'Me.LabelGRan_Total.Text = 0
        'Me.LabelSaldoAnterior.Text = 0
        'Me.TextImporte_Adic.Text = 0
        ''
        'Me.ImportePuntosAplicados.Text = 0
        'Me.ImporteTotalBonificado.Text = 0

        ''
        'Me.PanelTel.Visible = False
        'Me.PanelNrm.Visible = True
        'Me.DataGridView2.Visible = True
        Me.Clv_Session.Text = gloClv_Session
        gloClv_Session = gloClv_Session
        eBotonGuardar = False
        FrmServicios.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        PrimeraVezSelectedRow = True

        Me.ContratoTextBox.Text = 0
        GloContrato = 0
        Glocontratosel = 0
        Me.Clv_Session.Text = 0
        BUSCACLIENTES(0)
        Bloque(False)

        'Limpiamos Mensaje de Error
        LABEL19.BackColor = Me.BackColor
        Me.LABEL19.Text = ""
        Me.Panel5.Visible = False

        Me.limpiaGridFacturaNormal()
        Me.limpiaGridFacturaEstadoCta()

        'Limpiar GUI
        Me.lblEsEdoCta.Visible = False
        PanelTel.Visible = True
        PanelNrm.Visible = False

        ButtonPagoAbono.Visible = False
        btnVerEdoCta.Visible = False
        Button6.Visible = False

        '
        ButtonPagoAbono.Visible = False
        btnVerEdoCta.Visible = False
        Me.Label29.Visible = False
        LabelSaldoAnterior.Visible = False
        CMBLabel29.Visible = False
        LblCredito_Apagar.Visible = False

        Label31.Visible = True
        ImportePuntosAplicados.Visible = True
        Label32.Visible = True
        ImporteTotalBonificado.Visible = True
        Me.Button6.Visible = True

        Me.REDLabel25.Visible = False
        Me.lblEsEdoCta.Visible = False

        GloTelClv_Session = 0
        gloClv_Session = 0

        FacturacionNormal_EstadoDeCuentaNoGeneradoAun = False
        Me.btnHabilitaFacNormal.Visible = False

    End Sub
    Private Sub BotonGrabar()
        Try
            If Me.LblImporte_Total.Text < 0 Then
                MsgBox("El Importe tiene que ser mayor a 0", MsgBoxStyle.Information)
                Exit Sub
            End If

            If IsNumeric(Me.ContratoTextBox.Text) = True And (Me.DataGridView2.RowCount > 0 Or Me.DataGridView1.RowCount > 0) Then

                If Me.ContratoTextBox.Text > 0 And (Me.DataGridView2.RowCount > 0 Or Me.DataGridView1.RowCount > 0) Then
                    If GloTipo = "V" Then
                        If Len(Trim(Me.ComboBox2.Text)) = 0 Then
                            MsgBox("Seleccione la Serie ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.ComboBox1.SelectedValue) = False And Len(Trim(Me.ComboBox1.Text)) > 0 Then
                            MsgBox("Seleccione el Vendedor", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.FolioTextBox.Text) = False Then
                            MsgBox("Capture el Folio ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        Loc_Serie = Me.ComboBox2.Text
                        Loc_Clv_Vendedor = Me.ComboBox1.SelectedValue
                        Loc_Folio = Me.FolioTextBox.Text
                    End If


                    ' Asignamos el Importe Total a Pagar dependiendo si es que es Estado de Cuenta o Facturaci�n Normal
                    If (Me.EsEdoCta = True) Then

                        If IsNumeric(Me.LblImporte_Total.Text) = False Then Me.LblImporte_Total.Text = 0
                        GLOIMPTOTAL = Me.LblImporte_Total.Text
                        GLOSIPAGO = 0
                        eBotonGuardar = True
                        FrmPago.Show()

                    Else

                        If IsNumeric(Me.LblImporte_Total.Text) = False Then Me.LblImporte_Total.Text = 0
                        GLOIMPTOTAL = Me.LabelGRan_Total.Text ' <-- Estado de Cuenta
                        GLOSIPAGO = 0
                        eBotonGuardar = True
                        FrmPago.Show()

                    End If


                    'Me.Enabled = False
                Else
                    MsgBox("Seleccione un Cliente para Facturar ", MsgBoxStyle.Information)
                    eBotonGuardar = False
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show("No hay Servicios o Conceptos que Facturar")
            Me.ContratoTextBox.Text = 0
            GloContrato = 0
            Glocontratosel = 0
            Me.Clv_Session.Text = 0
            BUSCACLIENTES(0)
            eBotonGuardar = True

            Log_Descripcion = ex.Message.ToString & " - Al dar clic sobre el bot�n de Grabar"
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "BotonGrabar()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try
    End Sub
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BUSCACLIENTES(0)
    End Sub
    Private Sub Clv_Vendedor_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'If IsNumeric(Me.Clv_Vendedor.SelectedValue) = True And Len(Trim(Me.Clv_Vendedor.Text)) > 0 Then
        ' Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.NewsoftvDataSet.Ultimo_SERIEYFOLIO, Me.Clv_Vendedor.SelectedValue)
        ' Me.ComboBox2.Text = ""
        ' Else
        ' Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.NewsoftvDataSet.Ultimo_SERIEYFOLIO, 0)
        'End If
    End Sub
    Private Sub ComboBox2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsNumeric(Me.ComboBox2.SelectedValue) = True And Len(Trim(ComboBox2.Text)) Then
            Dame_UltimoFolio()
        Else
            Me.FolioTextBox.Text = ""
        End If
    End Sub
    Private Sub Dame_UltimoFolio()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.FolioTextBox.Text = 0
            Me.DAMEUltimo_FOLIOTableAdapter.Connection = CON
            Me.DAMEUltimo_FOLIOTableAdapter.Fill(Me.DataSetEdgar.DAMEUltimo_FOLIO, Me.ComboBox1.SelectedValue, Me.ComboBox2.Text, Me.FolioTextBox.Text)
            CON.Dispose()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Private Sub Dame_UltimoFolio()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try
    End Sub
    Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub
    Private Sub ComboBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.ComboBox1.SelectedValue) = True And Len(Trim(Me.ComboBox1.Text)) > 0 Then
            Me.Ultimo_SERIEYFOLIOTableAdapter.Connection = CON
            Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.DataSetEdgar.Ultimo_SERIEYFOLIO, Me.ComboBox1.SelectedValue)
            Me.ComboBox2.Text = ""
        Else
            Me.Ultimo_SERIEYFOLIOTableAdapter.Connection = CON
            Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.DataSetEdgar.Ultimo_SERIEYFOLIO, 0)
        End If
        CON.Dispose()
        CON.Close()
    End Sub
    Private Sub ComboBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox2.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub
    Private Sub ComboBox2_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.FolioTextBox.Text = ""
        If Len(Trim(ComboBox2.Text)) Then
            Dame_UltimoFolio()
        End If
    End Sub

    Private Sub ComboBox2_TextChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.TextChanged
        Me.FolioTextBox.Text = ""
    End Sub
    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'If IsNumeric(GloContrato) = True And GloContrato > 0 Then
        If IsNumeric(Me.ContratoTextBox.Text) = True And CInt(Me.ContratoTextBox.Text) > 0 Then
            'GloOpFacturas = 3
            'eBotonGuardar = False
            'BrwFacturas_Cancelar.Show()
            FrmSeleccionaTipo.Show()
        Else
            MsgBox("Seleccione un Cliente por favor", MsgBoxStyle.Information)
        End If
    End Sub
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click

        'Para quitar de la lista existen 2 opciones:
        'a) quitar desde PreDetFacturas cuando sea facturaci�nNormal sin Telefon�a
        'b) quitar directamente del GridView el cargo generado desde el Estado de Cuenta y volver a recalcular el total a pagar

        'Identificamos qu� tipo de Facturaci�n es:
        'If FacturacionNormal_EstadoDeCuentaNoGeneradoAun = True Or Dime_si_cobro_EdoCta(GloContrato) = False And Me.PanelNrm.Visible = True Then
        If GloYaFueGenerado = False Then
            'Estamos Cobrando Facturaci�n Normal

            Try
                If DataGridView1.RowCount > 0 Then
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Dim LOCCLAVE As Integer = 0

                    If IsNumeric(DataGridView1.SelectedCells(29).Value) = True And IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
                        If IsNumeric(DataGridView1.SelectedCells(4).Value) = False Then LOCCLAVE = 0 Else LOCCLAVE = DataGridView1.SelectedCells(4).Value

                        If (LOCCLAVE = 4) Then

                            MsgBox("No se puede quitar el BONO DE GARANT�A", MsgBoxStyle.Information)
                            Exit Sub

                        End If

                        If (LOCCLAVE = 1 Or LOCCLAVE = 3) And DataGridView1.SelectedCells(7).Value <> "Ext. Adicionales" Then

                            If LOCCLAVE = 1 Then MsgBox("No se puede quitar la Contrataci�n", MsgBoxStyle.Information)
                            If LOCCLAVE = 2 Then MsgBox("No se puede quitar la Reconexi�n", MsgBoxStyle.Information)
                            Exit Sub

                        ElseIf LOCCLAVE = 2 And PuedoQuitarDeLaLista = True Then

                            'Verificamos si es esta tratando de quitar un servicio con Promoci�n COMBO.
                            If GloClaveDelDetalle_UNO = DataGridView1.SelectedCells(29).Value Or GloClaveDelDetalle_DOS = DataGridView1.SelectedCells(29).Value Then


                                'Le preguntamos al Usuario si quiere quitar los 2 servicios ya que van de la mano por ser combo o si cancela quitarlo de la lista.
                                Dim Resp = MsgBox("El servicio que estas tratando de quitar esta ligado a una Promoci�n por COMBO. Si das clic en el bot�n de 'S�' se quitar�n los 2 servicios. �Deseas quitar el pago de la mensulidad del COMBO?", MsgBoxStyle.YesNoCancel, "�ESTAS QUITANDO UN SERVICIO COMBO!")

                                If Resp = MsgBoxResult.Yes Then

                                    'Quitamos ambos servicios que forman el COMBO por Promoci�n


                                    'QUITAMOS EL SERVICIO UNO
                                    Me.BORCAMDOCFAC_QUITATableAdapter.Connection = CON
                                    Me.BORCAMDOCFAC_QUITATableAdapter.Fill(Me.NewsoftvDataSet2.BORCAMDOCFAC_QUITA, GloTelClv_Session)
                                    Me.QUITARDELDETALLETableAdapter.Connection = CON
                                    Me.QUITARDELDETALLETableAdapter.Fill(Me.NewsoftvDataSet1.QUITARDELDETALLE, DataGridView1.SelectedCells(0).Value, GloClaveDelDetalle_UNO, IdSistema, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)

                                    bitsist(GloCajera, LiContrato, GloSistema, Me.Name, "", "Se quito del detalle", "Concepto: " + CStr(DataGridView1.SelectedCells(7).Value), LocClv_Ciudad)

                                    'QUITAMOS EL SERVICIO DOS
                                    Me.BORCAMDOCFAC_QUITATableAdapter.Connection = CON
                                    Me.BORCAMDOCFAC_QUITATableAdapter.Fill(Me.NewsoftvDataSet2.BORCAMDOCFAC_QUITA, GloTelClv_Session)
                                    Me.QUITARDELDETALLETableAdapter.Connection = CON
                                    Me.QUITARDELDETALLETableAdapter.Fill(Me.NewsoftvDataSet1.QUITARDELDETALLE, DataGridView1.SelectedCells(0).Value, GloClaveDelDetalle_DOS, IdSistema, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)

                                    bitsist(GloCajera, LiContrato, GloSistema, Me.Name, "", "Se quito del detalle", "Concepto: " + CStr(DataGridView1.SelectedCells(7).Value), LocClv_Ciudad)

                                Else
                                    Exit Sub
                                End If


                            End If


                        End If


                        'Como desde el m�dulo de adelantar pagos se pudo haber dado una mensualidad con el servicio como contratado tambien debemos actualizar la leyenda
                        'Mostramos la Leyenda de la Promoci�n en que aplica el pago (si es que aplica promoci�n) para Sahuayo (Mayo 2010)

                        lblLeyendaPromocion.Text = MuestraPromocionAplica()
                        If lblLeyendaPromocion.Text <> "" Then
                            lblLeyendaPromocion.Visible = True
                        Else
                            lblLeyendaPromocion.Visible = False
                        End If

                        'Fin de Actualizar la Leyenda de la Promoci�n
                        '----------------------------------------------------

                        Me.BORCAMDOCFAC_QUITATableAdapter.Connection = CON
                        Me.BORCAMDOCFAC_QUITATableAdapter.Fill(Me.NewsoftvDataSet2.BORCAMDOCFAC_QUITA, GloTelClv_Session)
                        Me.QUITARDELDETALLETableAdapter.Connection = CON
                        Me.QUITARDELDETALLETableAdapter.Fill(Me.NewsoftvDataSet1.QUITARDELDETALLE, DataGridView1.SelectedCells(0).Value, DataGridView1.SelectedCells(29).Value, IdSistema, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)

                        bitsist(GloCajera, LiContrato, GloSistema, Me.Name, "", "Se quito del detalle", "Concepto: " + CStr(DataGridView1.SelectedCells(7).Value), LocClv_Ciudad)

                        If BndError = 1 Then
                            Me.LABEL19.Text = Msg
                            Me.Panel5.Visible = True
                            Me.Bloque(False)

                        ElseIf BndError = 2 Then
                            MsgBox(Msg)

                        Else
                            Me.Bloque(True)
                        End If

                        Me.DameDetalleTableAdapter.Connection = CON
                        Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)

                        Me.SumaDetalleTableAdapter.Connection = CON
                        Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
                        '
                        RecalculaTotales_FacturacionNormal()

                    End If

                    CON.Dispose()
                    CON.Close()

                Else

                    MsgBox("No hay conceptos que quitar ", MsgBoxStyle.Information)

                End If

            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)

                Log_Descripcion = ex.Message.ToString & " - Al tratar de Quitar un concepto de la lista de cargos"
                Log_Formulario = Me.Name.ToString
                Log_ProcedimientoAlmacenado = "Button7_Click()"
                GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

            End Try
            eBotonGuardar = False

        Else

            MsgBox("No puedes quitar Cargos ya que son de un Estado de Cuenta.", MsgBoxStyle.Information, "NO PUEDES QUITAR DE LA LISTA")

            ''Estamos Cobrando Cargos generados desde el Estado de Cuenta:
            'If EsEdoCta = True And Me.PanelNrm.Visible = False Then

            '    If Me.DataGridView2.RowCount > 0 Then

            '        If PrimeraVezSelectedRow = True Then

            '            Me.DataGridView2.Rows(0).Selected = True
            '            Me.DataGridView2.CurrentCell = Me.DataGridView2.Rows(0).Cells(2)

            '            PrimeraVezSelectedRow = False
            '        Else

            '            QuitaCargoDeLaLista(Me.DataGridView2.CurrentRow, True)

            '        End If



            '    Else
            '        MsgBox("No hay cargos que quitar de la lista.", MsgBoxStyle.Information, "NADA QUE QUITAR.")
            '    End If

            'End If

        End If

    End Sub
    Private Sub DAMETIPOSCLIENTEDAME()
        Try
            Me.CLV_TIPOCLIENTELabel1.Text = ""
            Me.DESCRIPCIONLabel1.Text = ""
            If IsNumeric(GloContrato) = True Then
                If GloContrato > 0 Then
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.DAMETIPOSCLIENTESTableAdapter.Connection = CON
                    Me.DAMETIPOSCLIENTESTableAdapter.Fill(Me.DataSetEdgar.DAMETIPOSCLIENTES, Me.ContratoTextBox.Text)
                    CON.Dispose()
                    CON.Close()
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If BndError = 1 Then
            If Me.LABEL19.BackColor = Color.Yellow Then
                Me.LABEL19.BackColor = Me.Panel5.BackColor
            Else
                Me.LABEL19.BackColor = Color.Yellow
            End If

            Button5.Enabled = False
            Button7.Enabled = False
            Button12.Enabled = False
            btnAdelantarPagos.Enabled = False
            btnVerDetalle.Enabled = False
            Button6.Enabled = False
            'Obviamente no se pueden realizar pagos si existe alguna alerta
            Me.Button2.Enabled = False
            Me.ButtonPagoAbono.Enabled = False


        End If
    End Sub
    Private Sub Button6_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click


        Try
            If DataGridView1.RowCount > 0 Then
                If IsNumeric(DataGridView1.SelectedCells(29).Value) = True And IsNumeric(DataGridView1.SelectedCells(0).Value) Then
                    GloDes_Ser = DataGridView1.SelectedCells(6).Value
                    gloClv_Session = DataGridView1.SelectedCells(0).Value
                    gloClv_Detalle = DataGridView1.SelectedCells(29).Value
                    loctitulo = "Solo el Supervisor puede Bonificar"
                    locband_pant = 3
                    locBndBon1 = True
                    If IdSistema = "SA" And GloTipoUsuario = 1 Then
                        eAccesoAdmin = False
                    End If
                    FrmSupervisor.Show()
                Else
                    MsgBox("Seleccione el Concepto que deseas Bonificar ", MsgBoxStyle.Information)
                End If
            End If
            eBotonGuardar = False
        Catch ex As System.Exception

            Log_Descripcion = ex.Message.ToString & " - Al tratar de Bonificar"
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Button6_Click_1()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

            Exit Sub
        End Try

    End Sub
    'Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.GuardaMotivosBonificacionTableAdapter.Fill(Me.DataSetEdgar.GuardaMotivosBonificacion, New System.Nullable(Of Long)(CType(Clv_FacturaToolStripTextBox.Text, Long)), DescripcionToolStripTextBox.Text)
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub
    Private Sub DataGridView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DataGridView1.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        convierteDetalleCargos_a_PreDetFacturas()
        'If EsEdoCta = False Then
        '    CHECASIHAYREVISTAS(1)
        '    If BndRevista = False Then
        '        Op_Revista = MsgBox("�Desea El Cliente Adquirir La Nueva Revista?", MsgBoxStyle.YesNo)
        '        If Op_Revista = 6 Then
        '            GloClv_Txt = "REVIS"
        '            NumRevistas.Show()
        '        Else
        '            BotonGrabar()
        '        End If
        '    Else
        '        BotonGrabar()
        '    End If
        'Else
        BotonGrabar()
        'End If
        'BotonGrabar()
    End Sub
    Private Sub TreeView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TreeView1.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub
    Private Sub FolioTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles FolioTextBox.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub
    Private Sub Button1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Button1.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub Button8_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Button8.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub ConfigureCrystalReportsOrdenes(ByVal op As String, ByVal Titulo As String, ByVal Clv_Orden1 As Long, ByVal Clv_TipSer1 As Integer, ByVal GloNom_TipSer As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0



            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing



            If IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "AG" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
            End If
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            '@Clv_TipSer int
            customersByCityReport.SetParameterValue(0, 0)
            ',@op1 smallint
            customersByCityReport.SetParameterValue(1, 1)
            ',@op2 smallint
            customersByCityReport.SetParameterValue(2, 0)
            ',@op3 smallint
            customersByCityReport.SetParameterValue(3, 0)
            ',@op4 smallint,
            customersByCityReport.SetParameterValue(4, 0)
            '@op5 smallint
            customersByCityReport.SetParameterValue(5, 0)
            ',@StatusPen bit
            customersByCityReport.SetParameterValue(6, 0)
            ',@StatusEje bit
            customersByCityReport.SetParameterValue(7, 0)
            ',@StatusVis bit,
            customersByCityReport.SetParameterValue(8, 0)
            '@Clv_OrdenIni bigint
            customersByCityReport.SetParameterValue(9, CLng(Clv_Orden1))
            ',@Clv_OrdenFin bigint
            customersByCityReport.SetParameterValue(10, CLng(Clv_Orden1))
            ',@Fec1Ini Datetime
            customersByCityReport.SetParameterValue(11, "01/01/1900")
            ',@Fec1Fin Datetime,
            customersByCityReport.SetParameterValue(12, "01/01/1900")
            '@Fec2Ini Datetime
            customersByCityReport.SetParameterValue(13, "01/01/1900")
            ',@Fec2Fin Datetime
            customersByCityReport.SetParameterValue(14, "01/01/1900")
            ',@Clv_Trabajo int
            customersByCityReport.SetParameterValue(15, 0)
            ',@Clv_Colonia int
            customersByCityReport.SetParameterValue(16, 0)
            ',@OpOrden int
            customersByCityReport.SetParameterValue(17, OpOrdenar)
            mySelectFormula = "Orden De Servicio: "
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            Dim CON12 As New SqlConnection(MiConexion)
            CON12.Open()
            Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON12
            Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.NewsoftvDataSet2.Dame_Impresora_Ordenes, Impresora, a)
            CON12.Dispose()
            CON12.Close()
            If a = 1 Then
                MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
                Exit Sub
            Else
                customersByCityReport.PrintOptions.PrinterName = Impresora
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If
            '--SetDBLogonForReport(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Mana_ImprimirOrdenes(ByVal Clv_Factura As Long)
        Try

            If IdSistema <> "VA" Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.DamelasOrdenesque_GeneroFacturaTableAdapter.Connection = CON
                Me.DamelasOrdenesque_GeneroFacturaTableAdapter.Fill(Me.NewsoftvDataSet2.DamelasOrdenesque_GeneroFactura, New System.Nullable(Of Long)(CType(Clv_Factura, Long)))
                CON.Dispose()
                CON.Close()
                Dim FilaRow As DataRow
                'Me.TextBox1.Text = ""

                For Each FilaRow In Me.NewsoftvDataSet2.DamelasOrdenesque_GeneroFactura.Rows
                    If IsNumeric(FilaRow("Clv_Orden").ToString()) = True Then
                        ConfigureCrystalReportsOrdenes(0, "", FilaRow("Clv_Orden").ToString(), FilaRow("Clv_TipSer").ToString(), FilaRow("Concepto").ToString())
                    End If
                Next
            End If
        Catch ex As System.Exception

            System.Windows.Forms.MessageBox.Show(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Mana_ImprimirOrdenes()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)
        End Try
    End Sub
    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        Me.CMBPanel6.Visible = False
        SiPagos = 0
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            SiPagos = 1
            Me.Cobra_PagosTableAdapter.Connection = CON
            Me.Cobra_PagosTableAdapter.Fill(Me.DataSetEdgar.Cobra_Pagos, New System.Nullable(Of Long)(CType(GloContrato, Long)), New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)), New System.Nullable(Of Integer)(CType(Bnd, Integer)), New System.Nullable(Of Integer)(CType(CuantasTv, Integer)))
            Me.DameDetalleTableAdapter.Connection = CON
            Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)
            Me.SumaDetalleTableAdapter.Connection = CON
            Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
            '
            RecalculaTotales_FacturacionNormal()
            '
            CON.Dispose()
            CON.Close()
            Me.CMBPanel6.Visible = False
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Button10_Click()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try
    End Sub
    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click



        If GloYaFueGenerado = False Then
            Try
                Dim CON As New SqlConnection(MiConexion)

                DataGridView3.DataSource = DataGridView1.DataSource

                If IsNumeric(DataGridView3.CurrentRow.Cells(29).Value) Then
                    Me.CLV_DETALLETextBox.Text = DataGridView3.CurrentRow.Cells(29).Value
                Else
                    MsgBox("La clave del Detalle No es num�rica", MsgBoxStyle.Exclamation, ":: Aviso ::")
                    Exit Sub
                End If

                'If VerificaSiPideAparato(GloContrato, Me.CLV_DETALLETextBox.Text) = True Then

                '    Dim Resp = MsgBox("El Cliente cuenta con Promoci�n de Estudiante, antes de cancelar debe de regresar su Cablemodem. �El Cliente trae consigo el Aparato?", MsgBoxStyle.YesNo, "DEBE REGRESAR SU APARATO")

                '    If Resp = MsgBoxResult.No Then

                '        MsgBox("No se puede generar su Cobro de Adeudo ya que el Cliente debe de regresar el aparato.", MsgBoxStyle.Information)
                '        Exit Sub

                '    End If

                'End If

                If IsNumeric(Me.Clv_Session.Text) = True And IsNumeric(Me.CLV_DETALLETextBox.Text) = True Then
                    If Me.Clv_Session.Text > 0 And Me.CLV_DETALLETextBox.Text > 0 Then
                        ePideAparato = 0
                        Dim ERROR_1 As Integer = 0
                        Dim msgERROR_1 As String = Nothing
                        CON.Open()

                        Me.CobraAdeudoTableAdapter.Connection = CON
                        Me.CobraAdeudoTableAdapter.Fill(Me.DataSetEdgar.CobraAdeudo, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)), New System.Nullable(Of Long)(CType(Me.CLV_DETALLETextBox.Text, Long)), ERROR_1, msgERROR_1, ePideAparato, eClv_Detalle)

                        Me.DameDetalleTableAdapter.Connection = CON
                        Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, Me.Clv_Session.Text, 0)

                        Me.SumaDetalleTableAdapter.Connection = CON
                        Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, Me.Clv_Session.Text, False, 0)

                        '
                        RecalculaTotales_FacturacionNormal()
                        '

                        CON.Dispose()
                        CON.Close()
                        If ERROR_1 = 1 Then
                            Me.LABEL19.Text = msgERROR_1
                            Me.Panel5.Visible = True
                            Me.Bloque(False)
                        ElseIf ERROR_1 = 2 Then
                            MsgBox(msgERROR_1)
                        End If
                        'VALIDACION PARA VALLARTA. PREGUNTA SI EL CLIENTE LLEVA CONSIGO EL APARATO, SI NO, SE GENERA UNA ORDEN DE RETIRO DE APARATO O CABLEMODEM
                        If ePideAparato = 1 Then

                            eRes = MsgBox("�El Cliente trae con sigo el Aparato?", MsgBoxStyle.YesNo, "Atenci�n")
                            If eRes = 6 Then

                                Dim CON2 As New SqlConnection(MiConexion)
                                CON2.Open()

                                Me.EntregaAparatoTableAdapter.Connection = CON2
                                Me.EntregaAparatoTableAdapter.Fill(Me.EricDataSet2.EntregaAparato, CLng(Me.Clv_Session.Text), eClv_Detalle)
                                CON2.Close()

                            End If
                        End If

                    End If
                End If


            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)

                Log_Descripcion = ex.Message.ToString & " - Dentro de Cobro de Adeudo" & " Caja: " & GloCaja & " Cajera: " & GloCajera & " Clave Ciudad: " & LocClv_Ciudad & " Contrato: " & GloContrato
                Log_Formulario = Me.Name.ToString
                Log_ProcedimientoAlmacenado = "Button12_Click()"
                GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

            End Try
        Else
            ' Si es cliente con Estado de Cuenta
            'Try
            '    Me.LabelSubTotal.Text = 0
            '    Me.LabelIva.Text = 0
            '    Me.LabelIEPS.Text = 0
            '    Me.LabelTotal.Text = 0
            '    Me.LblCredito_Apagar.Text = 0
            '    Me.LblImporte_Total.Text = 0
            '    Me.LabelGRan_Total.Text = 0
            '    Me.LabelSaldoAnterior.Text = 0
            '    Me.TextImporte_Adic.Text = 0
            '    '
            '    Me.ImportePuntosAplicados.Text = 0
            '    Me.ImporteTotalBonificado.Text = 0
            '    '
            '    Me.PanelTel.Visible = False
            '    Me.PanelNrm.Visible = True
            '    Me.DataGridView2.Visible = True
            '    Me.Clv_Session.Text = GloTelClv_Session
            '    gloClv_Session = GloTelClv_Session
            '    eBotonGuardar = False



            '    'FrmCobrarAdeudoPorServicio.Show()
            '    'FrmDetalle_Conceptos_Facturar.Show()
            'Catch ex As System.Exception
            '    System.Windows.Forms.MessageBox.Show(ex.Message)

            '    Log_Descripcion = ex.Message.ToString & " - Dentro de Cobro de Adeudo por Estado de Cuenta" & " Caja: " & GloCaja & " Cajera: " & GloCajera & " Clave Ciudad: " & LocClv_Ciudad & " Contrato: " & GloContrato
            '    Log_Formulario = Me.Name.ToString
            '    Log_ProcedimientoAlmacenado = "Button12_Click()"
            '    GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

            'End Try
            MsgBox("Antes de cobrar tu Adeudo debes pagar tu Estado de Cuenta.", MsgBoxStyle.Information, "COBRO DE ADEUDO")
        End If
        'FrmCobrarAdeudoPorServicio.Show()
    End Sub
    Private Sub LblFecha_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LblFecha.TextChanged
        If IsDate(LblFecha.Text) = True Then
            Me.Fecha_Venta.Value = LblFecha.Text
        End If
    End Sub
    Private Sub Fecha_Venta_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_Venta.ValueChanged
        If IsNumeric(Me.ContratoTextBox.Text) = True Then
            If Me.ContratoTextBox.Text > 0 Then
                If bloqueado <> 1 Then
                    BUSCACLIENTES(0)
                    Me.Bloque(True)
                End If
            End If
        End If
    End Sub
    Private Sub TelMuestraDetalleCargos(ByVal Contrato As Long)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("TelMuestraDetalleCargos", conexion)

        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Try
            'Antes de Cargar los Cargos generados en el Estado de Cuenta Limpiamos el Grid que los contendr�
            Me.limpiaGridFacturaEstadoCta()
            conexion.Open()
            Dim reader As SqlDataReader = comando.ExecuteReader()

            'Recorremos los Cargos obtenidos desde el Procedimiento
            While (reader.Read())
                Me.DataGridView2.Rows.Add(reader(0), reader(1))
            End While

            'Limpiamos conexi�n
            conexion.Dispose()
            conexion.Close()

            TelMuestraDetalleCargosAPagar(Contrato)
            Bloque(True)

        Catch ex As Exception

            conexion.Dispose()
            conexion.Close()
            MsgBox("Error al obtener los Cargos generados en el Estado de Cuenta.", MsgBoxStyle.Critical, "Advertencia")
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Descripci�n del Error")

            Log_Descripcion = ex.Message.ToString & " Caja: " & GloCaja & " Cajera: " & GloCajera & " Clave Ciudad: " & LocClv_Ciudad & " Contrato: " & GloContrato
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "TelMuestraDetalleCargos"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try
    End Sub

    Private Sub TelMuestraDetalleCargosAPagar(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("TelMuestraDetalleCargosAPagar", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)
        Me.Clv_Session.Text = 0
        Try
            conexion.Open()
            Dim reader As SqlDataReader = comando.ExecuteReader()
            While reader.Read()
                Me.LabelSubTotal.Text = Microsoft.VisualBasic.FormatCurrency(reader(0).ToString, 2)
                Me.LabelIva.Text = Microsoft.VisualBasic.FormatCurrency(reader(1).ToString, 2)
                Me.LabelTotal.Text = Microsoft.VisualBasic.FormatCurrency(reader(2).ToString, 2)
                Me.LblCredito_Apagar.Text = Microsoft.VisualBasic.FormatCurrency(reader(3).ToString, 2)
                Me.LblImporte_Total.Text = reader(4).ToString
                Me.LabelGRan_Total.Text = Microsoft.VisualBasic.FormatCurrency(reader(4).ToString, 2)
                Me.Clv_Session.Text = reader(5).ToString
                Me.LabelSaldoAnterior.Text = Microsoft.VisualBasic.FormatCurrency(reader(6).ToString, 2)
                Me.LabelIEPS.Text = Microsoft.VisualBasic.FormatCurrency(reader(7).ToString, 2)

                'Estos Cargos no generan bonificaciones por eso van en ceros
                Me.ImportePuntosAplicados.Text = "$0.00"
                Me.ImporteTotalBonificado.Text = "$0.00"

            End While

            conexion.Dispose()
            conexion.Close()

        Catch ex As Exception

            conexion.Dispose()
            conexion.Close()

            MsgBox("Error al obtener los Costos de los Cargos generados en el Estado de Cuenta.", MsgBoxStyle.Critical, "Advertencia")
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Descripci�n del Error")

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "TelMuestraDetalleCargosAPagar"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try
    End Sub
    Private Sub TextImporte_Adic_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextImporte_Adic.TextChanged
        If Me.PanelNrm.Visible = True Then

            If IsNumeric(TextImporte_Adic.Text) = True Then
                Me.LabelSubTotal.Text = 0
                Me.LabelIva.Text = 0
                Me.LabelIEPS.Text = 0
                Me.LabelTotal.Text = 0
                Me.LblCredito_Apagar.Text = 0
                Me.LblImporte_Total.Text = 0
                Me.LabelGRan_Total.Text = 0
                Me.LabelSaldoAnterior.Text = 0

                'No generan costo, por eso van en ceros
                Me.ImportePuntosAplicados.Text = "$0.00"
                Me.ImporteTotalBonificado.Text = "$0.00"

                If TextImporte_Adic.Text > 0 Then
                    Me.LabelSubTotal.Text = Microsoft.VisualBasic.FormatCurrency(Me.TextImporte_Adic.Text / 1.15, 2)
                    Me.LabelIva.Text = Microsoft.VisualBasic.FormatCurrency(Me.TextImporte_Adic.Text - Me.LabelSubTotal.Text, 2)
                    Me.LabelTotal.Text = Microsoft.VisualBasic.FormatCurrency(Me.TextImporte_Adic.Text, 2)
                    Me.LblCredito_Apagar.Text = 0
                    Me.LblImporte_Total.Text = Microsoft.VisualBasic.FormatCurrency(Me.TextImporte_Adic.Text, 2)
                    Me.LabelGRan_Total.Text = Microsoft.VisualBasic.FormatCurrency(Me.TextImporte_Adic.Text, 2)
                    Me.LabelSaldoAnterior.Text = 0
                End If
                Me.LblImporte_Total.Text = Me.TextImporte_Adic.Text
            End If
        End If
    End Sub

    Private Sub Clv_SessionTel_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SessionTel.TextChanged
        If IsNumeric(Me.Clv_SessionTel.Text) = True Then
            Me.Panel5.Visible = False
            locBndBon1 = False
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameDetalleTableAdapter.Connection = CON
            Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)
            Me.SumaDetalleTableAdapter.Connection = CON
            Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
            '
            RecalculaTotales_FacturacionNormal()
            '
            CON.Dispose()
            CON.Close()
            ''CREAARBOL1()
        End If
    End Sub
    Private Sub ButtonVentaEq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonVentaEq.Click
        Me.LabelSubTotal.Text = 0
        Me.LabelIva.Text = 0
        Me.LabelIEPS.Text = 0
        Me.LabelTotal.Text = 0
        Me.LblCredito_Apagar.Text = 0
        Me.LblImporte_Total.Text = 0
        Me.LabelGRan_Total.Text = 0
        Me.LabelSaldoAnterior.Text = 0
        Me.TextImporte_Adic.Text = 0
        '
        Me.ImportePuntosAplicados.Text = 0
        Me.ImporteTotalBonificado.Text = 0

        '
        Me.PanelTel.Visible = False
        Me.PanelNrm.Visible = True
        Me.DataGridView2.Visible = True
        Me.Clv_Session.Text = GloTelClv_Session
        gloClv_Session = GloTelClv_Session
        eBotonGuardar = False
        FrmVentaDeEquipo.Show()
    End Sub
    Private Sub ButtonPagoAbono_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPagoAbono.Click
        'Me.LabelSubTotal.Text = 0
        'Me.LabelIva.Text = 0
        'Me.LabelIEPS.Text = 0
        'Me.LabelTotal.Text = 0
        'Me.LblCredito_Apagar.Text = 0
        'Me.LblImporte_Total.Text = 0
        'Me.LabelGRan_Total.Text = 0
        'Me.LabelSaldoAnterior.Text = 0
        ''
        'Me.ImportePuntosAplicados.Text = 0
        'Me.ImporteTotalBonificado.Text = 0

        ''
        'Me.TextImporte_Adic.Text = 0
        'Me.PanelTel.Visible = False
        'Me.PanelNrm.Visible = True
        'Me.DataGridView2.Visible = True
        'Me.Clv_Session.Text = GloTelClv_Session
        'gloClv_Session = GloTelClv_Session
        eBotonGuardar = False
        FrmPagoParaAbono.Show()
    End Sub
    Private Sub btnAdelantarPagos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdelantarPagos.Click

        If GloYaFueGenerado = False Then
            ''Este c�digo solo aplica para la Facturaci�n Normal
            'If e.ColumnIndex = 7 Then
            '    If SiPagos = 1 Then
            '        MsgBox("No se puede adelantar pagos con la promoci�n que se le esta aplicando")
            '        Exit Sub
            '    End If
            '    If DataGridView1.SelectedCells(7).Value = "Adelantar Pagos" Then
            If Me.DataGridView1.Rows.Count > 0 Then

                If DataGridView1.SelectedCells(7).Value = "Adelantar Pagos" Then
                    If IsNumeric(DataGridView1.SelectedCells(0).Value) = True And IsNumeric(DataGridView1.SelectedCells(1).Value) = True And IsNumeric(DataGridView1.SelectedCells(2).Value) = True And IsNumeric(Me.ContratoTextBox.Text) = True Then
                        gloClv_Session = DataGridView1.SelectedCells(0).Value
                        gloClv_Servicio = DataGridView1.SelectedCells(1).Value
                        gloClv_llave = DataGridView1.SelectedCells(2).Value
                        gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                        gloClave = DataGridView1.SelectedCells(4).Value
                        Dim ERROR_1 As Integer = 0
                        Dim MSGERROR_1 As String = Nothing
                        Dim CON As New SqlConnection(MiConexion)
                        CON.Open()
                        Me.Pregunta_Si_Puedo_AdelantarTableAdapter.Connection = CON
                        Me.Pregunta_Si_Puedo_AdelantarTableAdapter.Fill(Me.DataSetEdgar.Pregunta_Si_Puedo_Adelantar, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), ERROR_1, MSGERROR_1)
                        CON.Dispose()
                        CON.Close()
                        If ERROR_1 = 0 Then
                            My.Forms.FrmPagosAdelantados.Show()
                        ElseIf ERROR_1 = 2 Then
                            MsgBox(MSGERROR_1)
                        End If
                    ElseIf DataGridView1.SelectedCells(7).Value = "Ext. Adicionales" Then
                        GloClv_Txt = "CEXTV"
                        My.Forms.FrmExtecionesTv.Show()
                    ElseIf DataGridView1.SelectedCells(7).Value = "Ver Detalle" Then
                        gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                        FrmDetCobroDesc.Show()
                    End If
                    'ElseIf DataGridView1.SelectedCells(7).Value = "Ver Detalle" Then
                    '    gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                    '    FrmDetCobroDesc.Show()
                    'Else
                    '    MsgBox("No se pueden Adelantar pagos de Telefon�a. Esto hasta que se genere su primer estado de Cuenta", MsgBoxStyle.Information)
                End If

            End If
            'ElseIf DataGridView1.SelectedCells(7).Value = "Ver Detalle" Then
            'gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
            'FrmDetCobroDesc.Show()
            '    End If
            'End If
        Else
            ''El c�digo de abajo es el que aplicar� para los Estados de Cuenta
            'Me.LabelSubTotal.Text = 0
            'Me.LabelIva.Text = 0
            'Me.LabelIEPS.Text = 0
            'Me.LabelTotal.Text = 0
            'Me.LblCredito_Apagar.Text = 0
            'Me.LblImporte_Total.Text = 0
            'Me.LabelGRan_Total.Text = 0
            'Me.LabelSaldoAnterior.Text = 0
            'Me.TextImporte_Adic.Text = 0
            ''
            'Me.ImportePuntosAplicados.Text = 0
            'Me.ImporteTotalBonificado.Text = 0
            ''
            'Me.PanelTel.Visible = False
            'Me.PanelNrm.Visible = True
            'Me.DataGridView2.Visible = True
            'Me.Clv_Session.Text = GloTelClv_Session
            'gloClv_Session = GloTelClv_Session
            'eBotonGuardar = False

            ''Cargamos el Form de Adelantar Pagos
            'FrmAdelantarCargos.Show()
            MsgBox("Solo puedes adelantar pagos hasta que hayas saldado tu Estado de Cuenta", MsgBoxStyle.Information, "ADELANTAR PAGOS")
        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Sub
    Private Sub bloqueaBotones()
        Me.Button7.Enabled = False
        Me.Button6.Enabled = False
        Me.Button2.Enabled = False
        Me.Button5.Enabled = False
        Me.Button12.Enabled = False
        Me.ButtonVentaEq.Enabled = False
        ButtonPagoAbono.Enabled = False
        'Adelantar Pagos
        Me.btnAdelantarPagos.Enabled = False

    End Sub
    Private Sub habilitarBotones()
        Me.Button7.Enabled = True
        Me.Button6.Enabled = True
        Me.Button2.Enabled = True
        Me.Button5.Enabled = True
        Me.Button12.Enabled = True
        Me.ButtonVentaEq.Enabled = True
        ButtonPagoAbono.Enabled = True
        'Adelantar Pagos
        'Me.btnAdelantarPagos.Enabled = True
    End Sub

    Private Function Dime_si_cobro_EdoCta(ByVal Contrato As Integer) As Boolean
        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "sp_Dime_si_cobro_por_EdoCta"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 8

                Dim prmContrato As New SqlParameter("@Contrato", SqlDbType.Int)
                prmContrato.Value = Contrato
                .Parameters.Add(prmContrato)

                Dim prmBndEdoCta As New SqlParameter("@BndEdoCta", SqlDbType.TinyInt)
                prmBndEdoCta.Direction = ParameterDirection.Output
                prmBndEdoCta.Value = 0
                .Parameters.Add(prmBndEdoCta)

                Dim Result As Integer = .ExecuteNonQuery()

                If prmBndEdoCta.Value = 1 Then
                    EsEdoCta = True
                    Return True
                Else
                    EsEdoCta = False
                    Return False
                End If

            End With
            CON80.Dispose()
            CON80.Close()
        Catch ex As Exception
            CON80.Dispose()
            CON80.Close()
            MsgBox("Error al verificar si el cliente tiene un estado de cuenta.", MsgBoxStyle.Exclamation, ":: Advertencia ::")
            MsgBox(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "sp_Dime_si_cobro_por_EdoCta"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try

    End Function
    Public Sub CHECASIHAYREVISTAS(ByVal OP As Integer)
        Dim CONEXION As New SqlConnection(MiConexion)
        Dim COMANDO As New SqlCommand("CHECASIHAYREVISTAS", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure

        Dim PARAMETRO1 As New SqlParameter("@SUCURSAL", SqlDbType.Int)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = GloSucursal
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@BNDREVISTAS", SqlDbType.Bit)
        PARAMETRO2.Direction = ParameterDirection.Output
        COMANDO.Parameters.Add(PARAMETRO2)

        Dim PARAMETRO3 As New SqlParameter("@NOREVISTAS", SqlDbType.Int)
        PARAMETRO3.Direction = ParameterDirection.Output
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@OP", SqlDbType.Int)
        PARAMETRO4.Direction = ParameterDirection.Input
        PARAMETRO4.Value = OP
        COMANDO.Parameters.Add(PARAMETRO4)

        Try
            CONEXION.Open()
            COMANDO.ExecuteNonQuery()
            BndRevista = PARAMETRO2.Value
            TotalRevistas = PARAMETRO3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "CHECASIHAYREVISTAS"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub
    Private Sub DataGridView1_CellMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles DataGridView1.CellMouseDoubleClick
        'Activar la celda seleccionada


        Try
            If DataGridView1.SelectedCells(7).Value = "Ver Detalle" Then
                gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                FrmDetCobroDesc.Show()
                Exit Sub
            ElseIf DataGridView1.SelectedCells(7).Value = "Ext. Adicionales" Then
                GloClv_Txt = "CEXTV"
                My.Forms.FrmExtecionesTv.Show()
                Exit Sub
            End If

            'De lo contrario mostramos la pantalla de los detalles del concepto (Contrataci�n, Mensualidad, Reconexi�n)
            If Me.DataGridView1.Rows.Count > 0 And GloYaFueGenerado = False Then

                AsignaDetalles()
                FrmDetFactura.Show()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub RecalculaTotales_FacturacionNormal()
        'Barremos el Grid de SumaDetalle para mostrar los totales a pagar

        Dim i As Integer
        Try
            i = 0
            If Me.SumaDetalleDataGridView.Rows.Count > 0 Then

                'Asignamos SumaDetalle al Nuevo grid base
                Me.DataGridView4.DataSource = Me.SumaDetalleDataGridView.DataSource

                'Recorremos los totales generados en Facturaci�n Normal
                While i < Me.DataGridView4.RowCount


                    If Me.DataGridView4.Rows(i).Cells("Descripcion").Value.ToString = "Total de Puntos Aplicados " Then
                        Me.ImportePuntosAplicados.Text = "$" & Me.DataGridView4.Rows(i).Cells("Total").Value.ToString

                    ElseIf Me.DataGridView4.Rows(i).Cells("Descripcion").Value.ToString = "Total Bonificado " Then
                        Me.ImporteTotalBonificado.Text = "$" & Me.DataGridView4.Rows(i).Cells("Total").Value.ToString

                    ElseIf Me.DataGridView4.Rows(i).Cells("Descripcion").Value.ToString = "Importe Total" Then
                        Me.LabelGRan_Total.Text = "$" & Me.DataGridView4.Rows(i).Cells("Total").Value.ToString

                    ElseIf Me.DataGridView4.Rows(i).Cells("Descripcion").Value.ToString = "Subtotal " Then
                        Me.LabelTotal.Text = "$" & Me.DataGridView4.Rows(i).Cells("Total").Value.ToString

                    End If

                    i = i + 1
                End While

            End If
        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerEdoCta.Click
        'If (Me.EsEdoCta = True) Then

        CargaEstadoDeCuentaPorPeriodo_FAC(GloContrato, IDEdoCta)

        'End If
    End Sub

    Private Sub Button9_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerDetalle.Click
        If Me.DataGridView1.Rows.Count > 0 Then
            If DataGridView1.SelectedCells(7).Value = "Ver Detalle" Then
                gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                FrmDetCobroDesc.Show()
            Else
                'Activar la celda seleccionada
                Try
                    If Me.DataGridView1.Rows.Count > 0 And EsEdoCta = False Then
                        Me.DataGridView3.DataSource = DataGridView1.DataSource
                        AsignaDetalles()
                        FrmDetFactura.Show()
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)

                    Log_Descripcion = ex.Message.ToString & " - Al tratar de ver el Detalle del Concepto a Facturar"
                    Log_Formulario = Me.Name.ToString
                    Log_ProcedimientoAlmacenado = "Private Sub Button9_Click_1()"
                    GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

                End Try
            End If
        End If
    End Sub
    Public Sub AsignaDetalles()
        Try
            'Hacesmos el Source
            Me.DataGridView3.DataSource = DataGridView1.DataSource

            'DetFac_Puntos_Aplicados_Otros
            If IsDBNull(DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(13).Value) = True Then
                DetFac_Puntos_Aplicados_Otros = "0"
            Else
                DetFac_Puntos_Aplicados_Otros = DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(13).Value
            End If
            'DetFac_Clv_Detalle
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(29).Value) = True Then
                DetFac_Clv_Detalle = "0"
            Else
                DetFac_Clv_Detalle = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(29).Value
            End If
            'DetFac_Clv_Servicio
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(1).Value) = True Then
                DetFac_Clv_Servicio = "0"
            Else
                DetFac_Clv_Servicio = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(1).Value
            End If
            'DetFac_Descorta
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(5).Value) = True Then
                DetFac_Descorta = ""
            Else
                DetFac_Descorta = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(5).Value.ToString
            End If
            'DetFac_TVsAdic
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(7).Value) = True Then
                DetFac_TVsAdic = "0"
            Else
                DetFac_TVsAdic = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(7).Value
            End If
            'DetFac_Meses_Cortesia
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(8).Value) = True Then
                DetFac_Meses_Cortesia = "0"
            Else
                DetFac_Meses_Cortesia = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(8).Value
            End If
            'DetFac_Meses_Apagar
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(9).Value) = True Then
                DetFac_Meses_Apagar = "0"
            Else
                DetFac_Meses_Apagar = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(9).Value
            End If
            'DetFac_Importe
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(10).Value) = True Then
                DetFac_Importe = "0"
            Else
                DetFac_Importe = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(10).Value
            End If
            'DetFac_Periodo_Pagado_Inicial
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(11).Value) = True Then
                DetFac_Periodo_Pagado_Inicial = ""
            Else
                DetFac_Periodo_Pagado_Inicial = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(11).Value.ToString
            End If
            'DetFac_Periodo_Pagado_Final
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(12).Value) = True Then
                DetFac_Periodo_Pagado_Final = ""
            Else
                DetFac_Periodo_Pagado_Final = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(12).Value.ToString
            End If
            'DetFac_Puntos_Aplicados_Ant
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(14).Value) = True Then
                DetFac_Puntos_Aplicados_Ant = "0"
            Else
                DetFac_Puntos_Aplicados_Ant = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(14).Value
            End If
            'DetFac_Puntos_Aplicados_Pago_Oportuno
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(15).Value) = True Then
                DetFac_Puntos_Aplicados_Pago_Oportuno = "0"
            Else
                DetFac_Puntos_Aplicados_Pago_Oportuno = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(15).Value
            End If
            'DetFac_DescuentoNet
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(16).Value) = True Then
                DetFac_DescuentoNet = "0"
            Else
                DetFac_DescuentoNet = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(16).Value
            End If
            'DetFac_Ultimo_Mes
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(24).Value) = True Then
                DetFac_Ultimo_Mes = "0"
            Else
                DetFac_Ultimo_Mes = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(24).Value
            End If
            'DetFac_Ultimo_Anio
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(25).Value) = True Then
                DetFac_Ultimo_Anio = "0"
            Else
                DetFac_Ultimo_Anio = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(25).Value
            End If
            'DetFac_ImporteBonificacion
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(23).Value) = True Then
                DetFac_ImporteBonificacion = "0"
            Else
                DetFac_ImporteBonificacion = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(23).Value
            End If

            'DetFac_Desc_OtrosServ_Misma_Categoria
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(17).Value) = True Then
                DetFac_Desc_OtrosServ_Misma_Categoria = "0"
            Else
                DetFac_Desc_OtrosServ_Misma_Categoria = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(17).Value
            End If

            'DetFac_Dias_Bonifica
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(21).Value) = True Then
                DetFac_Dias_Bonifica = "0"
            Else
                DetFac_Dias_Bonifica = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(21).Value
            End If

            'DetFac_Aparato
            If IsDBNull(Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(28).Value) = True Then
                DetFac_Aparato = "0"
            Else
                DetFac_Aparato = Me.DataGridView3.Rows(Me.DataGridView3.CurrentRow.Index).Cells(28).Value
            End If

        Catch ex As Exception
        End Try


    End Sub
    Private Sub limpiaGridFacturaNormal()
        Dim i As Integer

        Try
            i = 0
            While i < Me.DataGridView1.RowCount
                Me.DataGridView1.Rows.RemoveAt(i)
            End While
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub limpiaGridFacturaEstadoCta()
        Dim i As Integer
        Try
            i = 0
            While i < Me.DataGridView2.RowCount
                Me.DataGridView2.Rows.RemoveAt(i)
            End While
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub HabilitaBotonesFacNormal(ByVal Bandera As Boolean)
        btnVerDetalle.Visible = Bandera
        Button6.Visible = Bandera


        If Bandera = True Then
            Me.ButtonPagoAbono.Visible = False
        Else
            Me.ButtonPagoAbono.Visible = Bandera
        End If


    End Sub
    Private Sub ConfigureCrystalReports_tickets_Normal(ByVal Clv_Factura As Long)


        Dim ba As Boolean = False
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        '        If GloImprimeTickets = False Then
        'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BusFacFiscalTableAdapter.Connection = CON
        Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
        CON.Dispose()
        CON.Close()

        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
        Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
        CON2.Close()

        If IdSistema = "SA" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
            ba = True
        ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
            ba = True
        ElseIf IdSistema = "AG" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
            ba = True
        ElseIf IdSistema = "VA" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
            ba = True
        Else
            If IdSistema = "VA" Then
                'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
                ConfigureCrystalReports_tickets_Nuevo(Clv_Factura)
                Exit Sub
            Else
                ConfigureCrystalReports_tickets_Nuevo(Clv_Factura)
                Exit Sub
            End If
        End If


        'End If

        customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        '    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        'End If
        SetDBLogonForReport(connectionInfo, customersByCityReport)


        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, Clv_Factura)
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, "0")
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, "0")
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, "0")

        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            If eActTickets = True Then
                customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            End If
        End If

        'If facticket = 1 Then
        '    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"
        If (IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "AG" Or IdSistema = "VA") And facnormal = True And identi > 0 Then

            customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        Else
            customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        End If

        If IdSistema = "AG" And facnormal = True And identi > 0 Then
            eCont = 1
            eRes = 0
            Do
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
                eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont) + "/3, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
                '6=Yes;7=No
                If eRes = 6 Then eCont = eCont + 1

            Loop While eCont <= 3
        Else
            If IdSistema = "SA" Then
                MsgBox("Se va a imprimir una Factura Fiscal. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
                MsgBox("Se va a imprimir la copia de la Factura Fiscal anterior. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            Else
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If

        End If

        '-----------------------------------------------------------------------------------------------------------------------------------------------------------------

        'CrystalReportViewer1.ReportSource = customersByCityReport

        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing





        'Try
        'Dim ba As Boolean = False
        'Select Case IdSistema
        '    Case "VA"
        '        customersByCityReport = New ReporteCajasTickets_2VA
        '    Case "LO"
        '        customersByCityReport = New ReporteCajasTickets_2Log
        '    Case "AG"
        '        customersByCityReport = New ReporteCajasTickets_2AG
        '    Case "SA"
        '        customersByCityReport = New ReporteCajasTickets_2SA
        '    Case "TO"
        '        customersByCityReport = New ReporteCajasTickets_2TOM
        '    Case Else
        '        customersByCityReport = New ReporteCajasTickets_2OLD
        'End Select


        'Dim connectionInfo As New ConnectionInfo

        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        'Dim reportPath As String = Nothing

        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.BusFacFiscalTableAdapter.Connection = CON
        'Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
        'CON.Close()
        'CON.Dispose()

        'eActTickets = False
        'Dim CON2 As New SqlConnection(MiConexion)
        'CON2.Open()
        'Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
        'Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
        'CON2.Close()
        'CON2.Dispose()
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, Clv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")

        'If ba = False Then
        '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        '    If eActTickets = True Then
        '        customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
        '    End If
        'End If

        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        'customersByCityReport.PrintToPrinter(1, True, 1, 1)
        'customersByCityReport.Dispose()

        'Catch ex As Exception
        '    MsgBox("Error al Imprimir el Ticket.", MsgBoxStyle.Critical, ":: Aviso de Error :: ")
        '    MsgBox(ex.Message)

        '    Log_Descripcion = ex.Message.ToString
        '    Log_Formulario = Me.Name.ToString
        '    Log_ProcedimientoAlmacenado = "ConfigureCrystalReports_tickets_Normal()"
        '    GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        'End Try

    End Sub
    Private Sub Guarda_Tipo_Tarjeta(ByVal Clv_factura As Long, ByVal Tipo As Integer, ByVal Monto As Decimal)
        Dim CON100 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()

        Try
            SQL = New SqlCommand()
            CON100.Open()
            With SQL
                .CommandText = "Nuevo_Rel_Pago_Tarjeta_Factura"
                .Connection = CON100
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@Clv_factura bigint,@Tipo int,@Monto money
                Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Clv_factura
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Tipo", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = Tipo
                .Parameters.Add(prm1)

                prm = New SqlParameter("@Monto", SqlDbType.Money)
                prm.Direction = ParameterDirection.Input
                prm.Value = Monto
                .Parameters.Add(prm)

                Dim ia As Integer = .ExecuteNonQuery()
            End With
            CON100.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReports_tickets_Nuevo(ByVal Clv_Factura As Long)

        'ConfigureCrystalReports

        Dim ba As Boolean = False
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        '        If GloImprimeTickets = False Then
        'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BusFacFiscalTableAdapter.Connection = CON
        Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
        CON.Dispose()
        CON.Close()

        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
        Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
        CON2.Close()

        If IdSistema = "SA" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
            ba = True
        ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
            ba = True
        ElseIf IdSistema = "AG" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
            ba = True
        ElseIf IdSistema = "VA" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
            ba = True
        Else
            If IdSistema = "VA" Then
                'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
                ConfigureCrystalReports_tickets(Clv_Factura, "Original")
                Exit Sub
            Else
                ConfigureCrystalReports_tickets(Clv_Factura, "Original")
                Exit Sub
            End If
        End If


        'End If

        customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        '    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        'End If
        SetDBLogonForReport(connectionInfo, customersByCityReport)


        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, Clv_Factura)
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, "0")
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, "0")
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, "0")

        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            If eActTickets = True Then
                customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            End If
        End If

        'If facticket = 1 Then
        '    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"
        If (IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "AG" Or IdSistema = "VA") And facnormal = True And identi > 0 Then

            customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        Else
            customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        End If

        If IdSistema = "AG" And facnormal = True And identi > 0 Then
            eCont = 1
            eRes = 0
            Do
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
                eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont) + "/3, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
                '6=Yes;7=No
                If eRes = 6 Then eCont = eCont + 1

            Loop While eCont <= 3
        Else
            If IdSistema = "SA" Then
                MsgBox("Se va a imprimir una Factura Fiscal. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
                MsgBox("Se va a imprimir la copia de la Factura Fiscal anterior. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            Else
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If

        End If

        'CrystalReportViewer1.ReportSource = customersByCityReport

        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing


    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged

        'Limpiamos Mensaje de Error
        LABEL19.BackColor = Me.BackColor
        Me.LABEL19.Text = ""
        Me.Panel5.Visible = False

        'Default para antes del primer periodo
        Me.ButtonPagoAbono.Enabled = False
        Me.btnVerEdoCta.Enabled = False

        BndError = 0

        Me.lblEsEdoCta.Visible = False
        Me.btnAdelantarPagos.Enabled = False

        'Button5.Enabled = True
        'Button7.Enabled = True
        'Button12.Enabled = True
        'btnAdelantarPagos.Enabled = True

    End Sub

    Public Function MuestraPromocionAplica() As String

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "DameLeyendaPromocion"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 8

                Dim prmContrato As New SqlParameter("@Clv_Session", SqlDbType.Int)
                prmContrato.Value = GloTelClv_Session
                .Parameters.Add(prmContrato)

                Dim prmBndEdoCta As New SqlParameter("@LeyendaPromocionAplica", SqlDbType.VarChar, 40000)
                prmBndEdoCta.Direction = ParameterDirection.Output
                prmBndEdoCta.Value = ""
                .Parameters.Add(prmBndEdoCta)


                'Nos dice si puedo o no quitar de la lista por Promoci�n COMBO.
                Dim prmPuedoQuitarDeLaLista As New SqlParameter("@PuedoQuitarDeLaLista", SqlDbType.Bit)
                prmPuedoQuitarDeLaLista.Direction = ParameterDirection.Output
                prmPuedoQuitarDeLaLista.Value = PuedoQuitarDeLaLista
                .Parameters.Add(prmPuedoQuitarDeLaLista)

                'Agregamos dos nuevos par�metros por si es un COMBO con Promoci�n al tratar de quitar de la lista alguno quitar los 2 ya que es PROMOCI�N - COMBO.

                Dim prmClaveDetalle_UNO As New SqlParameter("@ClaveDetalle_UNO", SqlDbType.BigInt)
                prmClaveDetalle_UNO.Direction = ParameterDirection.Output
                prmClaveDetalle_UNO.Value = 0
                .Parameters.Add(prmClaveDetalle_UNO)

                Dim prmClaveDetalle_DOS As New SqlParameter("@ClaveDetalle_DOS", SqlDbType.BigInt)
                prmClaveDetalle_DOS.Direction = ParameterDirection.Output
                prmClaveDetalle_DOS.Value = 0
                .Parameters.Add(prmClaveDetalle_DOS)


                Dim Result As Integer = .ExecuteNonQuery()

                PuedoQuitarDeLaLista = prmPuedoQuitarDeLaLista.Value

                GloClaveDelDetalle_UNO = prmClaveDetalle_UNO.Value
                GloClaveDelDetalle_DOS = prmClaveDetalle_DOS.Value

                Return prmBndEdoCta.Value.ToString


            End With
            CON80.Dispose()
            CON80.Close()
        Catch ex As Exception
            CON80.Dispose()
            CON80.Close()
            MsgBox("Error al verificar si el cliente aplica para alguna promoci�n..", MsgBoxStyle.Exclamation, ":: Advertencia ::")
            MsgBox(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Procedimiento: DameLeyendaPromocion, Funci�n: MuestraPromocionAplica()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try

    End Function

    Private Function VerificaSiPideAparato(ByVal Contrato As Integer, ByVal Clv_Detalle As Integer) As Boolean

        'Procedimientos para verificar si es que se le debe de pedir el aparato al Cliente  
        
        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet


        Try
            Dim comando As New SqlClient.SqlCommand("VerificaSiPideAparato", conn)
            comando.CommandType = CommandType.StoredProcedure


            comando.Parameters.Add("@Contrato", SqlDbType.Int)
            comando.Parameters(0).Value = Contrato

            comando.Parameters.Add("@PideAparato", SqlDbType.Bit)
            comando.Parameters(1).Direction = ParameterDirection.Output
            comando.Parameters(1).Value = False

            comando.Parameters.Add("@Clv_Detalle", SqlDbType.BigInt)
            comando.Parameters(2).Value = Clv_Detalle

            conn.Open()

            comando.ExecuteNonQuery()

            conn.Close()
            conn.Dispose()

            Return comando.Parameters(1).Value

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de verificar si el cliente ya pago su cobro de adeudo.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            conn.Close()
            conn.Dispose()

            Return False


        Finally

            conn.Close()
            conn.Dispose()

        End Try


    End Function

    Public Sub CargaEstadoDeCuenta(ByVal Contrato As Integer, ByVal Clv_Proceso As Integer)


        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet

        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("MuestraEstadoDeCuenta", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Adaptador.SelectCommand.Parameters.Add("@CONTRATO", SqlDbType.BigInt).Value = Contrato


            Dim Bs As New BindingSource
            Adaptador.Fill(Dataset)

            '               DATOS GENERALES

            Dataset.Tables(0).TableName = "tbl_DatosCliente"
            Dataset.Tables(1).TableName = "tbl_CargosDelCliente"
            Dataset.Tables(2).TableName = "tbl_Saldos"

            '               CONSUMOS

            Dataset.Tables(3).TableName = "tbl_044"
            Dataset.Tables(4).TableName = "tbl_045"
            Dataset.Tables(5).TableName = "tbl_LDN"
            Dataset.Tables(6).TableName = "tbl_EUC"
            Dataset.Tables(7).TableName = "tbl_RESTO"

            '               RESUMEN DE LLAMADAS

            Dataset.Tables(8).TableName = "tbl_TitLlamadas_044"
            Dataset.Tables(9).TableName = "tbl_TitLlamadas_045"
            Dataset.Tables(10).TableName = "tbl_TitLlamadas_LDN"
            Dataset.Tables(11).TableName = "tbl_TitLlamadas_EUC"
            Dataset.Tables(12).TableName = "tbl_TitLlamadas_RESTO"

            Dataset.Tables(13).TableName = "Tel_Tit_Resumen_Locales"
            Dataset.Tables(14).TableName = "Detalle_Llamadas_Locales"
            Dataset.Tables(15).TableName = "Detalle_Llamadas_EntreClientes"

            conn.Close()
            conn.Dispose()

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de enlazar los Datos Principales, los resultados de las b�squedas pueden ser inconsistentes.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Exit Sub

        Finally

            conn.Close()
            conn.Dispose()

        End Try

        'Cargamos el Informe en base al procediemiento
        Try

            'Asignamos el Reporte al Objeto Global
            rDocument = New ReportDocument()

            'Ruta del Reporte
            'rDocument.Load("\\192.168.60.200\Exes\SofTv\Reportes\rptEstadoDeCuenta.rpt")
            'rDocument.Load("E:\ProyectosTeamFundation\Developer\Sahuayo\Facturaci�n\softvFacturacion\softvFacturacion\rptEstadoDeCuenta.rpt")

            'Formulas
            'rDocument.DataDefinition.FormulaFields("EMPRESA").Text = "'" & GloEmpresa & "'"
            'rDocument.DataDefinition.FormulaFields("DIRECCION_EMPRESA").Text = "'" & GloDireccionEmpresa & "'"
            'rDocument.DataDefinition.FormulaFields("COLONIA DE LA EMPRESA").Text = "'" & GloColonia_CpEmpresa & "'"
            'rDocument.DataDefinition.FormulaFields("CIUDAD DE LA EMPRESA").Text = "'" & GloCiudadEmpresa & "'"
            'rDocument.DataDefinition.FormulaFields("RFC EMPRESA").Text = "'" & GloRfcEmpresa & "'"
            'rDocument.DataDefinition.FormulaFields("TELEFONO EMPRESA").Text = "'" & GloTelefonoEmpresa & "'"
            'rDocument.DataDefinition.FormulaFields("Original O Copia").Text = "'" & Original_o_Copia & "'"

            'Origen de Datos
            rDocument.SetDataSource(Dataset)

            'Lo mandamos al visor de Reportes
            FrmVisorDeReportes.Show()

        Catch ex As Exception
            MsgBox("Ocurri� un error al momento de generar la reimpresi�n de la devoluci�n.", MsgBoxStyle.Critical)
        End Try

    End Sub
    Public Sub CargaEstadoDeCuentaPorPeriodo(ByVal Contrato As Integer, ByVal PeriodoDeCobro As Integer)


        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet

        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("MuestraEstadoDeCuentaPorPeriodo", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Adaptador.SelectCommand.Parameters.Add("@CONTRATO", SqlDbType.BigInt).Value = Contrato
            Adaptador.SelectCommand.Parameters.Add("@PERIODO", SqlDbType.BigInt).Value = PeriodoDeCobro


            Dim Bs As New BindingSource
            Adaptador.Fill(Dataset)

            '               DATOS GENERALES

            Dataset.Tables(0).TableName = "tbl_DatosCliente"
            Dataset.Tables(1).TableName = "tbl_CargosDelCliente"
            Dataset.Tables(2).TableName = "tbl_Saldos"

            '               CONSUMOS

            Dataset.Tables(3).TableName = "tbl_044"
            Dataset.Tables(4).TableName = "tbl_045"
            Dataset.Tables(5).TableName = "tbl_LDN"
            Dataset.Tables(6).TableName = "tbl_EUC"
            Dataset.Tables(7).TableName = "tbl_RESTO"

            '               RESUMEN DE LLAMADAS

            Dataset.Tables(8).TableName = "tbl_TitLlamadas_044"
            Dataset.Tables(9).TableName = "tbl_TitLlamadas_045"
            Dataset.Tables(10).TableName = "tbl_TitLlamadas_LDN"
            Dataset.Tables(11).TableName = "tbl_TitLlamadas_EUC"
            Dataset.Tables(12).TableName = "tbl_TitLlamadas_RESTO"

            Dataset.Tables(13).TableName = "tbl_TitLlamadas_EUC_LDN"

            Dataset.Tables(14).TableName = "Tel_Tit_Resumen_Locales"
            Dataset.Tables(15).TableName = "Detalle_Llamadas_Locales"
            Dataset.Tables(16).TableName = "Detalle_Llamadas_EntreClientes"
            Dataset.Tables(17).TableName = "tbl_Impuestos"
            Dataset.Tables(18).TableName = "tbl_GeneralesEdoCta"

            'Dataset.Tables(16).TableName = "tbl_NumerosTelefonicos"

            conn.Close()
            conn.Dispose()

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de enlazar los Datos Principales, los resultados de las b�squedas pueden ser inconsistentes.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Exit Sub

        Finally

            conn.Close()
            conn.Dispose()

        End Try

        'Cargamos el Informe en base al procediemiento
        Try

            'Asignamos el Reporte al Objeto Global
            rDocument = New ReportDocument()

            'Ruta del Reporte
            rDocument.Load("\\192.168.60.200\Exes\SofTv\Reportes\rptEstadoDeCuenta.rpt")
            'rDocument.Load("E:\ProyectosTeamFundation\Developer\Sahuayo\Facturaci�n\softvFacturacion\softvFacturacion\rptEstadoDeCuenta.rpt")

          
            'Origen de Datos
            rDocument.SetDataSource(Dataset)

            'Lo mandamos al visor de Reportes
            FrmVisorDeReportes.Show()

        Catch ex As Exception
            MsgBox("Ocurri� un error.", MsgBoxStyle.Critical)
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub
    Public Sub CargaEstadoDeCuentaPorPeriodo_FAC(ByVal Contrato As Integer, ByVal PeriodoDeCobro As Integer)


        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet

        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("MuestraEstadoDeCuentaPorPeriodo_FAC", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Adaptador.SelectCommand.Parameters.Add("@CONTRATO", SqlDbType.BigInt).Value = Contrato
            Adaptador.SelectCommand.Parameters.Add("@ID_EDOCTA", SqlDbType.BigInt).Value = PeriodoDeCobro


            Dim Bs As New BindingSource
            Adaptador.Fill(Dataset)

            '               DATOS GENERALES

            Dataset.Tables(0).TableName = "tbl_DatosCliente"
            Dataset.Tables(1).TableName = "tbl_CargosDelCliente"
            Dataset.Tables(2).TableName = "tbl_Saldos"

            '               CONSUMOS

            Dataset.Tables(3).TableName = "tbl_044"
            Dataset.Tables(4).TableName = "tbl_045"
            Dataset.Tables(5).TableName = "tbl_LDN"
            Dataset.Tables(6).TableName = "tbl_EUC"
            Dataset.Tables(7).TableName = "tbl_RESTO"

            '               RESUMEN DE LLAMADAS

            Dataset.Tables(8).TableName = "tbl_TitLlamadas_044"
            Dataset.Tables(9).TableName = "tbl_TitLlamadas_045"
            Dataset.Tables(10).TableName = "tbl_TitLlamadas_LDN"
            Dataset.Tables(11).TableName = "tbl_TitLlamadas_EUC"
            Dataset.Tables(12).TableName = "tbl_TitLlamadas_RESTO"

            Dataset.Tables(13).TableName = "tbl_TitLlamadas_EUC_LDN"

            Dataset.Tables(14).TableName = "Tel_Tit_Resumen_Locales"
            Dataset.Tables(15).TableName = "Detalle_Llamadas_Locales"
            Dataset.Tables(16).TableName = "Detalle_Llamadas_EntreClientes"
            Dataset.Tables(17).TableName = "tbl_Impuestos"
            Dataset.Tables(18).TableName = "tbl_GeneralesEdoCta"

            'Dataset.Tables(16).TableName = "tbl_NumerosTelefonicos"

            conn.Close()
            conn.Dispose()

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de enlazar los Datos Principales, los resultados de las b�squedas pueden ser inconsistentes.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Exit Sub

        Finally

            conn.Close()
            conn.Dispose()

        End Try

        'Cargamos el Informe en base al procediemiento
        Try

            'Asignamos el Reporte al Objeto Global
            rDocument = New ReportDocument()

            'Ruta del Reporte
            rDocument.Load("\\192.168.60.200\Exes\SofTv\Reportes\rptEstadoDeCuenta.rpt")
            'rDocument.Load("E:\ProyectosTeamFundation\Developer\Sahuayo\Facturaci�n\softvFacturacion\softvFacturacion\rptEstadoDeCuenta.rpt")

            'Formulas

            'rDocument.DataDefinition.FormulaFields("EMPRESA").Text = "'" & GloEmpresa & "'"
            'rDocument.DataDefinition.FormulaFields("DIRECCION_EMPRESA").Text = "'" & GloDireccionEmpresa & "'"
            'rDocument.DataDefinition.FormulaFields("COLONIA DE LA EMPRESA").Text = "'" & GloColonia_CpEmpresa & "'"
            'rDocument.DataDefinition.FormulaFields("CIUDAD DE LA EMPRESA").Text = "'" & GloCiudadEmpresa & "'"
            'rDocument.DataDefinition.FormulaFields("RFC EMPRESA").Text = "'" & GloRfcEmpresa & "'"
            'rDocument.DataDefinition.FormulaFields("TELEFONO EMPRESA").Text = "'" & GloTelefonoEmpresa & "'"
            'rDocument.DataDefinition.FormulaFields("Original O Copia").Text = "'" & Original_o_Copia & "'"

            'Origen de Datos
            rDocument.SetDataSource(Dataset)

            'Lo mandamos al visor de Reportes
            FrmVisorDeReportes.Show()

        Catch ex As Exception
            MsgBox("Ocurri� un error.", MsgBoxStyle.Critical)
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    'Funci�n para mostrar los Cargos y Totales a pagar del Estado de Cuenta
    Private Sub MuestraDetalleCargos_EdoCta()

        Dim conn As New SqlConnection(MiConexion)

        Dim ImporteAnterior As Double = 0
        Dim ImporteDelMes As Double = 0
        Dim ImporteTotal As Double = 0
        Dim CreditoAPagar As Double = 0
        Dim FechaGeneracion As String = ""
        Dim TotalAbonadoACuenta As Double = 0

        Try

            conn.Open()
            Dim comando As New SqlClient.SqlCommand("MuestraDetalleCargos", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()

            Adaptador.SelectCommand = comando
            Adaptador.SelectCommand.Parameters.Add("@CONTRATO", SqlDbType.BigInt)
            Adaptador.SelectCommand.Parameters(0).Value = GloContrato

            Adaptador.SelectCommand = comando
            Adaptador.SelectCommand.Parameters.Add("@ImporteAnterior", SqlDbType.BigInt)
            Adaptador.SelectCommand.Parameters(1).Direction = ParameterDirection.Output
            Adaptador.SelectCommand.Parameters(1).Value = 0

            Adaptador.SelectCommand = comando
            Adaptador.SelectCommand.Parameters.Add("@ImporteDelMes", SqlDbType.Money)
            Adaptador.SelectCommand.Parameters(2).Direction = ParameterDirection.Output
            Adaptador.SelectCommand.Parameters(2).Value = 0

            Adaptador.SelectCommand = comando
            Adaptador.SelectCommand.Parameters.Add("@ImporteTotal", SqlDbType.Money)
            Adaptador.SelectCommand.Parameters(3).Direction = ParameterDirection.Output
            Adaptador.SelectCommand.Parameters(3).Value = 0

            Adaptador.SelectCommand = comando
            Adaptador.SelectCommand.Parameters.Add("@CreditoAPagar", SqlDbType.Money)
            Adaptador.SelectCommand.Parameters(4).Direction = ParameterDirection.Output
            Adaptador.SelectCommand.Parameters(4).Value = 0

            Adaptador.SelectCommand = comando
            Adaptador.SelectCommand.Parameters.Add("@ID_EDOCTA", SqlDbType.BigInt)
            Adaptador.SelectCommand.Parameters(5).Direction = ParameterDirection.Output
            Adaptador.SelectCommand.Parameters(5).Value = 0

            Adaptador.SelectCommand = comando
            Adaptador.SelectCommand.Parameters.Add("@FechaDeGeneracion_TXT", SqlDbType.VarChar, 100)
            Adaptador.SelectCommand.Parameters(6).Direction = ParameterDirection.Output
            Adaptador.SelectCommand.Parameters(6).Value = 0

            Adaptador.SelectCommand = comando
            Adaptador.SelectCommand.Parameters.Add("@TOTAL_ABONADO_A_CUENTA", SqlDbType.Money)
            Adaptador.SelectCommand.Parameters(7).Direction = ParameterDirection.Output
            Adaptador.SelectCommand.Parameters(7).Value = 0

            Dim Dataset As New DataSet

            Adaptador.Fill(Dataset)

            Dataset.Tables(0).TableName = "DetalleCargos"

            Me.DataGridView2.DataSource = Dataset.Tables("DetalleCargos")

            Dim cont As Integer = 0

            For cont = 0 To DataGridView2.ColumnCount - 1

                Me.DataGridView2.Columns(cont).Visible = False

            Next

            Me.DataGridView2.Columns(2).Visible = True
            Me.DataGridView2.Columns(30).Visible = True

            Me.DataGridView2.Columns(2).Width = 450
            Me.DataGridView2.Columns(30).Width = 130
            Me.DataGridView2.Columns(30).DefaultCellStyle.Format = "c"

            Me.DataGridView2.Columns(2).HeaderText = "Cargo Generado por Estado de Cuenta"
            Me.DataGridView2.Columns(30).HeaderText = "Costo del Cargo:"

            'Asignamos los Totales

            Me.LblImporte_Total.Text = Adaptador.SelectCommand.Parameters(2).Value.ToString
            Me.LabelSaldoAnterior.Text = "$" & Adaptador.SelectCommand.Parameters(1).Value.ToString


            IDEdoCta = Adaptador.SelectCommand.Parameters(5).Value
            FechaGeneracion = Adaptador.SelectCommand.Parameters(6).Value.ToString
            TotalAbonadoACuenta = Adaptador.SelectCommand.Parameters(7).Value

            Glo_TotalAbonadoACuenta = TotalAbonadoACuenta

            'Obtenemos la suma de los Cargos

            Me.Label29.Visible = True
            Me.CMBLabel29.Visible = True
            Me.LabelSaldoAnterior.Visible = True
            Me.LblCredito_Apagar.Visible = True

            Me.Label31.Visible = False
            Me.Label32.Visible = False
            Me.ImportePuntosAplicados.Visible = False
            Me.ImporteTotalBonificado.Visible = False

            Me.ButtonPagoAbono.Visible = True

            'Obtenemos los Valores
            Me.LblImporte_Total.Text = Adaptador.SelectCommand.Parameters(3).Value.ToString
            Me.LabelTotal.Text = Adaptador.SelectCommand.Parameters(2).Value.ToString
            Me.LabelGRan_Total.Text = Adaptador.SelectCommand.Parameters(3).Value.ToString
            Me.LblCredito_Apagar.Text = Adaptador.SelectCommand.Parameters(4).Value.ToString

            'Aplicamos Formato de 2 D�gitos
            Me.LblImporte_Total.Text = CType(Me.LblImporte_Total.Text, Double)
            Me.LabelTotal.Text = CType(Me.LabelTotal.Text, Double)
            Me.LabelGRan_Total.Text = CType(Me.LabelGRan_Total.Text, Double)
            Me.LblCredito_Apagar.Text = CType(Me.LblCredito_Apagar.Text, Double)

            'Aplicamos Vista
            Me.LblImporte_Total.Text = "$" & Me.LblImporte_Total.Text
            Me.LabelTotal.Text = "$" & Me.LabelTotal.Text
            Me.LabelGRan_Total.Text = "$" & Me.LabelGRan_Total.Text
            Me.LblCredito_Apagar.Text = "$" & Me.LblCredito_Apagar.Text

            'Asignamos el Importe total a lavariable Global para validar los Abonos a Cuenta
            GloImporteTotalEstadoDeCuenta = Adaptador.SelectCommand.Parameters(3).Value


            If IDEdoCta = 0 Then

                Me.lblEsEdoCta.Text = "Cliente con Estado de Cuenta - A�n no generado." & " - Total Abonado a Cuenta: $" & TotalAbonadoACuenta.ToString
                Me.btnHabilitaFacNormal.Visible = True

                Me.LabelTotal.Text = "$0.00"
                Me.LblImporte_Total.Text = "$0.00"
                Me.LabelGRan_Total.Text = "$0.00"
                Me.LabelSaldoAnterior.Text = "$0.00"

            Else

                Me.lblEsEdoCta.Text = "Cliente con Estado de Cuenta - Fecha de Generaci�n: " & FechaGeneracion.ToString & " - Total Abonado a Cuenta: $" & TotalAbonadoACuenta.ToString
                Me.btnHabilitaFacNormal.Visible = False

            End If

        Catch ex As Exception

            MsgBox("Ocurrio un error al momento de cargar los Cargos Generados en el Estado de Cuenta.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Procedimiento: MuestraDetalleCargos, Funci�n: MuestraDetalleCargos_EdoCta(), Contrato:" & GloContrato.ToString
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        Finally

            conn.Dispose()
            conn.Close()

        End Try

    End Sub
    'Procedimiento que convierte los Detalles de los Cargos generados en el Estado de Cuenta en PreDetFacturas
    Private Sub convierteDetalleCargos_a_PreDetFacturas()

        'Para poder generar un registro dentro de Facturas y DetFacturas vamos a transformar los cargos generados en el Estado de Ceunta en registros
        'de la tabla PreDetFacturas

        'Barremos todo lo que haya en el Grid de DetalleCargos
        Dim sw As New StringWriter()

        Dim w As New XmlTextWriter(sw)
        Dim i As Integer

        Try

            w.Formatting = Formatting.Indented

            w.WriteStartElement("DetalleCargos")
            w.WriteAttributeString("Cargo", "DetCargo")

            For i = 0 To Me.DataGridView2.RowCount - 1

                w.WriteStartElement("PreDetCargo")

                w.WriteAttributeString("ident", Me.DataGridView2.Rows(i).Cells(0).Value.ToString)
                w.WriteAttributeString("idEstadoDeCuenta", Me.DataGridView2.Rows(i).Cells(1).Value.ToString)
                w.WriteAttributeString("Descripcion", Me.DataGridView2.Rows(i).Cells(2).Value.ToString)
                w.WriteAttributeString("Clv_Servicio", Me.DataGridView2.Rows(i).Cells(3).Value.ToString)
                w.WriteAttributeString("Clv_LlaveDelServicio", Me.DataGridView2.Rows(i).Cells(4).Value.ToString)
                w.WriteAttributeString("Clv_UnicaNet", Me.DataGridView2.Rows(i).Cells(5).Value.ToString)
                w.WriteAttributeString("Bonificacion", Me.DataGridView2.Rows(i).Cells(6).Value.ToString)
                w.WriteAttributeString("Meses_Cortesia", Me.DataGridView2.Rows(i).Cells(7).Value.ToString)
                w.WriteAttributeString("MesesAPagar", Me.DataGridView2.Rows(i).Cells(8).Value.ToString)
                w.WriteAttributeString("PeriodoPagadoIni", DateTime.Parse(Me.DataGridView2.Rows(i).Cells(9).Value.ToString).ToShortDateString) 'Las fechas deben de ir Parseadas :)
                w.WriteAttributeString("PeriodoPagadoFin", DateTime.Parse(DataGridView2.Rows(i).Cells(10).Value.ToString).ToShortDateString)
                w.WriteAttributeString("columnaDetalle", Me.DataGridView2.Rows(i).Cells(11).Value.ToString)
                w.WriteAttributeString("puntosAplicadosAnt", Me.DataGridView2.Rows(i).Cells(12).Value.ToString)
                w.WriteAttributeString("PuntosAplicadosPagoAdelantado", Me.DataGridView2.Rows(i).Cells(13).Value.ToString)
                w.WriteAttributeString("PuntosAplicadosOtros", Me.DataGridView2.Rows(i).Cells(14).Value.ToString)
                w.WriteAttributeString("TVAdic", Me.DataGridView2.Rows(i).Cells(15).Value.ToString)
                w.WriteAttributeString("DiasBonifica", Me.DataGridView2.Rows(i).Cells(16).Value.ToString)
                w.WriteAttributeString("MesesBonificar", Me.DataGridView2.Rows(i).Cells(17).Value.ToString)
                w.WriteAttributeString("ImporteBonifica", Me.DataGridView2.Rows(i).Cells(18).Value.ToString)
                w.WriteAttributeString("Ultimo_Mes", Me.DataGridView2.Rows(i).Cells(19).Value.ToString)
                w.WriteAttributeString("Ultimo_anio", Me.DataGridView2.Rows(i).Cells(20).Value.ToString)
                w.WriteAttributeString("Adelantado", Me.DataGridView2.Rows(i).Cells(21).Value.ToString)
                w.WriteAttributeString("Clv_Txt", Me.DataGridView2.Rows(i).Cells(22).Value.ToString)
                w.WriteAttributeString("Se_cob_como_Principal", Me.DataGridView2.Rows(i).Cells(23).Value.ToString)
                w.WriteAttributeString("DescuentoNet", Me.DataGridView2.Rows(i).Cells(24).Value.ToString)
                w.WriteAttributeString("Des_Otr_Ser_Misma_Categoria", Me.DataGridView2.Rows(i).Cells(25).Value.ToString)
                w.WriteAttributeString("Se_Cobra_Proporcional", Me.DataGridView2.Rows(i).Cells(26).Value.ToString)
                w.WriteAttributeString("COSTO", Me.DataGridView2.Rows(i).Cells(27).Value.ToString)
                w.WriteAttributeString("IEPS", Me.DataGridView2.Rows(i).Cells(28).Value.ToString)
                w.WriteAttributeString("IVA", Me.DataGridView2.Rows(i).Cells(29).Value.ToString)
                w.WriteAttributeString("COSTONETO", Me.DataGridView2.Rows(i).Cells(30).Value.ToString)
                w.WriteAttributeString("Saldado", Me.DataGridView2.Rows(i).Cells(31).Value.ToString)
                w.WriteAttributeString("Factura", Me.DataGridView2.Rows(i).Cells(32).Value.ToString)
                w.WriteAttributeString("Sesion", gloClv_Session)

                w.WriteEndElement()

            Next

            w.WriteEndElement()
            w.Close()

            XML_DetCargos = ""
            XML_DetCargos = sw.ToString()

        Catch ex As Exception

            MsgBox("Ocurri� un error al momento de Generar los PreDetalleCargos.", MsgBoxStyle.Exclamation, "ERROR AL GENERAR LOS PREDETCARGOS")
            MsgBox(ex.Message, MsgBoxStyle.Information)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Funci�n: convierteDetalleCargos_a_PreDetFacturas(), Contrato:" & GloContrato.ToString
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try
       

    End Sub


    Private Sub Button9_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        CargaEstadoDeCuenta(GloContrato, 0)
    End Sub

    Private Sub btnHabilitaFacNormal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHabilitaFacNormal.Click

        Me.btnHabilitaFacNormal.Visible = False

        Me.FacturacionNormal_EstadoDeCuentaNoGeneradoAun = True
        Me.BUSCACLIENTES(0)

        GloTelClv_Session = gloClv_Session

        Dim CON20 As New SqlConnection(MiConexion)
        CON20.Open()

        Me.DameDetalleTableAdapter.Connection = CON20
        Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)

        Me.SumaDetalleTableAdapter.Connection = CON20
        Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
        '
        RecalculaTotales_FacturacionNormal()

        CON20.Close()
        CON20.Dispose()


    End Sub

    'Funci�n para quitar un Cargo de la Lista ( Cargos Generados desde el Estado de Cuenta )
    Private Sub QuitaCargoDeLaLista(ByVal Fila As DataGridViewRow, ByVal SePuedeQuitar As Boolean)
        '
        Try
            If SePuedeQuitar = True Then

                Me.DataGridView2.Rows.Remove(Fila)
                SumaTotalDeCargos()

            Else
                MsgBox("Este Cargo no se puede quitar de la lista. Debe ser saldado.")
            End If
        Catch ex As Exception

            MsgBox("Ocurri� un error al tratar de quitar el Cargo de la lista.", MsgBoxStyle.Exclamation, "NO SE PUDO QUITAR EL CARGO DE LA LISTA")
            MsgBox(ex.Message.ToString)

        End Try

    End Sub

    'Funci�n para Contablizar el costo de todos los cargos del estado de cuenta mostrados en la lista
    Private Sub SumaTotalDeCargos()

        Dim cont As Integer = 0
        Dim TotalDeCargos As Double = 0

        Try
            For cont = 0 To Me.DataGridView2.RowCount - 1

                'La suma de todos los Cargos
                TotalDeCargos = TotalDeCargos + Me.DataGridView2.Rows(cont).Cells(30).Value

            Next
        Catch ex As Exception

            MsgBox("Ocurri� un error al momento de obtener el total de Cargos a Pagar", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message)

        End Try

        TotalDeCargos = TotalDeCargos - Glo_TotalAbonadoACuenta

        Me.LblImporte_Total.Text = TotalDeCargos
        Me.LabelTotal.Text = "$" & TotalDeCargos.ToString
        Me.LabelGRan_Total.Text = "$" & TotalDeCargos.ToString

    End Sub

    'Procedimiento para Verificar si el Cliente tiene o NO con Estado de Cuenta y si es que ya ha sido Generado.
    Private Sub DimeSiCobroPorEstadoDeCuenta()

        Dim conn As New SqlConnection(MiConexion)
        Dim BndEdoCta As Boolean = True
        Dim YaFueGenerado As Boolean = True

        Try

            Dim comando As New SqlClient.SqlCommand("DimeSiCobroPorEstadoDeCuenta", conn)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@Contrato", SqlDbType.BigInt)
            comando.Parameters.Add("@BndEdoCta", SqlDbType.Bit)
            comando.Parameters.Add("@YaFueGenerado", SqlDbType.Bit)

            comando.Parameters(0).Value = GloContrato 'Cliente
            comando.Parameters(1).Value = True ' Me dice si el Cliente se cobra por Estado de Cuenta o por Facturaci�n Normal
            comando.Parameters(2).Value = True ' Me dice si el Estado de Cuenta ya fue generado

            comando.Parameters(1).Direction = ParameterDirection.Output
            comando.Parameters(2).Direction = ParameterDirection.Output

            conn.Open()
            comando.ExecuteNonQuery()

            'Obtengo los Valores del Cliente para decidir que Interfaz se muestra, si FacNormal o Fac por Estado de Cuenta.
            BndEdoCta = comando.Parameters(1).Value
            YaFueGenerado = comando.Parameters(2).Value

            'Establecemos los Valores Globales
            GloBndEdoCta = BndEdoCta
            GloYaFueGenerado = YaFueGenerado

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

    End Sub

    Private Sub ReportesFacturasXsd(ByVal prmClvFactura As Long, ByVal prmClvFacturaIni As Long, ByVal prmClvFacturaFin As Long, ByVal prmFechaIni As Date, _
                                    ByVal prmFechaFin As Date, ByVal prmOp As Integer, ByVal RUTAREP As String)
        Dim CON As New SqlConnection(MiConexion)

        Dim CMD As New SqlCommand("ReportesFacturasXsd", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Clv_Factura", prmClvFactura)
        CMD.Parameters.AddWithValue("@Clv_Factura_Ini", prmClvFacturaIni)
        CMD.Parameters.AddWithValue("@Clv_Factura_Fin", prmClvFacturaFin)
        CMD.Parameters.AddWithValue("@Fecha_Ini", prmFechaIni)
        CMD.Parameters.AddWithValue("@Fecha_Fin", prmFechaFin)
        CMD.Parameters.AddWithValue("@op", prmOp)
        Dim DA As New SqlDataAdapter(CMD)

        Dim DS As New DataSet()

        DA.Fill(DS)

        DS.Tables(0).TableName = "FacturaFiscal"

        customersByCityReport.Load(RUTAREP)
        customersByCityReport.SetDataSource(DS)
    End Sub

    Private Sub btnRoboSe�al_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRoboSe�al.Click
        eGloContrato = Me.ContratoTextBox.Text
        FrmRoboDeSe�al.Show()
    End Sub
End Class
