﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAdelantarCargos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.btn_enviar_a_derecha = New System.Windows.Forms.Button()
        Me.btn_agregar_todos_a_izquierda = New System.Windows.Forms.Button()
        Me.btn_agregar_todos_a_derecha = New System.Windows.Forms.Button()
        Me.btn_enviar_a_Izquierda = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_total_a_pagar = New System.Windows.Forms.TextBox()
        Me.btn_Aceptar = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.pnlAplicaPorcentajeDescuento = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lbl_MsgAyudaDobleClic = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtNombreServicio = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAplicaPorcentajeDescuento.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(86, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(171, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Servicios del Cliente"
        '
        'ListBox1
        '
        Me.ListBox1.BackColor = System.Drawing.Color.White
        Me.ListBox1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 18
        Me.ListBox1.Location = New System.Drawing.Point(18, 66)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(358, 328)
        Me.ListBox1.TabIndex = 1
        '
        'ListBox2
        '
        Me.ListBox2.BackColor = System.Drawing.Color.White
        Me.ListBox2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox2.ForeColor = System.Drawing.Color.Maroon
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 18
        Me.ListBox2.Location = New System.Drawing.Point(576, 66)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(418, 328)
        Me.ListBox2.TabIndex = 2
        '
        'btn_enviar_a_derecha
        '
        Me.btn_enviar_a_derecha.BackColor = System.Drawing.Color.DarkRed
        Me.btn_enviar_a_derecha.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_enviar_a_derecha.ForeColor = System.Drawing.Color.White
        Me.btn_enviar_a_derecha.Location = New System.Drawing.Point(402, 66)
        Me.btn_enviar_a_derecha.Name = "btn_enviar_a_derecha"
        Me.btn_enviar_a_derecha.Size = New System.Drawing.Size(146, 55)
        Me.btn_enviar_a_derecha.TabIndex = 3
        Me.btn_enviar_a_derecha.Text = ">"
        Me.btn_enviar_a_derecha.UseVisualStyleBackColor = False
        '
        'btn_agregar_todos_a_izquierda
        '
        Me.btn_agregar_todos_a_izquierda.BackColor = System.Drawing.Color.DarkRed
        Me.btn_agregar_todos_a_izquierda.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_agregar_todos_a_izquierda.ForeColor = System.Drawing.Color.White
        Me.btn_agregar_todos_a_izquierda.Location = New System.Drawing.Point(402, 335)
        Me.btn_agregar_todos_a_izquierda.Name = "btn_agregar_todos_a_izquierda"
        Me.btn_agregar_todos_a_izquierda.Size = New System.Drawing.Size(146, 59)
        Me.btn_agregar_todos_a_izquierda.TabIndex = 4
        Me.btn_agregar_todos_a_izquierda.Text = "<<"
        Me.btn_agregar_todos_a_izquierda.UseVisualStyleBackColor = False
        '
        'btn_agregar_todos_a_derecha
        '
        Me.btn_agregar_todos_a_derecha.BackColor = System.Drawing.Color.DarkRed
        Me.btn_agregar_todos_a_derecha.Enabled = False
        Me.btn_agregar_todos_a_derecha.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_agregar_todos_a_derecha.ForeColor = System.Drawing.Color.White
        Me.btn_agregar_todos_a_derecha.Location = New System.Drawing.Point(402, 149)
        Me.btn_agregar_todos_a_derecha.Name = "btn_agregar_todos_a_derecha"
        Me.btn_agregar_todos_a_derecha.Size = New System.Drawing.Size(146, 58)
        Me.btn_agregar_todos_a_derecha.TabIndex = 5
        Me.btn_agregar_todos_a_derecha.Text = ">>"
        Me.btn_agregar_todos_a_derecha.UseVisualStyleBackColor = False
        '
        'btn_enviar_a_Izquierda
        '
        Me.btn_enviar_a_Izquierda.BackColor = System.Drawing.Color.DarkRed
        Me.btn_enviar_a_Izquierda.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_enviar_a_Izquierda.ForeColor = System.Drawing.Color.White
        Me.btn_enviar_a_Izquierda.Location = New System.Drawing.Point(402, 238)
        Me.btn_enviar_a_Izquierda.Name = "btn_enviar_a_Izquierda"
        Me.btn_enviar_a_Izquierda.Size = New System.Drawing.Size(146, 63)
        Me.btn_enviar_a_Izquierda.TabIndex = 6
        Me.btn_enviar_a_Izquierda.Text = "<"
        Me.btn_enviar_a_Izquierda.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(298, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(367, 29)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Selecciona los Servicios a Adelantar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(717, 41)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(177, 23)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Servicios a Adelantar"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 55)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(190, 18)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Número de Meses a Adelantar"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.White
        Me.TextBox1.Font = New System.Drawing.Font("Trebuchet MS", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.Maroon
        Me.TextBox1.Location = New System.Drawing.Point(285, 226)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(194, 38)
        Me.TextBox1.TabIndex = 10
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.White
        Me.TextBox2.Font = New System.Drawing.Font("Trebuchet MS", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.Color.Maroon
        Me.TextBox2.Location = New System.Drawing.Point(13, 226)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(183, 38)
        Me.TextBox2.TabIndex = 12
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(310, 205)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(143, 18)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Aplicará hasta el Mes:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(35, 205)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(142, 18)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Aplicará hasta el año:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(853, 683)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(141, 29)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Total a Pagar"
        Me.Label7.Visible = False
        '
        'txt_total_a_pagar
        '
        Me.txt_total_a_pagar.BackColor = System.Drawing.Color.White
        Me.txt_total_a_pagar.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_total_a_pagar.ForeColor = System.Drawing.Color.Maroon
        Me.txt_total_a_pagar.Location = New System.Drawing.Point(656, 683)
        Me.txt_total_a_pagar.Multiline = True
        Me.txt_total_a_pagar.Name = "txt_total_a_pagar"
        Me.txt_total_a_pagar.ReadOnly = True
        Me.txt_total_a_pagar.Size = New System.Drawing.Size(118, 26)
        Me.txt_total_a_pagar.TabIndex = 15
        Me.txt_total_a_pagar.Text = "$0.00"
        Me.txt_total_a_pagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txt_total_a_pagar.Visible = False
        '
        'btn_Aceptar
        '
        Me.btn_Aceptar.BackColor = System.Drawing.Color.DarkRed
        Me.btn_Aceptar.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Aceptar.ForeColor = System.Drawing.Color.White
        Me.btn_Aceptar.Location = New System.Drawing.Point(804, 654)
        Me.btn_Aceptar.Name = "btn_Aceptar"
        Me.btn_Aceptar.Size = New System.Drawing.Size(190, 75)
        Me.btn_Aceptar.TabIndex = 17
        Me.btn_Aceptar.Text = "Aceptar"
        Me.btn_Aceptar.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkRed
        Me.Button6.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.White
        Me.Button6.Location = New System.Drawing.Point(601, 654)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(182, 75)
        Me.Button6.TabIndex = 18
        Me.Button6.Text = "Salir"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.BackColor = System.Drawing.Color.White
        Me.NumericUpDown1.Font = New System.Drawing.Font("Verdana", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown1.ForeColor = System.Drawing.Color.Maroon
        Me.NumericUpDown1.Location = New System.Drawing.Point(55, 78)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumericUpDown1.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.ReadOnly = True
        Me.NumericUpDown1.Size = New System.Drawing.Size(98, 46)
        Me.NumericUpDown1.TabIndex = 19
        Me.NumericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown1.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'NumericUpDown2
        '
        Me.NumericUpDown2.BackColor = System.Drawing.Color.White
        Me.NumericUpDown2.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown2.ForeColor = System.Drawing.Color.Maroon
        Me.NumericUpDown2.Location = New System.Drawing.Point(730, 456)
        Me.NumericUpDown2.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumericUpDown2.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown2.Name = "NumericUpDown2"
        Me.NumericUpDown2.ReadOnly = True
        Me.NumericUpDown2.Size = New System.Drawing.Size(217, 47)
        Me.NumericUpDown2.TabIndex = 20
        Me.NumericUpDown2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDown2.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(782, 428)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(212, 20)
        Me.Label8.TabIndex = 21
        Me.Label8.Text = "Porcentaje de Descuento"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(953, 415)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(52, 42)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "%"
        '
        'pnlAplicaPorcentajeDescuento
        '
        Me.pnlAplicaPorcentajeDescuento.Controls.Add(Me.Label10)
        Me.pnlAplicaPorcentajeDescuento.Location = New System.Drawing.Point(600, 415)
        Me.pnlAplicaPorcentajeDescuento.Name = "pnlAplicaPorcentajeDescuento"
        Me.pnlAplicaPorcentajeDescuento.Size = New System.Drawing.Size(394, 201)
        Me.pnlAplicaPorcentajeDescuento.TabIndex = 23
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Calibri", 15.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(51, 91)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(310, 26)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "No aplica porcentaje de descuento"
        '
        'lbl_MsgAyudaDobleClic
        '
        Me.lbl_MsgAyudaDobleClic.AutoSize = True
        Me.lbl_MsgAyudaDobleClic.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_MsgAyudaDobleClic.Location = New System.Drawing.Point(14, 400)
        Me.lbl_MsgAyudaDobleClic.Name = "lbl_MsgAyudaDobleClic"
        Me.lbl_MsgAyudaDobleClic.Size = New System.Drawing.Size(428, 18)
        Me.lbl_MsgAyudaDobleClic.TabIndex = 551
        Me.lbl_MsgAyudaDobleClic.Text = "Coloca los servicios a los cuales quieres cobrar el Adeudo en la lista derecha"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtNombreServicio)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown1)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(18, 428)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(540, 301)
        Me.GroupBox1.TabIndex = 552
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Selecciona los Meses a Adelantar por Servicio"
        '
        'txtNombreServicio
        '
        Me.txtNombreServicio.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtNombreServicio.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombreServicio.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombreServicio.ForeColor = System.Drawing.Color.Maroon
        Me.txtNombreServicio.Location = New System.Drawing.Point(197, 83)
        Me.txtNombreServicio.Multiline = True
        Me.txtNombreServicio.Name = "txtNombreServicio"
        Me.txtNombreServicio.ReadOnly = True
        Me.txtNombreServicio.Size = New System.Drawing.Size(323, 30)
        Me.txtNombreServicio.TabIndex = 20
        Me.txtNombreServicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(310, 57)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(137, 18)
        Me.Label11.TabIndex = 21
        Me.Label11.Text = "Servicio a Adelantar:"
        '
        'FrmAdelantarCargos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 741)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lbl_MsgAyudaDobleClic)
        Me.Controls.Add(Me.pnlAplicaPorcentajeDescuento)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.NumericUpDown2)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.btn_Aceptar)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_total_a_pagar)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btn_enviar_a_Izquierda)
        Me.Controls.Add(Me.btn_agregar_todos_a_derecha)
        Me.Controls.Add(Me.btn_agregar_todos_a_izquierda)
        Me.Controls.Add(Me.btn_enviar_a_derecha)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.Name = "FrmAdelantarCargos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Adelantar Cargos"
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAplicaPorcentajeDescuento.ResumeLayout(False)
        Me.pnlAplicaPorcentajeDescuento.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents btn_enviar_a_derecha As System.Windows.Forms.Button
    Friend WithEvents btn_agregar_todos_a_izquierda As System.Windows.Forms.Button
    Friend WithEvents btn_agregar_todos_a_derecha As System.Windows.Forms.Button
    Friend WithEvents btn_enviar_a_Izquierda As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_total_a_pagar As System.Windows.Forms.TextBox
    Friend WithEvents btn_Aceptar As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents pnlAplicaPorcentajeDescuento As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lbl_MsgAyudaDobleClic As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtNombreServicio As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
End Class
