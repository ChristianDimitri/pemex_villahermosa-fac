Imports System.Data.SqlClient
Imports System.Text
Public Class FrmSelCiudad2

    Private fechaInicial As DateTime
    Private fechaFinal As DateTime

    Private Sub DameSession()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Dame_clv_session_Reportes", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()

            eClv_Session = CLng(parametro.Value)

        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ConSelCiudadesGloPro(ByVal Clv_Session As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC ConSelCiudadesGloPro ")
        strSQL.Append(CStr(Clv_Session) & ", ")
        strSQL.Append(CStr(op))

        Try
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            conexion.Close()


            Me.DataGridPro.DataSource = bindingSource


        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ConSelCiudadesGloTmp(ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC ConSelCiudadesGloTmp ")
        strSQL.Append(CStr(Clv_Session))

        Try
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            conexion.Close()


            Me.DataGridTmp.DataSource = bindingSource


        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub InsertarSelCiudadesGloTmp(ByVal Clv_Id As Integer, ByVal Clv_Session As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertarSelCiudadesGloTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro2 As New SqlParameter("@Clv_Id", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Id
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_Session
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Op", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Op
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub BorrarSelCiudadesGloTmp(ByVal Clv_Id As Integer, ByVal Clv_Session As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorrarSelCiudadesGloTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro2 As New SqlParameter("@Clv_Id", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Id
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_Session
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Op", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Op
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub Refrescar()
        ConSelCiudadesGloPro(eClv_Session, 1)
        ConSelCiudadesGloTmp(eClv_Session)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.DataGridPro.RowCount = 0 Then
            Exit Sub
        End If
        InsertarSelCiudadesGloTmp(CInt(Me.DataGridPro.SelectedCells(1).Value), eClv_Session, 0)
        Refrescar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.DataGridPro.RowCount = 0 Then
            Exit Sub
        End If
        InsertarSelCiudadesGloTmp(0, eClv_Session, 1)
        Refrescar()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridTmp.RowCount = 0 Then
            Exit Sub
        End If
        BorrarSelCiudadesGloTmp(CInt(Me.DataGridTmp.SelectedCells(1).Value), eClv_Session, 0)
        Refrescar()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridTmp.RowCount = 0 Then
            Exit Sub
        End If
        BorrarSelCiudadesGloTmp(0, eClv_Session, 1)
        Refrescar()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub

    Private Sub FrmSelCiudad2_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
        DameSession()
        ConSelCiudadesGloPro(eClv_Session, 0)
    End Sub

    Public Sub getDates(ByVal fechaI As DateTime, ByVal fechaF As DateTime)
        Me.fechaInicial = fechaI
        Me.fechaFinal = fechaF
    End Sub
End Class