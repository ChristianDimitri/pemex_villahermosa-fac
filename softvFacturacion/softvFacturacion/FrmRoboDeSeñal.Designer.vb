﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRoboDeSeñal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRoboDeSeñal))
        Me.ConRoboDeSeñalBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ConRoboDeSeñalBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbxCobrado = New System.Windows.Forms.CheckBox()
        Me.lblMonto = New System.Windows.Forms.Label()
        Me.txtMonto = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        DescripcionLabel = New System.Windows.Forms.Label()
        CType(Me.ConRoboDeSeñalBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConRoboDeSeñalBindingNavigator.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.Red
        DescripcionLabel.Location = New System.Drawing.Point(160, 6)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(187, 29)
        DescripcionLabel.TabIndex = 4
        DescripcionLabel.Text = "Robo de Señal"
        '
        'ConRoboDeSeñalBindingNavigator
        '
        Me.ConRoboDeSeñalBindingNavigator.AddNewItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.CountItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.ConRoboDeSeñalBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConRoboDeSeñalBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConRoboDeSeñalBindingNavigatorSaveItem})
        Me.ConRoboDeSeñalBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConRoboDeSeñalBindingNavigator.MoveFirstItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.MoveLastItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.MoveNextItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.MovePreviousItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.Name = "ConRoboDeSeñalBindingNavigator"
        Me.ConRoboDeSeñalBindingNavigator.PositionItem = Nothing
        Me.ConRoboDeSeñalBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConRoboDeSeñalBindingNavigator.Size = New System.Drawing.Size(512, 25)
        Me.ConRoboDeSeñalBindingNavigator.TabIndex = 1
        Me.ConRoboDeSeñalBindingNavigator.TabStop = True
        Me.ConRoboDeSeñalBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(88, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        Me.BindingNavigatorDeleteItem.Visible = False
        '
        'ConRoboDeSeñalBindingNavigatorSaveItem
        '
        Me.ConRoboDeSeñalBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConRoboDeSeñalBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConRoboDeSeñalBindingNavigatorSaveItem.Name = "ConRoboDeSeñalBindingNavigatorSaveItem"
        Me.ConRoboDeSeñalBindingNavigatorSaveItem.Size = New System.Drawing.Size(79, 22)
        Me.ConRoboDeSeñalBindingNavigatorSaveItem.Text = "&GRABAR"
        Me.ConRoboDeSeñalBindingNavigatorSaveItem.Visible = False
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(12, 34)
        Me.DescripcionTextBox.Multiline = True
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DescripcionTextBox.Size = New System.Drawing.Size(475, 170)
        Me.DescripcionTextBox.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.cbxCobrado)
        Me.Panel1.Controls.Add(Me.lblMonto)
        Me.Panel1.Controls.Add(Me.txtMonto)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.DescripcionTextBox)
        Me.Panel1.Controls.Add(DescripcionLabel)
        Me.Panel1.Location = New System.Drawing.Point(0, 24)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(515, 280)
        Me.Panel1.TabIndex = 6
        '
        'cbxCobrado
        '
        Me.cbxCobrado.AutoSize = True
        Me.cbxCobrado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCobrado.Location = New System.Drawing.Point(118, 236)
        Me.cbxCobrado.Name = "cbxCobrado"
        Me.cbxCobrado.Size = New System.Drawing.Size(80, 19)
        Me.cbxCobrado.TabIndex = 7
        Me.cbxCobrado.Text = "Cobrado"
        Me.cbxCobrado.UseVisualStyleBackColor = True
        Me.cbxCobrado.Visible = False
        '
        'lblMonto
        '
        Me.lblMonto.AutoSize = True
        Me.lblMonto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMonto.Location = New System.Drawing.Point(11, 220)
        Me.lblMonto.Name = "lblMonto"
        Me.lblMonto.Size = New System.Drawing.Size(47, 15)
        Me.lblMonto.TabIndex = 6
        Me.lblMonto.Text = "Monto"
        Me.lblMonto.Visible = False
        '
        'txtMonto
        '
        Me.txtMonto.Location = New System.Drawing.Point(12, 236)
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.Size = New System.Drawing.Size(100, 20)
        Me.txtMonto.TabIndex = 5
        Me.txtMonto.Visible = False
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(354, 225)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FrmRoboDeSeñal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(512, 302)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ConRoboDeSeñalBindingNavigator)
        Me.MaximizeBox = False
        Me.Name = "FrmRoboDeSeñal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Robo de Señal"
        Me.TopMost = True
        CType(Me.ConRoboDeSeñalBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConRoboDeSeñalBindingNavigator.ResumeLayout(False)
        Me.ConRoboDeSeñalBindingNavigator.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ConRoboDeSeñalBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConRoboDeSeñalBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblMonto As System.Windows.Forms.Label
    Friend WithEvents txtMonto As System.Windows.Forms.TextBox
    Friend WithEvents cbxCobrado As System.Windows.Forms.CheckBox
End Class
