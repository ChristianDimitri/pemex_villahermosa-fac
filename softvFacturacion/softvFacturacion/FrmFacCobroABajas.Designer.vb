﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFacCobroABajas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ESHOTELLabel1 As System.Windows.Forms.Label
        Dim SOLOINTERNETLabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.BUSCLIPORCONTRATOFACBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet()
        Me.CNOpanelServicios = New System.Windows.Forms.Panel()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.CNOpanelCliente = New System.Windows.Forms.Panel()
        Me.REDLabel25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.CLV_TIPOCLIENTELabel1 = New System.Windows.Forms.Label()
        Me.DESCRIPCIONLabel1 = New System.Windows.Forms.Label()
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.bnBuscarCliente = New System.Windows.Forms.Button()
        Me.NOMBRELabel1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Clv_Session = New System.Windows.Forms.TextBox()
        Me.dGDetalle = New System.Windows.Forms.DataGridView()
        Me.CNOpanelTotal = New System.Windows.Forms.Panel()
        Me.CMBLabel30 = New System.Windows.Forms.Label()
        Me.labelTotal = New System.Windows.Forms.Label()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.bnPagar = New System.Windows.Forms.Button()
        Me.labelError = New System.Windows.Forms.TextBox()
        Me.panelError = New System.Windows.Forms.Panel()
        Me.txtMsjError = New System.Windows.Forms.TextBox()
        Me.CLV_DETALLETextBox = New System.Windows.Forms.TextBox()
        Me.BUSCLIPORCONTRATO_FACTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.BUSCLIPORCONTRATO_FACTableAdapter()
        Me.BorraClv_SessionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorraClv_SessionTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.BorraClv_SessionTableAdapter()
        Me.DameSerDELCliFACTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameSerDELCliFACTableAdapter()
        Me.DameSerDELCliFACBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMETIPOSCLIENTESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMETIPOSCLIENTESTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DAMETIPOSCLIENTESTableAdapter()
        Me.BusFacFiscalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BusFacFiscalTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter()
        Me.NewsoftvDataSet2 = New softvFacturacion.NewsoftvDataSet2()
        Me.EricDataSet2 = New softvFacturacion.EricDataSet2()
        Me.DameGeneralMsjTicketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameGeneralMsjTicketsTableAdapter = New softvFacturacion.EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LblVersion = New System.Windows.Forms.Label()
        Me.LblFecha = New System.Windows.Forms.Label()
        Me.DameDatosGeneralesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LblUsuario = New System.Windows.Forms.Label()
        Me.DamedatosUsuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LblSucursal = New System.Windows.Forms.Label()
        Me.DAMENOMBRESUCURSALBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LblNomCaja = New System.Windows.Forms.Label()
        Me.LblSistema = New System.Windows.Forms.Label()
        Me.LblNomEmpresa = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DameDatosGeneralesTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameDatosGeneralesTableAdapter()
        Me.DamedatosUsuarioTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DamedatosUsuarioTableAdapter()
        Me.DAMENOMBRESUCURSALTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DAMENOMBRESUCURSALTableAdapter()
        Me.CMBlblSumaCargos = New System.Windows.Forms.Label()
        ESHOTELLabel1 = New System.Windows.Forms.Label()
        SOLOINTERNETLabel1 = New System.Windows.Forms.Label()
        CType(Me.BUSCLIPORCONTRATOFACBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CNOpanelServicios.SuspendLayout()
        Me.CNOpanelCliente.SuspendLayout()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dGDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CNOpanelTotal.SuspendLayout()
        Me.panelError.SuspendLayout()
        CType(Me.BorraClv_SessionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameSerDELCliFACBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMETIPOSCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BusFacFiscalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EricDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamedatosUsuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMENOMBRESUCURSALBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ESHOTELLabel1
        '
        ESHOTELLabel1.AutoSize = True
        ESHOTELLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        ESHOTELLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "ESHOTEL", True))
        ESHOTELLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ESHOTELLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        ESHOTELLabel1.Location = New System.Drawing.Point(332, 26)
        ESHOTELLabel1.Name = "ESHOTELLabel1"
        ESHOTELLabel1.Size = New System.Drawing.Size(67, 16)
        ESHOTELLabel1.TabIndex = 526
        ESHOTELLabel1.Text = "Es Hotel"
        ESHOTELLabel1.Visible = False
        '
        'BUSCLIPORCONTRATOFACBindingSource
        '
        Me.BUSCLIPORCONTRATOFACBindingSource.DataMember = "BUSCLIPORCONTRATO_FAC"
        Me.BUSCLIPORCONTRATOFACBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SOLOINTERNETLabel1
        '
        SOLOINTERNETLabel1.AutoSize = True
        SOLOINTERNETLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        SOLOINTERNETLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "SOLOINTERNET", True))
        SOLOINTERNETLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        SOLOINTERNETLabel1.Location = New System.Drawing.Point(223, 26)
        SOLOINTERNETLabel1.Name = "SOLOINTERNETLabel1"
        SOLOINTERNETLabel1.Size = New System.Drawing.Size(95, 16)
        SOLOINTERNETLabel1.TabIndex = 523
        SOLOINTERNETLabel1.Text = "Solo Internet"
        '
        'CNOpanelServicios
        '
        Me.CNOpanelServicios.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CNOpanelServicios.Controls.Add(Me.TreeView1)
        Me.CNOpanelServicios.Location = New System.Drawing.Point(636, 64)
        Me.CNOpanelServicios.Name = "CNOpanelServicios"
        Me.CNOpanelServicios.Size = New System.Drawing.Size(374, 219)
        Me.CNOpanelServicios.TabIndex = 196
        '
        'TreeView1
        '
        Me.TreeView1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.ForeColor = System.Drawing.Color.Black
        Me.TreeView1.Location = New System.Drawing.Point(0, -3)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(374, 216)
        Me.TreeView1.TabIndex = 19
        Me.TreeView1.TabStop = False
        '
        'CNOpanelCliente
        '
        Me.CNOpanelCliente.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CNOpanelCliente.Controls.Add(Me.REDLabel25)
        Me.CNOpanelCliente.Controls.Add(Me.Label24)
        Me.CNOpanelCliente.Controls.Add(Me.Label23)
        Me.CNOpanelCliente.Controls.Add(Me.ContratoTextBox)
        Me.CNOpanelCliente.Controls.Add(Me.Label20)
        Me.CNOpanelCliente.Controls.Add(Me.CLV_TIPOCLIENTELabel1)
        Me.CNOpanelCliente.Controls.Add(Me.DESCRIPCIONLabel1)
        Me.CNOpanelCliente.Controls.Add(ESHOTELLabel1)
        Me.CNOpanelCliente.Controls.Add(Me.Label2)
        Me.CNOpanelCliente.Controls.Add(Me.ESHOTELCheckBox)
        Me.CNOpanelCliente.Controls.Add(SOLOINTERNETLabel1)
        Me.CNOpanelCliente.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.CNOpanelCliente.Controls.Add(Me.Label1)
        Me.CNOpanelCliente.Controls.Add(Me.bnBuscarCliente)
        Me.CNOpanelCliente.Controls.Add(Me.NOMBRELabel1)
        Me.CNOpanelCliente.Controls.Add(Me.Label8)
        Me.CNOpanelCliente.Controls.Add(Me.CIUDADLabel1)
        Me.CNOpanelCliente.Controls.Add(Me.NUMEROLabel1)
        Me.CNOpanelCliente.Controls.Add(Me.Label11)
        Me.CNOpanelCliente.Controls.Add(Me.COLONIALabel1)
        Me.CNOpanelCliente.Controls.Add(Me.Label10)
        Me.CNOpanelCliente.Controls.Add(Me.CALLELabel1)
        Me.CNOpanelCliente.Controls.Add(Me.Label9)
        Me.CNOpanelCliente.Controls.Add(Me.Clv_Session)
        Me.CNOpanelCliente.Location = New System.Drawing.Point(8, 64)
        Me.CNOpanelCliente.Name = "CNOpanelCliente"
        Me.CNOpanelCliente.Size = New System.Drawing.Size(622, 219)
        Me.CNOpanelCliente.TabIndex = 197
        '
        'REDLabel25
        '
        Me.REDLabel25.AutoSize = True
        Me.REDLabel25.BackColor = System.Drawing.Color.Yellow
        Me.REDLabel25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.REDLabel25.ForeColor = System.Drawing.Color.Red
        Me.REDLabel25.Location = New System.Drawing.Point(299, 6)
        Me.REDLabel25.Name = "REDLabel25"
        Me.REDLabel25.Size = New System.Drawing.Size(90, 13)
        Me.REDLabel25.TabIndex = 535
        Me.REDLabel25.Text = "Datos Fiscales"
        Me.REDLabel25.Visible = False
        '
        'Label24
        '
        Me.Label24.AutoEllipsis = True
        Me.Label24.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label24.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "TELEFONO", True))
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(290, 99)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(114, 26)
        Me.Label24.TabIndex = 534
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label23.Location = New System.Drawing.Point(247, 98)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(46, 18)
        Me.Label23.TabIndex = 533
        Me.Label23.Text = "Tel. :"
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ContratoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoTextBox.Location = New System.Drawing.Point(75, 16)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(91, 24)
        Me.ContratoTextBox.TabIndex = 511
        Me.ContratoTextBox.Text = "0"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label20.Location = New System.Drawing.Point(9, 166)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(85, 15)
        Me.Label20.TabIndex = 532
        Me.Label20.Text = "Tipo Cobro :"
        '
        'CLV_TIPOCLIENTELabel1
        '
        Me.CLV_TIPOCLIENTELabel1.ForeColor = System.Drawing.Color.DarkOrange
        Me.CLV_TIPOCLIENTELabel1.Location = New System.Drawing.Point(62, 160)
        Me.CLV_TIPOCLIENTELabel1.Name = "CLV_TIPOCLIENTELabel1"
        Me.CLV_TIPOCLIENTELabel1.Size = New System.Drawing.Size(11, 26)
        Me.CLV_TIPOCLIENTELabel1.TabIndex = 531
        '
        'DESCRIPCIONLabel1
        '
        Me.DESCRIPCIONLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSetEdgar, "DAMETIPOSCLIENTES.DESCRIPCION", True))
        Me.DESCRIPCIONLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DESCRIPCIONLabel1.ForeColor = System.Drawing.Color.Black
        Me.DESCRIPCIONLabel1.Location = New System.Drawing.Point(100, 163)
        Me.DESCRIPCIONLabel1.Name = "DESCRIPCIONLabel1"
        Me.DESCRIPCIONLabel1.Size = New System.Drawing.Size(176, 22)
        Me.DESCRIPCIONLabel1.TabIndex = 530
        Me.DESCRIPCIONLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(13, 146)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 15)
        Me.Label2.TabIndex = 528
        Me.Label2.Text = "Ciudad :"
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESHOTELCheckBox.ForeColor = System.Drawing.Color.Black
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(319, 19)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(21, 27)
        Me.ESHOTELCheckBox.TabIndex = 527
        Me.ESHOTELCheckBox.TabStop = False
        Me.ESHOTELCheckBox.Visible = False
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SOLOINTERNETCheckBox.ForeColor = System.Drawing.Color.Black
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(212, 19)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(21, 27)
        Me.SOLOINTERNETCheckBox.TabIndex = 525
        Me.SOLOINTERNETCheckBox.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(3, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 15)
        Me.Label1.TabIndex = 513
        Me.Label1.Text = "Contrato :"
        '
        'bnBuscarCliente
        '
        Me.bnBuscarCliente.BackColor = System.Drawing.Color.DarkRed
        Me.bnBuscarCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnBuscarCliente.ForeColor = System.Drawing.Color.White
        Me.bnBuscarCliente.Location = New System.Drawing.Point(171, 13)
        Me.bnBuscarCliente.Name = "bnBuscarCliente"
        Me.bnBuscarCliente.Size = New System.Drawing.Size(30, 28)
        Me.bnBuscarCliente.TabIndex = 512
        Me.bnBuscarCliente.Text = "..."
        Me.bnBuscarCliente.UseVisualStyleBackColor = False
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.AutoEllipsis = True
        Me.NOMBRELabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.NOMBRELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "NOMBRE", True))
        Me.NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel1.ForeColor = System.Drawing.Color.Black
        Me.NOMBRELabel1.Location = New System.Drawing.Point(75, 43)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(329, 28)
        Me.NOMBRELabel1.TabIndex = 515
        Me.NOMBRELabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(9, 44)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(66, 15)
        Me.Label8.TabIndex = 520
        Me.Label8.Text = "Nombre :"
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.AutoEllipsis = True
        Me.CIUDADLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CIUDADLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "CIUDAD", True))
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.ForeColor = System.Drawing.Color.Black
        Me.CIUDADLabel1.Location = New System.Drawing.Point(75, 145)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(232, 26)
        Me.CIUDADLabel1.TabIndex = 519
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.AutoEllipsis = True
        Me.NUMEROLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.NUMEROLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "NUMERO", True))
        Me.NUMEROLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROLabel1.ForeColor = System.Drawing.Color.Black
        Me.NUMEROLabel1.Location = New System.Drawing.Point(75, 95)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(172, 26)
        Me.NUMEROLabel1.TabIndex = 518
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label11.Location = New System.Drawing.Point(9, 121)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(64, 15)
        Me.Label11.TabIndex = 524
        Me.Label11.Text = "Colonia :"
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.AutoEllipsis = True
        Me.COLONIALabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.COLONIALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "COLONIA", True))
        Me.COLONIALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIALabel1.ForeColor = System.Drawing.Color.Black
        Me.COLONIALabel1.Location = New System.Drawing.Point(75, 120)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(314, 26)
        Me.COLONIALabel1.TabIndex = 517
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label10.Location = New System.Drawing.Point(48, 94)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(27, 18)
        Me.Label10.TabIndex = 522
        Me.Label10.Text = "# :"
        '
        'CALLELabel1
        '
        Me.CALLELabel1.AutoEllipsis = True
        Me.CALLELabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CALLELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "CALLE", True))
        Me.CALLELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLELabel1.ForeColor = System.Drawing.Color.Black
        Me.CALLELabel1.Location = New System.Drawing.Point(75, 70)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(329, 26)
        Me.CALLELabel1.TabIndex = 516
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label9.Location = New System.Drawing.Point(-1, 71)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(76, 15)
        Me.Label9.TabIndex = 521
        Me.Label9.Text = "Dirección :"
        '
        'Clv_Session
        '
        Me.Clv_Session.BackColor = System.Drawing.Color.DarkOrange
        Me.Clv_Session.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_Session.Location = New System.Drawing.Point(88, 20)
        Me.Clv_Session.Name = "Clv_Session"
        Me.Clv_Session.Size = New System.Drawing.Size(78, 13)
        Me.Clv_Session.TabIndex = 529
        Me.Clv_Session.TabStop = False
        Me.Clv_Session.Text = "0"
        '
        'dGDetalle
        '
        Me.dGDetalle.AllowUserToAddRows = False
        Me.dGDetalle.AllowUserToDeleteRows = False
        Me.dGDetalle.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dGDetalle.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dGDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dGDetalle.DefaultCellStyle = DataGridViewCellStyle4
        Me.dGDetalle.Location = New System.Drawing.Point(8, 289)
        Me.dGDetalle.Name = "dGDetalle"
        Me.dGDetalle.ReadOnly = True
        Me.dGDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dGDetalle.Size = New System.Drawing.Size(622, 254)
        Me.dGDetalle.TabIndex = 198
        '
        'CNOpanelTotal
        '
        Me.CNOpanelTotal.BackColor = System.Drawing.Color.LightGray
        Me.CNOpanelTotal.Controls.Add(Me.CMBLabel30)
        Me.CNOpanelTotal.Controls.Add(Me.labelTotal)
        Me.CNOpanelTotal.Location = New System.Drawing.Point(650, 496)
        Me.CNOpanelTotal.Name = "CNOpanelTotal"
        Me.CNOpanelTotal.Size = New System.Drawing.Size(381, 47)
        Me.CNOpanelTotal.TabIndex = 524
        '
        'CMBLabel30
        '
        Me.CMBLabel30.AutoSize = True
        Me.CMBLabel30.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel30.Location = New System.Drawing.Point(3, 17)
        Me.CMBLabel30.Name = "CMBLabel30"
        Me.CMBLabel30.Size = New System.Drawing.Size(117, 22)
        Me.CMBLabel30.TabIndex = 522
        Me.CMBLabel30.Text = "Total a Pagar:"
        Me.CMBLabel30.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'labelTotal
        '
        Me.labelTotal.Font = New System.Drawing.Font("Trebuchet MS", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelTotal.ForeColor = System.Drawing.Color.Maroon
        Me.labelTotal.Location = New System.Drawing.Point(124, 7)
        Me.labelTotal.Name = "labelTotal"
        Me.labelTotal.Size = New System.Drawing.Size(254, 32)
        Me.labelTotal.TabIndex = 523
        Me.labelTotal.Text = "$0.00"
        Me.labelTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'bnSalir
        '
        Me.bnSalir.BackColor = System.Drawing.Color.DarkRed
        Me.bnSalir.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.ForeColor = System.Drawing.Color.White
        Me.bnSalir.Location = New System.Drawing.Point(890, 680)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(139, 53)
        Me.bnSalir.TabIndex = 526
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = False
        '
        'bnPagar
        '
        Me.bnPagar.BackColor = System.Drawing.Color.DarkRed
        Me.bnPagar.Enabled = False
        Me.bnPagar.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnPagar.ForeColor = System.Drawing.Color.White
        Me.bnPagar.Location = New System.Drawing.Point(724, 680)
        Me.bnPagar.Name = "bnPagar"
        Me.bnPagar.Size = New System.Drawing.Size(139, 52)
        Me.bnPagar.TabIndex = 525
        Me.bnPagar.Text = "&PAGAR"
        Me.bnPagar.UseVisualStyleBackColor = False
        '
        'labelError
        '
        Me.labelError.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.labelError.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.labelError.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelError.ForeColor = System.Drawing.Color.Red
        Me.labelError.Location = New System.Drawing.Point(3, 3)
        Me.labelError.Multiline = True
        Me.labelError.Name = "labelError"
        Me.labelError.ReadOnly = True
        Me.labelError.Size = New System.Drawing.Size(374, 112)
        Me.labelError.TabIndex = 528
        Me.labelError.TabStop = False
        '
        'panelError
        '
        Me.panelError.BackColor = System.Drawing.Color.WhiteSmoke
        Me.panelError.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panelError.Controls.Add(Me.labelError)
        Me.panelError.Controls.Add(Me.txtMsjError)
        Me.panelError.Controls.Add(Me.CLV_DETALLETextBox)
        Me.panelError.Location = New System.Drawing.Point(649, 554)
        Me.panelError.Name = "panelError"
        Me.panelError.Size = New System.Drawing.Size(381, 120)
        Me.panelError.TabIndex = 527
        Me.panelError.Visible = False
        '
        'txtMsjError
        '
        Me.txtMsjError.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtMsjError.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMsjError.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMsjError.ForeColor = System.Drawing.Color.Red
        Me.txtMsjError.Location = New System.Drawing.Point(0, 95)
        Me.txtMsjError.Multiline = True
        Me.txtMsjError.Name = "txtMsjError"
        Me.txtMsjError.ReadOnly = True
        Me.txtMsjError.Size = New System.Drawing.Size(359, 54)
        Me.txtMsjError.TabIndex = 538
        '
        'CLV_DETALLETextBox
        '
        Me.CLV_DETALLETextBox.Enabled = False
        Me.CLV_DETALLETextBox.Location = New System.Drawing.Point(343, 9)
        Me.CLV_DETALLETextBox.Name = "CLV_DETALLETextBox"
        Me.CLV_DETALLETextBox.Size = New System.Drawing.Size(100, 20)
        Me.CLV_DETALLETextBox.TabIndex = 509
        Me.CLV_DETALLETextBox.Visible = False
        '
        'BUSCLIPORCONTRATO_FACTableAdapter
        '
        Me.BUSCLIPORCONTRATO_FACTableAdapter.ClearBeforeFill = True
        '
        'BorraClv_SessionBindingSource
        '
        Me.BorraClv_SessionBindingSource.DataMember = "BorraClv_Session"
        Me.BorraClv_SessionBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'BorraClv_SessionTableAdapter
        '
        Me.BorraClv_SessionTableAdapter.ClearBeforeFill = True
        '
        'DameSerDELCliFACTableAdapter
        '
        Me.DameSerDELCliFACTableAdapter.ClearBeforeFill = True
        '
        'DameSerDELCliFACBindingSource
        '
        Me.DameSerDELCliFACBindingSource.DataMember = "DameSerDELCliFAC"
        Me.DameSerDELCliFACBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'DAMETIPOSCLIENTESBindingSource
        '
        Me.DAMETIPOSCLIENTESBindingSource.DataMember = "DAMETIPOSCLIENTES"
        '
        'DAMETIPOSCLIENTESTableAdapter
        '
        Me.DAMETIPOSCLIENTESTableAdapter.ClearBeforeFill = True
        '
        'BusFacFiscalBindingSource
        '
        Me.BusFacFiscalBindingSource.DataMember = "BusFacFiscal"
        '
        'BusFacFiscalTableAdapter
        '
        Me.BusFacFiscalTableAdapter.ClearBeforeFill = True
        '
        'NewsoftvDataSet2
        '
        Me.NewsoftvDataSet2.DataSetName = "NewsoftvDataSet2"
        Me.NewsoftvDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'EricDataSet2
        '
        Me.EricDataSet2.DataSetName = "EricDataSet2"
        Me.EricDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameGeneralMsjTicketsBindingSource
        '
        Me.DameGeneralMsjTicketsBindingSource.DataMember = "DameGeneralMsjTickets"
        Me.DameGeneralMsjTicketsBindingSource.DataSource = Me.EricDataSet2
        '
        'DameGeneralMsjTicketsTableAdapter
        '
        Me.DameGeneralMsjTicketsTableAdapter.ClearBeforeFill = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.LblVersion)
        Me.Panel1.Controls.Add(Me.LblFecha)
        Me.Panel1.Controls.Add(Me.LblUsuario)
        Me.Panel1.Controls.Add(Me.LblSucursal)
        Me.Panel1.Controls.Add(Me.LblNomCaja)
        Me.Panel1.Controls.Add(Me.LblSistema)
        Me.Panel1.Controls.Add(Me.LblNomEmpresa)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Location = New System.Drawing.Point(12, 9)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(998, 49)
        Me.Panel1.TabIndex = 529
        '
        'LblVersion
        '
        Me.LblVersion.AutoSize = True
        Me.LblVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVersion.Location = New System.Drawing.Point(879, 28)
        Me.LblVersion.Name = "LblVersion"
        Me.LblVersion.Size = New System.Drawing.Size(52, 13)
        Me.LblVersion.TabIndex = 187
        Me.LblVersion.Text = "Label14"
        '
        'LblFecha
        '
        Me.LblFecha.AutoSize = True
        Me.LblFecha.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Fecha", True))
        Me.LblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFecha.Location = New System.Drawing.Point(879, 7)
        Me.LblFecha.Name = "LblFecha"
        Me.LblFecha.Size = New System.Drawing.Size(52, 13)
        Me.LblFecha.TabIndex = 186
        Me.LblFecha.Text = "Label14"
        '
        'DameDatosGeneralesBindingSource
        '
        Me.DameDatosGeneralesBindingSource.DataMember = "DameDatosGenerales"
        Me.DameDatosGeneralesBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'LblUsuario
        '
        Me.LblUsuario.AutoSize = True
        Me.LblUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DamedatosUsuarioBindingSource, "Nombre", True))
        Me.LblUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUsuario.Location = New System.Drawing.Point(579, 7)
        Me.LblUsuario.Name = "LblUsuario"
        Me.LblUsuario.Size = New System.Drawing.Size(52, 13)
        Me.LblUsuario.TabIndex = 185
        Me.LblUsuario.Text = "Label14"
        '
        'DamedatosUsuarioBindingSource
        '
        Me.DamedatosUsuarioBindingSource.DataMember = "DamedatosUsuario"
        Me.DamedatosUsuarioBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'LblSucursal
        '
        Me.LblSucursal.AutoSize = True
        Me.LblSucursal.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMENOMBRESUCURSALBindingSource, "Nombre", True))
        Me.LblSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSucursal.Location = New System.Drawing.Point(458, 28)
        Me.LblSucursal.Name = "LblSucursal"
        Me.LblSucursal.Size = New System.Drawing.Size(52, 13)
        Me.LblSucursal.TabIndex = 184
        Me.LblSucursal.Text = "Label14"
        '
        'DAMENOMBRESUCURSALBindingSource
        '
        Me.DAMENOMBRESUCURSALBindingSource.DataMember = "DAMENOMBRESUCURSAL"
        Me.DAMENOMBRESUCURSALBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'LblNomCaja
        '
        Me.LblNomCaja.AutoSize = True
        Me.LblNomCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNomCaja.Location = New System.Drawing.Point(383, 7)
        Me.LblNomCaja.Name = "LblNomCaja"
        Me.LblNomCaja.Size = New System.Drawing.Size(52, 13)
        Me.LblNomCaja.TabIndex = 183
        Me.LblNomCaja.Text = "Label14"
        '
        'LblSistema
        '
        Me.LblSistema.AutoSize = True
        Me.LblSistema.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Ciudad", True))
        Me.LblSistema.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSistema.Location = New System.Drawing.Point(78, 28)
        Me.LblSistema.Name = "LblSistema"
        Me.LblSistema.Size = New System.Drawing.Size(52, 13)
        Me.LblSistema.TabIndex = 182
        Me.LblSistema.Text = "Label14"
        '
        'LblNomEmpresa
        '
        Me.LblNomEmpresa.AutoSize = True
        Me.LblNomEmpresa.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Nombre", True))
        Me.LblNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNomEmpresa.Location = New System.Drawing.Point(78, 7)
        Me.LblNomEmpresa.Name = "LblNomEmpresa"
        Me.LblNomEmpresa.Size = New System.Drawing.Size(52, 13)
        Me.LblNomEmpresa.TabIndex = 181
        Me.LblNomEmpresa.Text = "Label14"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(498, 5)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(75, 15)
        Me.Label13.TabIndex = 180
        Me.Label13.Text = "Cajero(a) :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(806, 26)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(67, 15)
        Me.Label12.TabIndex = 179
        Me.Label12.Text = "Versión : "
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(815, 5)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 15)
        Me.Label7.TabIndex = 178
        Me.Label7.Text = "Fecha : "
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(383, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 15)
        Me.Label6.TabIndex = 177
        Me.Label6.Text = "Sucursal : "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(336, 5)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 15)
        Me.Label5.TabIndex = 176
        Me.Label5.Text = "Caja : "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(9, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 15)
        Me.Label4.TabIndex = 175
        Me.Label4.Text = "Sistema : "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(4, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 15)
        Me.Label3.TabIndex = 174
        Me.Label3.Text = "Empresa : "
        '
        'DameDatosGeneralesTableAdapter
        '
        Me.DameDatosGeneralesTableAdapter.ClearBeforeFill = True
        '
        'DamedatosUsuarioTableAdapter
        '
        Me.DamedatosUsuarioTableAdapter.ClearBeforeFill = True
        '
        'DAMENOMBRESUCURSALTableAdapter
        '
        Me.DAMENOMBRESUCURSALTableAdapter.ClearBeforeFill = True
        '
        'CMBlblSumaCargos
        '
        Me.CMBlblSumaCargos.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblSumaCargos.ForeColor = System.Drawing.Color.Maroon
        Me.CMBlblSumaCargos.Location = New System.Drawing.Point(649, 289)
        Me.CMBlblSumaCargos.Name = "CMBlblSumaCargos"
        Me.CMBlblSumaCargos.Size = New System.Drawing.Size(382, 24)
        Me.CMBlblSumaCargos.TabIndex = 532
        Me.CMBlblSumaCargos.Text = "TOTAL DE CARGOS A PAGAR"
        Me.CMBlblSumaCargos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FrmFacCobroABajas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1041, 745)
        Me.Controls.Add(Me.CMBlblSumaCargos)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.panelError)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnPagar)
        Me.Controls.Add(Me.CNOpanelTotal)
        Me.Controls.Add(Me.dGDetalle)
        Me.Controls.Add(Me.CNOpanelCliente)
        Me.Controls.Add(Me.CNOpanelServicios)
        Me.Name = "FrmFacCobroABajas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cobro A Bajas"
        CType(Me.BUSCLIPORCONTRATOFACBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CNOpanelServicios.ResumeLayout(False)
        Me.CNOpanelCliente.ResumeLayout(False)
        Me.CNOpanelCliente.PerformLayout()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dGDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CNOpanelTotal.ResumeLayout(False)
        Me.CNOpanelTotal.PerformLayout()
        Me.panelError.ResumeLayout(False)
        Me.panelError.PerformLayout()
        CType(Me.BorraClv_SessionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameSerDELCliFACBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMETIPOSCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BusFacFiscalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EricDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamedatosUsuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMENOMBRESUCURSALBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CNOpanelServicios As System.Windows.Forms.Panel
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents CNOpanelCliente As System.Windows.Forms.Panel
    Friend WithEvents REDLabel25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents CLV_TIPOCLIENTELabel1 As System.Windows.Forms.Label
    Friend WithEvents DESCRIPCIONLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents bnBuscarCliente As System.Windows.Forms.Button
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Clv_Session As System.Windows.Forms.TextBox
    Friend WithEvents dGDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents CNOpanelTotal As System.Windows.Forms.Panel
    Friend WithEvents CMBLabel30 As System.Windows.Forms.Label
    Friend WithEvents labelTotal As System.Windows.Forms.Label
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents bnPagar As System.Windows.Forms.Button
    Friend WithEvents labelError As System.Windows.Forms.TextBox
    Friend WithEvents panelError As System.Windows.Forms.Panel
    Friend WithEvents CLV_DETALLETextBox As System.Windows.Forms.TextBox
    Friend WithEvents txtMsjError As System.Windows.Forms.TextBox
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents BUSCLIPORCONTRATO_FACTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.BUSCLIPORCONTRATO_FACTableAdapter
    Friend WithEvents BorraClv_SessionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorraClv_SessionTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.BorraClv_SessionTableAdapter
    Friend WithEvents DameSerDELCliFACTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameSerDELCliFACTableAdapter
    Friend WithEvents DameSerDELCliFACBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCLIPORCONTRATOFACBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMETIPOSCLIENTESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMETIPOSCLIENTESTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DAMETIPOSCLIENTESTableAdapter
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents BusFacFiscalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BusFacFiscalTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
    Friend WithEvents NewsoftvDataSet2 As softvFacturacion.NewsoftvDataSet2
    Friend WithEvents EricDataSet2 As softvFacturacion.EricDataSet2
    Friend WithEvents DameGeneralMsjTicketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameGeneralMsjTicketsTableAdapter As softvFacturacion.EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LblVersion As System.Windows.Forms.Label
    Friend WithEvents LblFecha As System.Windows.Forms.Label
    Friend WithEvents LblUsuario As System.Windows.Forms.Label
    Friend WithEvents LblSucursal As System.Windows.Forms.Label
    Friend WithEvents LblNomCaja As System.Windows.Forms.Label
    Friend WithEvents LblSistema As System.Windows.Forms.Label
    Friend WithEvents LblNomEmpresa As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DameDatosGeneralesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameDatosGeneralesTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameDatosGeneralesTableAdapter
    Friend WithEvents DamedatosUsuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DamedatosUsuarioTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DamedatosUsuarioTableAdapter
    Friend WithEvents DAMENOMBRESUCURSALBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMENOMBRESUCURSALTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DAMENOMBRESUCURSALTableAdapter
    Friend WithEvents CMBlblSumaCargos As System.Windows.Forms.Label
End Class
