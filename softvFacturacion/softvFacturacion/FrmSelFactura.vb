Imports System.Data.SqlClient
Public Class FrmSelFactura

    Private Sub FrmSelFactura_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Me.GroupBox1.Text = " Facturas Generadas por el CLiente " + Glocontratosel
        BUSCA(Glocontratosel)
    End Sub

    Private Sub BUSCA(ByVal CONTRATO As Integer)
        Try
            Dim CON As New SqlClient.SqlConnection(MiConexion)
            Me.DAME_FACTURASDECLIENTETableAdapter.Connection = CON
            Me.DAME_FACTURASDECLIENTETableAdapter.Fill(Me.DataSetLydia.DAME_FACTURASDECLIENTE, CONTRATO, 0)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        LIFACTURA = Me.ComboBox1.SelectedValue
    End Sub
End Class