﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Collections
Public Class FrmCobrarAdeudoPorServicio

    'Declaraciones básicas
    Public diccionario As Dictionary(Of String, ServicioCobroAdeudo) = New Dictionary(Of String, ServicioCobroAdeudo)
    Dim listaI As New List(Of ServicioCobroAdeudo)
    Dim listaD As New List(Of ServicioCobroAdeudo)
    Dim listaAuxiliar As New List(Of ServicioCobroAdeudo)
    Dim listaOriginal As New List(Of ServicioCobroAdeudo)
    Dim servicios As ServicioCobroAdeudo = Nothing
    Dim arreglo As New ArrayList


    Private Sub FrmCobrarAdeudoPorServicio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Dame el diseño de la Interfaz
        colorea(Me)
        Me.Label1.ForeColor = Color.Black
        Me.Label2.ForeColor = Color.Black
        Me.Label3.ForeColor = Color.Black
        Me.Label4.ForeColor = Color.Black
        'Me.Label5.ForeColor = Color.Black
        'Me.Label6.ForeColor = Color.Black
        Me.Label7.ForeColor = Color.Black
        'Me.Label8.ForeColor = Color.Black
        'Me.Label9.ForeColor = Color.Black
        Me.lbl_MsgAyudaDobleClic.ForeColor = Color.Black

        'Cargamos los servicios del Cliente (los cuales va a poder adelantar)
        Dame_servicios_Cliente(GloContrato)

    End Sub

    Private Sub Dame_servicios_Cliente(ByVal contrato As Integer)

        Dim I As Integer = 0
        Dim Fila2 As DataRow
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        Dim oDataSet As New DataSet

        Try

            'Procedimiento que obtiene los servicios que tiene asignados el cliente
            Dim cmd As New SqlCommand("usp_Dame_servicios_Cliente", conexion)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Contrato", contrato) ' Le pasamos el Contrato


            'Declaramos la lista de la clase 
            Dim listaCargos As New List(Of ServicioCobroAdeudo)

            Dim dr As SqlDataReader
            conexion.Open()
            dr = cmd.ExecuteReader()
            'diccionario.Clear()
            listaI.Clear()

            Using dr

                While dr.Read
                    If diccionario.ContainsKey(dr("ident").ToString().Trim()) Then
                        If Not listaD.Contains(diccionario(dr("ident").ToString().Trim())) Then
                            listaI.Add(diccionario(dr("ident").ToString().Trim()))
                        End If

                    Else

                        servicios = New ServicioCobroAdeudo()

                        servicios.ident = dr("ident").ToString().Trim()
                        servicios.clv_Servicio = dr("clv_Servicio").ToString().Trim()
                        servicios.concepto = dr("concepto").ToString().Trim()
                        servicios.precio = dr("precio").ToString.Trim()
                        servicios.clv_llave = dr("clv_llave").ToString.Trim()
                        servicios.clv_unicanet = dr("clv_unicanet").ToString.Trim()
                        servicios.clv_txt = dr("clv_txt").ToString.Trim()
                        servicios.ult_mes = dr("ult_mes").ToString.Trim()
                        servicios.ult_anio = dr("ult_anio").ToString.Trim()
                        servicios.clv_TipSer = dr("clv_TipSer").ToString.Trim()
                        servicios.status_serv = dr("status_serv").ToString.Trim()
                        servicios.esServicioPrincipal = dr("esPrincipal").ToString.Trim()

                        diccionario.Add(servicios.ident, servicios)

                        listaI.Add(servicios)

                    End If
                End While
            End Using



            Me.ListBox1.DataSource = Nothing
            Me.ListBox1.DataSource = listaI
            Me.ListBox1.DisplayMember = "DescripcionCompleta"
            Me.ListBox1.ValueMember = "ident"

            For Each servicios In listaI
                listaOriginal.Add(servicios)
            Next

        Catch ex As Exception

            MsgBox("Ocurrió un error. No se han podido obtener los servicios del cliente, no se podrán realizar el Cobro de Adeudo.", MsgBoxStyle.Critical)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "usp_Dame_servicios_Cliente"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub btn_enviar_a_derecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_enviar_a_derecha.Click
        If Me.ListBox1.SelectedValue <> Nothing And esUnServicio(Me.ListBox1.SelectedValue) = True Then

            If bloqueaTelefoniaUnica(Me.ListBox1.SelectedValue) = True Then
                listaD.Add(diccionario(ListBox1.SelectedValue.ToString()))
                listaI.Remove(diccionario(ListBox1.SelectedValue.ToString()))

                refreshLists()
            Else
                MsgBox("No se puede dejar el servicio de Telefonía solo, ya que para conservarlo se debe de tener asignado por lo menos otro servicio principal.", MsgBoxStyle.Exclamation, "Aviso Importante")
            End If

        End If
    End Sub

    Private Sub btn_agregar_todos_a_derecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_agregar_todos_a_derecha.Click
        If ListBox1.Items.Count > 0 Then

            For Each servicios In listaI
                If servicios.concepto <> "___________    SERVICIOS DIGITALES _____________" And servicios.concepto <> "___________   SERVICIOS DE INTERNET ____________" And servicios.concepto <> "___________ SERVICIOS DE T.V. ANÁLOGA __________" And servicios.concepto <> "_____________ SERVICIOS DE TELEFONIA ___________" Then
                    listaD.Add(servicios)
                End If
            Next
            listaI.Clear()
            refreshLists()

        End If
    End Sub

    Private Sub btn_enviar_a_Izquierda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_enviar_a_Izquierda.Click
        If Me.ListBox2.SelectedValue <> Nothing Then


            If RegresaTelefoniaUnica(Me.ListBox2.SelectedValue) = True Then
                MsgBox("No se puede dejar el servicio de Telefonía solo, ya que para conservarlo se debe de tener asignado por lo menos otro servicio principal.", MsgBoxStyle.Exclamation, "Aviso Importante")
            Else

                listaI.Add(diccionario(ListBox2.SelectedValue.ToString()))
                listaD.Remove(diccionario(ListBox2.SelectedValue.ToString()))

                arreglo.Clear()

                For Each servicios In listaI
                    arreglo.Add(servicios.ident - 1)
                    listaAuxiliar.Add(servicios)
                    'listaAuxiliar.
                Next

                'Ordenamos el Arreglo
                'arreglo.Sort()
                'Array.Sort(arreglo.ToArray)
                'Borramos la lista que NO esta Ordenada
                listaI.Clear()

                'Barremos los elementos del arreglo para regresarlos a la lista Izquierda ya Ordenados 
                Dim i As Integer
                Dim j As Integer
                j = 0

                For i = 1 To arreglo.Count
                    Me.Minimo()
                Next

                listaAuxiliar.Clear()
                refreshLists()

            End If

        End If
    End Sub

    Private Sub btn_agregar_todos_a_izquierda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_agregar_todos_a_izquierda.Click
        If ListBox2.Items.Count > 0 Then
            'For Each servicios In listaD
            '    listaI.Add(servicios)
            'Next
            listaI.Clear()
            For Each servicios In listaOriginal
                listaI.Add(servicios)
            Next
            listaD.Clear()
            refreshLists()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Dispose()
        Me.Close()
    End Sub
    Public Sub refreshLists()

        ListBox2.DataSource = Nothing
        ListBox2.DataSource = listaD
        ListBox2.DisplayMember = "DescripcionCompleta"
        ListBox2.ValueMember = "ident"

        ListBox1.DataSource = Nothing
        ListBox1.DataSource = listaI
        ListBox1.DisplayMember = "DescripcionCompleta"
        ListBox1.ValueMember = "ident"
        ListBox1.Refresh()



    End Sub

    Private Sub btn_Aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Aceptar.Click
        If ListBox2.Items.Count > 0 Then

            generaXML()

        Else
            MsgBox("No hay servicios seleccionados para cobrar su Adeudo. Selecciona mínimo un servicio o da clic en el botón SALIR para no generar ningún Cobro de Adeudo.", MsgBoxStyle.Information, " :: No se ha seleccionado ningún servicio :: ")
        End If

    End Sub
    'Generamos el XML en base a los datos
    Private Sub generaXML()

        Try

            Dim sw As New StringWriter()
            Dim w As New XmlTextWriter(sw)
            Dim i As Integer
            Dim serv As ServicioCobroAdeudo

            w.Formatting = Formatting.Indented

            w.WriteStartElement("ServicioCobroAdeudo")
            w.WriteAttributeString("servicio", "Adeudos")

            For Each serv In listaD

                w.WriteStartElement("servicio")
                w.WriteAttributeString("clv_Servicio", serv.clv_Servicio)
                w.WriteAttributeString("concepto", serv.concepto)
                w.WriteAttributeString("precio", serv.precio)
                w.WriteAttributeString("clv_llave", serv.clv_llave)
                w.WriteAttributeString("clv_unicanet", serv.clv_unicanet)
                w.WriteAttributeString("clv_txt", serv.clv_txt)
                w.WriteAttributeString("ult_mes", serv.ult_mes)
                w.WriteAttributeString("ult_anio", serv.ult_anio)
                w.WriteAttributeString("clv_TipSer", serv.clv_TipSer)
                w.WriteAttributeString("status_serv", serv.status_serv)
                w.WriteEndElement()


            Next

            w.WriteEndElement()
            w.Close()
            sw.ToString()

            Guarda_Servicios_CobroAdeudo(sw.ToString())

        Catch ex As Exception
            MsgBox("No se ha podido establecer los servicios como Adeudos", MsgBoxStyle.Critical, "Aviso Importante")
            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Private Sub generaXML() - Error al tratar de Convertir los servicios en XML"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)
        End Try
    End Sub

    'Método para Guardar los servicios a adelantar y guardarlos en PreDetFacturas
    Private Sub Guarda_Servicios_CobroAdeudo(ByVal x As String)

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("CobraAdeudoTel", con)
        com.CommandType = CommandType.StoredProcedure

        'Le pasamos el conjunto de Serivicio a Adelantar (Clave del servisio, descripción, costo) en XML
        Dim par1 As New SqlParameter("@xml", SqlDbType.Xml)
        par1.Direction = ParameterDirection.Input
        par1.Value = x
        com.Parameters.Add(par1)


        'Clave de Sesión
        Dim prmClv_Session As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        prmClv_Session.Direction = ParameterDirection.Input
        prmClv_Session.Value = GloTelClv_Session 'gloClv_Session
        com.Parameters.Add(prmClv_Session)

        ''Contrato (para buscar contrataciones)
        'Dim prmContrato As New SqlParameter("@Contrato", SqlDbType.BigInt)
        'prmContrato.Direction = ParameterDirection.Input
        'prmContrato.Value = GloContrato
        'com.Parameters.Add(prmContrato)

        'Id del Sistema
        Dim prmIdSistema As New SqlParameter("@IdSistema", SqlDbType.VarChar, 10)
        prmIdSistema.Direction = ParameterDirection.Input
        prmIdSistema.Value = IdSistema
        com.Parameters.Add(prmIdSistema)

        'Error
        Dim prmError As New SqlParameter("@Error", SqlDbType.SmallInt)
        prmError.Direction = ParameterDirection.Output
        prmError.Value = 0
        com.Parameters.Add(prmError)

        'Mensaje de Error
        Dim prmMsjError As New SqlParameter("@MSGERROR", SqlDbType.VarChar, 250)
        prmMsjError.Direction = ParameterDirection.Output
        prmMsjError.Value = ""
        com.Parameters.Add(prmMsjError)

        Try
            con.Open()
            com.ExecuteNonQuery()


            If (prmError.Value = 1) Then


                MsgBox("Ocurrió un error al momento de guardar los servicios a Cobrar el Adeudo." & prmMsjError.ToString)
                MsgBox("Procedimiento: CobraAdeudoTel, Función: Guarda_Servicios_CobroAdeudo()", MsgBoxStyle.Exclamation)

                GloServiciosCobroAdeudo = False

                Exit Sub

                Me.Dispose()
                Me.Close()

            End If

            GloServiciosCobroAdeudo = True

            'Prendemos la bandera para refrescar la pantalla con DameDetalle y Suma Detalle en la pantalla de Facturacion con Estado de Cuenta
            GloAgregar_a_lista = True

            Me.Dispose()
            Me.Close()



        Catch ex As Exception

            MsgBox("Ocurrió un error, no se ha podido guardar la relación de los servicios a Cobrar el Adeudo.", MsgBoxStyle.Critical)
            MsgBox(ex.Message)
            Me.Dispose()
            Me.Close()

            GloServiciosCobroAdeudo = False

            Log_Descripcion = ex.Message.ToString & " Contrato: " & GloContrato.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Procedimiento: CobraAdeudoTel, Función: Guarda_Servicios_CobroAdeudo()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)


            Exit Sub
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub

    Private Function esUnServicio(ByRef identificador As Integer) As Boolean

        'Barremos los diferentes servicios para verificar si el que esta seleccionado es un Servicio válido o si es una leyenda de tipo de servicios
        Try
            If ListBox1.Items.Count > 0 Then
                For Each servicios In listaI
                    If servicios.ident = identificador Then
                        If servicios.concepto = "___________    SERVICIOS DIGITALES _____________" Or servicios.concepto = "___________   SERVICIOS DE INTERNET ____________" Or servicios.concepto = "___________ SERVICIOS DE T.V. ANÁLOGA __________" Or servicios.concepto = "_____________ SERVICIOS DE TELEFONIA ___________" Then
                            Return False
                        End If
                    End If
                Next
                Return True
            End If
        Catch ex As Exception
            MsgBox("Ocurrió un error en la función: esUnServicio() As Boolean" & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Function
    Public Sub Minimo()

        Dim num_minimo As Integer
        'Dim compara As Integer
        Dim i As Integer
        i = 0
        num_minimo = -1

        For Each servicios In listaAuxiliar

            If num_minimo = -1 Then
                num_minimo = servicios.ident
            End If

            If num_minimo > servicios.ident Then
                num_minimo = servicios.ident
            End If

            If listaAuxiliar.Count = 1 Then
                num_minimo = servicios.ident
            End If
        Next

        Dim IndexList As Integer
        'Veces = listaAuxiliar.Count - 1

        For Each servicios In listaAuxiliar

            If num_minimo = servicios.ident Then
                listaI.Add(servicios)
                IndexList = i
            End If
            i = i + 1
        Next

        listaAuxiliar.RemoveAt(IndexList)

    End Sub

    Public Function bloqueaTelefoniaUnica(ByVal identificador As Integer) As Boolean
        Dim existenOtrosServiciosPrincipales As Boolean
        Dim existeTelefonia As Boolean

        existenOtrosServiciosPrincipales = False
        existeTelefonia = False

        'Barremos los diferentes servicios para verificar queno se vaya a quedar el servicio de Telefoní solo, ya que no hay clientes de SOLO TELEFONÍA
        Try
            If ListBox1.Items.Count > 0 Then
                For Each servicios In listaI

                    'Verificamos sis es que existe un servicio de Telefonía en la lista izquierda
                    If servicios.clv_TipSer = 5 And servicios.esServicioPrincipal = 1 And servicios.status_serv <> "B" Then
                        existeTelefonia = True
                    End If

                    'Checamos si es que hay otros servicios además del servicio de Telefonía
                    If servicios.clv_TipSer <> 5 And servicios.esServicioPrincipal = 1 And servicios.ident <> identificador And servicios.status_serv <> "B" And servicios.status_serv <> "F" Then
                        existenOtrosServiciosPrincipales = True
                    End If

                Next

                If existeTelefonia = True And existenOtrosServiciosPrincipales = False Then
                    Return False
                Else
                    Return True
                End If

            End If
        Catch ex As Exception
            MsgBox("Ocurrió un error en la función: bloqueaTelefoniaUnica() " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Function

    Public Function RegresaTelefoniaUnica(ByVal identificador As Integer) As Boolean

        Dim existenOtrosServiciosPrincipales As Boolean
        Dim existeTelefonia As Boolean

        existenOtrosServiciosPrincipales = False
        existeTelefonia = False

        'Barremos los diferentes servicios para verificar queno se vaya a quedar el servicio de Telefoní solo, ya que no hay clientes de SOLO TELEFONÍA
        Try
            If ListBox2.Items.Count > 0 Then
                For Each servicios In listaD

                    'checo si el servicio que estoy regresando es el de Telefonía
                    If servicios.clv_TipSer = 5 And servicios.esServicioPrincipal = 1 And servicios.status_serv <> "B" And identificador = servicios.ident Then
                        existeTelefonia = True
                    End If
                Next

                For Each servicios In listaI

                    'Checamos si es que hay otros servicios que se conservarán ade´mas de la telefonía
                    If servicios.clv_TipSer <> 5 And servicios.esServicioPrincipal = 1 And servicios.status_serv <> "B" And servicios.status_serv <> "F" Then
                        existenOtrosServiciosPrincipales = True
                    End If
                Next
                If existeTelefonia = True And existenOtrosServiciosPrincipales = False Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            MsgBox("Ocurrió un error en la función: bloqueaTelefoniaUnica() " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Function

End Class
'Definimos la Clase de los Serivicio a los cuales se les cobrará el Adeudo
Public Class ServicioCobroAdeudo
    Implements IComparer

    Dim _ident As Integer
    Dim _clv_Servicio As Integer
    Dim _concepto As String
    Dim _precio As Double
    Dim _clv_llave As Integer
    Dim _clv_unicanet As Integer
    Dim _clv_txt As String
    Dim _ult_mes As Integer
    Dim _ult_anio As Integer
    Dim _clv_TipSer As Integer
    Dim _status_serv As String
    Dim _esServicioPrincipal As Integer
    'Public Static _StatusDesc As String


    Public Property ident() As Integer
        Get
            Return _ident
        End Get
        Set(ByVal Value As Integer)
            _ident = Value
        End Set
    End Property
    Public Property clv_Servicio() As Integer
        Get
            Return _clv_Servicio
        End Get
        Set(ByVal Value As Integer)
            _clv_Servicio = Value
        End Set
    End Property
    Public Property concepto() As String
        Get
            Return _concepto
        End Get
        Set(ByVal Value As String)
            _concepto = Value
        End Set
    End Property
    Public Property precio() As Double
        Get
            Return _precio
        End Get
        Set(ByVal Value As Double)
            _precio = Value
        End Set
    End Property
    Public Property clv_llave() As Integer
        Get
            Return _clv_llave
        End Get
        Set(ByVal Value As Integer)
            _clv_llave = Value
        End Set
    End Property
    Public Property clv_unicanet() As Integer
        Get
            Return _clv_unicanet
        End Get
        Set(ByVal Value As Integer)
            _clv_unicanet = Value
        End Set
    End Property
    Public Property clv_txt() As String
        Get
            Return _clv_txt
        End Get
        Set(ByVal Value As String)
            _clv_txt = Value
        End Set
    End Property
    Public Property clv_TipSer() As Integer
        Get
            Return _clv_TipSer
        End Get
        Set(ByVal Value As Integer)
            _clv_TipSer = Value
        End Set
    End Property
    Public Property status_serv() As String
        Get
            Return _status_serv
        End Get
        Set(ByVal Value As String)
            _status_serv = Value
        End Set
    End Property
    Public Property esServicioPrincipal() As Integer
        Get
            Return _esServicioPrincipal
        End Get
        Set(ByVal Value As Integer)
            _esServicioPrincipal = Value
        End Set
    End Property
    Public ReadOnly Property DescripcionCompleta() As String

        Get
            Dim StatusMostrar As String
            StatusMostrar = ""
            If _status_serv = "C" Then
                StatusMostrar = "Contratado"
            ElseIf _status_serv = "I" Then
                StatusMostrar = "Instalado"
            ElseIf _status_serv = "D" Then
                StatusMostrar = "Desconectado"
            ElseIf _status_serv = "S" Then
                StatusMostrar = "Suspendido"
            ElseIf _status_serv = "B" Then
                StatusMostrar = "Baja"
            ElseIf _status_serv = "F" Then
                StatusMostrar = "Fuera de Area"
            End If
            If _clv_Servicio <> 0 Then
                Return _concepto + " :: " + CType(StatusMostrar, String)
            Else
                Return _concepto
            End If

        End Get
    End Property
    Public Property ult_mes() As Integer
        Get
            Return _ult_mes
        End Get
        Set(ByVal Value As Integer)
            _ult_mes = Value
        End Set
    End Property
    Public Property ult_anio() As Integer
        Get
            Return _ult_anio
        End Get
        Set(ByVal Value As Integer)
            _ult_anio = Value
        End Set
    End Property
    Public Sub ServicioCobroAdeudo(ByVal descripcion As String, ByVal costo As String)
        'Constructor de la Clase
        Me.ident = ident
        Me.clv_Servicio = clv_Servicio
        Me.concepto = concepto
        Me.precio = precio
        Me.clv_llave = clv_llave
        Me.clv_unicanet = clv_unicanet
        Me.clv_txt = clv_txt
        Me.ult_mes = ult_mes
        Me.ult_anio = ult_anio
        Me.clv_TipSer = clv_TipSer
        Me.status_serv = status_serv
        Me.esServicioPrincipal = esServicioPrincipal
    End Sub


    Function Compare(ByVal a As Object, ByVal b As Object) As Integer Implements IComparer.Compare

        Dim c1 As ServicioCobroAdeudo = CType(a, ServicioCobroAdeudo)
        Dim c2 As ServicioCobroAdeudo = CType(b, ServicioCobroAdeudo)

        If (c1.ident > c2.ident) Then
            Return 1
        End If

        If (c1.ident < c2.ident) Then
            Return -1
        Else
            Return 0
        End If
    End Function


End Class





