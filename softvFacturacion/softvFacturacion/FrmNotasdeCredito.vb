Imports System.Data.SqlClient
Imports System.Text
Public Class FrmNotasdeCredito
    Dim monto As Double
    Dim factura As Long = 0
    Dim factura_inicila As Long = 0
    Dim clave_txt As String
    Dim BndChange As Boolean = False
    'Variables biracora

    'Dim monto As Double = 0
    Dim monto1 As Double = 0
    Dim Total_sum As Double = 0
    Dim Totales As Double = 0
    Dim Total_sumAux As Double = 0
    Dim sub_totales() As Double

    Private suc_aplica As String = Nothing
    Private caja As String = Nothing
    Private cajero As String = Nothing
    Private fecha_caducidad As String = Nothing
    Private Locactura As String = Nothing
    Private Observa As String = Nothing
    Private Locmonto As String = Nothing

    ' Private Locsaldo As String = nothing
    Private conGlo As New SqlConnection(MiConexion)

   
    Private Sub damedatosbitacora()
        Try
            suc_aplica = Me.ComboBox7.Text
            caja = Me.ComboBox8.Text
            cajero = Me.ComboBox5.Text
            fecha_caducidad = Me.Fecha_CaducidadDateTimePicker.Text
            Locactura = Me.ComboBox3.Text
            Observa = Me.ObservacionesTextBox.Text
            If IsNumeric(Me.MontoTextBox.Text) = False Then Me.MontoTextBox.Text = 0
            Locmonto = Me.MontoTextBox.Text
            'Locsaldo = Me.TextBox1.Text
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora()
        Try
            If OPCION = "M" Then
                'suc_aplica = Me.ComboBox7.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Sucural Aplica" + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), suc_aplica, Me.ComboBox7.Text, SubCiudad)
                'caja = Me.ComboBox8.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Caja Aplica" + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), caja, Me.ComboBox8.Text, SubCiudad)
                'cajero = Me.ComboBox5.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Cajero Aplica" + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), cajero, Me.ComboBox5.Text, SubCiudad)
                'fecha_caducidad = Me.Fecha_CaducidadDateTimePicker.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, Me.Fecha_CaducidadDateTimePicker.Name + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), fecha_caducidad, Me.Fecha_CaducidadDateTimePicker.Text, SubCiudad)
                'Locactura = Me.ComboBox3.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Factura Genera" + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), Locactura, Me.ComboBox3.Text, SubCiudad)
                'Observa = Me.ObservacionesTextBox.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, Me.ObservacionesTextBox.Name + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), Observa, Me.ObservacionesTextBox.Text, SubCiudad)
                'Locmonto = Me.MontoTextBox.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, Me.MontoTextBox.Name + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), Locmonto, Me.MontoTextBox.Text, SubCiudad)
                ''Locsaldo = Me.TextBox1.Text
                'bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, Me.MontoTextBox.Name + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), Locsaldo, Me.TextBox1.Text, SubCiudad)
                damedatosbitacora()
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub DetalleNOTASDECREDITO_View(ByVal MFactura As Long, ByVal MClv_Session As Long, ByVal MClv_NotadeCredito As Long, ByVal MOP As String)
        Try
            '@Factura bigint,@Clv_Session bigint,@Clv_NotadeCredito bigint
            borra_Predetalle(0)

            Dim conn As New SqlConnection(MiConexion)
            conn.Open()
            Dim comando As New SqlClient.SqlCommand("Detalle_NotasdeCredito", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0
            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando
            '@Tablas varchar(150),@values varchar(max),@Valores varchar(max),@Condicion varchar(150),
            '@TipMov int,@BNDIDENTITY BIT,@CLV_ID BIGINT OUTPUT

            'Agrego el par�metro
            Adaptador.SelectCommand.Parameters.Add("@Factura", SqlDbType.BigInt).Value = MFactura
            Adaptador.SelectCommand.Parameters.Add("@Clv_Session", SqlDbType.BigInt).Value = MClv_Session
            Adaptador.SelectCommand.Parameters.Add("@Clv_NotadeCredito", SqlDbType.BigInt).Value = MClv_NotadeCredito
            Adaptador.SelectCommand.Parameters.Add("@op", SqlDbType.VarChar, 1).Value = MOP

            Dim Dataset As New DataSet
            'Creamos nuestros BidingSource
            Dim Bs As New BindingSource
            'Llenamos el Adaptador con el DataSet
            'Dataset.Clear()
            'Bs.Clear()

            Adaptador.Fill(Dataset, "Detalle_NotasdeCredito")
            Bs.DataSource = Dataset.Tables("Detalle_NotasdeCredito")
            'dgvResultadosClasificacion.DataSource = Bs

            Me.Detalle_NotasdeCreditoDataGridView.DataSource = Bs

            'For i As Integer = 0 To Detalle_NotasdeCreditoDataGridView.RowCount - 1

            '    sub_totales(i) = Me.Detalle_NotasdeCreditoDataGridView.Item(2, i).Value

            'Next

            'Total_sumAux = Sumar()
            conn.Close()
            Calcula_monto()
            'Me.dgvResultadosClasificacion.Columns(0).Visible = False
            'Me.dgvResultadosClasificacion.Columns(3).Visible = False
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub busca()
        Try
            Dim con2 As New SqlConnection(MiConexion)
            con2.Open()
            Me.Consulta_NotaCreditoTableAdapter.Connection = con2
            Me.Consulta_NotaCreditoTableAdapter.Fill(Me.DataSetLydia.Consulta_NotaCredito, CLng(gloClvNota))
            con2.Close()
            Despliega_Datos_Del_cliente()
            'Me.DetalleNOTASDECREDITOTableAdapter.Connection = con2
            'Me.DetalleNOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.DetalleNOTASDECREDITO, gloClvNota)
            'DetalleNOTASDECREDITO_View(factura, MClv_Session, gloClvNota)
            'DameTiponota] (@Clv_nota bigint,@tipo int output)
            con2.Open()
            Dim comando As New SqlClient.SqlCommand
            With comando
                .CommandText = "DameTipoNota"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con2

                Dim prm As New SqlParameter("@Clv_Nota", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = gloClvNota
                .Parameters.Add(prm)

                Dim prm2 As New SqlParameter("Tipo", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)
                .ExecuteNonQuery()
                glotipoNota = prm2.Value
            End With
            con2.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmNotasdeCredito_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Glocontratosel > 0 Then
            Me.ContratoTextBox.Text = Glocontratosel
            'Me.Panel1.Enabled = True
            Me.Panel4.Enabled = True
            Me.Panel5.Enabled = True
            Despliega_Datos_Del_cliente()
            Glocontratosel = 0
        End If
        If bnd = 3 Then
            If glotipoNota = 0 Then
                'Me.TextBox1.Text = 0
                Me.TextBox1.Visible = False
                CMBLabel1.Visible = False
                Me.CMBREDLabel3.Text = "Nota de Cr�dito en Efectivo "
                Me.Panel4.Show()
                Me.Panel5.Hide()
            ElseIf glotipoNota = 1 Then
                'Me.TextBox1.Text = 0
                Me.TextBox1.Visible = True
                CMBLabel1.Visible = True
                Me.CMBREDLabel3.Text = "Nota de Cr�dito por Factura "
                Me.Panel4.Show()
                Me.Panel5.Hide()
                'ElseIf glotipoNota = 2 Then
                '    Me.REDLabel3.Text = "Nota de Cr�dito por Concepto de Servicio"
                '    Me.Panel4.Hide()
                '    Me.Panel5.Show()
                '    conGlo.Open()
                '    Me.MuestraDetalleNota_por_ConceptoTableAdapter.Connection = conGlo
                '    Me.MuestraDetalleNota_por_ConceptoTableAdapter.Fill(Me.DataSetLydia.MuestraDetalleNota_por_Concepto, Me.Clv_NotadeCreditoTextBox.Text)
                '    conGlo.Close()
            End If
            bnd = 2
        End If






    End Sub



    Private Sub FrmNotasdeCredito_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        borra_Predetalle(0)
        gloClvNota = 0
        MClv_Session = 0
    End Sub

    Private Sub FrmNotasdeCredito_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'borra_Predetalle(0)

        'Me.Close()
    End Sub

    Private Sub FrmNotasdeCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            colorea(Me)
            'Voy por la MClv_Session para lo posibles Movimientos que tenga que hacer

            'Fin
            Dim con3 As New SqlConnection(MiConexion)
            con3.Open()
            'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.MuestraTipSerPrincipal_SER' Puede moverla o quitarla seg�n sea necesario.
            Me.MuestraTipSerPrincipal_SERTableAdapter.Connection = con3
            Me.MuestraTipSerPrincipal_SERTableAdapter.Fill(Me.DataSetLydia.MuestraTipSerPrincipal_SER)
            con3.Close()
            ' ''TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.Muestra_Tipo_Nota' Puede moverla o quitarla seg�n sea necesario.
            con3.Open()
            Me.Muestra_Tipo_NotaTableAdapter.Connection = con3
            Me.Muestra_Tipo_NotaTableAdapter.Fill(Me.DataSetLydia.Muestra_Tipo_Nota, 1)
            con3.Close()
            'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.MUESTRAUSUARIOSIN' Puede moverla o quitarla seg�n sea necesario.
            con3.Open()
            Me.MUESTRAUSUARIOSin2TableAdapter.Connection = con3
            Me.MUESTRAUSUARIOSin2TableAdapter.Fill(Me.DataSetLydia.MUESTRAUSUARIOSin2, 0)
            con3.Close()
            ''TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.MUESTRAUSUARIOSIN' Puede moverla o quitarla seg�n sea necesario.
            con3.Open()
            Me.MUESTRAUSUARIOSINTableAdapter.Connection = con3
            Me.MUESTRAUSUARIOSINTableAdapter.Fill(Me.DataSetLydia.MUESTRAUSUARIOSIN)
            con3.Close()
            ''TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.DAMEFECHADELSERVIDOR_2' Puede moverla o quitarla seg�n sea necesario.
            con3.Open()
            Me.DAMEFECHADELSERVIDOR_2TableAdapter.Connection = con3
            Me.DAMEFECHADELSERVIDOR_2TableAdapter.Fill(Me.DataSetLydia.DAMEFECHADELSERVIDOR_2)
            con3.Close()
            ''TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLydia.StatusNotadeCredito' Puede moverla o quitarla seg�n sea necesario.
            con3.Open()
            Me.StatusNotadeCreditoTableAdapter.Connection = con3
            Me.StatusNotadeCreditoTableAdapter.Fill(Me.DataSetLydia.StatusNotadeCredito)
            con3.Close()
            'Me.MUESTRASUCURSALES2TableAdapter.Connection = con3
            'Me.MUESTRASUCURSALES2TableAdapter.Fill(Me.DataSetLydia.MUESTRASUCURSALES2, 0)
            MUESTRASUCURSALES2()

            MUESTRACAJERAS()
            factura = 0
            Glocontratosel = 0
            If OPCION = "N" Then
                gloClvNota = 0
                'Me.Consulta_NotaCreditoBindingSource.AddNew()
                Me.Fecha_deGeneracionDateTimePicker.Value = Me.DateTimePicker1.Text
                Me.Clv_NotadeCreditoTextBox.Text = 0
                Me.ComboBox4.Text = GloUsuario
                Me.ComboBox1.SelectedValue = GloSucursal
                'Me.Panel1.Enabled = False
                Me.Panel4.Enabled = False
                Me.Panel5.Enabled = False
                Me.ComboBox2.SelectedValue = "A"
                Me.ComboBox2.Enabled = False
                Me.ComboBox4.Enabled = False
                Me.Button4.Enabled = False
                Me.ContratoTextBox.Text = 0
                If bnd = 1 Then
                    LiTipo = 1
                    FrmTipoNota.Show()
                    'MUESTRACAJA2()
                End If
            ElseIf OPCION = "M" Or OPCION = "C" Then
                Me.ContratoTextBox.Enabled = False
                'Me.Panel1.Enabled = False
                Me.Panel4.Enabled = False
                Me.Panel5.Enabled = False
                Me.Button2.Enabled = False
                Me.ContratoTextBox.ReadOnly = True
                Me.ContratoTextBox.BackColor = Color.White
                Me.ComboBox1.Enabled = True
                Me.Fecha_CaducidadDateTimePicker.Enabled = True
                Me.ComboBox2.Enabled = False
                Me.ComboBox4.Enabled = False
                MUESTRCAJERONOTAS()
                DAMESALDONOTACREDITO()
                busca()
                damedatosbitacora()

                'If CInt(Me.MontoTextBox.Text) = CInt(Me.TextBox1.Text) Then
                '    Me.ComboBox3.Enabled = True
                '    'Me.DAME_FACTURASDECLIENTETableAdapter.Connection = con3
                '    'Me.DAME_FACTURASDECLIENTETableAdapter.Fill(Me.DataSetLydia.DAME_FACTURASDECLIENTE, Me.ContratoTextBox.Text, Me.Clv_NotadeCreditoTextBox.Text)
                'ElseIf CInt(Me.TextBox1.Text) = 0 Then
                '    Me.ComboBox3.Enabled = False
                '    'Me.Detalle_NotasdeCreditoDataGridView.Enabled = False
                'Else
                Me.ComboBox3.Enabled = False
                'End If
                Me.ComboBox3.SelectedValue = Me.TextBox4.Text
                Factura_inicial = CInt(Me.TextBox4.Text)

            End If




            'If IdSistema = "VA" Then
            '    glotipoNota = 2
            '    Me.Panel4.Hide()
            '    Me.Panel5.Show()
            '    conGlo.Open()
            '    Me.MuestraDetalleNota_por_ConceptoTableAdapter.Connection = conGlo
            '    Me.MuestraDetalleNota_por_ConceptoTableAdapter.Fill(Me.DataSetLydia.MuestraDetalleNota_por_Concepto, Me.Clv_NotadeCreditoTextBox.Text)
            '    conGlo.Close()
            'End If

            If OPCION = "C" Or status = "Cancelada" Or status = "Saldada" Then
                DAME_FACTURASDECLIENTE()
                Me.Button5.Enabled = True
                Me.Panel3.Enabled = True
                Me.ComboBox1.Enabled = False
                Me.ComboBox2.Enabled = False
                Me.ComboBox3.Enabled = False
                Me.ComboBox4.Enabled = False
                Me.ComboBox5.Enabled = False
                Me.ComboBox6.Enabled = False
                Me.ComboBox7.Enabled = False
                Me.ComboBox8.Enabled = False
                Me.Panel4.Enabled = False
                Me.Panel5.Enabled = False
                Me.Fecha_CaducidadDateTimePicker.Enabled = False
                Me.Fecha_deGeneracionDateTimePicker.Enabled = False
                Me.ObservacionesTextBox.Enabled = False
                Me.Button5.Enabled = True
                Me.Detalle_NotasdeCreditoDataGridView.Enabled = False
            End If
            Me.SOLOINTERNETCheckBox.Enabled = False
            Me.ESHOTELCheckBox.Enabled = False

            'If IdSistema = "VA" Then
            '    Me.TextBox1.Visible = False
            '    Me.CMBLabel1.Visible = False
            '    Me.Text = "Cat�logo de Devoluciones en Efectivo"
            '    Me.Label2.Text = "Pagos Realizados con La Devoluci�n :"
            '    ' Me.REDLabel3.Text = "Tipo de Devolucion"
            '    Me.REDLabel3.Text = "Devoluci�n en Efectivo "
            '    Me.Label4.Text = "Sucursal donde se Aplica la Devoluci�n :"
            '    Me.Usuario_AutorizoLabel.Text = "Usuario que realiza la Devoluci�n :"
            '    Me.Clv_sucursalLabel.Text = "Sucursal que Realiza la Devoluci�n :"
            '    Me.CMBClv_NotadeCreditoLabel.Text = "Devoluci�n :"
            '    'Me.Button4.Text = "Pagos Realizados con esta Devoluci�n"
            '    Me.Button4.Visible = False

            '    ComboBox7.SelectedValue = GloSucursal
            '    ComboBox8.SelectedValue = 0
            '    ComboBox8.SelectedValue = GloCaja
            '    ComboBox5.SelectedValue = 0
            '    ComboBox5.SelectedValue = GloUsuario
            '    ComboBox7.Enabled = False
            '    ComboBox8.Enabled = False
            '    ComboBox5.Enabled = False
            'End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        borra_Predetalle(0)
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Consulta_NotaCreditoBindingSource.CancelEdit()
        Me.Close()
    End Sub

    Private Sub ContratoTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ContratoTextBox.GotFocus
        Detalle_NotasdeCreditoDataGridView.EndEdit()
    End Sub

    Private Sub Despliega_Datos_Del_cliente()
        Dim nota As Integer
        Dim CONE As New SqlClient.SqlConnection(MiConexion)
        Try
            borra_Predetalle(0)
            If IsNumeric(Me.ContratoTextBox.Text) = False Then
                'Me.Panel1.Enabled = False
                Me.Panel5.Enabled = False
                Me.Panel4.Enabled = False
                MClv_Session = 0
                MClv_Session = DAMESclv_Sessionporfavor()
                CONE.Open()
                Me.BUSCLIPORCONTRATOTableAdapter.Connection = CONE
                Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.DataSetLydia.BUSCLIPORCONTRATO, 0, "", "", "", "", 0, 0)
                CONE.Close()
                DAME_FACTURASDECLIENTE()

            ElseIf CLng(Me.ContratoTextBox.Text) = 0 Then
                'Me.Panel1.Enabled = False
                Me.Panel5.Enabled = False
                Me.Panel4.Enabled = False
                MClv_Session = 0
                MClv_Session = DAMESclv_Sessionporfavor()
                CONE.Open()
                Me.BUSCLIPORCONTRATOTableAdapter.Connection = CONE
                Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.DataSetLydia.BUSCLIPORCONTRATO, 0, "", "", "", "", 0, 0)
                CONE.Close()
                DAME_FACTURASDECLIENTE()

            ElseIf CLng(Me.ContratoTextBox.Text) > 0 Then
                MClv_Session = 0
                MClv_Session = DAMESclv_Sessionporfavor()
                If OPCION = "N" Or OPCION = "M" Then
                    Me.ComboBox1.Enabled = True
                    Me.ComboBox2.Enabled = True
                    Me.ComboBox3.Enabled = True
                    Me.ComboBox4.Enabled = True
                    Me.ComboBox5.Enabled = True
                    Me.ComboBox6.Enabled = True
                    Me.ComboBox7.Enabled = True
                    Me.ComboBox8.Enabled = True
                    Me.Panel4.Enabled = True
                    Me.Panel5.Enabled = True
                    Me.Fecha_CaducidadDateTimePicker.Enabled = True
                    Me.Fecha_deGeneracionDateTimePicker.Enabled = True
                    Me.ObservacionesTextBox.Enabled = True
                    Me.Button5.Enabled = True
                    Me.Detalle_NotasdeCreditoDataGridView.Enabled = True
                    If IsNumeric(Me.ContratoTextBox.Text) = True Then
                        Me.ComboBox3.Enabled = True
                        If Me.Clv_NotadeCreditoTextBox.Text = "" Then
                            nota = 0
                        Else
                            nota = Me.Clv_NotadeCreditoTextBox.Text
                        End If
                        CONE.Open()
                        Me.BUSCLIPORCONTRATOTableAdapter.Connection = CONE
                        Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.DataSetLydia.BUSCLIPORCONTRATO, Me.ContratoTextBox.Text, "", "", "", "", 0, 0)
                        DAME_FACTURASDECLIENTE()
                        'Me.DAME_FACTURASDECLIENTETableAdapter.Connection = CONE
                        'Me.DAME_FACTURASDECLIENTETableAdapter.Fill(Me.DataSetLydia.DAME_FACTURASDECLIENTE, Me.ContratoTextBox.Text, nota)
                        CONE.Close()
                        CREAARBOL()

                        If IsNumeric(Me.ComboBox3.SelectedValue) = True Then
                            'borra_Predetalle(0)
                            'Muestra_Predetalle(Me.ComboBox3.SelectedValue)
                            'Calcula_monto(Me.ComboBox3.SelectedValue, 0)
                            'Me.MontoTextBox.Text = Me.ComboBox6.Text
                            If IsNumeric(Me.MontoTextBox.Text) = False Then Me.MontoTextBox.Text = 0
                            'Me.TextBox1.Text = Me.MontoTextBox.Text
                        Else
                            'borra_Predetalle(0)
                            Me.ComboBox3.Text = ""
                            Me.ComboBox3.SelectedValue = 0
                            'Me.TextBox1.Text = 0
                            'Calcula_monto(0, 0)
                        End If
                        'If Me.ComboBox3.SelectedValue Is Nothing And Me.TextBox5.Text = "" Then
                        '    Muestra_Predetalle(0)
                        'End If
                        'Me.Panel1.Enabled = True
                        Me.Panel5.Enabled = True
                        Me.Panel4.Enabled = True

                    Else
                        Me.ComboBox3.Text = ""
                    End If
                ElseIf OPCION = "C" Then
                    MClv_Session = 0
                    MClv_Session = DAMESclv_Sessionporfavor()
                    borra_Predetalle(0)
                    CONE.Open()
                    Me.BUSCLIPORCONTRATOTableAdapter.Connection = CONE
                    Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.DataSetLydia.BUSCLIPORCONTRATO, Me.ContratoTextBox.Text, "", "", "", "", 0, 0)
                    CONE.Close()
                    CREAARBOL()
                    'Me.Panel1.Enabled = False
                    Me.Panel5.Enabled = False
                    Me.Panel4.Enabled = False

                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)

            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next
            CON.Open()
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.DataSetLydia.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.DataSetLydia.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            CON.Close()
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.DataSetLydia.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next

            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        FrmSelCliente.Show()
    End Sub

    Private Sub Muestra_Predetalle(ByVal factura As Integer)
        Try


            'Dim conlidia2 As New SqlClient.SqlConnection(MiConexion)
            'conlidia2.Open()
            'Dim filarow As DataRow
            'Me.Detalle_NotasdeCreditoTableAdapter.Connection = conlidia2
            'Me.Detalle_NotasdeCreditoTableAdapter.Fill(Me.DataSetLydia.Detalle_NotasdeCredito, factura)
            'Detalle_NotasdeCredito()
            If IsNumeric(factura) = False Then factura = 0
            'If IsNumeric(MClv_Session) = False Then MClv_Session = 0
            If IsNumeric(gloClvNota) = False Then gloClvNota = 0
            DetalleNOTASDECREDITO_View(factura, MClv_Session, gloClvNota, OPCION)
            'For Each FilaRow In Me.DataSetLydia.Detalle_NotasdeCredito.Rows
            '    If filarow("Descripcion".ToString()) Is Nothing Then
            '        Exit For
            '    End If
            'Next
            'conlidia2.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub borra_Predetalle(ByVal factura1 As Integer)
        Dim conn1 As New SqlClient.SqlConnection(MiConexion)
        Try
            Dim comando As New SqlClient.SqlCommand
            conn1.Open()
            With comando
                .Connection = conn1
                .CommandText = "Borrar_Session_Notas "
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                ' Create a SqlParameter for each parameter in the stored procedure.
                Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = factura1
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@CLV_SESSION", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = MClv_Session
                .Parameters.Add(prm1)

                Dim i As Integer = comando.ExecuteNonQuery()

            End With
            conn1.Close()
        Catch ex As Exception
            If conn1.State <> ConnectionState.Closed Then conn1.Close()
            'MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim comando2 As New SqlClient.SqlCommand(MiConexion)
        Dim comando3 As New SqlClient.SqlCommand(MiConexion)
        Dim comando4 As New SqlClient.SqlCommand(MiConexion)
        Dim cone As New SqlClient.SqlConnection(MiConexion)
        Dim cone2 As New SqlClient.SqlConnection(MiConexion)
        Dim comando As New SqlClient.SqlCommand
        Dim msg As String
        Dim bnd As Integer
        Try
            If Me.ComboBox2.SelectedValue = "S" Or Me.ComboBox2.SelectedValue = "C" Then
                MsgBox("No se puede realizar ningun cambio", MsgBoxStyle.Information)
                Exit Sub
            End If
            Me.Calcula_monto()
            If Me.MontoTextBox.Text > 0 Then
                If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
                If OPCION = "N" Then
                    If IsNumeric(Me.ContratoTextBox.Text) = False Then
                        MsgBox(" Es Necesario que se Capture un N�mero de Contrato ", MsgBoxStyle.Information)
                    ElseIf IsNumeric(Me.ComboBox1.SelectedValue) = False Then
                        MsgBox(" Es Necesario que se Capture una Sucursal ", MsgBoxStyle.Information)
                    ElseIf IsNumeric(Me.ComboBox4.SelectedValue) = False Then
                        MsgBox(" Es Necesario que se Capture un Usuario que Autorize la Nota de Cr�dito ", MsgBoxStyle.Information)
                    ElseIf IsNumeric(Me.MontoTextBox.Text) = False Then
                        MsgBox(" Es Necesario que se Capture el Monto de la Nota de Cr�dito ", MsgBoxStyle.Information)
                    ElseIf IsNumeric(Me.ComboBox7.SelectedValue) = False Then
                        MsgBox("Es Necesario que se Elija una Sucursal ", MsgBoxStyle.Information)
                    ElseIf Len(Me.ComboBox5.SelectedValue) <= 0 Then
                        MsgBox("Es Necesario que se Elija un(a) Cajero(a) ", MsgBoxStyle.Information)
                    ElseIf IsNumeric(Me.ComboBox8.SelectedValue) = False Then
                        MsgBox("Es Necesario que se Elija una Caja", MsgBoxStyle.Information)
                    ElseIf Me.ComboBox5.Text = "" Then
                        MsgBox("Es Necesario que se Elija un Cajero ", MsgBoxStyle.Information)
                    Else


                        cone.Open()
                        With comando
                            .Connection = cone
                            .CommandText = "Nueva_NotadeCredito "
                            .CommandType = CommandType.StoredProcedure
                            .CommandTimeout = 0
                            ' Create a SqlParameter for each parameter in the stored procedure.
                            Dim prm As New SqlParameter("@Clv_Notadecredito", SqlDbType.BigInt)
                            Dim prm1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
                            Dim prm12 As New SqlParameter("@Factura", SqlDbType.BigInt)
                            Dim prm2 As New SqlParameter("@Fecha_deGeneracion", SqlDbType.DateTime)
                            Dim prm3 As New SqlParameter("@Usuario_Captura", SqlDbType.VarChar)
                            Dim prm4 As New SqlParameter("@Usuario_Autorizo", SqlDbType.VarChar)
                            Dim prm5 As New SqlParameter("@Fecha_Caducidad", SqlDbType.DateTime)
                            Dim prm6 As New SqlParameter("@Monto", SqlDbType.Decimal)
                            Dim prm7 As New SqlParameter("@Status", SqlDbType.VarChar)
                            Dim prm8 As New SqlParameter("@Observaciones", SqlDbType.VarChar)
                            Dim prm9 As New SqlParameter("@Sucursal", SqlDbType.Int)
                            Dim prm10 As New SqlParameter("@suc_aplica", SqlDbType.Int)
                            Dim prm11 As New SqlParameter("@tipo", SqlDbType.Int)
                            Dim prm13 As New SqlParameter("@caja", SqlDbType.Int)
                            Dim prm14 As New SqlParameter("@saldo", SqlDbType.Decimal)
                            ' bigint, bigint,@Factura bigint, datetime , varchar(50)  varchar(50), datetime, decimal(18,0), varchar(50), varchar(max), int)
                            prm.Direction = ParameterDirection.Output
                            prm1.Direction = ParameterDirection.Input
                            prm2.Direction = ParameterDirection.Input
                            prm12.Direction = ParameterDirection.Input
                            prm3.Direction = ParameterDirection.Input
                            prm4.Direction = ParameterDirection.Input
                            prm5.Direction = ParameterDirection.Input
                            prm6.Direction = ParameterDirection.Input
                            prm7.Direction = ParameterDirection.Input
                            prm8.Direction = ParameterDirection.Input
                            prm9.Direction = ParameterDirection.Input
                            prm10.Direction = ParameterDirection.Input
                            prm11.Direction = ParameterDirection.Input
                            prm13.Direction = ParameterDirection.Input
                            prm14.Direction = ParameterDirection.Input


                            prm.Value = Me.Clv_NotadeCreditoTextBox.Text
                            prm1.Value = Me.ContratoTextBox.Text
                            prm2.Value = Me.Fecha_deGeneracionDateTimePicker.Text
                            If IsNumeric(Me.ComboBox3.SelectedValue) = False Then
                                prm12.Value = 0
                            Else
                                prm12.Value = Me.ComboBox3.SelectedValue
                            End If
                            prm3.Value = Me.ComboBox5.SelectedValue
                            prm4.Value = Me.ComboBox4.SelectedValue
                            prm5.Value = Me.Fecha_CaducidadDateTimePicker.Text
                            If IsNumeric(Me.MontoTextBox.Text) = False Then Me.MontoTextBox.Text = 0
                            prm6.Value = CDec(Me.MontoTextBox.Text)
                            prm7.Value = Me.ComboBox2.SelectedValue
                            prm8.Value = Me.ObservacionesTextBox.Text
                            prm9.Value = Me.ComboBox1.SelectedValue
                            prm10.Value = Me.ComboBox7.SelectedValue
                            prm11.Value = glotipoNota
                            prm13.Value = Me.ComboBox8.SelectedValue
                            If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
                            prm14.Value = CDec(Me.TextBox1.Text)

                            .Parameters.Add(prm)
                            .Parameters.Add(prm1)
                            .Parameters.Add(prm12)
                            .Parameters.Add(prm2)
                            .Parameters.Add(prm3)
                            .Parameters.Add(prm4)
                            .Parameters.Add(prm5)
                            .Parameters.Add(prm6)
                            .Parameters.Add(prm7)
                            .Parameters.Add(prm8)
                            .Parameters.Add(prm9)
                            .Parameters.Add(prm10)
                            .Parameters.Add(prm11)
                            .Parameters.Add(prm13)
                            .Parameters.Add(prm14)

                            Dim i As Integer = comando.ExecuteNonQuery()
                            Me.Clv_NotadeCreditoTextBox.Text = prm.Value
                            gloClvNota = Me.Clv_NotadeCreditoTextBox.Text
                        End With
                        ',@clv_Session bigint,@Clv_NotadeCredito bigint
                        If glotipoNota = 0 Or glotipoNota = 1 Then
                            Dim J As Integer = 0
                            For J = 0 To Me.Detalle_NotasdeCreditoDataGridView.RowCount - 1

                                If Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(4).Value = True Then
                                    Guarda_Grid(Me.ComboBox3.SelectedValue, gloClvNota, Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(0).Value, 1, Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(2).Value, MClv_Session)
                                Else
                                    Guarda_Grid(Me.ComboBox3.SelectedValue, gloClvNota, Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(0).Value, 0, Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(2).Value, MClv_Session)
                                End If

                            Next J
                            'With comando2
                            '    .Connection = cone
                            '    .CommandTimeout = 0
                            '    .CommandText = "Guarda_DetalleNota"
                            '    .CommandType = CommandType.StoredProcedure
                            '    Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                            '    Dim prm1 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                            '    Dim prm2 As New SqlParameter("@Clv_NotadeCredito", SqlDbType.BigInt)
                            '    prm.Direction = ParameterDirection.Input
                            '    prm1.Direction = ParameterDirection.Input
                            '    prm2.Direction = ParameterDirection.Input
                            '    prm.Value = Me.ComboBox3.SelectedValue
                            '    prm1.Value = MClv_Session
                            '    prm2.Value = gloClvNota
                            '    .Parameters.Add(prm)
                            '    .Parameters.Add(prm1)
                            '    .Parameters.Add(prm2)
                            '    Dim i As Integer = comando2.ExecuteNonQuery
                            'End With
                        ElseIf glotipoNota = 2 Then
                            Agregar_Conceptos(2)
                        End If

                        'Cancelacion_FacturasPorNotas](@CLV_FACTURA BIGINT,@MSG VARCHAR(250) OUTPUT,@BNDERROR INT OUTPUT)
                        '@Clv_NotadeCredito bigint
                        If glotipoNota = 1 Then
                            With comando3
                                .CommandText = "Cancelacion_FacturasPorNotas"
                                .CommandTimeout = 0
                                .CommandType = CommandType.StoredProcedure
                                .Connection = cone
                                Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                                prm.Direction = ParameterDirection.Input
                                prm.Value = Me.ComboBox3.SelectedValue
                                .Parameters.Add(prm)

                                Dim prm2 As New SqlParameter("@MSG", SqlDbType.VarChar, 250)
                                prm2.Direction = ParameterDirection.Output
                                prm2.Value = ""
                                .Parameters.Add(prm2)


                                Dim prm3 As New SqlParameter("@BNDERROR", SqlDbType.Int)
                                prm3.Direction = ParameterDirection.Output
                                prm3.Value = 1
                                .Parameters.Add(prm3)

                                Dim prm4 As New SqlParameter("Clv_NotadeCredito", SqlDbType.BigInt)
                                prm4.Direction = ParameterDirection.Input
                                prm4.Value = gloClvNota
                                .Parameters.Add(prm4)

                                Dim i As Integer = comando3.ExecuteNonQuery
                                msg = prm2.Value
                                bnd = prm3.Value
                            End With
                        End If
                        cone.Close()
                        bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Se Hizo una Nueva nota de Cr�dito", "Tipo:" + CStr(glotipoNota), "Monto:" + CStr(Me.MontoTextBox.Text), SubCiudad)
                        'borra_Predetalle(0)
                        MsgBox("Se ha Guardado con �xito")
                        'Genera_Factura_Digital_Nota(gloClvNota)
                        'GeneraDocumentoTxt_DIGITALNota(gloClvNota, "C:\Users\TeamEdgar\Documents\Grupo7")
                        refrescar = True
                        gloClvNota = Me.Clv_NotadeCreditoTextBox.Text
                        LocBndNotasReporteTick = True
                        locoprepnotas = 1
                        FrmImprimirRepGral.Show()
                        Me.Close()
                        'borra_Predetalle(0)
                    End If

                ElseIf OPCION = "M" Then
                    If IsNumeric(Me.ComboBox4.SelectedValue) = False Then
                        MsgBox(" Es Necesario que se Capture un Usuario que Autorize la Nota de Cr�dito ", MsgBoxStyle.Information)
                        Exit Sub
                    ElseIf IsNumeric(Me.MontoTextBox.Text) = False Then
                        MsgBox(" Es Necesario que se Capture el Monto de la Nota de Cr�dito ", MsgBoxStyle.Information)
                        Exit Sub
                    ElseIf Me.ComboBox5.Text = "" Then
                        MsgBox("Es Necesario que se Elija un Cajero ", MsgBoxStyle.Information)
                        Exit Sub
                    ElseIf IsNumeric(Me.ComboBox7.SelectedValue) = False Then
                        MsgBox("Es Necesario que se Elija una Sucursal ", MsgBoxStyle.Information)
                        Exit Sub
                    ElseIf IsNumeric(Me.ComboBox8.SelectedValue) = False Then
                        MsgBox("Es Necesario que se Elija una Caja", MsgBoxStyle.Information)
                        Exit Sub
                    End If

                    cone.Open()
                    With comando
                        .Connection = cone
                        .CommandText = "Modifica_NotaCredito "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Notadecredito", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
                        Dim prm12 As New SqlParameter("@Factura", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Fecha_deGeneracion", SqlDbType.DateTime)
                        Dim prm3 As New SqlParameter("@Usuario_Captura", SqlDbType.VarChar)
                        Dim prm4 As New SqlParameter("@Usuario_Autorizo", SqlDbType.VarChar)
                        Dim prm5 As New SqlParameter("@Fecha_Caducidad", SqlDbType.DateTime)
                        Dim prm6 As New SqlParameter("@Monto", SqlDbType.Decimal)
                        Dim prm7 As New SqlParameter("@Status", SqlDbType.VarChar)
                        Dim prm8 As New SqlParameter("@Observaciones", SqlDbType.VarChar)
                        Dim prm9 As New SqlParameter("@Sucursal", SqlDbType.Int)
                        Dim prm10 As New SqlParameter("@suc_aplica", SqlDbType.Int)
                        Dim prm11 As New SqlParameter("@tipo", SqlDbType.Int)
                        Dim prm13 As New SqlParameter("@caja", SqlDbType.Int)

                        ' bigint, bigint,@Factura bigint, datetime , varchar(50)  varchar(50), datetime, decimal(18,0), varchar(50), varchar(max), int)
                        prm.Direction = ParameterDirection.Input
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm12.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm5.Direction = ParameterDirection.Input
                        prm6.Direction = ParameterDirection.Input
                        prm7.Direction = ParameterDirection.Input
                        prm8.Direction = ParameterDirection.Input
                        prm9.Direction = ParameterDirection.Input
                        prm10.Direction = ParameterDirection.Input
                        prm11.Direction = ParameterDirection.Input
                        prm13.Direction = ParameterDirection.Input

                        prm.Value = Me.Clv_NotadeCreditoTextBox.Text
                        prm1.Value = Me.ContratoTextBox.Text
                        prm2.Value = Me.Fecha_deGeneracionDateTimePicker.Text
                        If IsNumeric(Me.ComboBox3.SelectedValue) = False Then
                            prm12.Value = 0
                        Else
                            prm12.Value = Me.ComboBox3.SelectedValue
                        End If
                        prm3.Value = Me.ComboBox5.SelectedValue
                        prm4.Value = Me.ComboBox4.SelectedValue
                        prm5.Value = Me.Fecha_CaducidadDateTimePicker.Text
                        If IsNumeric(Me.MontoTextBox.Text) = False Then Me.MontoTextBox.Text = 0
                        prm6.Value = CDec(Me.MontoTextBox.Text)
                        prm7.Value = Me.ComboBox2.SelectedValue
                        prm8.Value = Me.ObservacionesTextBox.Text
                        prm9.Value = Me.ComboBox1.SelectedValue
                        prm10.Value = Me.ComboBox7.SelectedValue
                        prm11.Value = glotipoNota
                        prm13.Value = Me.ComboBox8.SelectedValue

                        .Parameters.Add(prm)
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm12)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm5)
                        .Parameters.Add(prm6)
                        .Parameters.Add(prm7)
                        .Parameters.Add(prm8)
                        .Parameters.Add(prm9)
                        .Parameters.Add(prm10)
                        .Parameters.Add(prm11)
                        .Parameters.Add(prm13)

                        Dim i As Integer = comando.ExecuteNonQuery()
                    End With
                    cone.Close()
                    '@Clv_Session bigint,@Clv_NotadeCredito bigint
                    If glotipoNota = 0 Or glotipoNota = 1 Then
                        If Factura_inicial <> Me.ComboBox3.SelectedValue Then
                            borra_detalle()
                            'cone.Open()
                            'With comando2
                            '    .Connection = cone
                            '    .CommandTimeout = 0
                            '    .CommandText = "Modifica_DetalleNota"
                            '    .CommandType = CommandType.StoredProcedure
                            '    Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                            '    prm.Direction = ParameterDirection.Input
                            '    prm.Value = Me.ComboBox3.SelectedValue
                            '    .Parameters.Add(prm)

                            '    Dim prm1 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                            '    prm1.Direction = ParameterDirection.Input
                            '    prm1.Value = MClv_Session
                            '    .Parameters.Add(prm1)

                            '    Dim prm2 As New SqlParameter("@Clv_NotadeCredito", SqlDbType.BigInt)
                            '    prm2.Direction = ParameterDirection.Input
                            '    prm2.Value = gloClvNota
                            '    .Parameters.Add(prm2)

                            '    Dim i As Integer = comando2.ExecuteNonQuery
                            'End With
                            'cone.Close()
                        End If


                        Dim J As Integer = 0
                        For J = 0 To Me.Detalle_NotasdeCreditoDataGridView.RowCount - 1
                            If Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(4).Value = True Then
                                Guarda_Grid(Me.ComboBox3.SelectedValue, gloClvNota, Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(0).Value, 1, Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(2).Value, MClv_Session)
                            Else
                                Guarda_Grid(Me.ComboBox3.SelectedValue, gloClvNota, Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(0).Value, 0, Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(2).Value, MClv_Session)
                            End If
                        Next J

                        'cone.Open()
                        'With comando2
                        '    .Connection = cone
                        '    .CommandTimeout = 0
                        '    .CommandText = "Guarda_DetalleNota"
                        '    .CommandType = CommandType.StoredProcedure
                        '    Dim prm As New SqlParameter("Clv_Factura", SqlDbType.BigInt)
                        '    Dim prm1 As New SqlParameter("Clv_Session", SqlDbType.BigInt)
                        '    Dim prm2 As New SqlParameter("Clv_NotadeCredito", SqlDbType.BigInt)
                        '    prm.Direction = ParameterDirection.Input
                        '    prm1.Direction = ParameterDirection.Input
                        '    prm2.Direction = ParameterDirection.Input
                        '    prm.Value = Me.ComboBox3.SelectedValue
                        '    prm1.Value = MClv_Session
                        '    prm2.Value = gloClvNota
                        '    .Parameters.Add(prm)
                        '    .Parameters.Add(prm1)
                        '    .Parameters.Add(prm2)
                        '    Dim i As Integer = comando2.ExecuteNonQuery
                        'End With
                        'cone.Close()


                        'borra_Predetalle(0)
                    End If
                    Guarda_Recalcula_Saldo_NotasdeCredito(gloClvNota)
                    'borra_Predetalle(0)
                    MsgBox("Se ha Guardado con �xito")
                    guardabitacora()
                    refrescar = True
                    Me.Close()
                End If
            Else
                MsgBox("El Monto Debe Ser Mayor a Cero", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub Guarda_Grid(ByVal MClv_Factura As Long, ByVal MClv_NotadeCredito As Long, ByVal MClv_Detalle As Long, ByVal MSeCobra As Integer, ByVal MImporte As Double, ByVal MiClv_Session As Long)
        'MiConexion = "Data Source=TeamEdgar-PC;Initial Catalog=Sahuayo ;Persist Security Info=True;User ID=sa;Password=06011975;Connect Timeout=0"
        Dim conn As New SqlConnection(MiConexion)
        Try

            Dim cmd As New SqlCommand("Guarda_PrDetalleNota", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim prm1 As New SqlParameter("@clv_factura", SqlDbType.BigInt)
            prm1.Direction = ParameterDirection.Input
            prm1.Value = MClv_Factura
            cmd.Parameters.Add(prm1)

            Dim prm2 As New SqlParameter("@Clv_NotadeCredito", SqlDbType.BigInt)
            prm2.Direction = ParameterDirection.Input
            prm2.Value = MClv_NotadeCredito
            cmd.Parameters.Add(prm2)

            Dim prm3 As New SqlParameter("@Clv_Detalle", SqlDbType.BigInt)
            prm3.Direction = ParameterDirection.Input
            prm3.Value = MClv_Detalle
            cmd.Parameters.Add(prm3)


            Dim prm4 As New SqlParameter("@SeCobra", SqlDbType.Int)
            prm4.Direction = ParameterDirection.Input
            prm4.Value = MSeCobra
            cmd.Parameters.Add(prm4)

            Dim prm5 As New SqlParameter("@Importe", SqlDbType.Money)
            prm5.Direction = ParameterDirection.Input
            prm5.Value = MImporte
            cmd.Parameters.Add(prm5)

            Dim prm6 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            prm6.Direction = ParameterDirection.Input
            prm6.Value = MiClv_Session
            cmd.Parameters.Add(prm6)

            conn.Open()
            Dim i As Integer = cmd.ExecuteNonQuery



            conn.Close()

        Catch ex As Exception

            If conn.State <> ConnectionState.Closed Then conn.Close()
            MsgBox(ex.Message)

        End Try
    End Sub

    Public Sub Guarda_Recalcula_Saldo_NotasdeCredito(ByVal MClv_NotadeCredito As Long)
        'MiConexion = "Data Source=TeamEdgar-PC;Initial Catalog=Sahuayo ;Persist Security Info=True;User ID=sa;Password=06011975;Connect Timeout=0"
        Dim conn As New SqlConnection(MiConexion)
        Try
            Dim cmd As New SqlCommand("Recalcula_Saldo_NotasdeCredito ", conn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim prm1 As New SqlParameter("@Clv_NotadeCredito", SqlDbType.BigInt)
            prm1.Direction = ParameterDirection.Input
            prm1.Value = MClv_NotadeCredito
            cmd.Parameters.Add(prm1)

            conn.Open()
            Dim i As Integer = cmd.ExecuteNonQuery
            conn.Close()
        Catch ex As Exception
            If conn.State <> ConnectionState.Closed Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub borra_detalle()
        Try


            Dim conLidia2 As New SqlClient.SqlConnection(MiConexion)
            Dim comando4 As New SqlClient.SqlCommand(MiConexion)
            conLidia2.Open()
            With comando4
                .Connection = conLidia2
                .CommandTimeout = 0
                .CommandText = "Borra_DetFactura_nota"
                .CommandType = CommandType.StoredProcedure
                Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Factura_inicial
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Clv_NotadeCredito", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = gloClvNota
                .Parameters.Add(prm1)

                Dim i As Integer = comando4.ExecuteNonQuery
            End With
            conLidia2.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub modifica_PreDetalle(ByVal factura2 As Integer, ByVal value As Integer, ByVal MClv_Session As Long)
        Try


            Dim conlidia As New SqlClient.SqlConnection(MiConexion)
            Dim comando As New SqlClient.SqlCommand
            conlidia.Open()
            With comando
                .Connection = conlidia
                .CommandText = "Modifica_DetalleNotas "
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                ' Create a SqlParameter for each parameter in the stored procedure.
                Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@Value", SqlDbType.Int)
                Dim prm2 As New SqlParameter("@Clv_detalle", SqlDbType.Int)
                Dim prm3 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Input

                prm.Value = factura2
                prm1.Value = value
                prm2.Value = Me.Detalle_NotasdeCreditoDataGridView.Item(0, Me.Detalle_NotasdeCreditoDataGridView.CurrentRow.Index).Value
                prm3.Value = MClv_Session
                .Parameters.Add(prm2)
                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm3)
                Dim i As Integer = comando.ExecuteNonQuery()
            End With
            conlidia.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Calcula_monto()
        Try
            If BndChange = False Then
                BndChange = True
                Dim Total As Double = 0
                Dim J
                For J = 0 To Me.Detalle_NotasdeCreditoDataGridView.RowCount - 1
                    If IsNumeric(Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(2).Value) = False Then Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(2).Value = 0
                    If Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(2).Value > Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(5).Value Then
                        Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(2).Value = Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(5).Value
                    End If
                    Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(2).ReadOnly = True
                    If Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(4).Value = True Then
                        Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(2).ReadOnly = False
                        If IsNumeric(Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(2).Value) = True Then
                            Total = Total + Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(2).Value
                        End If
                    Else
                        Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(2).Value = Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(J).Cells.Item(5).Value
                    End If
                Next J
                Me.MontoTextBox.Text = Format(Total, "##,##0.00")

                BndChange = False
            End If

            'monto1 = (Me.Detalle_NotasdeCreditoDataGridView.Item(2, Me.Detalle_NotasdeCreditoDataGridView.CurrentRow.Index).Value)

            ''Me.MontoTextBox.Text = Format(monto1, "##,##0.00")
            'Dim x As Double
            'Dim conlidia As New SqlClient.SqlConnection(MiConexion)
            'Dim comando As New SqlClient.SqlCommand
            'conlidia.Open()
            'With comando
            '    .Connection = conlidia
            '    .CommandText = "Calcula_monto "
            '    .CommandType = CommandType.StoredProcedure
            '    .CommandTimeout = 0
            '    ' Create a SqlParameter for each parameter in the stored procedure.
            '    Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
            '    Dim prm2 As New SqlParameter("@Monto", SqlDbType.Money)
            '    Dim prm3 As New SqlParameter("@Opc", SqlDbType.Int)
            '    Dim prm4 As New SqlParameter("@clv_session", SqlDbType.BigInt)

            '    prm.Direction = ParameterDirection.Input
            '    prm2.Direction = ParameterDirection.Output
            '    prm3.Direction = ParameterDirection.Input
            '    prm4.Direction = ParameterDirection.Input

            '    prm.Value = fac
            '    prm2.Value = 0
            '    prm3.Value = opc
            '    prm4.Value = MClv_Session

            '    .Parameters.Add(prm)
            '    .Parameters.Add(prm2)
            '    .Parameters.Add(prm3)
            '    .Parameters.Add(prm4)

            '    Dim i As Integer = comando.ExecuteNonQuery()
            '    x = prm2.Value
            '    Me.MontoTextBox.Text = Format(x, "##,##0.00")
            'End With
            'conlidia.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ComboBox3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox3.GotFocus
        Detalle_NotasdeCreditoDataGridView.EndEdit()
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        Try

            'If IsNumeric(factura) = True Then
            '    If factura <> Me.ComboBox3.SelectedValue Then
            '        'borra_Predetalle(0)
            '    End If
            'End If
            factura = Me.ComboBox3.SelectedValue
            If IsNumeric(Me.ComboBox3.SelectedValue) = True Then
                Muestra_Predetalle(Me.ComboBox3.SelectedValue)
                'Calcula_monto(Me.ComboBox3.SelectedValue, 0)
                'Me.TextBox1.Text = CDec(Me.MontoTextBox.Text)
            Else
                Muestra_Predetalle(0)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    'Private Sub Detalle_NotasdeCreditoDataGridView_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Detalle_NotasdeCreditoDataGridView.CellClick
    '    Try

    '        Dim bol As String
    '        If e.ColumnIndex = 0 Then
    '            bol = Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value.ToString

    '            If Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value.ToString = "False" Then
    '                Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value = True
    '                modifica_PreDetalle(Me.ComboBox3.SelectedValue, 1)
    '                Calcula_monto(Me.ComboBox3.SelectedValue, 0)
    '                'If Me.ComboBox3.Enabled = True Then
    '                'monto1 = (Me.Detalle_NotasdeCreditoDataGridView.Item(2, Me.Detalle_NotasdeCreditoDataGridView.CurrentRow.Index).Value)
    '                'Me.MontoTextBox.Text = Format(monto1, "##,##0.00")
    '                Me.TextBox1.Text = CDec(Me.MontoTextBox.Text)
    '                'End If
    '            ElseIf Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value.ToString = "True" Then
    '                Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value = False
    '                modifica_PreDetalle(Me.ComboBox3.SelectedValue, 0)
    '                Calcula_monto(Me.ComboBox3.SelectedValue, 0)
    '                'If Me.ComboBox3.Enabled = True Then
    '                'monto1 = 0
    '                'Me.MontoTextBox.Text = Format(monto1, "##,##0.00")
    '                Me.TextBox1.Text = CDec(Me.MontoTextBox.Text)
    '                'End If
    '            End If
    '            '    If Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value.ToString = "True" Then
    '            '        Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value = False
    '            '        modifica_PreDetalle(Me.ComboBox3.SelectedValue, 1)
    '            '        'Calcula_monto(Me.ComboBox3.SelectedValue, 0)
    '            '        'If Me.ComboBox3.Enabled = True Then
    '            '        suma_totales(1)
    '            '        Me.TextBox1.Text = CDec(Me.MontoTextBox.Text)
    '            '        'End If
    '            '    ElseIf (Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value.ToString) = "False" Then
    '            '        Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value = True
    '            '        modifica_PreDetalle(Me.ComboBox3.SelectedValue, 0)
    '            '        'Calcula_monto(Me.ComboBox3.SelectedValue, 0)
    '            '        'If Me.ComboBox3.Enabled = True Then
    '            '        suma_totales(0)
    '            '        Me.TextBox1.Text = CDec(Me.MontoTextBox.Text)
    '            '        'End If
    '            '    End If
    '        End If

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '        Exit Sub
    '    End Try
    'End Sub

    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox4.TextChanged
        factura = Me.TextBox4.Text
    End Sub

    Private Sub ComboBox7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        Detalle_NotasdeCreditoDataGridView.EndEdit()
    End Sub
    Private Sub ComboBox7_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try


            'Dim conLidia As New SqlClient.SqlConnection(MiConexion)
            'conLidia.Open()
            'Me.MUESTRACAJAS2TableAdapter.Connection = conLidia
            'Me.MUESTRACAJAS2TableAdapter.Fill(Me.DataSetLydia.MUESTRACAJAS2, Me.ComboBox7.SelectedValue)
            'conLidia.Close()
            If IsNumeric(ComboBox7.SelectedValue) = True Then
                MUESTRACAJA2()
            End If
            If OPCION = "N" And IdSistema <> "VA" Then
                Me.ComboBox8.Text = ""
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ComboBox8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        Detalle_NotasdeCreditoDataGridView.EndEdit()
    End Sub

    Private Sub ComboBox8_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try


            'Dim conLidia2 As New SqlClient.SqlConnection(MiConexion)
            'conLidia2.Open()
            'Me.MUESTRACAJERASTableAdapter.Connection = conLidia2
            'Me.MUESTRACAJERASTableAdapter.Fill(Me.DataSetLydia.MUESTRACAJERAS, 0)
            'conLidia2.Close()
            If IsNumeric(ComboBox8.SelectedValue) = True Then
                MUESTRACAJERAS()
                If OPCION = "M" Or OPCION = "C" Then
                    MUESTRCAJERONOTAS()
                End If
            End If

            If OPCION = "N" And IdSistema <> "VA" Then
                Me.ComboBox5.Text = ""
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged
        Try


            'If IsNumeric(factura) = True Then
            'borra_Predetalle(factura)
            ' borra_Predetalle(Me.TextBox5.Text)
            'End If
            factura = Me.TextBox5.Text
            'If IsNumeric(Me.TextBox5.Text) = True Then
            'Muestra_Predetalle(Me.TextBox5.Text)
            'Calcula_monto(Me.TextBox5.Text, 0)
            'If IsNumeric(Me.MontoTextBox.Text) = True Then Me.MontoTextBox.Text = 0
            'Me.TextBox1.Text = CDec(Me.MontoTextBox.Text)
            'End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub TextBox6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox6.TextChanged
        Try


            'Dim conlidia As New SqlClient.SqlConnection(MiConexion)
            'conlidia.Open()
            'Me.MUESTRACAJERASTableAdapter.Connection = conlidia
            'Me.MUESTRACAJERASTableAdapter.Fill(Me.DataSetLydia.MUESTRACAJERAS, 0)
            'Me.ComboBox5.SelectedValue = Me.TextBox6.Text
            'conlidia.Close()
            MUESTRACAJERAS()
            Me.ComboBox5.SelectedValue = Me.TextBox6.Text
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Me.Panel1.Enabled = True
        Me.Panel3.Enabled = True
        Me.Button5.Enabled = True
        Me.Panel3.Visible = True

    End Sub

    'Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
    '    Me.Panel1.Enabled = False
    '    Me.Panel3.Visible = False
    'End Sub

    'Private Sub ComboBox9_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox9.SelectedIndexChanged
    '    Try


    '        Dim conFact As New SqlConnection(MiConexion)

    '        If IsNumeric(Me.ComboBox9.SelectedValue) = True Then
    '            conFact.Open()
    '            Me.MuestraServicios_por_TipoTableAdapter.Connection = conFact
    '            Me.MuestraServicios_por_TipoTableAdapter.Fill(Me.DataSetLydia.MuestraServicios_por_Tipo, Me.ComboBox9.SelectedValue)
    '            conFact.Close()
    '        End If
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    'Private Sub ComboBox10_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox10.SelectedIndexChanged
    '    If IsNumeric(Me.ComboBox10.SelectedValue) = True Then
    '        Dame_Datos_Servicio(Me.ComboBox10.SelectedValue)
    '    End If
    'End Sub
    Private Sub Dame_Datos_Servicio(ByVal clv_serv As Integer)
        Try



            Dim confact2 As New SqlConnection(MiConexion)

            Dim Cmd As New SqlCommand
            Dim reader As SqlDataReader
            confact2.Open()
            With Cmd
                .CommandText = "Dame_DatosServicio"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = confact2
                Dim prm As New SqlParameter("@clv_servicio", SqlDbType.Int)
                Dim prm2 As New SqlParameter("@clv_tipser", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm.Value = clv_serv
                prm2.Value = Me.ComboBox9.SelectedValue
                .Parameters.Add(prm)
                .Parameters.Add(prm2)
                reader = .ExecuteReader()
                Using reader
                    While reader.Read
                        clave_txt = reader.GetValue(0)
                        Me.TextBox9.Text = reader.GetValue(1)
                    End While
                End Using

            End With
            confact2.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub busca_detalleNota()
        Try


            Dim ConFact4 As New SqlConnection(MiConexion)
            Dim Cmd4 As New SqlCommand
            ConFact4.Open()
            Me.MuestraDetalleNota_por_ConceptoTableAdapter.Connection = ConFact4
            Me.MuestraDetalleNota_por_ConceptoTableAdapter.Fill(Me.DataSetLydia.MuestraDetalleNota_por_Concepto, Me.Clv_NotadeCreditoTextBox.Text)
            ConFact4.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Try


            Agregar_Conceptos(0)
            busca_detalleNota()
            'Calcula_monto(Me.Clv_NotadeCreditoTextBox.Text, 1)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Agregar_Conceptos(ByVal opc As Integer)
        Try


            Dim conFact3 As New SqlConnection(MiConexion)
            Dim Cmd3 As New SqlCommand
            conFact3.Open()
            With Cmd3
                .CommandText = "Agregar_Conceptos_X_Nota"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = conFact3
                Dim prm1 As New SqlParameter("@clv_NotaCredito", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@clv_Servicio", SqlDbType.Int)
                Dim prm3 As New SqlParameter("@clv_Tipser", SqlDbType.Int)
                Dim prm4 As New SqlParameter("@Precio", SqlDbType.Money)
                Dim prm5 As New SqlParameter("@opc", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Input
                prm4.Direction = ParameterDirection.Input
                prm5.Direction = ParameterDirection.Input
                prm1.Value = Me.Clv_NotadeCreditoTextBox.Text
                prm2.Value = Me.ComboBox10.SelectedValue
                prm3.Value = Me.ComboBox9.SelectedValue
                prm4.Value = Me.TextBox9.Text
                prm5.Value = opc
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)
                .Parameters.Add(prm4)
                .Parameters.Add(prm5)
                Dim i As Integer = .ExecuteNonQuery
            End With
            conFact3.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Borra_conceptos()
        Try


            Dim conFact5 As New SqlConnection(MiConexion)
            Dim Cmd5 As New SqlCommand
            conFact5.Open()
            With Cmd5
                .CommandText = "Borrar_Conceptos_X_Nota"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = conFact5
                Dim prm1 As New SqlParameter("@clv_Detalle", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = Me.TextBox10.Text
                .Parameters.Add(prm1)
                Dim i As Integer = .ExecuteNonQuery
            End With
            conFact5.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Try


            Borra_conceptos()
            busca_detalleNota()
            'Calcula_monto(Me.Clv_NotadeCreditoTextBox.Text, 1)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TextBox9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox9.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Agregar_Conceptos(0)
            busca_detalleNota()
            'Calcula_monto(Me.Clv_NotadeCreditoTextBox.Text, 1)
        End If
    End Sub
    Private Sub suma_totales(ByVal op As Integer)
        Try


            If op = 1 Then

                monto1 = (Me.Detalle_NotasdeCreditoDataGridView.Item(2, Me.Detalle_NotasdeCreditoDataGridView.CurrentRow.Index).Value)
                Me.MontoTextBox.Text = Format(monto1, "##,##0.00")
            ElseIf op = 0 Then

                monto1 = 0
                Me.MontoTextBox.Text = Format(monto1, "##,##0.00")

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Function Sumar() As Double


        Dim total As Double = 0
        Try
            For i As Integer = 0 To Detalle_NotasdeCreditoDataGridView.RowCount - 1
                If CDbl(Me.Detalle_NotasdeCreditoDataGridView.Item(2, i).Value) <= sub_totales(i) Then
                    total = total + CDbl(Me.Detalle_NotasdeCreditoDataGridView.Item(2, i).Value)
                Else
                    MsgBox("Error la cantida es mayor", MsgBoxStyle.Information)
                End If
            Next

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try

        Return total

    End Function

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Calcula_monto()
        'MsgBox(Me.Detalle_NotasdeCreditoDataGridView.Item(4, Me.Detalle_NotasdeCreditoDataGridView.CurrentRow.Index).Value)
        'Dim total As Double = 0
        'Total_sum = Total_sumAux
        'For i As Integer = 0 To Detalle_NotasdeCreditoDataGridView.RowCount - 1
        '    If (Me.Detalle_NotasdeCreditoDataGridView.Item(4, i).Value) = "True" Then
        '        'Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value = False
        '        modifica_PreDetalle(Me.ComboBox3.SelectedValue, 1, MClv_Session)
        '        'Calcula_monto(Me.ComboBox3.SelectedValue, 0)
        '        'If Me.ComboBox3.Enabled = True Then

        '        Try

        '            If CDbl(Me.Detalle_NotasdeCreditoDataGridView.Item(2, i).Value) <= sub_totales(i) Then
        '                total = total + CDbl(Me.Detalle_NotasdeCreditoDataGridView.Item(2, i).Value)
        '            Else
        '                MsgBox("Error la cantida es mayor", MsgBoxStyle.Information)
        '                If IsNumeric(factura) = True Then
        '                    borra_Predetalle(factura)
        '                    borra_Predetalle(Me.ComboBox3.SelectedValue)
        '                End If
        '                factura = Me.ComboBox3.SelectedValue
        '                'if IsNumeric(Me.ComboBox3.SelectedValue) = True Then
        '                'Muestra_Predetalle(Me.ComboBox3.SelectedValue)
        '                'End If
        '            End If


        '        Catch ex As Exception
        '            MsgBox(ex.Message.ToString)
        '        End Try
        '        Me.MontoTextBox.Text = Format(total, "##,##0.00")
        '        Totales = Total_sum - total
        '        Me.TextBox1.Text = Format(Totales, "##,##0.00")
        '        'End If
        '    ElseIf (Me.Detalle_NotasdeCreditoDataGridView.Item(4, i).Value) = "False" Then
        '        'Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells(0).Value = True
        '        modifica_PreDetalle(Me.ComboBox3.SelectedValue, 0, MClv_Session)
        '        'Calcula_monto(Me.ComboBox3.SelectedValue, 0)
        '        'If Me.ComboBox3.Enabled = True Then
        '        'suma_totales(0)
        '        Me.MontoTextBox.Text = Format(total, "##,##0.00")
        '        Totales = Total_sum - total
        '        Me.TextBox1.Text = Format(Totales, "##,##0.00")
        '    End If
        'Next
    End Sub
    Private Sub MUESTRASUCURSALES2()



        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MUESTRASUCURSALES2 '0'")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBox7.DataSource = bindingSource
            Me.ComboBox1.DataSource = bindingSource
            'Me.CONTRATOLabel1.Text = CStr(Folio)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub MUESTRACAJA2()
        If IsNumeric(Me.ComboBox7.SelectedValue) = True Then
            Dim conexion As New SqlConnection(MiConexion)
            Dim strSQL As New StringBuilder
            strSQL.Append("EXEC MUESTRACAJAS2 ")
            strSQL.Append(CStr(Me.ComboBox7.SelectedValue))


            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            Try
                conexion.Open()
                dataAdapter.Fill(dataTable)
                bindingSource.DataSource = dataTable
                Me.ComboBox8.DataSource = bindingSource
                'Me.CONTRATOLabel1.Text = CStr(Folio)
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Finally
                conexion.Close()
                conexion.Dispose()
            End Try
        End If
    End Sub
    Private Sub MUESTRACAJERAS()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MUESTRACAJERAS ")
        'strSQL.Append(CStr(Me.ComboBox8.SelectedValue))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBox5.DataSource = bindingSource
            'Me.CONTRATOLabel1.Text = CStr(Folio)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub DAME_FACTURASDECLIENTE()
        borra_Predetalle(0)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        Dim CmiContrato As Long = 0

        If IsNumeric(Me.ContratoTextBox.Text) = False Then CmiContrato = 0 Else CmiContrato = Me.ContratoTextBox.Text
        If IsNumeric(gloClvNota) = False Then gloClvNota = 0

        strSQL.Append("EXEC DAME_FACTURASDECLIENTE  ")
        strSQL.Append((CmiContrato) & ",")
        strSQL.Append((gloClvNota))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            'Me.ComboBox11.DataSource = bindingSource
            Me.ComboBox3.DataSource = bindingSource
            'Me.CONTRATOLabel1.Text = CStr(Folio)
            If Me.ComboBox3.Items.Count > 0 Then
                'Me.Panel1.Enabled = True
                Muestra_Predetalle(Me.ComboBox3.SelectedValue)
            Else
                Muestra_Predetalle(0)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Detalle_NotasdeCreditoDataGridView_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Detalle_NotasdeCreditoDataGridView.CellContentClick

        If Detalle_NotasdeCreditoDataGridView.IsCurrentCellDirty Then
            Detalle_NotasdeCreditoDataGridView.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If

        Calcula_monto()
        'Me.Detalle_NotasdeCreditoDataGridView.Rows(e.RowIndex).Cells("Seleccion").Value

    End Sub

    'Private Sub Detalle_NotasdeCredito()
    '    Dim conexion As New SqlConnection(MiConexion)
    '    Dim strSQL As New StringBuilder

    '    ReDim sub_totales(20)

    '    strSQL.Append("EXEC Detalle_NotasdeCredito ")
    '    strSQL.Append(CStr(factura))



    '    Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
    '    Dim dataTable As New DataTable
    '    Dim bindingSource As New BindingSource

    '    Try
    '        conexion.Open()
    '        dataAdapter.Fill(dataTable)
    '        bindingSource.DataSource = dataTable
    '        Me.Detalle_NotasdeCreditoDataGridView.DataSource = bindingSource

    '        For i As Integer = 0 To Detalle_NotasdeCreditoDataGridView.RowCount - 1

    '            sub_totales(i) = Me.Detalle_NotasdeCreditoDataGridView.Item(2, i).Value

    '        Next

    '        Total_sumAux = Sumar()


    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
    '    Finally
    '        conexion.Close()
    '        conexion.Dispose()
    '    End Try
    'End Sub



    Private Sub Detalle_NotasdeCreditoDataGridView_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Detalle_NotasdeCreditoDataGridView.CellEndEdit
        Calcula_monto()
    End Sub

    Private Sub Detalle_NotasdeCreditoDataGridView_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Detalle_NotasdeCreditoDataGridView.CellLeave
        Calcula_monto()
    End Sub


    Private Sub Detalle_NotasdeCreditoDataGridView_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Detalle_NotasdeCreditoDataGridView.CellValueChanged
        If IsNumeric(e.RowIndex) = True And BndChange = False Then
            If e.RowIndex > 0 Then
                If Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(e.RowIndex).Cells.Item(4).Value = False Then
                    Me.Detalle_NotasdeCreditoDataGridView.Rows.Item(e.RowIndex).Cells.Item(2).ReadOnly = True
                End If
            End If
        End If

    End Sub





    Private Sub Detalle_NotasdeCreditoDataGridView_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detalle_NotasdeCreditoDataGridView.LostFocus

    End Sub

    Private Sub Button8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button8.GotFocus
        Detalle_NotasdeCreditoDataGridView.EndEdit()
    End Sub

    Private Sub Button3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.GotFocus
        Detalle_NotasdeCreditoDataGridView.EndEdit()
    End Sub

    Private Sub Button1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.GotFocus
        Detalle_NotasdeCreditoDataGridView.EndEdit()
    End Sub

    Private Sub ObservacionesTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        Detalle_NotasdeCreditoDataGridView.EndEdit()
    End Sub

    Private Sub ObservacionesTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.GotFocus
        Detalle_NotasdeCreditoDataGridView.EndEdit()
    End Sub

    Private Sub ComboBox5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        Detalle_NotasdeCreditoDataGridView.EndEdit()
    End Sub

    Private Sub MUESTRCAJERONOTAS()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("MUESTRCAJERONOTAS", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@clv_notacredito", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = gloClvNota
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@CLV_USUARIO", SqlDbType.VarChar, 6)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            Me.ComboBox5.SelectedValue = CStr(PRM2.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub DAMESALDONOTACREDITO()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("DAMESALDONOTACREDITO", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@clv_notacredito", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = gloClvNota
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@SALDO", SqlDbType.Money)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            'Me.TextBox1.Text = CStr(PRM2.Value)
            Me.TextBox1.Text = Format(PRM2.Value, "##,##0.00")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub ContratoTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ContratoTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Despliega_Datos_Del_cliente()
        End If
    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged

    End Sub

    Private Sub MiPanel_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MiPanel.Paint

    End Sub

    Private Sub ComboBox7_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox7.SelectedIndexChanged
            MUESTRACAJA2()
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged

    End Sub
End Class