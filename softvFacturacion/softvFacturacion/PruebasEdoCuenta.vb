Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Public Class PruebasEdoCuenta

    Private Sub PruebasEdoCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        muestraEdoCuenta()
    End Sub
    Private Sub muestraEdoCuenta()
        Try

            Me.Text = "Reporte Estado de Cuenta."

            'customersByCityReport = New EstadoDeCuentaFinal
            Dim customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            Dim reportPath As String = Nothing

            'Concatenamos la ruta de los reportes
            reportPath = "//EYENNY-PC/Exes/NewSoftvFacturacion/Reportes/Estado_De_Cuenta.rpt"
            'reportPath = RutaReportesLocal & "EdoCuentaFinal_BannersDinamicos.rpt"
            customersByCityReport.Load(reportPath)

            Dim connection As IConnectionInfo
            'Dim serverName1 As String = GlobalServerName
            'Dim serverName1 As String = "192.168.1.114"
            'Dim serverName1 As String = "10.4.247.10"
            Dim serverName1 As String = "EYENNY-PC"


            ' Establecer conexi�n con base de datos al informe principal
            For Each connection In customersByCityReport.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetConnection("EYENNY-PC", "NewSoftv", False)
                        connection.SetLogon("sa", "06011975")
                End Select
            Next

            ' Establecer conexi�n al subinforme
            Dim subreport As ReportDocument
            For Each subreport In customersByCityReport.Subreports
                For Each connection In subreport.DataSourceConnections
                    connection.SetConnection("EYENNY-PC", "NewSoftv", False)
                    connection.SetLogon("sa", "06011975")
                Next
            Next


            'customersByCityReport.SetParameterValue(0, 11)   '--------------------------agregada----------------------------
            'customersByCityReport.SetParameterValue(1, 11)   '--------------------------agregada----------------------------

            ''@contrato1
            'customersByCityReport.SetParameterValue(2, 6)
            ''contrato2
            'customersByCityReport.SetParameterValue(3, 11)


            'Cargamos el Reporte en base al periodo de cobro establecido por el usuario
            customersByCityReport.SetParameterValue(0, 1)
            customersByCityReport.SetParameterValue(1, GloContrato)
            customersByCityReport.SetParameterValue(2, GloContrato)
            customersByCityReport.SetParameterValue(3, GloContrato)

            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(110)

        Catch ex As Exception
            MsgBox("Ocurrio un error al cargar el reporte, parece ser que no se encontr� el reporte � la ruta del mismo no existe. Verifica con tu administrador que el reporte exista y se encuentre en su lugar por defecto.", MsgBoxStyle.Critical)
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CrystalReportViewer1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CrystalReportViewer1.Load

    End Sub
End Class