Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class BWRFACTURAGLOBAL
    Dim Letra2 As String = Nothing
    Dim Importe As String = Nothing
    Private customersByCityReport As ReportDocument
    Dim importeTotal As Decimal


    Dim SubTotal, Ieps, Iva, Total, SubTotalIeps As Double

    Private Sub CancelaFacturaDigitalGlobal(ByVal LocClv_FacturaGlobal As Integer)
        FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
        Dim identi As Integer = 0
        FacturacionDigitalSoftv.ClassCFDI.Locop = 1
        FacturacionDigitalSoftv.ClassCFDI.DameId_FacturaCDF(LocClv_FacturaGlobal, "G", MiConexion)
        Try
            If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                FacturacionDigitalSoftv.ClassCFDI.Dime_Aque_Compania_FacturarleGlobal(LocClv_FacturaGlobal, MiConexion)
                FacturacionDigitalSoftv.ClassCFDI.Cancelacion_FacturaCFD(FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD, MiConexion)
            Else
                MsgBox("No se genero la factura digital cancele y vuelva a intentar por favor")
            End If
            'FormPruebaDigital.Show()
        Catch ex As Exception
        End Try
        FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
        FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""
    End Sub

    Private Sub ImprimeFacturaDigitalGlobal(ByVal LocClv_FacturaGlobal As Integer)
        FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
        Dim identi As Integer = 0
        FacturacionDigitalSoftv.ClassCFDI.Locop = 1
        FacturacionDigitalSoftv.ClassCFDI.DameId_FacturaCDF(LocClv_FacturaGlobal, "G", MiConexion)
        Try
            If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                Dim frm As New FacturacionDigitalSoftv.FrmImprimir
                frm.ShowDialog()
            Else
                MsgBox("No se genero la factura digital cancele y vuelva a intentar por favor")
            End If
            'FormPruebaDigital.Show()
        Catch ex As Exception
        End Try

        Dim r As New Globalization.CultureInfo("es-MX")
        r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
        System.Threading.Thread.CurrentThread.CurrentCulture = r

        FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
        FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""
    End Sub

    Private Sub busqueda(ByVal op As Integer)
        Try
            '    Dim CON As New SqlConnection(MiConexion)
            '    CON.Open()
            '    Select Case op
            '        Case 0
            '            Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
            '            Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, Me.TextBox1.Text, Me.TextBox4.Text, "01/01/1900", "", op, bec_tipo)
            '        Case 1
            '            Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
            '            Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", Me.TextBox2.Text, "", op, bec_tipo)
            '        Case 2
            '            Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
            '            Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", Me.TextBox3.Text, op, bec_tipo)
            '        Case 3
            '            Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
            '            Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", "", op, bec_tipo)
            '        Case 4
            '            Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
            '            Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", "", op, bec_tipo)
            '    End Select
            '    CON.Close()
            'Catch ex As System.Exception
            '    System.Windows.Forms.MessageBox.Show(ex.Message)
            'End Try
            Dim oIdCompania As Long = 1
            Select Case op
                Case 0
                    '@ClvCompania int,@Serie varchar(10), @factura varchar(50), @fecha DateTime, @sucursal  varchar(150),@Op int,@tipo varchar(1)
                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, Me.TextBox1.Text, Me.TextBox4.Text, "01/01/1900", "", op, bec_tipo)
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, oIdCompania)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, Me.TextBox1.Text)
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, Me.TextBox4.Text)
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "")
                    Me.BuscaFacturasGlobalesDataGridView.DataSource = BaseII.ConsultaDT("usp_BuscaFacturasGlobales_New")

                Case 1

                    ' Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", Me.TextBox2.Text, "", op, bec_tipo)
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, oIdCompania)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, Me.TextBox2.Text)
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "")
                    Me.BuscaFacturasGlobalesDataGridView.DataSource = BaseII.ConsultaDT("usp_BuscaFacturasGlobales_New")
                Case 2
                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", Me.TextBox3.Text, op, bec_tipo)
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, oIdCompania)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "")
                    Me.BuscaFacturasGlobalesDataGridView.DataSource = BaseII.ConsultaDT("usp_BuscaFacturasGlobales_New")
                Case 3

                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", "", op, bec_tipo)
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, oIdCompania)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "")
                    Me.BuscaFacturasGlobalesDataGridView.DataSource = BaseII.ConsultaDT("usp_BuscaFacturasGlobales_New")
                Case 4

                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", "", op, bec_tipo)
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, oIdCompania)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "")
                    Me.BuscaFacturasGlobalesDataGridView.DataSource = BaseII.ConsultaDT("usp_BuscaFacturasGlobales_New")
                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales
            End Select
            BuscaFacturasGlobalesDataGridView.Columns(0).HeaderText = "Serie"
            BuscaFacturasGlobalesDataGridView.Columns(1).HeaderText = "Factura"
            BuscaFacturasGlobalesDataGridView.Columns(2).HeaderText = "Fecha"
            BuscaFacturasGlobalesDataGridView.Columns(3).HeaderText = "Cajera(o)"
            BuscaFacturasGlobalesDataGridView.Columns(4).HeaderText = "Importe"
            BuscaFacturasGlobalesDataGridView.Columns(5).HeaderText = "Cancelada"
            BuscaFacturasGlobalesDataGridView.Columns(6).Visible = False
            BuscaFacturasGlobalesDataGridView.Columns(7).Visible = False
            BuscaFacturasGlobalesDataGridView.Columns(8).HeaderText = "Sucursal"


        Catch ex As Exception
            ' MsgBox(Err.Description, MsgBoxStyle.Exclamation, "Error")

        End Try

    End Sub

    Private Sub ConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal importe2 As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, _
                                                     ByVal Factura2 As String)
        Try

            DameImporteFacturaGlobal(Today, Today, 0, "", CLng(Me.CMBIdFacturaTextBox.Text), 2)

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim cliente2 As String = "P�blico en General"
            Dim concepto2 As String = "Ingreso por Pago de Servicios"
            Dim txtsubtotal As String = Nothing
            Dim subtotal2 As Double
            Dim iva2 As Double
            Dim myString As String = iva2.ToString("00.00")
            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
            customersByCityReport.Load(reportPath)

            customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
            customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & Format(CDec(SubTotal.ToString()), "####0.00") & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTotal").Text = "'" & Format(CDec(SubTotal.ToString()), "####0.00") & "'"
            customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & Format(CDec(Iva.ToString()), "####0.00") & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ieps").Text = "'" & Format(CDec(Ieps.ToString()), "####0.00") & "'"
            customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & Format(CDec(importeTotal.ToString()), "####0.00") & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTotalConIeps").Text = "'" & Format(CDec(SubTotalIeps.ToString()), "####0.00") & "'"

            customersByCityReport.PrintOptions.PrinterName = impresorafiscal

            MsgBox("Se va a imprimir una Factura Fiscal. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            MsgBox("Se va a imprimir la copia de la Factura Fiscal anterior. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub DameImporteFacturaGlobal(ByVal Fecha As DateTime, ByVal FechaFin As DateTime, ByVal SelSucursal As Integer, ByVal Tipo As Char, ByVal idFactura As Long, ByVal Op As Integer)

        Try
            Dim conexion As New SqlConnection(MiConexion)
            Dim strSQL As New StringBuilder
            strSQL.Append("EXEC DameImporteFacturaGlobal ")
            strSQL.Append("'" & CStr(Fecha) & "', ")
            strSQL.Append("'" & CStr(FechaFin) & "', ")
            strSQL.Append(CStr(SelSucursal) & ", ")
            strSQL.Append("'" & Tipo & "', ")
            strSQL.Append(CStr(idFactura) & ", ")
            strSQL.Append(CStr(Op))
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
            Dim dataTable As New DataTable



            dataAdapter.Fill(dataTable)
            importeTotal = Format(CDec(dataTable.Rows(0)(0).ToString()), "#####0.0000")
            SubTotal = Format(CDec(dataTable.Rows(0)(1).ToString()), "#####0.0000")
            Iva = Format(CDec(dataTable.Rows(0)(2).ToString()), "#####0.0000")
            Ieps = Format(CDec(dataTable.Rows(0)(3).ToString()), "#####0.0000")
            SubTotalIeps = SubTotal + Ieps

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Private Sub BWRFACTURAGLOBAL_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bec_bandera = 1 Then
            bec_bandera = 0
            busqueda(4)
        End If
    End Sub
    Private Sub CantidadaLetra()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameCantidadALetraTableAdapter.Connection = CON
            Me.DameCantidadALetraTableAdapter.Fill(Me.NewsoftvDataSet2.DameCantidadALetra, CDec(Importe), 0, Letra2)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BWRFACTURAGLOBAL_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.DameFechadelServidorHora' Puede moverla o quitarla seg�n sea necesario.
        Me.DameFechadelServidorHoraTableAdapter.Connection = CON
        Me.DameFechadelServidorHoraTableAdapter.Fill(Me.NewsoftvDataSet2.DameFechadelServidorHora)
        ''TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.MUESTRATIPOFACTGLO' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRATIPOFACTGLOTableAdapter.Connection = CON
        Me.MUESTRATIPOFACTGLOTableAdapter.Fill(Me.NewsoftvDataSet2.MUESTRATIPOFACTGLO)
        CON.Close()
        bec_tipo = Me.ComboBox2.SelectedValue
        busqueda(4)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        busqueda(0)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Me.TextBox2.Text = String.Format("dd/mm/yyyy")
        busqueda(1)
    End Sub
    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 And Me.TextBox4.Text = "" Then
            MsgBox("Selecciona Primero la Factura")
        ElseIf (Asc(e.KeyChar) = 13) Then
            busqueda(0)
        End If
    End Sub
    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        If (Asc(e.KeyChar) = 13) And Me.TextBox1.Text = "" Then
            MsgBox("Selecciona Primero la Serie")
        ElseIf (Asc(e.KeyChar) = 13) Then
            busqueda(0)
        End If
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(2)
        End If
    End Sub
    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(1)
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Gloop = "N"
        LiTipo = 4
        bnd = 1
        CAPTURAFACTURAGLOBAL.Show()
    End Sub
    Private Sub BuscaFacturasGlobalesDataGridView_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BuscaFacturasGlobalesDataGridView.CurrentCellChanged
        LlenaVariablesFila()
    End Sub

    Public Sub LlenaVariablesFila()
        Try

            LocSerieglo = BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value
            LocFacturaGlo = Me.BuscaFacturasGlobalesDataGridView.SelectedCells(1).Value
            LocFechaGlo = FormatDateTime(Me.BuscaFacturasGlobalesDataGridView.SelectedCells(2).Value, DateFormat.ShortDate)
            LocCajeroGlo = Me.BuscaFacturasGlobalesDataGridView.SelectedCells(3).Value
            LocImporteGlo = Me.BuscaFacturasGlobalesDataGridView.SelectedCells(4).Value
            'Me.Dame_clv_sucursalTableAdapter.Fill(Me.Procedimientos_arnoldo.Dame_clv_sucursal, Me.BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value, Locclv_sucursalglo)
            LocFechaGloFinal = LocFechaGlo
            bec_factura = Me.BuscaFacturasGlobalesDataGridView.SelectedCells(6).Value
            CMBSerieTextBox.Text = LocSerieglo
            Me.FacturaLabel1.Text = LocFacturaGlo
            Me.CMBFechaTextBox.Text = LocFechaGlo.ToShortDateString
            Me.CMBCajeraTextBox.Text = LocCajeroGlo
            Me.CMBCanceladaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(5).Value
            Me.CMBIdFacturaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(6).Value
            Me.CMBImporteTextBox.Text = FormatCurrency(LocImporteGlo, 2, TriState.True)
            Me.Label6.Text = LocSucursal
            'GloFacGlobalclvcompania = BuscaFacturasGlobalesDataGridView.SelectedCells(7).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        'If Me.CMBCanceladaTextBox.Text = 1 Then
        '    MsgBox("La Factura ya ha sido Cancelada con Anterioridad")
        'Else 'If Me.DateTimePicker1.Text = Me.CMBFechaTextBox.Text Then
        '    Dim resp As MsgBoxResult = MsgBoxResult.Cancel
        '    resp = MsgBox("� Esta Seguro de que Desea Cancelar la Factura Global con Serie: " + Me.CMBSerieTextBox.Text + " y Folio:" + Me.FacturaLabel1.Text + " ?", MsgBoxStyle.YesNo)
        '    If resp = MsgBoxResult.Yes Then
        '        bec_bandera = 1
        '        Dim CON As New SqlConnection(MiConexion)
        '        CON.Open()
        '        Me.CANCELAFACTGLOBALTableAdapter.Connection = CON
        '        Me.CANCELAFACTGLOBALTableAdapter.Fill(Me.NewsoftvDataSet2.CANCELAFACTGLOBAL, Me.CMBIdFacturaTextBox.Text, " hola")
        '        CON.Close()
        '        MsgBox("La Factura ha sido Cancelada")
        '    End If
        'End If
        If Me.CMBCanceladaTextBox.Text = "CANCELADA" Then
            MsgBox("La Factura ya ha sido Cancelada con Anterioridad")
        Else 'If Me.DateTimePicker1.Text = Me.CMBFechaTextBox.Text Then
            Dim resp As MsgBoxResult = MsgBoxResult.Cancel
            resp = MsgBox("� Esta Seguro de que Desea Cancelar la Factura Global con Serie: " + Me.CMBSerieTextBox.Text + " y Folio:" + Me.FacturaLabel1.Text + " ?", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.Yes Then
                'Inicio Edgar 04/Feb/2012 Factura_Digital
                'DameId_FacturaCDF(Me.CMBIdFacturaTextBox.Text, "G")
                'If Me.CMBIdFacturaTextBox.Text > 0 And GloClv_FacturaCFD > 0 Then
                '    Cancelacion_FacturaCFD(GloClv_FacturaCFD)
                'End If
                ''Fin

                bec_bandera = 1
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.CANCELAFACTGLOBALTableAdapter.Connection = CON
                Me.CANCELAFACTGLOBALTableAdapter.Fill(Me.NewsoftvDataSet2.CANCELAFACTGLOBAL, Me.CMBIdFacturaTextBox.Text, " hola")
                CON.Close()

                If GloActivarCFD = 1 Then
                    CancelaFacturaDigitalGlobal(Me.CMBIdFacturaTextBox.Text)
                End If

                MsgBox("La Factura ha sido Cancelada")
            End If
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        busqueda(2)
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        bec_tipo = Me.ComboBox2.SelectedValue
        busqueda(3)
    End Sub




    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim Serie As String = Nothing
        Dim fecha As String = Nothing
        Dim Cajera As String = Nothing
        Dim Factura As String = Nothing
        Dim FacturaID As Integer = 0
        If CMBIdFacturaTextBox.Text > 0 Then


            Serie = Me.CMBSerieTextBox.Text
            Factura = Me.FacturaLabel1.Text
            fecha = Me.CMBFechaTextBox.Text
            FacturaID = CInt(CMBIdFacturaTextBox.Text)
            Cajera = Me.CMBCajeraTextBox.Text
            Importe = Me.CMBImporteTextBox.Text
            'CantidadaLetra()
            ''FACTURA DIGITAL 4/FEB/2012        
            Cajera = Me.CMBCajeraTextBox.Text
            Importe = Me.CMBImporteTextBox.Text
            CantidadaLetra()
            'Locop = 1
            'DameId_FacturaCDF(CMBIdFacturaTextBox.Text, "G")
            'Try
            '    FormPruebaDigital.Show()
            'Catch ex As Exception

            'End Try

            If LocImpresoraTickets = "" Then
                MsgBox("No Se Ha Asigando Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
            ElseIf LocImpresoraTickets <> "" Then
                'Me.ConfigureCrystalReportefacturaGlobal(Letra2, Importe, Serie, fecha, Cajera, Factura)
                If GloActivarCFD = 0 Then
                    DameImporteFacturaGlobal(Today, Today, 0, " ", CMBIdFacturaTextBox.Text, 2)
                    ConfigureCrystalReportefacturaGlobal(Letra2, Importe, Serie, fecha, Cajera, Factura)
                ElseIf GloActivarCFD = 1 Then
                    ImprimeFacturaDigitalGlobal(CMBIdFacturaTextBox.Text)
                End If
            End If

            'FIN
            'If LocImpresoraTickets = "" Then
            '    MsgBox("No Se Ha Asigando Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
            'ElseIf LocImpresoraTickets <> "" Then
            '    LiTipo = 7
            '    bec_factura = Factura
            '    FrmImprimir.Show()
            '    'Me.ConfigureCrystalReportefacturaGlobal(Letra2, Importe, Serie, fecha, Cajera, Factura, Factura)
            'End If
        Else
            MsgBox("Seleccione la factura global", vbInformation)
        End If
    End Sub
    'Dim Serie As String = Nothing
    'Dim fecha As String = Nothing
    'Dim Cajera As String = Nothing
    'Dim Factura As String = Nothing

    'Serie = Me.CMBSerieTextBox.Text
    'Factura = Me.FacturaLabel1.Text
    'fecha = Me.CMBFechaTextBox.Text

    'Cajera = Me.CMBCajeraTextBox.Text
    'Importe = Me.CMBImporteTextBox.Text
    'CantidadaLetra()
    'If LocImpresoraTickets = "" Then
    '    MsgBox("No Se Ha Asigando Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
    'ElseIf LocImpresoraTickets <> "" Then
    '    Me.ConfigureCrystalReportefacturaGlobal(Letra2, Importe, Serie, fecha, Cajera, Factura)
    'End If

    'End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If Me.FacturaLabel1.Text <> "" Then
            LocSerieglo = Me.CMBSerieTextBox.Text '.BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value
            LocFacturaGlo = Me.FacturaLabel1.Text 'CInt(Me.BuscaFacturasGlobalesDataGridView.SelectedCells(1).Value)
            LocFechaGlo = Me.CMBFechaTextBox.Text 'Me.BuscaFacturasGlobalesDataGridView.SelectedCells(3).Value
            LocCajeroGlo = Me.CMBCajeraTextBox.Text 'Me.BuscaFacturasGlobalesDataGridView.SelectedCells(6).Value
            LocImporteGlo = Me.CMBImporteTextBox.Text 'Me.BuscaFacturasGlobalesDataGridView.SelectedCells(2).Value
            'Me.Dame_clv_sucursalTableAdapter.Fill(Me.Procedimientos_arnoldo.Dame_clv_sucursal, Me.BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value, Locclv_sucursalglo)
            Locclv_sucursalglo = Me.Label6.Text
            LocFechaGloFinal = Me.Label8.Text
            Gloop = "C"
            CAPTURAFACTURAGLOBAL.Show()
        Else
            MsgBox("No Se Ha Seleccionado Alguna Factura para Mostrar", MsgBoxStyle.Information)
        End If
    End Sub



    Private Sub BuscaFacturasGlobalesDataGridView_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles BuscaFacturasGlobalesDataGridView.CellContentClick

    End Sub
End Class
