
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class FrmImprimirRepGral

    Private customersByCityReport As ReportDocument
    Private op As String = Nothing
    Private Sub ConfigureCrystalReportsNotasCredito1()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim ba As Boolean
        Dim opc1, opc2 As String
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        reportPath = RutaReportes + "\ReporteNotasdeCredito.rpt"

        'busfac.Connection = CON
        'busfac.Fill(bfac, Clv_Factura, identi)
        'If IdSistema = "SA" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
        '    ba = True
        'ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
        '    ba = True

        'Else
        '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        'End If

        'End If

        customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ' End If
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, gloClvNota)
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, "0")
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, "0")
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, "0")
        'If GloImprimeTickets = True Then
        If IdSistema = "VA" Then
            opc1 = "Devoluci�n en Efecitvo"
            opc2 = "Devoluci�n en Efectivo:"
        Else
            opc1 = "Nota de Cr�dito"
            opc2 = "Nota de Cr�dito :"
        End If
        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & opc1 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Clave").Text = "'" & opc2 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            If locoprepnotas = 0 Then
                customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
            ElseIf locoprepnotas = 1 Then
                customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Original'"
            End If
        End If

        'If (IdSistema = "TO" Or IdSistema = "SA") Then 'And facnormal = True And identi > 0 
        '    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        'Else

        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        '' End If
        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.ShowPrintButton = True

        'customersByCityReport.PrintToPrinter(1, True, 1, 1)
        CON.Close()
        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub

    Private Sub ConfigureCrystalReports(ByVal Clave As Long, ByVal Titulo As String, ByVal SubTitulo As String)
        Try


            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim mySelectFormula As String = Nothing
            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\ReporteListadoPreliminar.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            '@Clv_SessionBancos
            customersByCityReport.SetParameterValue(0, CStr(GloClv_SessionBancos))
            '@Op
            customersByCityReport.SetParameterValue(1, "0")

            mySelectFormula = "Listado de Clientes con Cargo Autom�tico"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            mySelectFormula = " "
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & mySelectFormula & "'"
            CrystalReportViewer1.ReportSource = customersByCityReport
            'Me.CrystalReportViewer1.RefreshReport()
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportsOxxo(ByVal Clave As Long, ByVal Titulo As String, ByVal SubTitulo As String)
        Try


            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim mySelectFormula As String = Nothing
            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\RepListadoOxxo_1.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            '@Clv_SessionBancos
            customersByCityReport.SetParameterValue(0, CStr(GloClv_SessionBancos))

            mySelectFormula = "Listado de Clientes (Proceso de Oxxo)"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & GloEmpresa & "'"
            mySelectFormula = " "
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            'Me.CrystalReportViewer1.RefreshReport()
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportsParciales(ByVal Consecutivo As Long)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDataBaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim mySelectFormula As String = Nothing

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\Reporte_EntregaParcial.rpt"
        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '@Op
        customersByCityReport.SetParameterValue(0, 0)
        '@Consecutivo
        customersByCityReport.SetParameterValue(1, CStr(Consecutivo))
        '@Supervisor
        customersByCityReport.SetParameterValue(2, locnomsupervisor)

        'mySelectFormula = "Listado de Clientes con Cargo Autom�tico"
        'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        'mySelectFormula = " "
        'customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & mySelectFormula & "'"
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloNomSucursal & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport
        'Me.CrystalReportViewer1.RefreshReport()
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub

    Private Sub ConfigureCrystalDesglose2(ByVal Consecutivo As Long)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDataBaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim mySelectFormula As String = Nothing

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\Reporte_DesgloseMoneda.rpt"
        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '@Op
        customersByCityReport.SetParameterValue(0, 0)
        '@Consecutivo
        customersByCityReport.SetParameterValue(1, CStr(Consecutivo))

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport

        customersByCityReport = Nothing
    End Sub

    Private Sub ConfigureCrystalArqueo(ByVal Fecha As Date, ByVal Cajera As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_usuario", SqlDbType.VarChar, GloCajera)
            BaseII.CreateMyParameter("@Sucursal", SqlDbType.VarChar, 150, ParameterDirection.Output, "")
            Dim Diccionario As Dictionary(Of String, Object) = BaseII.ProcedimientoOutPut("uspDameNombreSucursalUsuario")
            Dim SucursalCajera As String = Diccionario("@Sucursal").ToString()

            customersByCityReport = New ReportDocument
            Dim basededatos As String = Nothing
            Dim Total As Double = 0
            Dim Efectivo_Entergas As Double = 0
            Dim Tarjeta As Double = 0
            Dim Cheques As Double = 0
            Dim mySelectFormula As String = Nothing
            Dim sumaefectivo As Long = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.SumaArqueoTableAdapter.Connection = CON
            Me.SumaArqueoTableAdapter.Fill(Me.Procedimientos_arnoldo.SumaArqueo, Fecha_ini, GloCajera, LocDesglose, LocParciales, LocAuto, LocTarjeta, LocCheque, LocEfectivo, Efectivo_Entergas, Tarjeta, Cheques)
            Me.Dame_base_datosTableAdapter.Connection = CON
            Me.Dame_base_datosTableAdapter.Fill(Me.Procedimientos_arnoldo.Dame_base_datos, basededatos)
            CON.Close()

            GastosArqueo(Fecha_ini, GloCajera)

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3.rpt"

            ReporteArqueoPrincipalXsd_3(Fecha_ini, GloCajera, reportPath)

            mySelectFormula = "Arqueo de Caja"
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            mySelectFormula = "Sucursal : " & SucursalCajera
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("TotalEfectivoParciales").Text = Efectivo_Entergas.ToString
            customersByCityReport.DataDefinition.FormulaFields("Efectivo").Text = LocEfectivo.ToString
            customersByCityReport.DataDefinition.FormulaFields("TotalTarjeta").Text = Tarjeta.ToString
            customersByCityReport.DataDefinition.FormulaFields("TarjetaCredito").Text = LocTarjeta.ToString
            customersByCityReport.DataDefinition.FormulaFields("TotalCheques").Text = Cheques.ToString
            customersByCityReport.DataDefinition.FormulaFields("Cheques").Text = LocCheque.ToString
            customersByCityReport.DataDefinition.FormulaFields("CargoAutomatico").Text = LocAuto.ToString
            customersByCityReport.DataDefinition.FormulaFields("FondoF").Text = "0"
            customersByCityReport.DataDefinition.FormulaFields("Gastos").Text = LocGastos.ToString
            customersByCityReport.DataDefinition.FormulaFields("SaldoAnterior").Text = LocSaldoAnterior.ToString


            Total = Efectivo_Entergas + Tarjeta + Cheques + LocAuto
            'MsgBox(Total)
            customersByCityReport.DataDefinition.FormulaFields("Total").Text = Total.ToString
            customersByCityReport.DataDefinition.FormulaFields("FechaGenerado").Text = "'" & Fecha_ini & "'"
            customersByCityReport.DataDefinition.FormulaFields("NomCajera").Text = "'" & GLONOMCAJERAARQUEO & "'"
            'Total = LocDesglose + LocParciales
            ' customersByCityReport.DataDefinition.FormulaFields("Total2").Text = Total.ToString
            CrystalReportViewer1.ReportSource = customersByCityReport

            'customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub
    Private Sub SetDBLogonForSubReport2(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        customersByCityReport.Subreports(1).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        customersByCityReport.Subreports(2).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        customersByCityReport.Subreports(3).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub


    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub
    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub
    Private Sub ConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal importe2 As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim cliente2 As String = "P�blico en General"
        Dim concepto2 As String = "Ingreso por Pago de Servicios"
        Dim txtsubtotal As String = Nothing
        Dim subtotal2 As Double
        Dim iva2 As Double
        Dim myString As String = iva2.ToString("00.00")
        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReporteFacturaGlobalticket.rpt"
        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
        subtotal2 = CDec(importe2) / 1.15
        txtsubtotal = subtotal2
        txtsubtotal = subtotal2.ToString("##0.00")
        iva2 = CDec(importe2) / 1.15 * 0.15
        myString = iva2.ToString("##0.00")
        customersByCityReport.DataDefinition.FormulaFields("Subtotal").Text = "'" & txtsubtotal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & myString & "'"
        customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & txtsubtotal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & importe2 & "'"

        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing


    End Sub

    Private Sub ConfigureCrystalReportefacturaGlobal2(ByVal Fecha As String, ByVal Tipo As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Fecha1 As String = " "
        Dim Extra As String = " "
        Dim OpOrdenar As String = "0"

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReporteFacturaGlobalW.rpt"
        ' MsgBox(reportPath)
        customersByCityReport.Load(reportPath)

        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        If Tipo = "V" Then

            ' @fecha DateTime,
            customersByCityReport.SetParameterValue(0, Fecha)
            '  @Selsucursal  int
            customersByCityReport.SetParameterValue(1, CStr(0))
            '  @Op int
            customersByCityReport.SetParameterValue(2, CStr(0))

            mySelectFormula = "Comprobaci�n de Facturas Globales de Ventas por Sucursal"


        Else
            If Tipo = "C" Then
                '        'Fec_Ini
                customersByCityReport.SetParameterValue(0, Fecha)
                '        @Selsucursal  int
                customersByCityReport.SetParameterValue(1, GloSucursal)
                '        '@Op
                customersByCityReport.SetParameterValue(2, CStr(2))

                '        'Titulo del Reporte
                mySelectFormula = "Comprobaci�n de Facturas Globales de Cajas por Sucursal"

            End If
        End If
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Aguascalientes "
        customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Fecha: " & bec_fecha
        customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & mySelectFormula & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing

    End Sub
    Private Sub ConfigureCrystalReportefacturaGlobal3(ByVal Fecha As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Fecha1 As String = " "
        Dim Extra As String = " "
        Dim OpOrdenar As String = "0"

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReporteFacturaGlobalW.rpt"
        ' MsgBox(reportPath)
        customersByCityReport.Load(reportPath)

        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '    'Fecha
        customersByCityReport.SetParameterValue(0, Fecha)
        '    '@Sucursal
        customersByCityReport.SetParameterValue(1, CStr(0))
        '    '@Op
        customersByCityReport.SetParameterValue(2, CStr(3))

        mySelectFormula = " Cortes Generales de Cajas, Ventas y Facturas Globales "
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Aguascalientes "
        customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Fecha: " & bec_fecha
        customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & mySelectFormula & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing

    End Sub

    Private Sub ConfigureCrystalReporteListEntregaParcial(ByVal clv_session As Integer, ByVal Fecha1 As String, ByVal Fecha As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Fecha2 As String = " "
        Dim Extra As String = " "

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\Listadp_Entregas_Parciales.rpt"
        ' MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '    'clave session
        customersByCityReport.SetParameterValue(0, clv_session)
        '    '@Fecha_ini
        customersByCityReport.SetParameterValue(1, Fecha1)
        '    '@Fecha_fin
        customersByCityReport.SetParameterValue(2, Fecha)
        ''Encabezados Reporte
        mySelectFormula = "Listado de Entregas Parciales por Cajera"
        Fecha2 = "Desde Fecha: " & Fecha1 & "  Hasta Fecha: " & Fecha
        Extra = "Sucursal:" & NomSucursal
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Aguascalientes "
        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Fecha2 & "'"
        mySelectFormula = " Fecha: " & bec_fecha
        customersByCityReport.DataDefinition.FormulaFields("Encabezado").Text = "'" & Extra & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing

    End Sub
    Private Sub ConfigureCrystalBonificaciones()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Nothing
            Dim Fecha2 As String = " "
            Dim Extra As String = " "

            Dim reportPath As String = Nothing
            If LocResumenBon = True Then
                reportPath = RutaReportes + "\ResumenBonificaciones.rpt"
                mySelectFormula = "Resumen de Facturas Bonificadas"
            Else
                reportPath = RutaReportes + "\ListadodeBonificaciones.rpt"
                mySelectFormula = "Listado de Facturas Bonificadas"
            End If



            ' MsgBox(reportPath)
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            '    '@Fecha_ini
            customersByCityReport.SetParameterValue(0, LocFecha1)
            '    '@Fecha_fin
            customersByCityReport.SetParameterValue(1, LocFecha2)
            ' @clv_txt varchar(5)
            customersByCityReport.SetParameterValue(2, Locclv_usuario)
            '@OP
            customersByCityReport.SetParameterValue(3, OPLISTADOCORTEFAC)
            '@TIPOCAJA
            customersByCityReport.SetParameterValue(4, TIPOCAJA)


            Dim Nomsucursal As String = Nothing
            Nomsucursal = "Sucursal:" + GloNomSucursal

            Dim RangoFechas As String = Nothing
            RangoFechas = "De la Fecha: " + LocFecha1 + " A la Fecha: " + LocFecha2

            Dim Supervisor As String = Nothing
            Supervisor = "Supervisor: " + LocNombreusuario



            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & RangoFechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Nomsucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Supervisor").Text = "'" & Supervisor & "'"

            If LocResumenBon = True Then
                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            Else
                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            End If

            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
            LocBndBon = False
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalFacturasCanceladas()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Nothing
            Dim Fecha2 As String = " "
            Dim Extra As String = " "

            Dim reportPath As String = Nothing
            Select Case LocBanderaRep1
                Case 0
                    mySelectFormula = "Listado de Facturas Canceladas"
                Case 1
                    mySelectFormula = "Listado de Facturas Reimpresas"
            End Select

            reportPath = RutaReportes + "\ListadoFacturasCanceladas.rpt"





            ' MsgBox(reportPath)
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            '    '@Fecha_ini
            customersByCityReport.SetParameterValue(0, LocFecha1)
            '    '@Fecha_fin
            customersByCityReport.SetParameterValue(1, LocFecha2)
            ' @clv_txt varchar(5)
            customersByCityReport.SetParameterValue(2, Locclv_usuario)
            '@clv_reporte
            customersByCityReport.SetParameterValue(3, LocBanderaRep1 + 1)

            ''Encabezados Reporte

            Dim Nomsucursal As String = Nothing
            Nomsucursal = "Sucursal:" + GloNomSucursal

            Dim RangoFechas As String = Nothing
            RangoFechas = "De la Fecha: " + LocFecha1 + " A la Fecha: " + LocFecha2

            Dim Cajero As String = Nothing
            Cajero = "Usuario: " + LocNombreusuario


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & RangoFechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Nomsucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Cajero").Text = "'" & Cajero & "'"

            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait


            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
            LocBndrepfac1 = False
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalNotasCredito()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Nothing
            Dim Fecha2 As String = " "
            Dim Extra As String = " "

            Dim reportPath As String = Nothing

            Select Case Locbndrepnotas
                Case 0
                    reportPath = RutaReportes + "\ReporteNotasdeCredito(cajero).rpt"
                Case 1
                    reportPath = RutaReportes + "\ReporteNotasdeCredito(sucursal).rpt"
            End Select

            'reportPath = RutaReportes + "\Reporte_Notas_Credito01.rpt"




            '(@op int,@clv_Usuario varchar(6),@clv_sucursal bigint,@cancelada bit,@saldada bit,@activa bit,@fecha1 datetime,@fecha2 datetime)


            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '@op int
            customersByCityReport.SetParameterValue(0, Locbndrepnotas)
            '    '@clv_Usuario int
            customersByCityReport.SetParameterValue(1, LocUsuariosNotas)
            '@clv_sucursal bigint
            customersByCityReport.SetParameterValue(2, Locclv_sucursalnotas)
            '    '@cancelada bit
            customersByCityReport.SetParameterValue(3, LocCancelada)
            ' @saldada bit
            customersByCityReport.SetParameterValue(4, LocSaldada)
            '@activa bit
            customersByCityReport.SetParameterValue(5, LocActiva)
            '@fecha1 datetime
            customersByCityReport.SetParameterValue(6, LocFecha1)
            '@fecha2 datetime
            customersByCityReport.SetParameterValue(7, LocFecha2)


            ''Encabezados Reporte

            Dim Nomsucursal As String = Nothing
            Nomsucursal = "Sucursal:" + GloNomSucursal

            Dim RangoFechas As String = Nothing
            RangoFechas = "De la Fecha: " + LocFecha1 + " A la Fecha: " + LocFecha2

            mySelectFormula = "Listado de Notas de Cr�dito"




            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & RangoFechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & Nomsucursal & "'"


            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape


            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
            LocBndrepfac1 = False
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ConfigureCrystalDesglosePagos()
        Try

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim reportPath As String = Nothing
            Dim Titulo As String = Nothing
            Dim Sucursal As String = Nothing
            Dim Ciudades As String = Nothing
            Ciudades = " Ciudad(es): " + LocCiudades

            reportPath = RutaReportes + "\ReportePagosRangoFechas.rpt"
            Titulo = "Relaci�n de Ingresos por Conceptos"

            Sucursal = " Sucursal: " + GloNomSucursal
            customersByCityReport.Load(reportPath)

            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)

            '@FECHA_INI
            customersByCityReport.SetParameterValue(0, eFechaInicial)
            '@FECHA_FIN
            customersByCityReport.SetParameterValue(1, eFechaFinal)
            '@TIPO
            customersByCityReport.SetParameterValue(2, "")
            '@SUCURSAL
            customersByCityReport.SetParameterValue(3, "0")
            '@CAJA
            customersByCityReport.SetParameterValue(4, "0")
            '@CAJERA
            customersByCityReport.SetParameterValue(5, "")
            '@OP
            customersByCityReport.SetParameterValue(6, "0")
            'Clv_Session
            customersByCityReport.SetParameterValue(7, gloClv_Session)




            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"


            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsDetalleConciliacion()
        Try

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim reportPath As String = Nothing
            Dim Titulo As String = Nothing
            Dim Sucursal As String = Nothing
            Dim Ciudades As String = Nothing
            ' Ciudades = " Ciudad(es): " + LocCiudades

            reportPath = RutaReportes + "\Detalle_Prefacturas_Pagolinea.rpt"
            Titulo = "Detalle De Movimientos Por Fecha"

            Sucursal = " Sucursal: " + GloNomSucursal
            customersByCityReport.Load(reportPath)

            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)

            '@FECHA_INI
            customersByCityReport.SetParameterValue(0, locGlo_Fechaini)
            '@FECHA_FIN
            customersByCityReport.SetParameterValue(1, locGlo_Fechafin)





            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            eFechaTitulo = "De la Fecha: " & locGlo_Fechaini & " a la Fecha: " & locGlo_Fechafin
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eFechaTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & Sucursal & "'"
            'ustomersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"


            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReportsCargosAutomaticos()
        Try

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim reportPath As String = Nothing
            Dim Titulo As String = Nothing
            Dim Sucursal As String = Nothing
            Dim Ciudades As String = Nothing
            ' Ciudades = " Ciudad(es): " + LocCiudades
            If losresumencargos = True Then
                reportPath = RutaReportes + "\ReporteFacturasCargoAutoResumen.rpt"
                Titulo = "Resumen De Facturas Con Cargo Automatico"
            Else
                reportPath = RutaReportes + "\ReporteFacturasCargoAutoDetallado.rpt"
                Titulo = "Listado De Facturas Con Cargo Automatico"
            End If


            Sucursal = " Sucursal: " + GloNomSucursal
            customersByCityReport.Load(reportPath)



            SetDBLogonForReport(connectionInfo, customersByCityReport)
        

            '(@clv_Session bigint,@op bigint,@FechaIni datetime,@FechaFin datetime,@SelCajera varchar(250)

            '@clv_session
            customersByCityReport.SetParameterValue(0, locclv_sessioncargosauto)
            '@op
            customersByCityReport.SetParameterValue(1, 0)
            '@fechaIni
            customersByCityReport.SetParameterValue(2, Fecha_IniCargo)
            '@fechaFin
            customersByCityReport.SetParameterValue(3, Fecha_FinCargo)
            '@SelCajera
            customersByCityReport.SetParameterValue(4, Cajero_Cargo)






            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            eFechaTitulo = "De la Fecha: " & Fecha_IniCargo & " a la Fecha: " & Fecha_FinCargo
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eFechaTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & Sucursal & "'"
            'ustomersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"


            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing
            bndcancelareportcargos = True
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReportsPagosEfectuadosCliente()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim reportPath As String = Nothing
            Dim Titulo As String = Nothing
            Dim Sucursal As String = Nothing
            Dim Ciudades As String = Nothing
            ' Ciudades = " Ciudad(es): " + LocCiudades
            reportPath = RutaReportes + "\Reporte_Clientes_facturas_detallado_Jiq.rpt"
            Titulo = "Listado De Pagos Efectuados Por El Cliente"
            Sucursal = " Sucursal: " + GloNomSucursal

            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '(@clv_Session bigint,@op bigint,@FechaIni datetime,@FechaFin datetime,@SelCajera varchar(250)

         
            '@op
            customersByCityReport.SetParameterValue(0, 0)
            '@contratoini
            customersByCityReport.SetParameterValue(1, contratoini)
            '@contratofin
            customersByCityReport.SetParameterValue(2, contratofin)



            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"


            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ConfigureCrystalReportsCorteGlobal()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim fechas As String = Nothing
            Dim reportPath As String = Nothing
            Dim titulo As String = Nothing

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            

            reportPath = RutaReportes + "\ReportCortesGlobal.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_Session
            customersByCityReport.SetParameterValue(0, CStr(eClv_Session))
            '@FechaInicial
            customersByCityReport.SetParameterValue(1, CStr(eFechaInicial))
            '@FechaFinal
            customersByCityReport.SetParameterValue(2, CStr(eFechaFinal))

            titulo = "Reporte de Cortes Global"
            fechas = "Del " & CStr(eFechaInicial) & " al " & CStr(eFechaFinal)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & titulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
  
    End Sub

    Private Sub ConfigureCrystalCobroDeAdeudo()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Nothing
            Dim Fecha2 As String = " "
            Dim Extra As String = " "

            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\ListadoCobroAdeudo.rpt"
            mySelectFormula = "Listado de los Cobros de Adeudo"

            ' MsgBox(reportPath)
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)


            'customersByCityReport.SetParameterValue(0, FechaInicial_RepCobroAdeudo)
            'customersByCityReport.SetParameterValue(1, FechaFinal_RepCobroAdeudo)
            customersByCityReport.SetParameterValue(0, Sucursal_RepCobroAdeudo)
            customersByCityReport.SetParameterValue(1, Caja_RepCobroAdeudo)
            customersByCityReport.SetParameterValue(2, Clv_Servicio_RepCobroAdeudo)
            customersByCityReport.SetParameterValue(3, StatusFactura_RepCobroAdeudo)
            customersByCityReport.SetParameterValue(4, StatusServicio_RepCobroAdeudo)



            ''Encabezados Reporte

            Dim Nomsucursal As String = Nothing
            Nomsucursal = "Sucursal:" + GloNomSucursal

            Dim RangoFechas As String = Nothing
            RangoFechas = "De la Fecha: " + LocFecha1 + " A la Fecha: " + LocFecha2

            Dim Supervisor As String = Nothing
            Supervisor = "Supervisor: " + LocNombreusuario



            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & RangoFechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Nomsucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Supervisor").Text = "'" & Supervisor & "'"

            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape

            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
            LocBndReporteBonificaciones = False
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmImprimirRepGral_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

    End Sub

    Private Sub FrmImprimirRepGral_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        OPLISTADOCORTEFAC = 0
        TIPOCAJA = ""
    End Sub
    Private Sub FrmImprimirRepGral_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If GloReporte = 1 Then
            ConfigureCrystalReports(GloClv_SessionBancos, GloTitulo, GloSubTitulo)
        ElseIf GloReporte = 2 Then
            ConfigureCrystalReportsParciales(GloConsecutivo)
        ElseIf GloReporte = 3 Then
            Me.ConfigureCrystalDesglose2(GloConsecutivo)
        ElseIf GloReporte = 4 Then
            Me.ConfigureCrystalArqueo(Fecha_ini, GloCajera)
        ElseIf GloReporte = 5 Then
            ' ConfigureCrystalReortefacturaGlobal("", bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura)
            Me.ConfigureCrystalReportefacturaGlobal(bec_letra, bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura)
        ElseIf GloReporte = 6 Then
            ' ConfigureCrystalReortefacturaGlobal2("", bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura)
            Me.ConfigureCrystalReportefacturaGlobal2(bec_fecha, bec_tipo)
        ElseIf GloReporte = 7 Then
            ' ConfigureCrystalReortefacturaGlobal3("", bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura)
            Me.ConfigureCrystalReportefacturaGlobal3(bec_fecha)
        ElseIf GloReporte = 8 Then
            Me.ConfigureCrystalReporteListEntregaParcial(LocClv_session, Fecha_ini, Fecha_Fin)
        ElseIf GloReporte = 9 Then
            ConfigureCrystalReportsOxxo(GloClv_SessionBancos, GloTitulo, GloSubTitulo)
        ElseIf GloReporte = 10 Then
            GloReporte = 0
            ConfigureCrystalReportsCorteGlobal()
        End If
        If LocBndBon = True Then
            LocBndBon = False
            ConfigureCrystalBonificaciones()
        End If
        If LocBndrepfac1 = True Then
            LocBndrepfac1 = False
            ConfigureCrystalFacturasCanceladas()
        End If
        If LocbndNotas = True Then
            LocbndNotas = False
            ConfigureCrystalNotasCredito()
        End If
        If LocbndDesPagos = True Then
            LocbndDesPagos = False
            ConfigureCrystalDesglosePagos()
            Me.CrystalReportViewer1.ShowPrintButton = True
        End If
        If LocBndNotasReporteTick = True Then
            LocBndNotasReporteTick = False
            ConfigureCrystalReportsNotasCredito1()
        End If
        If bndreporteconciliacion = True Then
            bndreporteconciliacion = False
            ConfigureCrystalReportsDetalleConciliacion()
        End If
        If bndreportcargos = True Then
            bndreportcargos = False
            ConfigureCrystalReportsCargosAutomaticos()
        End If
        If BndRepImporteClietnes = True Then
            BndRepImporteClietnes = False
            Me.Text = "Listado De Pagos Efectuados Por El Cliente"
            ConfigureCrystalReportsPagosEfectuadosCliente()
        End If

        If LocBndReporteBonificaciones = True Then

            LocBndReporteBonificaciones = False
            ConfigureCrystalCobroDeAdeudo()

        End If

    End Sub

    Private Sub ReporteArqueoPrincipalXsd_3(ByVal FECHA As Date, ByVal CAJERA As String, ByVal RUTAREP As String)
        Dim CON As New SqlConnection(MiConexion)

        Dim CMD As New SqlCommand("ArqueoCajas_tmp", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Fecha", FECHA)
        CMD.Parameters.AddWithValue("@NomCajera", CAJERA)
        Dim DA As New SqlDataAdapter(CMD)

        Dim DS As New DataSet()

        DA.Fill(DS)

        DS.Tables(0).TableName = "ArqueoCajas_tmp"
        DS.Tables(1).TableName = "ARQUEOCAJAS"
        DS.Tables(2).TableName = "ArqueoCajas_EntregasP"
        DS.Tables(3).TableName = "ArqueoCajas_CreditCard"
        DS.Tables(4).TableName = "ArqueoCajas_Cheques"

        customersByCityReport.Load(RUTAREP)
        customersByCityReport.SetDataSource(DS)
    End Sub
    Private Sub GastosArqueo(ByVal FECHA As Date, ByVal CAJERA As String)
        Dim CON As New SqlConnection(MiConexion)

        Dim CMD As New SqlCommand("GastosArqueo", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Fecha", FECHA)
        CMD.Parameters.AddWithValue("@NomCajera", CAJERA)

        Dim READER As SqlDataReader

        Try
            CON.Open()
            READER = CMD.ExecuteReader()

            While READER.Read
                LocGastos = READER(0).ToString()
                LocSaldoAnterior = READER(1).ToString()
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
End Class