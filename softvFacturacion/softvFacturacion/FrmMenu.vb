Imports System.Net.Sockets
Imports System.Data.SqlClient

Public Class FrmMenu
    Dim j As Integer = 0
    Private eRes As Integer = 0
    Private eMsg As String = String.Empty
    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        bitsist(GloUsuario, 0, GloSistema, "Salida Del Sistema", "", "Salida Del Sistema", GloNomSucursal, SubCiudad)
        End
    End Sub

    Private Sub CajasToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CajasToolStripMenuItem2.Click

        GloTipo = "C"

        'If IdSistema = "LO" Then

        FrmFACLogitel.Text = "Facturación"
        FrmFACLogitel.Show()

        'FrmFAC.Text = "Facturación"
        'FrmFAC.Show()

        'Else
        '    FrmFAC.Text = "Facturación"
        '    FrmFAC.Show()


        'End If
    End Sub

    Private Sub FrmMenu_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bndcancelareportcargos = True Then
            bndcancelareportcargos = False
            Dim con4 As New SqlClient.SqlConnection(MiConexion)
            Dim cmd As New SqlClient.SqlCommand()

            cmd = New SqlClient.SqlCommand()
            con4.Open()
            With cmd
                .CommandText = "Borra_tablas_reporte_cargos"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con4

                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = locclv_sessioncargosauto

                .Parameters.Add(prm)

                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            con4.Close()
        End If

    End Sub

    Private Sub FrmMenu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        bitsist(GloUsuario, 0, GloSistema, "Salida Del Sistema", "", "Salida Del Sistema", GloNomSucursal, SubCiudad)
        End
    End Sub

    Private Sub FrmMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        SP_Dame_General_sistema_II()

        'Me.PolizasToolStripMenuItem.Visible = False
        DesgloceDeMensualidadesToolStripMenuItem.Visible = False
        DesgloceDeContratacionesToolStripMenuItem.Visible = False
        Me.RelaciónDeClientesToolStripMenuItem.Visible = False
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameEspecifTableAdapter.Connection = CON
        Me.DameEspecifTableAdapter.Fill(Me.NewsoftvDataSet2.DameEspecif, ColorBut, ColorLetraBut, ColorMenu, ColorMenuLetra, ColorBwr, ColorBwrLetra, ColorGrid, ColorForm, ColorLabel, ColorLetraLabel, ColorLetraForm)
        Me.MUESTRAIMAGENTableAdapter.Connection = CON
        Me.MUESTRAIMAGENTableAdapter.Fill(Me.NewsoftvDataSet2.MUESTRAIMAGEN)
        'TODO: esta línea de código carga datos en la tabla 'NewsoftvDataSet.DameDatosGenerales' Puede moverla o quitarla según sea necesario.
        Me.DameDatosGeneralesTableAdapter.Connection = CON
        Me.DameDatosGeneralesTableAdapter.Fill(Me.NewsoftvDataSet.DameDatosGenerales)

        LocNomEmpresa = Me.CMBLabel1.Text
        'LLENA LA TABLA DE MENUS AUTOMATICAMENTE
        Me.RecorrerEstructuraMenu(Me.MenuStrip1)
        If IdSistema = "TO" Then
            Me.CToolStripMenuItem.Visible = False
            Me.VentasToolStripMenuItem2.Visible = False
        ElseIf IdSistema = "SA" Then
            ' If GloDatabaseName = "Jiquilpan" Or GloDatabaseName = "jiquilpan" Then
            Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem.Visible = True
            Me.NotasDeCréditoToolStripMenuItem.Visible = True
            'End If
        ElseIf IdSistema = "LO" Then
            Me.NotasDeCréditoToolStripMenuItem.Visible = True
            Me.ListadoDeNotasDeCréditoToolStripMenuItem.Visible = True
            Me.NotasDeCréditoToolStripMenuItem.Text = "Notas De Crédito"
            Me.ListadoDeNotasDeCréditoToolStripMenuItem.Text = "Listado de Notas de Crédito"
            FacturacionToolStripMenuItem1.Text = "Pagos"
            CancelaciònDeFacturasToolStripMenuItem.Text = "Cancelación de Pagos"
            ReImpresionDeFacturasToolStripMenuItem.Text = "Reimpresion de Pagos"
            CortesDeFacturasToolStripMenuItem.Text = "Cortes de Pagos"
            ListadoDeFacturasCanceadasToolStripMenuItem.Text = "Listado de Pagos Cancelados"
            ListadoDeFacturasReimpresasToolStripMenuItem.Text = "Listado de Pagos Reimpresos"
        End If
        Me.DameTipoUsusarioTableAdapter.Connection = CON
        Me.DameTipoUsusarioTableAdapter.Fill(Me.NewsoftvDataSet.DameTipoUsusario, GloUsuario, GloTipoUsuario)
        CON.Close()
        Select Case SubCiudad
            Case "AG"
                Me.NotasDeCréditoToolStripMenuItem.Visible = True
                Me.ListadoDeNotasDeCréditoToolStripMenuItem.Visible = True
            Case "TV"
                Me.PolizasToolStripMenuItem.Visible = True
                DesgloceDeMensualidadesToolStripMenuItem.Visible = True
                DesgloceDeContratacionesToolStripMenuItem.Visible = True
                Me.RelaciónDeClientesToolStripMenuItem.Visible = True
            Case "SA"
                Me.PolizasToolStripMenuItem.Visible = True
                Me.DesgloceDeMensualidadesToolStripMenuItem.Visible = True
                Me.DesgloceDeContratacionesToolStripMenuItem.Visible = True
                Me.RelaciónDeClientesToolStripMenuItem.Visible = True
            Case "VA"
                Me.PolizasToolStripMenuItem.Visible = True
                DesgloceDeMensualidadesToolStripMenuItem.Visible = True
                DesgloceDeContratacionesToolStripMenuItem.Visible = True
                Me.NotasDeCréditoToolStripMenuItem.Visible = True
                Me.NotasDeCréditoToolStripMenuItem.Text = "Devoluciones En Efectivo"
                Me.ListadoDeNotasDeCréditoToolStripMenuItem.Visible = True
                Me.ListadoDeNotasDeCréditoToolStripMenuItem.Text = "Listado de Devoluciones en Efectivo"
            Case "NV"
                Me.PolizasToolStripMenuItem.Visible = True
                DesgloceDeMensualidadesToolStripMenuItem.Visible = True
                DesgloceDeContratacionesToolStripMenuItem.Visible = True
                Me.NotasDeCréditoToolStripMenuItem.Visible = True
                Me.NotasDeCréditoToolStripMenuItem.Text = "Devoluciones En Efectivo"
                Me.ListadoDeNotasDeCréditoToolStripMenuItem.Visible = True
                Me.ListadoDeNotasDeCréditoToolStripMenuItem.Text = "Listado de Devoluciones en Efectivo"
        End Select

        Me.CobroDeContratosMaestrosToolStripMenuItem.Visible = False
        Me.NotasDeCréditoToolStripMenuItem.Visible = False
        Me.CobroDeAdeudoABajasToolStripMenuItem.Visible = False
        Me.GeneracionDeEstadosDeCuentaToolStripMenuItem.Visible = False
        Me.ImpresiónDeEstadosDeCuentaToolStripMenuItem.Visible = False
        Me.RelaciónDeClientesToolStripMenuItem.Visible = False
        Me.DesgloceDeMensualidadesToolStripMenuItem.Visible = False
        Me.DesgloceDeContratacionesToolStripMenuItem.Visible = False
        Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem.Visible = False
        colorea(Me)
        'Me.DamePermisosTableAdapter.Fill(Me.NewsoftvDataSet.DamePermisos, GloTipoUsuario, Me.Text, 2, glolec, gloescr, gloctr)
        bitsist(GloUsuario, 0, GloSistema, "Entrada Sistema", "", "Entrada Sistema", GloNomSucursal, SubCiudad)
        Me.RecorrerEstructuraMenu(Me.MenuStrip1)

        If GloTipoUsuario = 2 Then
            Me.ReportesToolStripMenuItem.Visible = False
        Else
            Me.ReportesToolStripMenuItem.Visible = True
        End If

        If GloTipoUsuario = 2 Then

            ArqueoDeCajasToolStripMenuItem1.Visible = False
        End If

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.VarChar, GloUsuario, 10)
        BaseII.CreateMyParameter("@Nombre", ParameterDirection.Output, SqlDbType.VarChar, 250)
        BaseII.ProcedimientoOutPut("UspDameNombreUsuario")
        Me.Label2.Text = ""
        Me.Label2.Text = BaseII.dicoPar("@Nombre").ToString()

    End Sub

    Private Sub CortesDeFacturasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CortesDeFacturasToolStripMenuItem.Click
        locband_pant = 4
        loctitulo = "Seguridad Corte de Facturas"
        FrmSupervisor.Show()
    End Sub

    Private Sub ListadosYAfectacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadosYAfectacionesToolStripMenuItem.Click
        FrmListadoPreliminar.Show()
    End Sub

    Private Sub VentasToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VentasToolStripMenuItem2.Click

        GloTipo = "V"

        'If IdSistema = "LO" Then

        FrmFACLogitel.Text = "Facturación Ventas"
        FrmFACLogitel.Show()

        'Else
        'FrmFAC.Text = "Facturación Ventas"
        'FrmFAC.Show()
        'End If

    End Sub

    Private Sub EntregasParcialesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        loctitulo = "Seguridad Entregas Parciales"
        locband_pant = 1
        FrmSupervisor.Show()
    End Sub

    Private Sub DesgloseDeMonedaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BwrDESGLOSEMONEDA.Show()
    End Sub

    Private Sub ArqueoDeCajasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        loctitulo = "Seguridad Arqueo de Cajas"
        locband_pant = 2
        FrmSupervisor.Show()
    End Sub

    Private Sub EntregasParcialesToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntregasParcialesToolStripMenuItem1.Click
        loctitulo = "Seguridad Entregas Parciales"
        locband_pant = 1
        FrmSupervisor.Show()
    End Sub

    Private Sub DesgloceDeMonedaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DesgloceDeMonedaToolStripMenuItem.Click
        BwrDESGLOSEMONEDA.Show()
    End Sub

   

    Private Sub ArqueoDeCajasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ArqueoDeCajasToolStripMenuItem1.Click
        loctitulo = "Seguridad Arqueo de Cajas"
        locband_pant = 2
        FrmSupervisor.Show()
    End Sub

    Private Sub CancelaciònDeFacturasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelaciònDeFacturasToolStripMenuItem.Click
        GloOpFacturas = 0 '--Cancelacion
        GloTipo = "C"
        loctitulo = "Seguridad Cancelación de Facturas"
        locband_pant = 5
        FrmSupervisor.Show()
    End Sub

    Private Sub ReImpresionDeFacturasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReImpresionDeFacturasToolStripMenuItem.Click
        GloOpFacturas = 1 '--ReImpresion
        loctitulo = "Seguridad ReImpresion de Facturas"
        locband_pant = 6
        FrmSupervisor.Show()

    End Sub

    Private Sub FACTURAGLOBALToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FACTURAGLOBALToolStripMenuItem.Click
        BWRFACTURAGLOBAL.Show()

    End Sub

    Private Sub FacturasGlobalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FacturasGlobalesToolStripMenuItem.Click
        ResumenFacturaGlobal.Show()
    End Sub

    Private Sub ResumenGeneralFacturaGlobalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenGeneralFacturaGlobalToolStripMenuItem.Click
        FrmResumenFacturaGlobal.Show()
    End Sub

    Private Sub ListadoDeEntragasParcialesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeEntragasParcialesToolStripMenuItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameClv_Session_UsuariosTableAdapter.Connection = CON
        Me.DameClv_Session_UsuariosTableAdapter.Fill(Me.Procedimientos_arnoldo.DameClv_Session_Usuarios, LocClv_session)
        CON.Close()
        My.Forms.SelFecha.Show()
        'My.Forms.FrmListadoEntregasParciales.Show()
    End Sub
    Private Sub RecorrerEstructuraMenu(ByVal oMenu As MenuStrip)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim menu As ToolStripMenuItem
        For Each oOpcionMenu As ToolStripMenuItem In oMenu.Items
            Me.DamePermisosTableAdapter.Connection = CON
            Me.DamePermisosTableAdapter.Fill(Me.DataSetLydia.DamePermisos, GloTipoUsuario, oOpcionMenu.Name, oOpcionMenu.Text, 2, glolec, gloescr, gloctr)
            menu = New ToolStripMenuItem
            menu = oOpcionMenu
            If gloctr = 1 Then
                menu.Enabled = False
            End If
            menu = Nothing
            j = j + 1
            Me.ALTASMENUSTableAdapter.Connection = CON
            Me.ALTASMENUSTableAdapter.Fill(Me.NewsoftvDataSet.ALTASMENUS, oOpcionMenu.Text, 2, oOpcionMenu.Name, 10)
            If oOpcionMenu.DropDownItems.Count > 0 Then
                Me.RecorrerSubmenu(oOpcionMenu.DropDownItems, "----")
            End If
        Next
        CON.Close()
    End Sub

    Private Sub RecorrerSubmenu(ByVal oSubmenuItems As ToolStripItemCollection, ByVal sGuiones As String)
        Dim submenu As ToolStripItem
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        For Each oSubitem As ToolStripItem In oSubmenuItems
            If oSubitem.GetType Is GetType(ToolStripMenuItem) Then
                Me.DamePermisosTableAdapter.Connection = CON
                Me.DamePermisosTableAdapter.Fill(Me.DataSetLydia.DamePermisos, GloTipoUsuario, oSubitem.Name, oSubitem.Text, 2, glolec, gloescr, gloctr)
                submenu = New ToolStripMenuItem
                submenu = oSubitem
                If gloctr = 1 Then
                    submenu.Enabled = False
                End If
                Me.ALTASMENUSTableAdapter.Connection = CON
                Me.ALTASMENUSTableAdapter.Fill(Me.NewsoftvDataSet.ALTASMENUS, oSubitem.Text, 2, oSubitem.Name, j)
                Menu = Nothing
                If CType(oSubitem, ToolStripMenuItem).DropDownItems.Count > 0 Then
                    Me.RecorrerSubmenu(CType(oSubitem, ToolStripMenuItem).DropDownItems, sGuiones & "----")
                End If
            End If
        Next
        CON.Close()
    End Sub

    Private Sub RelaciónDeIngresosPorConceptosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RelaciónDeIngresosPorConceptosToolStripMenuItem.Click
        LocbndPolizaCiudad = False
        LocBndrelingporconceptos = True
        FrmSelCiudad.Show()
        'FrmFechaIngresosConcepto.Show()
    End Sub

    Private Sub CMBLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLabel1.Click

    End Sub

    Private Sub CobroDeContratosMaestrosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CobroDeContratosMaestrosToolStripMenuItem.Click
        FrmFacContratoMaestro.Show()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FrmDetCobroDesc.Show()

    End Sub

    Private Sub ListadoDeBonificacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeBonificacionesToolStripMenuItem.Click
        LocBanderaRep1 = 2
        OPLISTADOCORTEFAC = 0
        TIPOCAJA = "1000"
        FrmSelFechas.Show()
    End Sub

    Private Sub ListadoDeFacturasCanceadasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeFacturasCanceadasToolStripMenuItem.Click
        LocBanderaRep1 = 0
        FrmSelFechas.Show()
    End Sub

    Private Sub ListadoDeFacturasReimpresasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeFacturasReimpresasToolStripMenuItem.Click
        LocBanderaRep1 = 1
        FrmSelFechas.Show()
    End Sub

    Private Sub NotasDeCréditoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NotasDeCréditoToolStripMenuItem.Click
        If IdSistema = "LO" Then
            BrwNotasdeCreditoLogitel.Show()
        Else
            BrwNotasdeCredito.Show()
        End If

    End Sub

    Private Sub ListadoDeNotasDeCréditoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeNotasDeCréditoToolStripMenuItem.Click
        FrmSelOpNotas.Show()
    End Sub

    Private Sub RecepciónDelArchivoDeOxxoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecepciónDelArchivoDeOxxoToolStripMenuItem.Click
        FrmRecepOxxo.Show()
    End Sub

    Private Sub RelaciónDeClientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RelaciónDeClientesToolStripMenuItem.Click
        LocClientesPagosAdelantados = True
        FrmSelCiudad.Show()
    End Sub
 
    Private Sub PolizasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PolizasToolStripMenuItem.Click
        BrwPolizas.Show()
    End Sub

    Private Sub DesgloceDeMensualidadesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DesgloceDeMensualidadesToolStripMenuItem.Click
        GloBnd_Des_Men = True
        'LocClientesPagosAdelantados = True
        FrmSelCiudad.Show()
    End Sub

    Private Sub DesgloceDeContratacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DesgloceDeContratacionesToolStripMenuItem.Click
        GloBnd_Des_Men = True
        GloBnd_Des_Cont = True
        'LocClientesPagosAdelantados = True
        FrmSelCiudad.Show()
    End Sub

    Private Sub FrmMenu_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LocationChanged

    End Sub

    Private Sub ConciliaciónBancariaDeSantanderSerfínToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConciliaciónBancariaDeSantanderSerfínToolStripMenuItem.Click
        FrmConciliacionBancarioSantander.Show()
    End Sub

    Private Sub FacturasConCargoAutomaticoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FacturasConCargoAutomaticoToolStripMenuItem.Click
        Dim con5 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        con5.Open()
        With cmd
            .CommandText = "Dame_clv_session_cargos_auto"
            .CommandTimeout = 0
            .Connection = con5
            .CommandType = CommandType.StoredProcedure

            Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Output
            prm.Value = 0

            .Parameters.Add(prm)

            Dim i As Integer = cmd.ExecuteNonQuery()

            locclv_sessioncargosauto = prm.Value
        End With
        con5.Close()
        FrmSelTipCargoAuto.Show()

    End Sub

    Private Sub ChecaDesgloseMoneda(ByVal Clv_Usuario As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaDesgloseMoneda", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("Clv_Usuario", SqlDbType.VarChar, 10)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Usuario
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Tipo", SqlDbType.VarChar, 50)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = String.Empty
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = 0
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = String.Empty
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(parametro2.Value.ToString)
            eMsg = parametro3.Value.ToString
            conexion.Close()

            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Information)
                op = "N"
                My.Forms.FrmDESGLOSEMONEDA.Show()

            End If

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub FacturacionToolStripMenuItem1_DropDownOpened(ByVal sender As Object, ByVal e As System.EventArgs) Handles FacturacionToolStripMenuItem1.DropDownOpened
        If IdSistema = "LO" Then
            ChecaDesgloseMoneda(GloUsuario)
            'ESTA VARIABLE LA APAGO, PORQUE SE UTULIZA EN EL DESGLOSE DE MONEDA,
            'ME ESTABA CREANDO CONFLICTOS EN FRMFACLOGITEL
            GloBnd = False
        End If
    End Sub

    Private Sub ListadoDePagosEfectuadosPorElClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDePagosEfectuadosPorElClienteToolStripMenuItem.Click
        FrmRangoContratos.Show()
    End Sub

    Private Sub DepositosDeAparatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DepositosDeAparatosToolStripMenuItem.Click
        'Busqueda de Bonos de Aparato (    Promoción para Estudiantes )
        BrwBonosDeAparatos.Show()
    End Sub

    Private Sub ListadoDeCobroDeAdeudoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeCobroDeAdeudoToolStripMenuItem.Click
        FrmSelFechasReportes2.Show()
    End Sub

    Private Sub GeneracionDeEstadosDeCuentaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneracionDeEstadosDeCuentaToolStripMenuItem.Click
        FrmGeneracionDeEstadosDeCuenta.Show()
    End Sub

    Private Sub CobroDeAdeudoABajasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CobroDeAdeudoABajasToolStripMenuItem.Click
        FrmFacCobroABajas.Show()
    End Sub

    Private Sub ImpresiónDeEstadosDeCuentaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImpresiónDeEstadosDeCuentaToolStripMenuItem.Click
        FrmImprimeEstadosDeCuentaPorPeriodo.Show()
    End Sub
End Class