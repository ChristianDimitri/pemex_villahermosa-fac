Imports System.Data.SqlClient
Public Class BwrDESGLOSEMONEDA

    Private Sub busqueda(ByVal op As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Select Case op
                Case 0
                    Me.BUSCADESSGLOCEMONEDATableAdapter.Connection = CON
                    Me.BUSCADESSGLOCEMONEDATableAdapter.Fill(Me.NewsoftvDataSet1.BUSCADESSGLOCEMONEDA, 0, eLoginCajera, "01/01/1900")
                Case 1
                    Me.BUSCADESSGLOCEMONEDATableAdapter.Connection = CON
                    Me.BUSCADESSGLOCEMONEDATableAdapter.Fill(Me.NewsoftvDataSet1.BUSCADESSGLOCEMONEDA, 1, Me.TextBox1.Text, "01/01/1900")
                Case 2
                    Me.BUSCADESSGLOCEMONEDATableAdapter.Connection = CON
                    Me.BUSCADESSGLOCEMONEDATableAdapter.Fill(Me.NewsoftvDataSet1.BUSCADESSGLOCEMONEDA, 2, eLoginCajera, Me.TextBox2.Text)
            End Select
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BwrEntregasParciales_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            busqueda(0)
        End If

        If eConModDesglose = True Then
            eConModDesglose = False

            If op = "C" Then

                My.Forms.FrmDESGLOSEMONEDA.Show()
            End If

            If op = "M" Then

                My.Forms.FrmDESGLOSEMONEDA.Show()
            End If
        End If
    End Sub

    Private Sub BwrEntregasParciales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        busqueda(0)
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        busqueda(1)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        busqueda(2)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(1)
        End If
    End Sub

    Private Sub TextBox1_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(1)
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(2)
        End If
    End Sub
    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            busqueda(5)
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Me.Close()


    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        op = "N"
        My.Forms.FrmDESGLOSEMONEDA.Show()

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If Me.DataGridView1.RowCount > 0 Then
            DameLoginCajera()
            If eCorresponde = True Then
                eCorresponde = False
                Consecutivo = Me.DataGridView1.SelectedCells(0).Value
                op = "C"
                ChecaCajera()

            End If
        Else
            MsgBox("No hay Desglose de Monedas que Consultar", , "Atenci�n")
        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If Me.DataGridView1.RowCount > 0 Then
            DameLoginCajera()
            If eCorresponde = True Then
                eCorresponde = False

                If Today.Date = Date.Parse(Me.FechaLabel1.Text).Date Then
                    Consecutivo = Me.DataGridView1.SelectedCells(0).Value
                    op = "M"
                    ChecaCajera()
                Else
                    MsgBox("S�lo puedes Modificar Desgloses del d�a de hoy", , "Atenci�n")
                End If
            End If
        Else
            MsgBox("No hay Desglose de Monedas que Modificar", , "Atenci�n")
        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        If Me.DataGridView1.RowCount > 0 Then
            op = "I"
            GloConsecutivo = Me.DataGridView1.SelectedCells(0).Value
            GloReporte = 3
            My.Forms.FrmImprimirRepGral.Show()
        Else
            MsgBox("Sin Datos, No Puedes Imprimir", , "Atenci�n")
        End If
    End Sub

    Private Sub ChecaCajera()
        FrmLoginCajera.Show()
    End Sub

    Public Sub DameLoginCajera()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameCveCajeraTableAdapter.Connection = CON
        Me.DameCveCajeraTableAdapter.Fill(Me.DataSetEdgar.DameCveCajera, Me.CMBNomCajeraTextBox.Text, eCveCajera)
        CON.Close()
        If eCveCajera = eLoginCajera Then
            eCorresponde = True
        Else
            MsgBox("No puedes Consultar Desgloses que No te Correspondan", , "Advertencia")
        End If

    End Sub


End Class
