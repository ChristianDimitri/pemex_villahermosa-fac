<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFACLogitel
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim SOLOINTERNETLabel1 As System.Windows.Forms.Label
        Dim ESHOTELLabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SumaDetalleBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet()
        Me.BUSCLIPORCONTRATO_FACTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.BUSCLIPORCONTRATO_FACTableAdapter()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.DameDetalleTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameDetalleTableAdapter()
        Me.BorraClv_SessionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorraClv_SessionTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.BorraClv_SessionTableAdapter()
        Me.CobraTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.CobraTableAdapter()
        Me.DameSerDELCliFACBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameSerDELCliFACTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameSerDELCliFACTableAdapter()
        Me.CobraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.DameDetalleBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AgregarServicioAdicionalesTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.AgregarServicioAdicionalesTableAdapter()
        Me.GrabaFacturasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GrabaFacturasTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.GrabaFacturasTableAdapter()
        Me.AgregarServicioAdicionalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SumaDetalleTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.SumaDetalleTableAdapter()
        Me.PagosAdelantadosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PagosAdelantadosTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.PagosAdelantadosTableAdapter()
        Me.BUSCLIPORCONTRATOFACBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.DameDatosGeneralesTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameDatosGeneralesTableAdapter()
        Me.DamedatosUsuarioTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DamedatosUsuarioTableAdapter()
        Me.DAMENOMBRESUCURSALTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DAMENOMBRESUCURSALTableAdapter()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LblVersion = New System.Windows.Forms.Label()
        Me.LblFecha = New System.Windows.Forms.Label()
        Me.DameDatosGeneralesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LblUsuario = New System.Windows.Forms.Label()
        Me.DamedatosUsuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LblSucursal = New System.Windows.Forms.Label()
        Me.DAMENOMBRESUCURSALBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LblNomCaja = New System.Windows.Forms.Label()
        Me.LblSistema = New System.Windows.Forms.Label()
        Me.LblNomEmpresa = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ClvSessionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVSERVICIODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvllavedelservicioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvUnicaNetDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLAVEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MACCABLEMODEM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCORTADataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pagos_Adelantados = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.TvAdicDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesCortesiaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesApagarDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescuentoNet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Des_Otr_Ser_Misma_Categoria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BonificacionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteAdicionalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnaDetalleDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiasBonificaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesBonificarDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteBonificaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimoMesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimoanioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AdelantadoDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DESCRIPCIONDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLV_DETALLE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Fecha_Venta = New System.Windows.Forms.DateTimePicker()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.UltimoSERIEYFOLIOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.FolioTextBox = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRAVENDEDORES2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label15 = New System.Windows.Forms.Label()
        Me.REDLabel26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.REDLabel25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.CLV_TIPOCLIENTELabel1 = New System.Windows.Forms.Label()
        Me.DAMETIPOSCLIENTESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DESCRIPCIONLabel1 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.NOMBRELabel1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Clv_Session = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.MUESTRAVENDEDORES_2TableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.MUESTRAVENDEDORES_2TableAdapter()
        Me.Ultimo_SERIEYFOLIOTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.Ultimo_SERIEYFOLIOTableAdapter()
        Me.DAMEUltimo_FOLIOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMEUltimo_FOLIOTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DAMEUltimo_FOLIOTableAdapter()
        Me.NewsoftvDataSet1 = New softvFacturacion.NewsoftvDataSet1()
        Me.DAMETOTALSumaDetalleBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMETOTALSumaDetalleTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.DAMETOTALSumaDetalleTableAdapter()
        Me.GUARDATIPOPAGOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GUARDATIPOPAGOTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.GUARDATIPOPAGOTableAdapter()
        Me.QUITARDELDETALLEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.QUITARDELDETALLETableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.QUITARDELDETALLETableAdapter()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.CLV_DETALLETextBox = New System.Windows.Forms.TextBox()
        Me.txtMsjError = New System.Windows.Forms.TextBox()
        Me.LABEL19 = New System.Windows.Forms.TextBox()
        Me.SumaDetalleDataGridView = New System.Windows.Forms.DataGridView()
        Me.ClvSessionDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PosicionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NivelDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DAMETIPOSCLIENTESTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DAMETIPOSCLIENTESTableAdapter()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Button6 = New System.Windows.Forms.Button()
        Me.GuardaMotivosBonificacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GuardaMotivosBonificacionTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.GuardaMotivosBonificacionTableAdapter()
        Me.NewsoftvDataSet2 = New softvFacturacion.NewsoftvDataSet2()
        Me.BORCAMDOCFAC_QUITABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORCAMDOCFAC_QUITATableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.BORCAMDOCFAC_QUITATableAdapter()
        Me.DamelasOrdenesque_GeneroFacturaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DamelasOrdenesque_GeneroFacturaTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DamelasOrdenesque_GeneroFacturaTableAdapter()
        Me.Dame_Impresora_OrdenesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_Impresora_OrdenesTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.Dame_Impresora_OrdenesTableAdapter()
        Me.DimesiahiConexBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DimesiahiConexTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DimesiahiConexTableAdapter()
        Me.DameServicioAsignadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BuscaBloqueadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BuscaBloqueadoTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.BuscaBloqueadoTableAdapter()
        Me.BusFacFiscalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BusFacFiscalTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter()
        Me.Inserta_Comentario2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Comentario2TableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.Inserta_Comentario2TableAdapter()
        Me.Hora_insBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Hora_insTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.Hora_insTableAdapter()
        Me.Selecciona_Impresora_SucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Selecciona_Impresora_SucursalTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.Selecciona_Impresora_SucursalTableAdapter()
        Me.Dime_ContratacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dime_ContratacionTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.Dime_ContratacionTableAdapter()
        Me.CMBPanel6 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Dime_Si_ProcedePagoParcialBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dime_Si_ProcedePagoParcialTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.Dime_Si_ProcedePagoParcialTableAdapter()
        Me.Cobra_PagosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Cobra_PagosTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.Cobra_PagosTableAdapter()
        Me.AgregarServicioAdicionales_PPEBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.AgregarServicioAdicionales_PPETableAdapter1 = New softvFacturacion.DataSetEdgarTableAdapters.AgregarServicioAdicionales_PPETableAdapter()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.CobraAdeudoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CobraAdeudoTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.CobraAdeudoTableAdapter()
        Me.Pregunta_Si_Puedo_AdelantarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Pregunta_Si_Puedo_AdelantarTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.Pregunta_Si_Puedo_AdelantarTableAdapter()
        Me.Cobra_VentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Cobra_VentasTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.Cobra_VentasTableAdapter()
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.Inserta_Bonificacion_SupervisorBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Bonificacion_SupervisorTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_Bonificacion_SupervisorTableAdapter()
        Me.EricDataSet2 = New softvFacturacion.EricDataSet2()
        Me.DameGeneralMsjTicketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameGeneralMsjTicketsTableAdapter = New softvFacturacion.EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter()
        Me.EntregaAparatoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EntregaAparatoTableAdapter = New softvFacturacion.EricDataSet2TableAdapters.EntregaAparatoTableAdapter()
        Me.LabelSubTotal = New System.Windows.Forms.Label()
        Me.LabelIva = New System.Windows.Forms.Label()
        Me.LabelTotal = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.CMBLabel29 = New System.Windows.Forms.Label()
        Me.LblCredito_Apagar = New System.Windows.Forms.Label()
        Me.LblImporte_Total = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.LabelSaldoAnterior = New System.Windows.Forms.Label()
        Me.CMBLabel30 = New System.Windows.Forms.Label()
        Me.CNOPanel10 = New System.Windows.Forms.Panel()
        Me.LabelGRan_Total = New System.Windows.Forms.Label()
        Me.TextImporte_Adic = New System.Windows.Forms.TextBox()
        Me.PanelNrm = New System.Windows.Forms.Panel()
        Me.PanelTel = New System.Windows.Forms.Panel()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.Clv_SessionTel = New System.Windows.Forms.TextBox()
        Me.ButtonVentaEq = New System.Windows.Forms.Button()
        Me.ButtonPagoAbono = New System.Windows.Forms.Button()
        Me.panelServicios = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.panelBasico = New System.Windows.Forms.Panel()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.txt_Puntos_PPE_B = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.txt_Puntos_x_Antiguedad_B = New System.Windows.Forms.TextBox()
        Me.txt_Puntos_x_pago_oportuno_B = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.txt_Periodo_Final_B = New System.Windows.Forms.TextBox()
        Me.txt_Periodo_Inicial_B = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.txt_TVs_adicionales_B = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.panelDigital = New System.Windows.Forms.Panel()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.panelInternet = New System.Windows.Forms.Panel()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.txt_Periodo_Final_I = New System.Windows.Forms.TextBox()
        Me.txt_Periodo_Inicial_I = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.panelTelefonia = New System.Windows.Forms.Panel()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.txt_Puntos_Desc_T = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.txt_Incluidas_LDN_T = New System.Windows.Forms.TextBox()
        Me.txt_Incluidas_Locales = New System.Windows.Forms.TextBox()
        Me.txt_Incluidas_Frcc_T = New System.Windows.Forms.TextBox()
        Me.txt_NumTel_T = New System.Windows.Forms.TextBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.lblSumaCargos = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.LabelIEPS = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.btnAdelantarPagos = New System.Windows.Forms.Button()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.DataGridView4 = New System.Windows.Forms.DataGridView()
        Me.lblEsEdoCta = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.ImportePuntosAplicados = New System.Windows.Forms.Label()
        Me.ImporteTotalBonificado = New System.Windows.Forms.Label()
        Me.btnVerEdoCta = New System.Windows.Forms.Button()
        Me.btnVerDetalle = New System.Windows.Forms.Button()
        Me.lbl_MsgAyudaDobleClic = New System.Windows.Forms.Label()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.lblLeyendaPromocion = New System.Windows.Forms.Label()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.btnHabilitaFacNormal = New System.Windows.Forms.Button()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.btnRoboSeñal = New System.Windows.Forms.Button()
        SOLOINTERNETLabel1 = New System.Windows.Forms.Label()
        ESHOTELLabel1 = New System.Windows.Forms.Label()
        CType(Me.SumaDetalleBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorraClv_SessionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameSerDELCliFACBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CobraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameDetalleBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrabaFacturasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AgregarServicioAdicionalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PagosAdelantadosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCLIPORCONTRATOFACBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamedatosUsuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMENOMBRESUCURSALBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.UltimoSERIEYFOLIOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAVENDEDORES2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMETIPOSCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.DAMEUltimo_FOLIOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMETOTALSumaDetalleBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GUARDATIPOPAGOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.QUITARDELDETALLEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.SumaDetalleDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GuardaMotivosBonificacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORCAMDOCFAC_QUITABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamelasOrdenesque_GeneroFacturaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_Impresora_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimesiahiConexBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameServicioAsignadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BuscaBloqueadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BusFacFiscalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Comentario2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Hora_insBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Selecciona_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dime_ContratacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel6.SuspendLayout()
        CType(Me.Dime_Si_ProcedePagoParcialBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cobra_PagosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AgregarServicioAdicionales_PPEBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CobraAdeudoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pregunta_Si_Puedo_AdelantarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cobra_VentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Bonificacion_SupervisorBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EricDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EntregaAparatoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CNOPanel10.SuspendLayout()
        Me.PanelNrm.SuspendLayout()
        Me.PanelTel.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelServicios.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.panelBasico.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.panelDigital.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.panelInternet.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.panelTelefonia.SuspendLayout()
        Me.Panel6.SuspendLayout()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SOLOINTERNETLabel1
        '
        SOLOINTERNETLabel1.AutoSize = True
        SOLOINTERNETLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        SOLOINTERNETLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        SOLOINTERNETLabel1.Location = New System.Drawing.Point(224, 24)
        SOLOINTERNETLabel1.Name = "SOLOINTERNETLabel1"
        SOLOINTERNETLabel1.Size = New System.Drawing.Size(95, 16)
        SOLOINTERNETLabel1.TabIndex = 16
        SOLOINTERNETLabel1.Text = "Solo Internet"
        '
        'ESHOTELLabel1
        '
        ESHOTELLabel1.AutoSize = True
        ESHOTELLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        ESHOTELLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ESHOTELLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        ESHOTELLabel1.Location = New System.Drawing.Point(333, 24)
        ESHOTELLabel1.Name = "ESHOTELLabel1"
        ESHOTELLabel1.Size = New System.Drawing.Size(67, 16)
        ESHOTELLabel1.TabIndex = 18
        ESHOTELLabel1.Text = "Es Hotel"
        ESHOTELLabel1.Visible = False
        '
        'SumaDetalleBindingSource
        '
        Me.SumaDetalleBindingSource.DataMember = "SumaDetalle"
        Me.SumaDetalleBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BUSCLIPORCONTRATO_FACTableAdapter
        '
        Me.BUSCLIPORCONTRATO_FACTableAdapter.ClearBeforeFill = True
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkRed
        Me.Button7.Enabled = False
        Me.Button7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.White
        Me.Button7.Location = New System.Drawing.Point(7, 595)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(183, 34)
        Me.Button7.TabIndex = 501
        Me.Button7.Text = "&QUITAR DE LA LISTA"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'DameDetalleTableAdapter
        '
        Me.DameDetalleTableAdapter.ClearBeforeFill = True
        '
        'BorraClv_SessionBindingSource
        '
        Me.BorraClv_SessionBindingSource.DataMember = "BorraClv_Session"
        Me.BorraClv_SessionBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'BorraClv_SessionTableAdapter
        '
        Me.BorraClv_SessionTableAdapter.ClearBeforeFill = True
        '
        'CobraTableAdapter
        '
        Me.CobraTableAdapter.ClearBeforeFill = True
        '
        'DameSerDELCliFACBindingSource
        '
        Me.DameSerDELCliFACBindingSource.DataMember = "DameSerDELCliFAC"
        Me.DameSerDELCliFACBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'DameSerDELCliFACTableAdapter
        '
        Me.DameSerDELCliFACTableAdapter.ClearBeforeFill = True
        '
        'CobraBindingSource
        '
        Me.CobraBindingSource.DataMember = "Cobra"
        Me.CobraBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkRed
        Me.Button5.Enabled = False
        Me.Button5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Location = New System.Drawing.Point(7, 552)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(183, 34)
        Me.Button5.TabIndex = 500
        Me.Button5.Text = "&AGREGAR A LA LISTA"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkRed
        Me.Button4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(552, 680)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(139, 51)
        Me.Button4.TabIndex = 504
        Me.Button4.Text = "&LIMPIAR PANTALLA"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkRed
        Me.Button3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(888, 680)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(139, 53)
        Me.Button3.TabIndex = 506
        Me.Button3.Text = "&SALIR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkRed
        Me.Button2.Enabled = False
        Me.Button2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(722, 680)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(139, 52)
        Me.Button2.TabIndex = 505
        Me.Button2.Text = "&PAGAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'DameDetalleBindingSource
        '
        Me.DameDetalleBindingSource.DataMember = "DameDetalle"
        Me.DameDetalleBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'AgregarServicioAdicionalesTableAdapter
        '
        Me.AgregarServicioAdicionalesTableAdapter.ClearBeforeFill = True
        '
        'GrabaFacturasBindingSource
        '
        Me.GrabaFacturasBindingSource.DataMember = "GrabaFacturas"
        Me.GrabaFacturasBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'GrabaFacturasTableAdapter
        '
        Me.GrabaFacturasTableAdapter.ClearBeforeFill = True
        '
        'AgregarServicioAdicionalesBindingSource
        '
        Me.AgregarServicioAdicionalesBindingSource.DataMember = "AgregarServicioAdicionales"
        Me.AgregarServicioAdicionalesBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'SumaDetalleTableAdapter
        '
        Me.SumaDetalleTableAdapter.ClearBeforeFill = True
        '
        'PagosAdelantadosBindingSource
        '
        Me.PagosAdelantadosBindingSource.DataMember = "PagosAdelantados"
        Me.PagosAdelantadosBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'PagosAdelantadosTableAdapter
        '
        Me.PagosAdelantadosTableAdapter.ClearBeforeFill = True
        '
        'BUSCLIPORCONTRATOFACBindingSource
        '
        Me.BUSCLIPORCONTRATOFACBindingSource.DataMember = "BUSCLIPORCONTRATO_FAC"
        Me.BUSCLIPORCONTRATOFACBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.TreeView1)
        Me.Panel2.Location = New System.Drawing.Point(630, 61)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(374, 219)
        Me.Panel2.TabIndex = 195
        '
        'TreeView1
        '
        Me.TreeView1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.ForeColor = System.Drawing.Color.Black
        Me.TreeView1.Location = New System.Drawing.Point(0, -3)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(374, 216)
        Me.TreeView1.TabIndex = 19
        Me.TreeView1.TabStop = False
        '
        'DameDatosGeneralesTableAdapter
        '
        Me.DameDatosGeneralesTableAdapter.ClearBeforeFill = True
        '
        'DamedatosUsuarioTableAdapter
        '
        Me.DamedatosUsuarioTableAdapter.ClearBeforeFill = True
        '
        'DAMENOMBRESUCURSALTableAdapter
        '
        Me.DAMENOMBRESUCURSALTableAdapter.ClearBeforeFill = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(4, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 15)
        Me.Label3.TabIndex = 174
        Me.Label3.Text = "Empresa : "
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.LblVersion)
        Me.Panel1.Controls.Add(Me.LblFecha)
        Me.Panel1.Controls.Add(Me.LblUsuario)
        Me.Panel1.Controls.Add(Me.LblSucursal)
        Me.Panel1.Controls.Add(Me.LblNomCaja)
        Me.Panel1.Controls.Add(Me.LblSistema)
        Me.Panel1.Controls.Add(Me.LblNomEmpresa)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Location = New System.Drawing.Point(6, 9)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(998, 49)
        Me.Panel1.TabIndex = 186
        '
        'LblVersion
        '
        Me.LblVersion.AutoSize = True
        Me.LblVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVersion.Location = New System.Drawing.Point(879, 28)
        Me.LblVersion.Name = "LblVersion"
        Me.LblVersion.Size = New System.Drawing.Size(52, 13)
        Me.LblVersion.TabIndex = 187
        Me.LblVersion.Text = "Label14"
        '
        'LblFecha
        '
        Me.LblFecha.AutoSize = True
        Me.LblFecha.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Fecha", True))
        Me.LblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFecha.Location = New System.Drawing.Point(879, 7)
        Me.LblFecha.Name = "LblFecha"
        Me.LblFecha.Size = New System.Drawing.Size(52, 13)
        Me.LblFecha.TabIndex = 186
        Me.LblFecha.Text = "Label14"
        '
        'DameDatosGeneralesBindingSource
        '
        Me.DameDatosGeneralesBindingSource.DataMember = "DameDatosGenerales"
        Me.DameDatosGeneralesBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'LblUsuario
        '
        Me.LblUsuario.AutoSize = True
        Me.LblUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DamedatosUsuarioBindingSource, "Nombre", True))
        Me.LblUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUsuario.Location = New System.Drawing.Point(579, 7)
        Me.LblUsuario.Name = "LblUsuario"
        Me.LblUsuario.Size = New System.Drawing.Size(52, 13)
        Me.LblUsuario.TabIndex = 185
        Me.LblUsuario.Text = "Label14"
        '
        'DamedatosUsuarioBindingSource
        '
        Me.DamedatosUsuarioBindingSource.DataMember = "DamedatosUsuario"
        Me.DamedatosUsuarioBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'LblSucursal
        '
        Me.LblSucursal.AutoSize = True
        Me.LblSucursal.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMENOMBRESUCURSALBindingSource, "Nombre", True))
        Me.LblSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSucursal.Location = New System.Drawing.Point(458, 28)
        Me.LblSucursal.Name = "LblSucursal"
        Me.LblSucursal.Size = New System.Drawing.Size(52, 13)
        Me.LblSucursal.TabIndex = 184
        Me.LblSucursal.Text = "Label14"
        '
        'DAMENOMBRESUCURSALBindingSource
        '
        Me.DAMENOMBRESUCURSALBindingSource.DataMember = "DAMENOMBRESUCURSAL"
        Me.DAMENOMBRESUCURSALBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'LblNomCaja
        '
        Me.LblNomCaja.AutoSize = True
        Me.LblNomCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNomCaja.Location = New System.Drawing.Point(383, 7)
        Me.LblNomCaja.Name = "LblNomCaja"
        Me.LblNomCaja.Size = New System.Drawing.Size(52, 13)
        Me.LblNomCaja.TabIndex = 183
        Me.LblNomCaja.Text = "Label14"
        '
        'LblSistema
        '
        Me.LblSistema.AutoSize = True
        Me.LblSistema.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Ciudad", True))
        Me.LblSistema.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSistema.Location = New System.Drawing.Point(78, 28)
        Me.LblSistema.Name = "LblSistema"
        Me.LblSistema.Size = New System.Drawing.Size(52, 13)
        Me.LblSistema.TabIndex = 182
        Me.LblSistema.Text = "Label14"
        '
        'LblNomEmpresa
        '
        Me.LblNomEmpresa.AutoSize = True
        Me.LblNomEmpresa.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Nombre", True))
        Me.LblNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNomEmpresa.Location = New System.Drawing.Point(78, 7)
        Me.LblNomEmpresa.Name = "LblNomEmpresa"
        Me.LblNomEmpresa.Size = New System.Drawing.Size(52, 13)
        Me.LblNomEmpresa.TabIndex = 181
        Me.LblNomEmpresa.Text = "Label14"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(498, 5)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(75, 15)
        Me.Label13.TabIndex = 180
        Me.Label13.Text = "Cajero(a) :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(806, 26)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(67, 15)
        Me.Label12.TabIndex = 179
        Me.Label12.Text = "Versión : "
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(815, 5)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 15)
        Me.Label7.TabIndex = 178
        Me.Label7.Text = "Fecha : "
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(383, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 15)
        Me.Label6.TabIndex = 177
        Me.Label6.Text = "Sucursal : "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(336, 5)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 15)
        Me.Label5.TabIndex = 176
        Me.Label5.Text = "Caja : "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(9, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 15)
        Me.Label4.TabIndex = 175
        Me.Label4.Text = "Sistema : "
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkOrange
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClvSessionDataGridViewTextBoxColumn, Me.CLVSERVICIODataGridViewTextBoxColumn, Me.ClvllavedelservicioDataGridViewTextBoxColumn, Me.ClvUnicaNetDataGridViewTextBoxColumn, Me.CLAVEDataGridViewTextBoxColumn, Me.MACCABLEMODEM, Me.DESCORTADataGridViewTextBoxColumn, Me.Pagos_Adelantados, Me.TvAdicDataGridViewTextBoxColumn, Me.MesesCortesiaDataGridViewTextBoxColumn, Me.MesesApagarDataGridViewTextBoxColumn, Me.ImporteDataGridViewTextBoxColumn, Me.PeriodoPagadoIniDataGridViewTextBoxColumn, Me.PeriodoPagadoFinDataGridViewTextBoxColumn, Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn, Me.PuntosAplicadosAntDataGridViewTextBoxColumn, Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn, Me.DescuentoNet, Me.Des_Otr_Ser_Misma_Categoria, Me.BonificacionDataGridViewTextBoxColumn, Me.ImporteAdicionalDataGridViewTextBoxColumn, Me.ColumnaDetalleDataGridViewTextBoxColumn, Me.DiasBonificaDataGridViewTextBoxColumn, Me.MesesBonificarDataGridViewTextBoxColumn, Me.ImporteBonificaDataGridViewTextBoxColumn, Me.UltimoMesDataGridViewTextBoxColumn, Me.UltimoanioDataGridViewTextBoxColumn, Me.AdelantadoDataGridViewCheckBoxColumn, Me.DESCRIPCIONDataGridViewTextBoxColumn, Me.CLV_DETALLE})
        Me.DataGridView1.DataSource = Me.DameDetalleBindingSource
        Me.DataGridView1.GridColor = System.Drawing.Color.DimGray
        Me.DataGridView1.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.DataGridView1.Location = New System.Drawing.Point(10, 4)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(638, 213)
        Me.DataGridView1.TabIndex = 187
        Me.DataGridView1.TabStop = False
        '
        'ClvSessionDataGridViewTextBoxColumn
        '
        Me.ClvSessionDataGridViewTextBoxColumn.DataPropertyName = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn.HeaderText = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn.Name = "ClvSessionDataGridViewTextBoxColumn"
        Me.ClvSessionDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvSessionDataGridViewTextBoxColumn.Visible = False
        '
        'CLVSERVICIODataGridViewTextBoxColumn
        '
        Me.CLVSERVICIODataGridViewTextBoxColumn.DataPropertyName = "CLV_SERVICIO"
        Me.CLVSERVICIODataGridViewTextBoxColumn.HeaderText = "CLV_SERVICIO"
        Me.CLVSERVICIODataGridViewTextBoxColumn.Name = "CLVSERVICIODataGridViewTextBoxColumn"
        Me.CLVSERVICIODataGridViewTextBoxColumn.ReadOnly = True
        Me.CLVSERVICIODataGridViewTextBoxColumn.Visible = False
        '
        'ClvllavedelservicioDataGridViewTextBoxColumn
        '
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.DataPropertyName = "Clv_llavedelservicio"
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.HeaderText = "Clv_llavedelservicio"
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.Name = "ClvllavedelservicioDataGridViewTextBoxColumn"
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.Visible = False
        '
        'ClvUnicaNetDataGridViewTextBoxColumn
        '
        Me.ClvUnicaNetDataGridViewTextBoxColumn.DataPropertyName = "Clv_UnicaNet"
        Me.ClvUnicaNetDataGridViewTextBoxColumn.HeaderText = "Clv_UnicaNet"
        Me.ClvUnicaNetDataGridViewTextBoxColumn.Name = "ClvUnicaNetDataGridViewTextBoxColumn"
        Me.ClvUnicaNetDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvUnicaNetDataGridViewTextBoxColumn.Visible = False
        '
        'CLAVEDataGridViewTextBoxColumn
        '
        Me.CLAVEDataGridViewTextBoxColumn.DataPropertyName = "CLAVE"
        Me.CLAVEDataGridViewTextBoxColumn.HeaderText = "CLAVE"
        Me.CLAVEDataGridViewTextBoxColumn.Name = "CLAVEDataGridViewTextBoxColumn"
        Me.CLAVEDataGridViewTextBoxColumn.ReadOnly = True
        Me.CLAVEDataGridViewTextBoxColumn.Visible = False
        '
        'MACCABLEMODEM
        '
        Me.MACCABLEMODEM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.MACCABLEMODEM.DataPropertyName = "MACCABLEMODEM"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MACCABLEMODEM.DefaultCellStyle = DataGridViewCellStyle2
        Me.MACCABLEMODEM.HeaderText = "Aparato"
        Me.MACCABLEMODEM.Name = "MACCABLEMODEM"
        Me.MACCABLEMODEM.ReadOnly = True
        Me.MACCABLEMODEM.Visible = False
        '
        'DESCORTADataGridViewTextBoxColumn
        '
        Me.DESCORTADataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DESCORTADataGridViewTextBoxColumn.DataPropertyName = "DESCORTA"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DESCORTADataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.DESCORTADataGridViewTextBoxColumn.HeaderText = "Concepto"
        Me.DESCORTADataGridViewTextBoxColumn.MinimumWidth = 350
        Me.DESCORTADataGridViewTextBoxColumn.Name = "DESCORTADataGridViewTextBoxColumn"
        Me.DESCORTADataGridViewTextBoxColumn.ReadOnly = True
        Me.DESCORTADataGridViewTextBoxColumn.Width = 350
        '
        'Pagos_Adelantados
        '
        Me.Pagos_Adelantados.DataPropertyName = "Pagos_Adelantados"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Pagos_Adelantados.DefaultCellStyle = DataGridViewCellStyle4
        Me.Pagos_Adelantados.HeaderText = ""
        Me.Pagos_Adelantados.Name = "Pagos_Adelantados"
        Me.Pagos_Adelantados.ReadOnly = True
        Me.Pagos_Adelantados.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Pagos_Adelantados.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Pagos_Adelantados.Visible = False
        '
        'TvAdicDataGridViewTextBoxColumn
        '
        Me.TvAdicDataGridViewTextBoxColumn.DataPropertyName = "tvAdic"
        Me.TvAdicDataGridViewTextBoxColumn.HeaderText = ""
        Me.TvAdicDataGridViewTextBoxColumn.Name = "TvAdicDataGridViewTextBoxColumn"
        Me.TvAdicDataGridViewTextBoxColumn.ReadOnly = True
        Me.TvAdicDataGridViewTextBoxColumn.Visible = False
        Me.TvAdicDataGridViewTextBoxColumn.Width = 50
        '
        'MesesCortesiaDataGridViewTextBoxColumn
        '
        Me.MesesCortesiaDataGridViewTextBoxColumn.DataPropertyName = "Meses_Cortesia"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MesesCortesiaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.MesesCortesiaDataGridViewTextBoxColumn.HeaderText = ""
        Me.MesesCortesiaDataGridViewTextBoxColumn.Name = "MesesCortesiaDataGridViewTextBoxColumn"
        Me.MesesCortesiaDataGridViewTextBoxColumn.ReadOnly = True
        Me.MesesCortesiaDataGridViewTextBoxColumn.Visible = False
        Me.MesesCortesiaDataGridViewTextBoxColumn.Width = 70
        '
        'MesesApagarDataGridViewTextBoxColumn
        '
        Me.MesesApagarDataGridViewTextBoxColumn.DataPropertyName = "mesesApagar"
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.Format = "N0"
        Me.MesesApagarDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle6
        Me.MesesApagarDataGridViewTextBoxColumn.HeaderText = ""
        Me.MesesApagarDataGridViewTextBoxColumn.Name = "MesesApagarDataGridViewTextBoxColumn"
        Me.MesesApagarDataGridViewTextBoxColumn.ReadOnly = True
        Me.MesesApagarDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MesesApagarDataGridViewTextBoxColumn.Visible = False
        Me.MesesApagarDataGridViewTextBoxColumn.Width = 70
        '
        'ImporteDataGridViewTextBoxColumn
        '
        Me.ImporteDataGridViewTextBoxColumn.DataPropertyName = "importe"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.Format = "C2"
        DataGridViewCellStyle7.NullValue = Nothing
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ImporteDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle7
        Me.ImporteDataGridViewTextBoxColumn.HeaderText = "Costo"
        Me.ImporteDataGridViewTextBoxColumn.MinimumWidth = 230
        Me.ImporteDataGridViewTextBoxColumn.Name = "ImporteDataGridViewTextBoxColumn"
        Me.ImporteDataGridViewTextBoxColumn.ReadOnly = True
        Me.ImporteDataGridViewTextBoxColumn.Width = 230
        '
        'PeriodoPagadoIniDataGridViewTextBoxColumn
        '
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.DataPropertyName = "periodoPagadoIni"
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle8
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.HeaderText = ""
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.Name = "PeriodoPagadoIniDataGridViewTextBoxColumn"
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.ReadOnly = True
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.Visible = False
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.Width = 80
        '
        'PeriodoPagadoFinDataGridViewTextBoxColumn
        '
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.DataPropertyName = "periodoPagadoFin"
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle9
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.HeaderText = ""
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.Name = "PeriodoPagadoFinDataGridViewTextBoxColumn"
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.ReadOnly = True
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.Visible = False
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.Width = 80
        '
        'PuntosAplicadosOtrosDataGridViewTextBoxColumn
        '
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.DataPropertyName = "PuntosAplicadosOtros"
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.Format = "N0"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle10
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.HeaderText = ""
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.Name = "PuntosAplicadosOtrosDataGridViewTextBoxColumn"
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.ReadOnly = True
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.Visible = False
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.Width = 80
        '
        'PuntosAplicadosAntDataGridViewTextBoxColumn
        '
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.DataPropertyName = "puntosAplicadosAnt"
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.Format = "N0"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle11
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.HeaderText = ""
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.Name = "PuntosAplicadosAntDataGridViewTextBoxColumn"
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.ReadOnly = True
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.Visible = False
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.Width = 80
        '
        'PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn
        '
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.DataPropertyName = "PuntosAplicadosPagoAdelantado"
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.Format = "N0"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle12
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.HeaderText = ""
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.Name = "PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn"
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.ReadOnly = True
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.Visible = False
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.Width = 80
        '
        'DescuentoNet
        '
        Me.DescuentoNet.DataPropertyName = "DescuentoNet"
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.Format = "N0"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.DescuentoNet.DefaultCellStyle = DataGridViewCellStyle13
        Me.DescuentoNet.HeaderText = ""
        Me.DescuentoNet.Name = "DescuentoNet"
        Me.DescuentoNet.ReadOnly = True
        Me.DescuentoNet.Visible = False
        Me.DescuentoNet.Width = 60
        '
        'Des_Otr_Ser_Misma_Categoria
        '
        Me.Des_Otr_Ser_Misma_Categoria.DataPropertyName = "Des_Otr_Ser_Misma_Categoria"
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.Format = "N0"
        DataGridViewCellStyle14.NullValue = Nothing
        Me.Des_Otr_Ser_Misma_Categoria.DefaultCellStyle = DataGridViewCellStyle14
        Me.Des_Otr_Ser_Misma_Categoria.HeaderText = ""
        Me.Des_Otr_Ser_Misma_Categoria.Name = "Des_Otr_Ser_Misma_Categoria"
        Me.Des_Otr_Ser_Misma_Categoria.ReadOnly = True
        Me.Des_Otr_Ser_Misma_Categoria.Visible = False
        Me.Des_Otr_Ser_Misma_Categoria.Width = 60
        '
        'BonificacionDataGridViewTextBoxColumn
        '
        Me.BonificacionDataGridViewTextBoxColumn.DataPropertyName = "bonificacion"
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BonificacionDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle15
        Me.BonificacionDataGridViewTextBoxColumn.HeaderText = ""
        Me.BonificacionDataGridViewTextBoxColumn.Name = "BonificacionDataGridViewTextBoxColumn"
        Me.BonificacionDataGridViewTextBoxColumn.ReadOnly = True
        Me.BonificacionDataGridViewTextBoxColumn.Visible = False
        '
        'ImporteAdicionalDataGridViewTextBoxColumn
        '
        Me.ImporteAdicionalDataGridViewTextBoxColumn.DataPropertyName = "importeAdicional"
        Me.ImporteAdicionalDataGridViewTextBoxColumn.HeaderText = ""
        Me.ImporteAdicionalDataGridViewTextBoxColumn.Name = "ImporteAdicionalDataGridViewTextBoxColumn"
        Me.ImporteAdicionalDataGridViewTextBoxColumn.ReadOnly = True
        Me.ImporteAdicionalDataGridViewTextBoxColumn.Visible = False
        '
        'ColumnaDetalleDataGridViewTextBoxColumn
        '
        Me.ColumnaDetalleDataGridViewTextBoxColumn.DataPropertyName = "columnaDetalle"
        Me.ColumnaDetalleDataGridViewTextBoxColumn.HeaderText = ""
        Me.ColumnaDetalleDataGridViewTextBoxColumn.Name = "ColumnaDetalleDataGridViewTextBoxColumn"
        Me.ColumnaDetalleDataGridViewTextBoxColumn.ReadOnly = True
        Me.ColumnaDetalleDataGridViewTextBoxColumn.Visible = False
        '
        'DiasBonificaDataGridViewTextBoxColumn
        '
        Me.DiasBonificaDataGridViewTextBoxColumn.DataPropertyName = "DiasBonifica"
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DiasBonificaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle16
        Me.DiasBonificaDataGridViewTextBoxColumn.HeaderText = ""
        Me.DiasBonificaDataGridViewTextBoxColumn.Name = "DiasBonificaDataGridViewTextBoxColumn"
        Me.DiasBonificaDataGridViewTextBoxColumn.ReadOnly = True
        Me.DiasBonificaDataGridViewTextBoxColumn.Visible = False
        '
        'MesesBonificarDataGridViewTextBoxColumn
        '
        Me.MesesBonificarDataGridViewTextBoxColumn.DataPropertyName = "mesesBonificar"
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MesesBonificarDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle17
        Me.MesesBonificarDataGridViewTextBoxColumn.HeaderText = ""
        Me.MesesBonificarDataGridViewTextBoxColumn.Name = "MesesBonificarDataGridViewTextBoxColumn"
        Me.MesesBonificarDataGridViewTextBoxColumn.ReadOnly = True
        Me.MesesBonificarDataGridViewTextBoxColumn.Visible = False
        '
        'ImporteBonificaDataGridViewTextBoxColumn
        '
        Me.ImporteBonificaDataGridViewTextBoxColumn.DataPropertyName = "importeBonifica"
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteBonificaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle18
        Me.ImporteBonificaDataGridViewTextBoxColumn.HeaderText = ""
        Me.ImporteBonificaDataGridViewTextBoxColumn.Name = "ImporteBonificaDataGridViewTextBoxColumn"
        Me.ImporteBonificaDataGridViewTextBoxColumn.ReadOnly = True
        Me.ImporteBonificaDataGridViewTextBoxColumn.Visible = False
        '
        'UltimoMesDataGridViewTextBoxColumn
        '
        Me.UltimoMesDataGridViewTextBoxColumn.DataPropertyName = "Ultimo_Mes"
        Me.UltimoMesDataGridViewTextBoxColumn.HeaderText = ""
        Me.UltimoMesDataGridViewTextBoxColumn.Name = "UltimoMesDataGridViewTextBoxColumn"
        Me.UltimoMesDataGridViewTextBoxColumn.ReadOnly = True
        Me.UltimoMesDataGridViewTextBoxColumn.Visible = False
        '
        'UltimoanioDataGridViewTextBoxColumn
        '
        Me.UltimoanioDataGridViewTextBoxColumn.DataPropertyName = "Ultimo_anio"
        Me.UltimoanioDataGridViewTextBoxColumn.HeaderText = ""
        Me.UltimoanioDataGridViewTextBoxColumn.Name = "UltimoanioDataGridViewTextBoxColumn"
        Me.UltimoanioDataGridViewTextBoxColumn.ReadOnly = True
        Me.UltimoanioDataGridViewTextBoxColumn.Visible = False
        '
        'AdelantadoDataGridViewCheckBoxColumn
        '
        Me.AdelantadoDataGridViewCheckBoxColumn.DataPropertyName = "Adelantado"
        Me.AdelantadoDataGridViewCheckBoxColumn.HeaderText = ""
        Me.AdelantadoDataGridViewCheckBoxColumn.Name = "AdelantadoDataGridViewCheckBoxColumn"
        Me.AdelantadoDataGridViewCheckBoxColumn.ReadOnly = True
        Me.AdelantadoDataGridViewCheckBoxColumn.Visible = False
        '
        'DESCRIPCIONDataGridViewTextBoxColumn
        '
        Me.DESCRIPCIONDataGridViewTextBoxColumn.DataPropertyName = "DESCRIPCION"
        Me.DESCRIPCIONDataGridViewTextBoxColumn.HeaderText = ""
        Me.DESCRIPCIONDataGridViewTextBoxColumn.Name = "DESCRIPCIONDataGridViewTextBoxColumn"
        Me.DESCRIPCIONDataGridViewTextBoxColumn.ReadOnly = True
        Me.DESCRIPCIONDataGridViewTextBoxColumn.Visible = False
        Me.DESCRIPCIONDataGridViewTextBoxColumn.Width = 5
        '
        'CLV_DETALLE
        '
        Me.CLV_DETALLE.DataPropertyName = "Clv_Detalle"
        Me.CLV_DETALLE.HeaderText = "CLV_DETALLE"
        Me.CLV_DETALLE.Name = "CLV_DETALLE"
        Me.CLV_DETALLE.ReadOnly = True
        Me.CLV_DETALLE.Width = 5
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label15)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel2.Controls.Add(Me.REDLabel26)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label25)
        Me.SplitContainer1.Panel2.Controls.Add(Me.REDLabel25)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label24)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label23)
        Me.SplitContainer1.Panel2.Controls.Add(Me.ContratoTextBox)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label20)
        Me.SplitContainer1.Panel2.Controls.Add(Me.CLV_TIPOCLIENTELabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.DESCRIPCIONLabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Button8)
        Me.SplitContainer1.Panel2.Controls.Add(ESHOTELLabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.ESHOTELCheckBox)
        Me.SplitContainer1.Panel2.Controls.Add(SOLOINTERNETLabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.NOMBRELabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label8)
        Me.SplitContainer1.Panel2.Controls.Add(Me.CIUDADLabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.NUMEROLabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label11)
        Me.SplitContainer1.Panel2.Controls.Add(Me.COLONIALabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label10)
        Me.SplitContainer1.Panel2.Controls.Add(Me.CALLELabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Clv_Session)
        Me.SplitContainer1.Size = New System.Drawing.Size(618, 219)
        Me.SplitContainer1.SplitterDistance = 206
        Me.SplitContainer1.TabIndex = 0
        Me.SplitContainer1.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel4.Controls.Add(Me.Fecha_Venta)
        Me.Panel4.Controls.Add(Me.Label22)
        Me.Panel4.Controls.Add(Me.ComboBox2)
        Me.Panel4.Controls.Add(Me.Label16)
        Me.Panel4.Controls.Add(Me.FolioTextBox)
        Me.Panel4.Controls.Add(Me.Label14)
        Me.Panel4.Controls.Add(Me.Label18)
        Me.Panel4.Controls.Add(Me.Label17)
        Me.Panel4.Controls.Add(Me.ComboBox1)
        Me.Panel4.Location = New System.Drawing.Point(3, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(200, 213)
        Me.Panel4.TabIndex = 8
        '
        'Fecha_Venta
        '
        Me.Fecha_Venta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_Venta.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Fecha_Venta.Location = New System.Drawing.Point(6, 174)
        Me.Fecha_Venta.Name = "Fecha_Venta"
        Me.Fecha_Venta.Size = New System.Drawing.Size(118, 24)
        Me.Fecha_Venta.TabIndex = 503
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label22.Location = New System.Drawing.Point(6, 158)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(50, 13)
        Me.Label22.TabIndex = 500
        Me.Label22.Text = "Fecha :"
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.UltimoSERIEYFOLIOBindingSource
        Me.ComboBox2.DisplayMember = "SERIE"
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(6, 81)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(103, 23)
        Me.ComboBox2.TabIndex = 499
        Me.ComboBox2.ValueMember = "SERIE"
        '
        'UltimoSERIEYFOLIOBindingSource
        '
        Me.UltimoSERIEYFOLIOBindingSource.DataMember = "Ultimo_SERIEYFOLIO"
        Me.UltimoSERIEYFOLIOBindingSource.DataSource = Me.DataSetEdgar
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label16.Location = New System.Drawing.Point(3, 5)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(69, 18)
        Me.Label16.TabIndex = 4
        Me.Label16.Text = "Ventas :"
        '
        'FolioTextBox
        '
        Me.FolioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FolioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FolioTextBox.Location = New System.Drawing.Point(6, 129)
        Me.FolioTextBox.Name = "FolioTextBox"
        Me.FolioTextBox.Size = New System.Drawing.Size(103, 24)
        Me.FolioTextBox.TabIndex = 7
        Me.FolioTextBox.TabStop = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label14.Location = New System.Drawing.Point(8, 23)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(69, 13)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Vendedor :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label18.Location = New System.Drawing.Point(8, 113)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(42, 13)
        Me.Label18.TabIndex = 6
        Me.Label18.Text = "Folio :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label17.Location = New System.Drawing.Point(8, 65)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(44, 13)
        Me.Label17.TabIndex = 5
        Me.Label17.Text = "Serie :"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MUESTRAVENDEDORES2BindingSource
        Me.ComboBox1.DisplayMember = "Nombre"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(6, 40)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(191, 23)
        Me.ComboBox1.TabIndex = 498
        Me.ComboBox1.ValueMember = "Clv_Vendedor"
        '
        'MUESTRAVENDEDORES2BindingSource
        '
        Me.MUESTRAVENDEDORES2BindingSource.DataMember = "MUESTRAVENDEDORES_2"
        Me.MUESTRAVENDEDORES2BindingSource.DataSource = Me.DataSetEdgar
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(4, 53)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(0, 13)
        Me.Label15.TabIndex = 3
        '
        'REDLabel26
        '
        Me.REDLabel26.AutoSize = True
        Me.REDLabel26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.REDLabel26.Location = New System.Drawing.Point(7, 197)
        Me.REDLabel26.Name = "REDLabel26"
        Me.REDLabel26.Size = New System.Drawing.Size(73, 13)
        Me.REDLabel26.TabIndex = 510
        Me.REDLabel26.Text = "Al Corriente"
        Me.REDLabel26.Visible = False
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(7, 181)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(50, 13)
        Me.Label25.TabIndex = 189
        Me.Label25.Text = "Periodo"
        '
        'REDLabel25
        '
        Me.REDLabel25.AutoSize = True
        Me.REDLabel25.BackColor = System.Drawing.Color.Yellow
        Me.REDLabel25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.REDLabel25.ForeColor = System.Drawing.Color.Red
        Me.REDLabel25.Location = New System.Drawing.Point(300, 4)
        Me.REDLabel25.Name = "REDLabel25"
        Me.REDLabel25.Size = New System.Drawing.Size(90, 13)
        Me.REDLabel25.TabIndex = 188
        Me.REDLabel25.Text = "Datos Fiscales"
        Me.REDLabel25.Visible = False
        '
        'Label24
        '
        Me.Label24.AutoEllipsis = True
        Me.Label24.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label24.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "TELEFONO", True))
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(291, 97)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(114, 26)
        Me.Label24.TabIndex = 187
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label23.Location = New System.Drawing.Point(248, 96)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(46, 18)
        Me.Label23.TabIndex = 186
        Me.Label23.Text = "Tel. :"
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ContratoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoTextBox.Location = New System.Drawing.Point(76, 14)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(91, 24)
        Me.ContratoTextBox.TabIndex = 0
        Me.ContratoTextBox.Text = "0"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label20.Location = New System.Drawing.Point(10, 164)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(85, 15)
        Me.Label20.TabIndex = 185
        Me.Label20.Text = "Tipo Cobro :"
        '
        'CLV_TIPOCLIENTELabel1
        '
        Me.CLV_TIPOCLIENTELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMETIPOSCLIENTESBindingSource, "CLV_TIPOCLIENTE", True))
        Me.CLV_TIPOCLIENTELabel1.ForeColor = System.Drawing.Color.DarkOrange
        Me.CLV_TIPOCLIENTELabel1.Location = New System.Drawing.Point(63, 158)
        Me.CLV_TIPOCLIENTELabel1.Name = "CLV_TIPOCLIENTELabel1"
        Me.CLV_TIPOCLIENTELabel1.Size = New System.Drawing.Size(11, 26)
        Me.CLV_TIPOCLIENTELabel1.TabIndex = 184
        '
        'DAMETIPOSCLIENTESBindingSource
        '
        Me.DAMETIPOSCLIENTESBindingSource.DataMember = "DAMETIPOSCLIENTES"
        Me.DAMETIPOSCLIENTESBindingSource.DataSource = Me.DataSetEdgar
        '
        'DESCRIPCIONLabel1
        '
        Me.DESCRIPCIONLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMETIPOSCLIENTESBindingSource, "DESCRIPCION", True))
        Me.DESCRIPCIONLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DESCRIPCIONLabel1.ForeColor = System.Drawing.Color.Black
        Me.DESCRIPCIONLabel1.Location = New System.Drawing.Point(101, 161)
        Me.DESCRIPCIONLabel1.Name = "DESCRIPCIONLabel1"
        Me.DESCRIPCIONLabel1.Size = New System.Drawing.Size(176, 22)
        Me.DESCRIPCIONLabel1.TabIndex = 183
        Me.DESCRIPCIONLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkRed
        Me.Button8.Enabled = False
        Me.Button8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.White
        Me.Button8.Location = New System.Drawing.Point(189, 182)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(211, 28)
        Me.Button8.TabIndex = 2
        Me.Button8.Text = "&VER HISTORIAL "
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(14, 144)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 15)
        Me.Label2.TabIndex = 173
        Me.Label2.Text = "Ciudad :"
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATOFACBindingSource, "ESHOTEL", True))
        Me.ESHOTELCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESHOTELCheckBox.ForeColor = System.Drawing.Color.Black
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(320, 17)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(21, 27)
        Me.ESHOTELCheckBox.TabIndex = 19
        Me.ESHOTELCheckBox.TabStop = False
        Me.ESHOTELCheckBox.Visible = False
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATOFACBindingSource, "SOLOINTERNET", True))
        Me.SOLOINTERNETCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SOLOINTERNETCheckBox.ForeColor = System.Drawing.Color.Black
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(213, 17)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(21, 27)
        Me.SOLOINTERNETCheckBox.TabIndex = 17
        Me.SOLOINTERNETCheckBox.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(4, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 15)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Contrato :"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkRed
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(172, 11)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(30, 28)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = False
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.AutoEllipsis = True
        Me.NOMBRELabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.NOMBRELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "NOMBRE", True))
        Me.NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel1.ForeColor = System.Drawing.Color.Black
        Me.NOMBRELabel1.Location = New System.Drawing.Point(76, 41)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(329, 28)
        Me.NOMBRELabel1.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(10, 42)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(66, 15)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Nombre :"
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.AutoEllipsis = True
        Me.CIUDADLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CIUDADLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "CIUDAD", True))
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.ForeColor = System.Drawing.Color.Black
        Me.CIUDADLabel1.Location = New System.Drawing.Point(76, 143)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(232, 26)
        Me.CIUDADLabel1.TabIndex = 11
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.AutoEllipsis = True
        Me.NUMEROLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.NUMEROLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "NUMERO", True))
        Me.NUMEROLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROLabel1.ForeColor = System.Drawing.Color.Black
        Me.NUMEROLabel1.Location = New System.Drawing.Point(76, 93)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(172, 26)
        Me.NUMEROLabel1.TabIndex = 9
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label11.Location = New System.Drawing.Point(10, 119)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(64, 15)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Colonia :"
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.AutoEllipsis = True
        Me.COLONIALabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.COLONIALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "COLONIA", True))
        Me.COLONIALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIALabel1.ForeColor = System.Drawing.Color.Black
        Me.COLONIALabel1.Location = New System.Drawing.Point(76, 118)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(314, 26)
        Me.COLONIALabel1.TabIndex = 7
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label10.Location = New System.Drawing.Point(49, 92)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(27, 18)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "# :"
        '
        'CALLELabel1
        '
        Me.CALLELabel1.AutoEllipsis = True
        Me.CALLELabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CALLELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "CALLE", True))
        Me.CALLELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLELabel1.ForeColor = System.Drawing.Color.Black
        Me.CALLELabel1.Location = New System.Drawing.Point(76, 68)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(329, 26)
        Me.CALLELabel1.TabIndex = 5
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label9.Location = New System.Drawing.Point(0, 69)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(76, 15)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Dirección :"
        '
        'Clv_Session
        '
        Me.Clv_Session.BackColor = System.Drawing.Color.DarkOrange
        Me.Clv_Session.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_Session.Location = New System.Drawing.Point(89, 18)
        Me.Clv_Session.Name = "Clv_Session"
        Me.Clv_Session.Size = New System.Drawing.Size(78, 13)
        Me.Clv_Session.TabIndex = 182
        Me.Clv_Session.TabStop = False
        Me.Clv_Session.Text = "0"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.SplitContainer1)
        Me.Panel3.Location = New System.Drawing.Point(6, 61)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(618, 219)
        Me.Panel3.TabIndex = 196
        '
        'MUESTRAVENDEDORES_2TableAdapter
        '
        Me.MUESTRAVENDEDORES_2TableAdapter.ClearBeforeFill = True
        '
        'Ultimo_SERIEYFOLIOTableAdapter
        '
        Me.Ultimo_SERIEYFOLIOTableAdapter.ClearBeforeFill = True
        '
        'DAMEUltimo_FOLIOBindingSource
        '
        Me.DAMEUltimo_FOLIOBindingSource.DataMember = "DAMEUltimo_FOLIO"
        Me.DAMEUltimo_FOLIOBindingSource.DataSource = Me.DataSetEdgar
        '
        'DAMEUltimo_FOLIOTableAdapter
        '
        Me.DAMEUltimo_FOLIOTableAdapter.ClearBeforeFill = True
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DAMETOTALSumaDetalleBindingSource
        '
        Me.DAMETOTALSumaDetalleBindingSource.DataMember = "DAMETOTALSumaDetalle"
        Me.DAMETOTALSumaDetalleBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'DAMETOTALSumaDetalleTableAdapter
        '
        Me.DAMETOTALSumaDetalleTableAdapter.ClearBeforeFill = True
        '
        'GUARDATIPOPAGOBindingSource
        '
        Me.GUARDATIPOPAGOBindingSource.DataMember = "GUARDATIPOPAGO"
        Me.GUARDATIPOPAGOBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'GUARDATIPOPAGOTableAdapter
        '
        Me.GUARDATIPOPAGOTableAdapter.ClearBeforeFill = True
        '
        'QUITARDELDETALLEBindingSource
        '
        Me.QUITARDELDETALLEBindingSource.DataMember = "QUITARDELDETALLE"
        Me.QUITARDELDETALLEBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'QUITARDELDETALLETableAdapter
        '
        Me.QUITARDELDETALLETableAdapter.ClearBeforeFill = True
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.CLV_DETALLETextBox)
        Me.Panel5.Controls.Add(Me.txtMsjError)
        Me.Panel5.Location = New System.Drawing.Point(658, 542)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(360, 120)
        Me.Panel5.TabIndex = 197
        Me.Panel5.Visible = False
        '
        'CLV_DETALLETextBox
        '
        Me.CLV_DETALLETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDetalleBindingSource, "CLV_DETALLE", True))
        Me.CLV_DETALLETextBox.Enabled = False
        Me.CLV_DETALLETextBox.Location = New System.Drawing.Point(343, 9)
        Me.CLV_DETALLETextBox.Name = "CLV_DETALLETextBox"
        Me.CLV_DETALLETextBox.Size = New System.Drawing.Size(100, 20)
        Me.CLV_DETALLETextBox.TabIndex = 509
        Me.CLV_DETALLETextBox.Visible = False
        '
        'txtMsjError
        '
        Me.txtMsjError.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtMsjError.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMsjError.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMsjError.ForeColor = System.Drawing.Color.Red
        Me.txtMsjError.Location = New System.Drawing.Point(0, 95)
        Me.txtMsjError.Multiline = True
        Me.txtMsjError.Name = "txtMsjError"
        Me.txtMsjError.ReadOnly = True
        Me.txtMsjError.Size = New System.Drawing.Size(359, 54)
        Me.txtMsjError.TabIndex = 538
        '
        'LABEL19
        '
        Me.LABEL19.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LABEL19.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.LABEL19.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LABEL19.ForeColor = System.Drawing.Color.Red
        Me.LABEL19.Location = New System.Drawing.Point(662, 546)
        Me.LABEL19.Multiline = True
        Me.LABEL19.Name = "LABEL19"
        Me.LABEL19.ReadOnly = True
        Me.LABEL19.Size = New System.Drawing.Size(354, 112)
        Me.LABEL19.TabIndex = 500
        Me.LABEL19.TabStop = False
        '
        'SumaDetalleDataGridView
        '
        Me.SumaDetalleDataGridView.AllowUserToAddRows = False
        Me.SumaDetalleDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle19.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle19
        Me.SumaDetalleDataGridView.AutoGenerateColumns = False
        Me.SumaDetalleDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.SumaDetalleDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SumaDetalleDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.SumaDetalleDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SumaDetalleDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClvSessionDataGridViewTextBoxColumn1, Me.PosicionDataGridViewTextBoxColumn, Me.NivelDataGridViewTextBoxColumn, Me.DescripcionDataGridViewTextBoxColumn1, Me.TotalDataGridViewTextBoxColumn})
        Me.SumaDetalleDataGridView.DataSource = Me.SumaDetalleBindingSource
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.SumaDetalleDataGridView.DefaultCellStyle = DataGridViewCellStyle23
        Me.SumaDetalleDataGridView.GridColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.Location = New System.Drawing.Point(13, 19)
        Me.SumaDetalleDataGridView.MultiSelect = False
        Me.SumaDetalleDataGridView.Name = "SumaDetalleDataGridView"
        Me.SumaDetalleDataGridView.RightToLeft = System.Windows.Forms.RightToLeft.No
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SumaDetalleDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle24
        DataGridViewCellStyle25.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.RowsDefaultCellStyle = DataGridViewCellStyle25
        Me.SumaDetalleDataGridView.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.Size = New System.Drawing.Size(67, 60)
        Me.SumaDetalleDataGridView.TabIndex = 199
        Me.SumaDetalleDataGridView.TabStop = False
        '
        'ClvSessionDataGridViewTextBoxColumn1
        '
        Me.ClvSessionDataGridViewTextBoxColumn1.DataPropertyName = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn1.HeaderText = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn1.Name = "ClvSessionDataGridViewTextBoxColumn1"
        Me.ClvSessionDataGridViewTextBoxColumn1.Visible = False
        '
        'PosicionDataGridViewTextBoxColumn
        '
        Me.PosicionDataGridViewTextBoxColumn.DataPropertyName = "Posicion"
        Me.PosicionDataGridViewTextBoxColumn.HeaderText = "Posicion"
        Me.PosicionDataGridViewTextBoxColumn.Name = "PosicionDataGridViewTextBoxColumn"
        Me.PosicionDataGridViewTextBoxColumn.Visible = False
        '
        'NivelDataGridViewTextBoxColumn
        '
        Me.NivelDataGridViewTextBoxColumn.DataPropertyName = "Nivel"
        Me.NivelDataGridViewTextBoxColumn.HeaderText = "Nivel"
        Me.NivelDataGridViewTextBoxColumn.Name = "NivelDataGridViewTextBoxColumn"
        Me.NivelDataGridViewTextBoxColumn.Visible = False
        '
        'DescripcionDataGridViewTextBoxColumn1
        '
        Me.DescripcionDataGridViewTextBoxColumn1.DataPropertyName = "Descripcion"
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle21
        Me.DescripcionDataGridViewTextBoxColumn1.HeaderText = "Descripcion"
        Me.DescripcionDataGridViewTextBoxColumn1.Name = "DescripcionDataGridViewTextBoxColumn1"
        Me.DescripcionDataGridViewTextBoxColumn1.Width = 200
        '
        'TotalDataGridViewTextBoxColumn
        '
        Me.TotalDataGridViewTextBoxColumn.DataPropertyName = "Total"
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomRight
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle22
        Me.TotalDataGridViewTextBoxColumn.HeaderText = "Total"
        Me.TotalDataGridViewTextBoxColumn.Name = "TotalDataGridViewTextBoxColumn"
        Me.TotalDataGridViewTextBoxColumn.Width = 245
        '
        'DAMETIPOSCLIENTESTableAdapter
        '
        Me.DAMETIPOSCLIENTESTableAdapter.ClearBeforeFill = True
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 500
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkRed
        Me.Button6.Enabled = False
        Me.Button6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.White
        Me.Button6.Location = New System.Drawing.Point(466, 551)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(181, 34)
        Me.Button6.TabIndex = 502
        Me.Button6.Text = "&BONIFICAR"
        Me.Button6.UseVisualStyleBackColor = False
        Me.Button6.Visible = False
        '
        'GuardaMotivosBonificacionBindingSource
        '
        Me.GuardaMotivosBonificacionBindingSource.DataMember = "GuardaMotivosBonificacion"
        Me.GuardaMotivosBonificacionBindingSource.DataSource = Me.DataSetEdgar
        '
        'GuardaMotivosBonificacionTableAdapter
        '
        Me.GuardaMotivosBonificacionTableAdapter.ClearBeforeFill = True
        '
        'NewsoftvDataSet2
        '
        Me.NewsoftvDataSet2.DataSetName = "NewsoftvDataSet2"
        Me.NewsoftvDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BORCAMDOCFAC_QUITABindingSource
        '
        Me.BORCAMDOCFAC_QUITABindingSource.DataMember = "BORCAMDOCFAC_QUITA"
        Me.BORCAMDOCFAC_QUITABindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'BORCAMDOCFAC_QUITATableAdapter
        '
        Me.BORCAMDOCFAC_QUITATableAdapter.ClearBeforeFill = True
        '
        'DamelasOrdenesque_GeneroFacturaBindingSource
        '
        Me.DamelasOrdenesque_GeneroFacturaBindingSource.DataMember = "DamelasOrdenesque_GeneroFactura"
        Me.DamelasOrdenesque_GeneroFacturaBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'DamelasOrdenesque_GeneroFacturaTableAdapter
        '
        Me.DamelasOrdenesque_GeneroFacturaTableAdapter.ClearBeforeFill = True
        '
        'Dame_Impresora_OrdenesBindingSource
        '
        Me.Dame_Impresora_OrdenesBindingSource.DataMember = "Dame_Impresora_Ordenes"
        Me.Dame_Impresora_OrdenesBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'Dame_Impresora_OrdenesTableAdapter
        '
        Me.Dame_Impresora_OrdenesTableAdapter.ClearBeforeFill = True
        '
        'DimesiahiConexBindingSource
        '
        Me.DimesiahiConexBindingSource.DataMember = "DimesiahiConex"
        Me.DimesiahiConexBindingSource.DataSource = Me.DataSetEdgar
        '
        'DimesiahiConexTableAdapter
        '
        Me.DimesiahiConexTableAdapter.ClearBeforeFill = True
        '
        'BuscaBloqueadoBindingSource
        '
        Me.BuscaBloqueadoBindingSource.DataMember = "BuscaBloqueado"
        Me.BuscaBloqueadoBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'BuscaBloqueadoTableAdapter
        '
        Me.BuscaBloqueadoTableAdapter.ClearBeforeFill = True
        '
        'BusFacFiscalBindingSource
        '
        Me.BusFacFiscalBindingSource.DataMember = "BusFacFiscal"
        Me.BusFacFiscalBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'BusFacFiscalTableAdapter
        '
        Me.BusFacFiscalTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Comentario2BindingSource
        '
        Me.Inserta_Comentario2BindingSource.DataMember = "Inserta_Comentario2"
        Me.Inserta_Comentario2BindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'Inserta_Comentario2TableAdapter
        '
        Me.Inserta_Comentario2TableAdapter.ClearBeforeFill = True
        '
        'Hora_insBindingSource
        '
        Me.Hora_insBindingSource.DataMember = "Hora_ins"
        Me.Hora_insBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'Hora_insTableAdapter
        '
        Me.Hora_insTableAdapter.ClearBeforeFill = True
        '
        'Selecciona_Impresora_SucursalBindingSource
        '
        Me.Selecciona_Impresora_SucursalBindingSource.DataSource = Me.NewsoftvDataSet2
        Me.Selecciona_Impresora_SucursalBindingSource.Position = 0
        '
        'Selecciona_Impresora_SucursalTableAdapter
        '
        Me.Selecciona_Impresora_SucursalTableAdapter.ClearBeforeFill = True
        '
        'Dime_ContratacionBindingSource
        '
        Me.Dime_ContratacionBindingSource.DataSource = Me.NewsoftvDataSet2
        Me.Dime_ContratacionBindingSource.Position = 0
        '
        'Dime_ContratacionTableAdapter
        '
        Me.Dime_ContratacionTableAdapter.ClearBeforeFill = True
        '
        'CMBPanel6
        '
        Me.CMBPanel6.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CMBPanel6.Controls.Add(Me.Label21)
        Me.CMBPanel6.Controls.Add(Me.Button11)
        Me.CMBPanel6.Controls.Add(Me.Button10)
        Me.CMBPanel6.Location = New System.Drawing.Point(22, 102)
        Me.CMBPanel6.Name = "CMBPanel6"
        Me.CMBPanel6.Size = New System.Drawing.Size(84, 70)
        Me.CMBPanel6.TabIndex = 507
        Me.CMBPanel6.Visible = False
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(34, 15)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(381, 90)
        Me.Label21.TabIndex = 2
        Me.Label21.Text = "¿ Deseas Hacer el Pago de la Contratación en Pagos Parciales ?"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button11
        '
        Me.Button11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.Location = New System.Drawing.Point(227, 118)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(95, 43)
        Me.Button11.TabIndex = 1
        Me.Button11.Text = "No"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.Location = New System.Drawing.Point(108, 118)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(95, 43)
        Me.Button10.TabIndex = 0
        Me.Button10.Text = "Sí"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Dime_Si_ProcedePagoParcialBindingSource
        '
        Me.Dime_Si_ProcedePagoParcialBindingSource.DataMember = "Dime_Si_ProcedePagoParcial"
        Me.Dime_Si_ProcedePagoParcialBindingSource.DataSource = Me.DataSetEdgar
        '
        'Dime_Si_ProcedePagoParcialTableAdapter
        '
        Me.Dime_Si_ProcedePagoParcialTableAdapter.ClearBeforeFill = True
        '
        'Cobra_PagosBindingSource
        '
        Me.Cobra_PagosBindingSource.DataMember = "Cobra_Pagos"
        Me.Cobra_PagosBindingSource.DataSource = Me.DataSetEdgar
        '
        'Cobra_PagosTableAdapter
        '
        Me.Cobra_PagosTableAdapter.ClearBeforeFill = True
        '
        'AgregarServicioAdicionales_PPEBindingSource1
        '
        Me.AgregarServicioAdicionales_PPEBindingSource1.DataMember = "AgregarServicioAdicionales_PPE"
        Me.AgregarServicioAdicionales_PPEBindingSource1.DataSource = Me.DataSetEdgar
        '
        'AgregarServicioAdicionales_PPETableAdapter1
        '
        Me.AgregarServicioAdicionales_PPETableAdapter1.ClearBeforeFill = True
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.DarkRed
        Me.Button12.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.ForeColor = System.Drawing.Color.White
        Me.Button12.Location = New System.Drawing.Point(9, 638)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(181, 34)
        Me.Button12.TabIndex = 508
        Me.Button12.Text = "&COBRA ADEUDO"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'CobraAdeudoBindingSource
        '
        Me.CobraAdeudoBindingSource.DataMember = "CobraAdeudo"
        Me.CobraAdeudoBindingSource.DataSource = Me.DataSetEdgar
        '
        'CobraAdeudoTableAdapter
        '
        Me.CobraAdeudoTableAdapter.ClearBeforeFill = True
        '
        'Pregunta_Si_Puedo_AdelantarBindingSource
        '
        Me.Pregunta_Si_Puedo_AdelantarBindingSource.DataMember = "Pregunta_Si_Puedo_Adelantar"
        Me.Pregunta_Si_Puedo_AdelantarBindingSource.DataSource = Me.DataSetEdgar
        '
        'Pregunta_Si_Puedo_AdelantarTableAdapter
        '
        Me.Pregunta_Si_Puedo_AdelantarTableAdapter.ClearBeforeFill = True
        '
        'Cobra_VentasBindingSource
        '
        Me.Cobra_VentasBindingSource.DataMember = "Cobra_Ventas"
        Me.Cobra_VentasBindingSource.DataSource = Me.DataSetEdgar
        '
        'Cobra_VentasTableAdapter
        '
        Me.Cobra_VentasTableAdapter.ClearBeforeFill = True
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Inserta_Bonificacion_SupervisorBindingSource
        '
        Me.Inserta_Bonificacion_SupervisorBindingSource.DataMember = "Inserta_Bonificacion_Supervisor"
        Me.Inserta_Bonificacion_SupervisorBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Inserta_Bonificacion_SupervisorTableAdapter
        '
        Me.Inserta_Bonificacion_SupervisorTableAdapter.ClearBeforeFill = True
        '
        'EricDataSet2
        '
        Me.EricDataSet2.DataSetName = "EricDataSet2"
        Me.EricDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameGeneralMsjTicketsBindingSource
        '
        Me.DameGeneralMsjTicketsBindingSource.DataMember = "DameGeneralMsjTickets"
        Me.DameGeneralMsjTicketsBindingSource.DataSource = Me.EricDataSet2
        '
        'DameGeneralMsjTicketsTableAdapter
        '
        Me.DameGeneralMsjTicketsTableAdapter.ClearBeforeFill = True
        '
        'EntregaAparatoBindingSource
        '
        Me.EntregaAparatoBindingSource.DataMember = "EntregaAparato"
        Me.EntregaAparatoBindingSource.DataSource = Me.EricDataSet2
        '
        'EntregaAparatoTableAdapter
        '
        Me.EntregaAparatoTableAdapter.ClearBeforeFill = True
        '
        'LabelSubTotal
        '
        Me.LabelSubTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelSubTotal.ForeColor = System.Drawing.Color.Maroon
        Me.LabelSubTotal.Location = New System.Drawing.Point(203, 4)
        Me.LabelSubTotal.Name = "LabelSubTotal"
        Me.LabelSubTotal.Size = New System.Drawing.Size(146, 25)
        Me.LabelSubTotal.TabIndex = 511
        Me.LabelSubTotal.Text = "$totalcargos"
        Me.LabelSubTotal.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelIva
        '
        Me.LabelIva.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelIva.ForeColor = System.Drawing.Color.Maroon
        Me.LabelIva.Location = New System.Drawing.Point(204, 60)
        Me.LabelIva.Name = "LabelIva"
        Me.LabelIva.Size = New System.Drawing.Size(146, 25)
        Me.LabelIva.TabIndex = 512
        Me.LabelIva.Text = "$iva"
        Me.LabelIva.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelTotal
        '
        Me.LabelTotal.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTotal.ForeColor = System.Drawing.Color.Black
        Me.LabelTotal.Location = New System.Drawing.Point(859, 322)
        Me.LabelTotal.Name = "LabelTotal"
        Me.LabelTotal.Size = New System.Drawing.Size(168, 25)
        Me.LabelTotal.TabIndex = 513
        Me.LabelTotal.Text = "$0.00"
        Me.LabelTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label26
        '
        Me.Label26.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(30, 13)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(75, 35)
        Me.Label26.TabIndex = 514
        Me.Label26.Text = "Total de Cargos :"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label27
        '
        Me.Label27.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(136, 65)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(48, 21)
        Me.Label27.TabIndex = 515
        Me.Label27.Text = "I.V.A.:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label28
        '
        Me.Label28.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(672, 322)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(187, 21)
        Me.Label28.TabIndex = 516
        Me.Label28.Text = "Total:"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'CMBLabel29
        '
        Me.CMBLabel29.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel29.Location = New System.Drawing.Point(669, 391)
        Me.CMBLabel29.Name = "CMBLabel29"
        Me.CMBLabel29.Size = New System.Drawing.Size(192, 21)
        Me.CMBLabel29.TabIndex = 517
        Me.CMBLabel29.Text = "Credito por Redondeo:"
        Me.CMBLabel29.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'LblCredito_Apagar
        '
        Me.LblCredito_Apagar.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCredito_Apagar.ForeColor = System.Drawing.Color.Black
        Me.LblCredito_Apagar.Location = New System.Drawing.Point(857, 391)
        Me.LblCredito_Apagar.Name = "LblCredito_Apagar"
        Me.LblCredito_Apagar.Size = New System.Drawing.Size(173, 25)
        Me.LblCredito_Apagar.TabIndex = 518
        Me.LblCredito_Apagar.Text = "$0.00"
        Me.LblCredito_Apagar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblImporte_Total
        '
        Me.LblImporte_Total.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblImporte_Total.Location = New System.Drawing.Point(587, 696)
        Me.LblImporte_Total.Name = "LblImporte_Total"
        Me.LblImporte_Total.Size = New System.Drawing.Size(90, 25)
        Me.LblImporte_Total.TabIndex = 519
        Me.LblImporte_Total.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label29
        '
        Me.Label29.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(682, 357)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(177, 21)
        Me.Label29.TabIndex = 520
        Me.Label29.Text = "Saldo Anterior:"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'LabelSaldoAnterior
        '
        Me.LabelSaldoAnterior.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelSaldoAnterior.ForeColor = System.Drawing.Color.Black
        Me.LabelSaldoAnterior.Location = New System.Drawing.Point(859, 357)
        Me.LabelSaldoAnterior.Name = "LabelSaldoAnterior"
        Me.LabelSaldoAnterior.Size = New System.Drawing.Size(168, 25)
        Me.LabelSaldoAnterior.TabIndex = 521
        Me.LabelSaldoAnterior.Text = "$0.00"
        Me.LabelSaldoAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CMBLabel30
        '
        Me.CMBLabel30.AutoSize = True
        Me.CMBLabel30.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel30.Location = New System.Drawing.Point(3, 19)
        Me.CMBLabel30.Name = "CMBLabel30"
        Me.CMBLabel30.Size = New System.Drawing.Size(117, 22)
        Me.CMBLabel30.TabIndex = 522
        Me.CMBLabel30.Text = "Total a Pagar:"
        Me.CMBLabel30.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'CNOPanel10
        '
        Me.CNOPanel10.BackColor = System.Drawing.Color.LightGray
        Me.CNOPanel10.Controls.Add(Me.CMBLabel30)
        Me.CNOPanel10.Controls.Add(Me.LabelGRan_Total)
        Me.CNOPanel10.Location = New System.Drawing.Point(656, 439)
        Me.CNOPanel10.Name = "CNOPanel10"
        Me.CNOPanel10.Size = New System.Drawing.Size(381, 63)
        Me.CNOPanel10.TabIndex = 523
        '
        'LabelGRan_Total
        '
        Me.LabelGRan_Total.Font = New System.Drawing.Font("Trebuchet MS", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelGRan_Total.ForeColor = System.Drawing.Color.Maroon
        Me.LabelGRan_Total.Location = New System.Drawing.Point(109, 4)
        Me.LabelGRan_Total.Name = "LabelGRan_Total"
        Me.LabelGRan_Total.Size = New System.Drawing.Size(254, 54)
        Me.LabelGRan_Total.TabIndex = 523
        Me.LabelGRan_Total.Text = "$0.00"
        Me.LabelGRan_Total.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextImporte_Adic
        '
        Me.TextImporte_Adic.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SumaDetalleBindingSource, "Total", True))
        Me.TextImporte_Adic.Location = New System.Drawing.Point(555, 699)
        Me.TextImporte_Adic.Name = "TextImporte_Adic"
        Me.TextImporte_Adic.Size = New System.Drawing.Size(124, 20)
        Me.TextImporte_Adic.TabIndex = 524
        '
        'PanelNrm
        '
        Me.PanelNrm.Controls.Add(Me.DataGridView1)
        Me.PanelNrm.Location = New System.Drawing.Point(-4, 283)
        Me.PanelNrm.Name = "PanelNrm"
        Me.PanelNrm.Size = New System.Drawing.Size(654, 219)
        Me.PanelNrm.TabIndex = 525
        Me.PanelNrm.Visible = False
        '
        'PanelTel
        '
        Me.PanelTel.Controls.Add(Me.DataGridView2)
        Me.PanelTel.Controls.Add(Me.CMBPanel6)
        Me.PanelTel.Location = New System.Drawing.Point(4, 284)
        Me.PanelTel.Name = "PanelTel"
        Me.PanelTel.Size = New System.Drawing.Size(645, 218)
        Me.PanelTel.TabIndex = 526
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle26.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle26
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle27.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView2.DefaultCellStyle = DataGridViewCellStyle27
        Me.DataGridView2.Location = New System.Drawing.Point(5, 4)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView2.Size = New System.Drawing.Size(635, 214)
        Me.DataGridView2.TabIndex = 510
        '
        'Clv_SessionTel
        '
        Me.Clv_SessionTel.Location = New System.Drawing.Point(577, 706)
        Me.Clv_SessionTel.Name = "Clv_SessionTel"
        Me.Clv_SessionTel.Size = New System.Drawing.Size(100, 20)
        Me.Clv_SessionTel.TabIndex = 527
        '
        'ButtonVentaEq
        '
        Me.ButtonVentaEq.BackColor = System.Drawing.Color.DarkRed
        Me.ButtonVentaEq.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonVentaEq.ForeColor = System.Drawing.Color.White
        Me.ButtonVentaEq.Location = New System.Drawing.Point(-1, 4)
        Me.ButtonVentaEq.Name = "ButtonVentaEq"
        Me.ButtonVentaEq.Size = New System.Drawing.Size(22, 13)
        Me.ButtonVentaEq.TabIndex = 528
        Me.ButtonVentaEq.Text = "&VENTA DE EQUIPO"
        Me.ButtonVentaEq.UseVisualStyleBackColor = False
        Me.ButtonVentaEq.Visible = False
        '
        'ButtonPagoAbono
        '
        Me.ButtonPagoAbono.BackColor = System.Drawing.Color.DarkRed
        Me.ButtonPagoAbono.Enabled = False
        Me.ButtonPagoAbono.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonPagoAbono.ForeColor = System.Drawing.Color.White
        Me.ButtonPagoAbono.Location = New System.Drawing.Point(466, 551)
        Me.ButtonPagoAbono.Name = "ButtonPagoAbono"
        Me.ButtonPagoAbono.Size = New System.Drawing.Size(181, 34)
        Me.ButtonPagoAbono.TabIndex = 529
        Me.ButtonPagoAbono.Text = "&PAGO PARA ABONAR"
        Me.ButtonPagoAbono.UseVisualStyleBackColor = False
        '
        'panelServicios
        '
        Me.panelServicios.Controls.Add(Me.TabPage1)
        Me.panelServicios.Controls.Add(Me.TabPage2)
        Me.panelServicios.Controls.Add(Me.TabPage3)
        Me.panelServicios.Controls.Add(Me.TabPage4)
        Me.panelServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panelServicios.Location = New System.Drawing.Point(29, 9)
        Me.panelServicios.Name = "panelServicios"
        Me.panelServicios.SelectedIndex = 0
        Me.panelServicios.Size = New System.Drawing.Size(74, 32)
        Me.panelServicios.TabIndex = 530
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.panelBasico)
        Me.TabPage1.Controls.Add(Me.Button13)
        Me.TabPage1.Controls.Add(Me.txt_Puntos_PPE_B)
        Me.TabPage1.Controls.Add(Me.Label44)
        Me.TabPage1.Controls.Add(Me.txt_Puntos_x_Antiguedad_B)
        Me.TabPage1.Controls.Add(Me.txt_Puntos_x_pago_oportuno_B)
        Me.TabPage1.Controls.Add(Me.Label45)
        Me.TabPage1.Controls.Add(Me.Label46)
        Me.TabPage1.Controls.Add(Me.txt_Periodo_Final_B)
        Me.TabPage1.Controls.Add(Me.txt_Periodo_Inicial_B)
        Me.TabPage1.Controls.Add(Me.Label42)
        Me.TabPage1.Controls.Add(Me.Label43)
        Me.TabPage1.Controls.Add(Me.txt_TVs_adicionales_B)
        Me.TabPage1.Controls.Add(Me.Label41)
        Me.TabPage1.Location = New System.Drawing.Point(4, 34)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(66, 0)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Básico"
        '
        'panelBasico
        '
        Me.panelBasico.Controls.Add(Me.Label65)
        Me.panelBasico.Controls.Add(Me.Label64)
        Me.panelBasico.Location = New System.Drawing.Point(3, 4)
        Me.panelBasico.Name = "panelBasico"
        Me.panelBasico.Size = New System.Drawing.Size(451, 158)
        Me.panelBasico.TabIndex = 577
        '
        'Label65
        '
        Me.Label65.AutoEllipsis = True
        Me.Label65.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label65.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "NOMBRE", True))
        Me.Label65.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.ForeColor = System.Drawing.Color.DarkRed
        Me.Label65.Location = New System.Drawing.Point(64, 23)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(329, 28)
        Me.Label65.TabIndex = 4
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.ForeColor = System.Drawing.Color.DarkRed
        Me.Label64.Location = New System.Drawing.Point(27, 67)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(394, 25)
        Me.Label64.TabIndex = 0
        Me.Label64.Text = "NO cuenta con el servicio de Básico"
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.DarkRed
        Me.Button13.Enabled = False
        Me.Button13.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.ForeColor = System.Drawing.Color.White
        Me.Button13.Location = New System.Drawing.Point(307, 128)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(145, 33)
        Me.Button13.TabIndex = 561
        Me.Button13.Text = "Adelantar Pagos"
        Me.Button13.UseVisualStyleBackColor = False
        Me.Button13.Visible = False
        '
        'txt_Puntos_PPE_B
        '
        Me.txt_Puntos_PPE_B.BackColor = System.Drawing.Color.White
        Me.txt_Puntos_PPE_B.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Puntos_PPE_B.ForeColor = System.Drawing.Color.DarkRed
        Me.txt_Puntos_PPE_B.Location = New System.Drawing.Point(334, 26)
        Me.txt_Puntos_PPE_B.Multiline = True
        Me.txt_Puntos_PPE_B.Name = "txt_Puntos_PPE_B"
        Me.txt_Puntos_PPE_B.Size = New System.Drawing.Size(114, 30)
        Me.txt_Puntos_PPE_B.TabIndex = 560
        Me.txt_Puntos_PPE_B.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label44
        '
        Me.Label44.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(369, 5)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(78, 21)
        Me.Label44.TabIndex = 559
        Me.Label44.Text = "Puntos PPE"
        Me.Label44.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txt_Puntos_x_Antiguedad_B
        '
        Me.txt_Puntos_x_Antiguedad_B.BackColor = System.Drawing.Color.White
        Me.txt_Puntos_x_Antiguedad_B.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Puntos_x_Antiguedad_B.ForeColor = System.Drawing.Color.DarkRed
        Me.txt_Puntos_x_Antiguedad_B.Location = New System.Drawing.Point(167, 77)
        Me.txt_Puntos_x_Antiguedad_B.Multiline = True
        Me.txt_Puntos_x_Antiguedad_B.Name = "txt_Puntos_x_Antiguedad_B"
        Me.txt_Puntos_x_Antiguedad_B.Size = New System.Drawing.Size(114, 30)
        Me.txt_Puntos_x_Antiguedad_B.TabIndex = 558
        Me.txt_Puntos_x_Antiguedad_B.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_Puntos_x_pago_oportuno_B
        '
        Me.txt_Puntos_x_pago_oportuno_B.BackColor = System.Drawing.Color.White
        Me.txt_Puntos_x_pago_oportuno_B.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Puntos_x_pago_oportuno_B.ForeColor = System.Drawing.Color.DarkRed
        Me.txt_Puntos_x_pago_oportuno_B.Location = New System.Drawing.Point(167, 26)
        Me.txt_Puntos_x_pago_oportuno_B.Multiline = True
        Me.txt_Puntos_x_pago_oportuno_B.Name = "txt_Puntos_x_pago_oportuno_B"
        Me.txt_Puntos_x_pago_oportuno_B.Size = New System.Drawing.Size(117, 30)
        Me.txt_Puntos_x_pago_oportuno_B.TabIndex = 557
        Me.txt_Puntos_x_pago_oportuno_B.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label45
        '
        Me.Label45.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(147, 5)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(178, 21)
        Me.Label45.TabIndex = 556
        Me.Label45.Text = "Puntos por Pago Oportuno"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label46
        '
        Me.Label46.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(147, 56)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(185, 21)
        Me.Label46.TabIndex = 555
        Me.Label46.Text = "Puntos por Pago Antigüedad"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txt_Periodo_Final_B
        '
        Me.txt_Periodo_Final_B.BackColor = System.Drawing.Color.White
        Me.txt_Periodo_Final_B.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Periodo_Final_B.ForeColor = System.Drawing.Color.DarkRed
        Me.txt_Periodo_Final_B.Location = New System.Drawing.Point(15, 131)
        Me.txt_Periodo_Final_B.Multiline = True
        Me.txt_Periodo_Final_B.Name = "txt_Periodo_Final_B"
        Me.txt_Periodo_Final_B.Size = New System.Drawing.Size(125, 30)
        Me.txt_Periodo_Final_B.TabIndex = 553
        Me.txt_Periodo_Final_B.Text = "- / - / -"
        Me.txt_Periodo_Final_B.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_Periodo_Inicial_B
        '
        Me.txt_Periodo_Inicial_B.BackColor = System.Drawing.Color.White
        Me.txt_Periodo_Inicial_B.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Periodo_Inicial_B.ForeColor = System.Drawing.Color.DarkRed
        Me.txt_Periodo_Inicial_B.Location = New System.Drawing.Point(14, 77)
        Me.txt_Periodo_Inicial_B.Multiline = True
        Me.txt_Periodo_Inicial_B.Name = "txt_Periodo_Inicial_B"
        Me.txt_Periodo_Inicial_B.Size = New System.Drawing.Size(125, 30)
        Me.txt_Periodo_Inicial_B.TabIndex = 552
        Me.txt_Periodo_Inicial_B.Text = "- / - / -"
        Me.txt_Periodo_Inicial_B.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label42
        '
        Me.Label42.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(-4, 110)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(130, 21)
        Me.Label42.TabIndex = 551
        Me.Label42.Text = "Ult. Año Pagado"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label43
        '
        Me.Label43.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(18, 56)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(105, 21)
        Me.Label43.TabIndex = 550
        Me.Label43.Text = "Ult. Mes Pagado"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txt_TVs_adicionales_B
        '
        Me.txt_TVs_adicionales_B.BackColor = System.Drawing.Color.White
        Me.txt_TVs_adicionales_B.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TVs_adicionales_B.ForeColor = System.Drawing.Color.DarkRed
        Me.txt_TVs_adicionales_B.Location = New System.Drawing.Point(12, 26)
        Me.txt_TVs_adicionales_B.Multiline = True
        Me.txt_TVs_adicionales_B.Name = "txt_TVs_adicionales_B"
        Me.txt_TVs_adicionales_B.Size = New System.Drawing.Size(123, 30)
        Me.txt_TVs_adicionales_B.TabIndex = 549
        Me.txt_TVs_adicionales_B.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label41
        '
        Me.Label41.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(9, 5)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(110, 21)
        Me.Label41.TabIndex = 548
        Me.Label41.Text = "TV's adicionales"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage2.Controls.Add(Me.panelDigital)
        Me.TabPage2.Controls.Add(Me.Button14)
        Me.TabPage2.Controls.Add(Me.TextBox11)
        Me.TabPage2.Controls.Add(Me.Label47)
        Me.TabPage2.Controls.Add(Me.TextBox12)
        Me.TabPage2.Controls.Add(Me.TextBox13)
        Me.TabPage2.Controls.Add(Me.Label48)
        Me.TabPage2.Controls.Add(Me.Label49)
        Me.TabPage2.Controls.Add(Me.TextBox14)
        Me.TabPage2.Controls.Add(Me.TextBox15)
        Me.TabPage2.Controls.Add(Me.Label50)
        Me.TabPage2.Controls.Add(Me.Label51)
        Me.TabPage2.Controls.Add(Me.TextBox16)
        Me.TabPage2.Controls.Add(Me.Label52)
        Me.TabPage2.Location = New System.Drawing.Point(4, 34)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(66, 0)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Digital"
        '
        'panelDigital
        '
        Me.panelDigital.Controls.Add(Me.Label66)
        Me.panelDigital.Controls.Add(Me.Label61)
        Me.panelDigital.Location = New System.Drawing.Point(5, 0)
        Me.panelDigital.Name = "panelDigital"
        Me.panelDigital.Size = New System.Drawing.Size(450, 160)
        Me.panelDigital.TabIndex = 575
        '
        'Label66
        '
        Me.Label66.AutoEllipsis = True
        Me.Label66.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label66.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "NOMBRE", True))
        Me.Label66.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.ForeColor = System.Drawing.Color.DarkRed
        Me.Label66.Location = New System.Drawing.Point(61, 22)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(329, 28)
        Me.Label66.TabIndex = 5
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.ForeColor = System.Drawing.Color.DarkRed
        Me.Label61.Location = New System.Drawing.Point(27, 67)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(390, 25)
        Me.Label61.TabIndex = 0
        Me.Label61.Text = "NO cuenta con el servicio de Digital"
        '
        'Button14
        '
        Me.Button14.BackColor = System.Drawing.Color.DarkRed
        Me.Button14.Enabled = False
        Me.Button14.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button14.ForeColor = System.Drawing.Color.White
        Me.Button14.Location = New System.Drawing.Point(307, 129)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(145, 33)
        Me.Button14.TabIndex = 574
        Me.Button14.Text = "Adelantar Pagos"
        Me.Button14.UseVisualStyleBackColor = False
        Me.Button14.Visible = False
        '
        'TextBox11
        '
        Me.TextBox11.BackColor = System.Drawing.Color.White
        Me.TextBox11.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox11.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox11.Location = New System.Drawing.Point(334, 27)
        Me.TextBox11.Multiline = True
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(114, 30)
        Me.TextBox11.TabIndex = 573
        Me.TextBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label47
        '
        Me.Label47.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(369, 6)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(78, 21)
        Me.Label47.TabIndex = 572
        Me.Label47.Text = "Puntos PPE"
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'TextBox12
        '
        Me.TextBox12.BackColor = System.Drawing.Color.White
        Me.TextBox12.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox12.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox12.Location = New System.Drawing.Point(167, 78)
        Me.TextBox12.Multiline = True
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(114, 30)
        Me.TextBox12.TabIndex = 571
        Me.TextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox13
        '
        Me.TextBox13.BackColor = System.Drawing.Color.White
        Me.TextBox13.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox13.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox13.Location = New System.Drawing.Point(167, 27)
        Me.TextBox13.Multiline = True
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(117, 30)
        Me.TextBox13.TabIndex = 570
        Me.TextBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label48
        '
        Me.Label48.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(147, 6)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(178, 21)
        Me.Label48.TabIndex = 569
        Me.Label48.Text = "Puntos por Pago Oportuno"
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label49
        '
        Me.Label49.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.Location = New System.Drawing.Point(147, 57)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(185, 21)
        Me.Label49.TabIndex = 568
        Me.Label49.Text = "Puntos por Pago Antigüedad"
        Me.Label49.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'TextBox14
        '
        Me.TextBox14.BackColor = System.Drawing.Color.White
        Me.TextBox14.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox14.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox14.Location = New System.Drawing.Point(15, 132)
        Me.TextBox14.Multiline = True
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(125, 30)
        Me.TextBox14.TabIndex = 567
        Me.TextBox14.Text = "- / - / -"
        Me.TextBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox15
        '
        Me.TextBox15.BackColor = System.Drawing.Color.White
        Me.TextBox15.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox15.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox15.Location = New System.Drawing.Point(14, 78)
        Me.TextBox15.Multiline = True
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(125, 30)
        Me.TextBox15.TabIndex = 566
        Me.TextBox15.Text = "- / - / -"
        Me.TextBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label50
        '
        Me.Label50.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(10, 111)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(92, 21)
        Me.Label50.TabIndex = 565
        Me.Label50.Text = "Periodo Final"
        Me.Label50.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label51
        '
        Me.Label51.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.Location = New System.Drawing.Point(6, 57)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(105, 21)
        Me.Label51.TabIndex = 564
        Me.Label51.Text = "Periodo Inicial"
        Me.Label51.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'TextBox16
        '
        Me.TextBox16.BackColor = System.Drawing.Color.White
        Me.TextBox16.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox16.ForeColor = System.Drawing.Color.DarkRed
        Me.TextBox16.Location = New System.Drawing.Point(12, 27)
        Me.TextBox16.Multiline = True
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New System.Drawing.Size(123, 30)
        Me.TextBox16.TabIndex = 563
        Me.TextBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label52
        '
        Me.Label52.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label52.Location = New System.Drawing.Point(9, 6)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(110, 21)
        Me.Label52.TabIndex = 562
        Me.Label52.Text = "TV's adicionales"
        Me.Label52.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage3.Controls.Add(Me.panelInternet)
        Me.TabPage3.Controls.Add(Me.txt_Periodo_Final_I)
        Me.TabPage3.Controls.Add(Me.txt_Periodo_Inicial_I)
        Me.TabPage3.Controls.Add(Me.Label58)
        Me.TabPage3.Controls.Add(Me.Label59)
        Me.TabPage3.Location = New System.Drawing.Point(4, 34)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(66, 0)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Internet"
        '
        'panelInternet
        '
        Me.panelInternet.Controls.Add(Me.Label67)
        Me.panelInternet.Controls.Add(Me.Label62)
        Me.panelInternet.Location = New System.Drawing.Point(3, 2)
        Me.panelInternet.Name = "panelInternet"
        Me.panelInternet.Size = New System.Drawing.Size(453, 158)
        Me.panelInternet.TabIndex = 576
        '
        'Label67
        '
        Me.Label67.AutoEllipsis = True
        Me.Label67.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label67.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "NOMBRE", True))
        Me.Label67.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.ForeColor = System.Drawing.Color.DarkRed
        Me.Label67.Location = New System.Drawing.Point(64, 27)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(329, 28)
        Me.Label67.TabIndex = 5
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.ForeColor = System.Drawing.Color.DarkRed
        Me.Label62.Location = New System.Drawing.Point(27, 67)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(403, 25)
        Me.Label62.TabIndex = 0
        Me.Label62.Text = "NO cuenta con el servicio de Internet"
        '
        'txt_Periodo_Final_I
        '
        Me.txt_Periodo_Final_I.BackColor = System.Drawing.Color.White
        Me.txt_Periodo_Final_I.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Periodo_Final_I.ForeColor = System.Drawing.Color.DarkRed
        Me.txt_Periodo_Final_I.Location = New System.Drawing.Point(15, 78)
        Me.txt_Periodo_Final_I.Multiline = True
        Me.txt_Periodo_Final_I.Name = "txt_Periodo_Final_I"
        Me.txt_Periodo_Final_I.Size = New System.Drawing.Size(125, 30)
        Me.txt_Periodo_Final_I.TabIndex = 571
        Me.txt_Periodo_Final_I.Text = "- / - / -"
        Me.txt_Periodo_Final_I.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_Periodo_Inicial_I
        '
        Me.txt_Periodo_Inicial_I.BackColor = System.Drawing.Color.White
        Me.txt_Periodo_Inicial_I.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Periodo_Inicial_I.ForeColor = System.Drawing.Color.DarkRed
        Me.txt_Periodo_Inicial_I.Location = New System.Drawing.Point(14, 24)
        Me.txt_Periodo_Inicial_I.Multiline = True
        Me.txt_Periodo_Inicial_I.Name = "txt_Periodo_Inicial_I"
        Me.txt_Periodo_Inicial_I.Size = New System.Drawing.Size(125, 30)
        Me.txt_Periodo_Inicial_I.TabIndex = 570
        Me.txt_Periodo_Inicial_I.Text = "- / - / -"
        Me.txt_Periodo_Inicial_I.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label58
        '
        Me.Label58.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.Location = New System.Drawing.Point(10, 57)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(92, 21)
        Me.Label58.TabIndex = 569
        Me.Label58.Text = "Periodo Final"
        Me.Label58.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label59
        '
        Me.Label59.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(6, 3)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(105, 21)
        Me.Label59.TabIndex = 568
        Me.Label59.Text = "Periodo Inicial"
        Me.Label59.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage4.Controls.Add(Me.panelTelefonia)
        Me.TabPage4.Controls.Add(Me.CheckBox3)
        Me.TabPage4.Controls.Add(Me.CheckBox2)
        Me.TabPage4.Controls.Add(Me.CheckBox1)
        Me.TabPage4.Controls.Add(Me.txt_Puntos_Desc_T)
        Me.TabPage4.Controls.Add(Me.Label57)
        Me.TabPage4.Controls.Add(Me.txt_Incluidas_LDN_T)
        Me.TabPage4.Controls.Add(Me.txt_Incluidas_Locales)
        Me.TabPage4.Controls.Add(Me.txt_Incluidas_Frcc_T)
        Me.TabPage4.Controls.Add(Me.txt_NumTel_T)
        Me.TabPage4.Controls.Add(Me.Label56)
        Me.TabPage4.Controls.Add(Me.Label55)
        Me.TabPage4.Controls.Add(Me.Label54)
        Me.TabPage4.Controls.Add(Me.Label53)
        Me.TabPage4.Location = New System.Drawing.Point(4, 34)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(66, 0)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Telefonía"
        '
        'panelTelefonia
        '
        Me.panelTelefonia.Controls.Add(Me.Label68)
        Me.panelTelefonia.Controls.Add(Me.Label63)
        Me.panelTelefonia.Location = New System.Drawing.Point(5, 3)
        Me.panelTelefonia.Name = "panelTelefonia"
        Me.panelTelefonia.Size = New System.Drawing.Size(448, 158)
        Me.panelTelefonia.TabIndex = 577
        '
        'Label68
        '
        Me.Label68.AutoEllipsis = True
        Me.Label68.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label68.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "NOMBRE", True))
        Me.Label68.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label68.ForeColor = System.Drawing.Color.DarkRed
        Me.Label68.Location = New System.Drawing.Point(64, 29)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(329, 28)
        Me.Label68.TabIndex = 5
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.ForeColor = System.Drawing.Color.DarkRed
        Me.Label63.Location = New System.Drawing.Point(13, 67)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(421, 25)
        Me.Label63.TabIndex = 0
        Me.Label63.Text = "NO cuenta con el servicio de Telefonía"
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.Location = New System.Drawing.Point(345, 136)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(89, 19)
        Me.CheckBox3.TabIndex = 564
        Me.CheckBox3.Text = "Ilimitadas"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(345, 85)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(89, 19)
        Me.CheckBox2.TabIndex = 563
        Me.CheckBox2.Text = "Ilimitadas"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(345, 33)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(89, 19)
        Me.CheckBox1.TabIndex = 562
        Me.CheckBox1.Text = "Ilimitadas"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'txt_Puntos_Desc_T
        '
        Me.txt_Puntos_Desc_T.BackColor = System.Drawing.Color.White
        Me.txt_Puntos_Desc_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Puntos_Desc_T.ForeColor = System.Drawing.Color.DarkRed
        Me.txt_Puntos_Desc_T.Location = New System.Drawing.Point(16, 81)
        Me.txt_Puntos_Desc_T.Multiline = True
        Me.txt_Puntos_Desc_T.Name = "txt_Puntos_Desc_T"
        Me.txt_Puntos_Desc_T.Size = New System.Drawing.Size(141, 30)
        Me.txt_Puntos_Desc_T.TabIndex = 561
        Me.txt_Puntos_Desc_T.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label57
        '
        Me.Label57.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.Location = New System.Drawing.Point(8, 60)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(161, 21)
        Me.Label57.TabIndex = 560
        Me.Label57.Text = "Total de Puntos de Desc."
        Me.Label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_Incluidas_LDN_T
        '
        Me.txt_Incluidas_LDN_T.BackColor = System.Drawing.Color.White
        Me.txt_Incluidas_LDN_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Incluidas_LDN_T.ForeColor = System.Drawing.Color.DarkRed
        Me.txt_Incluidas_LDN_T.Location = New System.Drawing.Point(214, 133)
        Me.txt_Incluidas_LDN_T.Multiline = True
        Me.txt_Incluidas_LDN_T.Name = "txt_Incluidas_LDN_T"
        Me.txt_Incluidas_LDN_T.Size = New System.Drawing.Size(125, 30)
        Me.txt_Incluidas_LDN_T.TabIndex = 559
        Me.txt_Incluidas_LDN_T.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_Incluidas_Locales
        '
        Me.txt_Incluidas_Locales.BackColor = System.Drawing.Color.White
        Me.txt_Incluidas_Locales.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Incluidas_Locales.ForeColor = System.Drawing.Color.DarkRed
        Me.txt_Incluidas_Locales.Location = New System.Drawing.Point(214, 82)
        Me.txt_Incluidas_Locales.Multiline = True
        Me.txt_Incluidas_Locales.Name = "txt_Incluidas_Locales"
        Me.txt_Incluidas_Locales.Size = New System.Drawing.Size(125, 30)
        Me.txt_Incluidas_Locales.TabIndex = 558
        Me.txt_Incluidas_Locales.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_Incluidas_Frcc_T
        '
        Me.txt_Incluidas_Frcc_T.BackColor = System.Drawing.Color.White
        Me.txt_Incluidas_Frcc_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Incluidas_Frcc_T.ForeColor = System.Drawing.Color.DarkRed
        Me.txt_Incluidas_Frcc_T.Location = New System.Drawing.Point(214, 30)
        Me.txt_Incluidas_Frcc_T.Multiline = True
        Me.txt_Incluidas_Frcc_T.Name = "txt_Incluidas_Frcc_T"
        Me.txt_Incluidas_Frcc_T.Size = New System.Drawing.Size(125, 30)
        Me.txt_Incluidas_Frcc_T.TabIndex = 557
        Me.txt_Incluidas_Frcc_T.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_NumTel_T
        '
        Me.txt_NumTel_T.BackColor = System.Drawing.Color.White
        Me.txt_NumTel_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_NumTel_T.ForeColor = System.Drawing.Color.DarkRed
        Me.txt_NumTel_T.Location = New System.Drawing.Point(14, 30)
        Me.txt_NumTel_T.Multiline = True
        Me.txt_NumTel_T.Name = "txt_NumTel_T"
        Me.txt_NumTel_T.Size = New System.Drawing.Size(169, 30)
        Me.txt_NumTel_T.TabIndex = 556
        Me.txt_NumTel_T.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label56
        '
        Me.Label56.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.Location = New System.Drawing.Point(11, 9)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(130, 21)
        Me.Label56.TabIndex = 535
        Me.Label56.Text = "Número Telefónico"
        Me.Label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label55
        '
        Me.Label55.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(203, 112)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(211, 21)
        Me.Label55.TabIndex = 534
        Me.Label55.Text = "Llamadas Incluidas L.D. Nacional"
        Me.Label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label54
        '
        Me.Label54.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.Location = New System.Drawing.Point(198, 60)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(186, 21)
        Me.Label54.TabIndex = 533
        Me.Label54.Text = "Llamadas Incluidas Locales"
        Me.Label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label53
        '
        Me.Label53.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.Location = New System.Drawing.Point(198, 9)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(211, 21)
        Me.Label53.TabIndex = 532
        Me.Label53.Text = "Llamadas Incluidas Mismo Frcc."
        Me.Label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSumaCargos
        '
        Me.lblSumaCargos.AutoSize = True
        Me.lblSumaCargos.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSumaCargos.ForeColor = System.Drawing.Color.Maroon
        Me.lblSumaCargos.Location = New System.Drawing.Point(703, 283)
        Me.lblSumaCargos.Name = "lblSumaCargos"
        Me.lblSumaCargos.Size = New System.Drawing.Size(261, 24)
        Me.lblSumaCargos.TabIndex = 531
        Me.lblSumaCargos.Text = "TOTAL DE CARGOS A PAGAR"
        '
        'Label30
        '
        Me.Label30.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(86, 37)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(97, 21)
        Me.Label30.TabIndex = 533
        Me.Label30.Text = "I.E.P.S.:"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'LabelIEPS
        '
        Me.LabelIEPS.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelIEPS.ForeColor = System.Drawing.Color.Maroon
        Me.LabelIEPS.Location = New System.Drawing.Point(203, 32)
        Me.LabelIEPS.Name = "LabelIEPS"
        Me.LabelIEPS.Size = New System.Drawing.Size(146, 25)
        Me.LabelIEPS.TabIndex = 532
        Me.LabelIEPS.Text = "$ieps"
        Me.LabelIEPS.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.ButtonVentaEq)
        Me.Panel6.Controls.Add(Me.panelServicios)
        Me.Panel6.Controls.Add(Me.TextBox1)
        Me.Panel6.Controls.Add(Me.DataGridView3)
        Me.Panel6.Location = New System.Drawing.Point(6, 7)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(10, 10)
        Me.Panel6.TabIndex = 534
        Me.Panel6.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.Red
        Me.TextBox1.Location = New System.Drawing.Point(305, 14)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(149, 25)
        Me.TextBox1.TabIndex = 500
        Me.TextBox1.TabStop = False
        '
        'DataGridView3
        '
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Location = New System.Drawing.Point(27, 17)
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.Size = New System.Drawing.Size(47, 13)
        Me.DataGridView3.TabIndex = 539
        '
        'btnAdelantarPagos
        '
        Me.btnAdelantarPagos.BackColor = System.Drawing.Color.DarkRed
        Me.btnAdelantarPagos.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdelantarPagos.ForeColor = System.Drawing.Color.White
        Me.btnAdelantarPagos.Location = New System.Drawing.Point(240, 551)
        Me.btnAdelantarPagos.Name = "btnAdelantarPagos"
        Me.btnAdelantarPagos.Size = New System.Drawing.Size(181, 34)
        Me.btnAdelantarPagos.TabIndex = 535
        Me.btnAdelantarPagos.Text = "A&DELANTAR PAGOS"
        Me.btnAdelantarPagos.UseVisualStyleBackColor = False
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Panel6)
        Me.Panel7.Controls.Add(Me.Panel8)
        Me.Panel7.Location = New System.Drawing.Point(569, 699)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(10, 10)
        Me.Panel7.TabIndex = 540
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.LabelIEPS)
        Me.Panel8.Controls.Add(Me.Label26)
        Me.Panel8.Controls.Add(Me.LabelIva)
        Me.Panel8.Controls.Add(Me.LabelSubTotal)
        Me.Panel8.Controls.Add(Me.Label27)
        Me.Panel8.Controls.Add(Me.Label30)
        Me.Panel8.Controls.Add(Me.DataGridView4)
        Me.Panel8.Controls.Add(Me.SumaDetalleDataGridView)
        Me.Panel8.Location = New System.Drawing.Point(18, 7)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(10, 16)
        Me.Panel8.TabIndex = 542
        '
        'DataGridView4
        '
        Me.DataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView4.Location = New System.Drawing.Point(9, 5)
        Me.DataGridView4.Name = "DataGridView4"
        Me.DataGridView4.Size = New System.Drawing.Size(87, 43)
        Me.DataGridView4.TabIndex = 546
        '
        'lblEsEdoCta
        '
        Me.lblEsEdoCta.AutoSize = True
        Me.lblEsEdoCta.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblEsEdoCta.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEsEdoCta.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblEsEdoCta.Location = New System.Drawing.Point(7, 503)
        Me.lblEsEdoCta.Name = "lblEsEdoCta"
        Me.lblEsEdoCta.Size = New System.Drawing.Size(175, 18)
        Me.lblEsEdoCta.TabIndex = 541
        Me.lblEsEdoCta.Text = "Cliente con Estado de Cuenta"
        Me.lblEsEdoCta.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label31
        '
        Me.Label31.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(672, 355)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(191, 21)
        Me.Label31.TabIndex = 542
        Me.Label31.Text = "Total Puntos Aplicados:"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label32
        '
        Me.Label32.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(665, 389)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(196, 21)
        Me.Label32.TabIndex = 543
        Me.Label32.Text = "Total Bonificado:"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'ImportePuntosAplicados
        '
        Me.ImportePuntosAplicados.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImportePuntosAplicados.ForeColor = System.Drawing.Color.Black
        Me.ImportePuntosAplicados.Location = New System.Drawing.Point(859, 355)
        Me.ImportePuntosAplicados.Name = "ImportePuntosAplicados"
        Me.ImportePuntosAplicados.Size = New System.Drawing.Size(168, 25)
        Me.ImportePuntosAplicados.TabIndex = 544
        Me.ImportePuntosAplicados.Text = "$0.00"
        Me.ImportePuntosAplicados.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ImporteTotalBonificado
        '
        Me.ImporteTotalBonificado.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteTotalBonificado.ForeColor = System.Drawing.Color.Black
        Me.ImporteTotalBonificado.Location = New System.Drawing.Point(864, 389)
        Me.ImporteTotalBonificado.Name = "ImporteTotalBonificado"
        Me.ImporteTotalBonificado.Size = New System.Drawing.Size(166, 25)
        Me.ImporteTotalBonificado.TabIndex = 545
        Me.ImporteTotalBonificado.Text = "$0.00"
        Me.ImporteTotalBonificado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnVerEdoCta
        '
        Me.btnVerEdoCta.BackColor = System.Drawing.Color.DarkRed
        Me.btnVerEdoCta.Enabled = False
        Me.btnVerEdoCta.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVerEdoCta.ForeColor = System.Drawing.Color.White
        Me.btnVerEdoCta.Location = New System.Drawing.Point(468, 595)
        Me.btnVerEdoCta.Name = "btnVerEdoCta"
        Me.btnVerEdoCta.Size = New System.Drawing.Size(180, 36)
        Me.btnVerEdoCta.TabIndex = 547
        Me.btnVerEdoCta.Text = "VER ESTADO DE CUENTA"
        Me.btnVerEdoCta.UseVisualStyleBackColor = False
        '
        'btnVerDetalle
        '
        Me.btnVerDetalle.BackColor = System.Drawing.Color.DarkRed
        Me.btnVerDetalle.Enabled = False
        Me.btnVerDetalle.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVerDetalle.ForeColor = System.Drawing.Color.White
        Me.btnVerDetalle.Location = New System.Drawing.Point(240, 595)
        Me.btnVerDetalle.Name = "btnVerDetalle"
        Me.btnVerDetalle.Size = New System.Drawing.Size(181, 34)
        Me.btnVerDetalle.TabIndex = 548
        Me.btnVerDetalle.Text = "V&ER DETALLE"
        Me.btnVerDetalle.UseVisualStyleBackColor = False
        '
        'lbl_MsgAyudaDobleClic
        '
        Me.lbl_MsgAyudaDobleClic.AutoSize = True
        Me.lbl_MsgAyudaDobleClic.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_MsgAyudaDobleClic.Location = New System.Drawing.Point(334, 503)
        Me.lbl_MsgAyudaDobleClic.Name = "lbl_MsgAyudaDobleClic"
        Me.lbl_MsgAyudaDobleClic.Size = New System.Drawing.Size(310, 18)
        Me.lbl_MsgAyudaDobleClic.TabIndex = 549
        Me.lbl_MsgAyudaDobleClic.Text = "Doble clic sobre algún concepto para ver sus detalles."
        '
        'lblLeyendaPromocion
        '
        Me.lblLeyendaPromocion.AutoSize = True
        Me.lblLeyendaPromocion.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblLeyendaPromocion.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeyendaPromocion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLeyendaPromocion.Location = New System.Drawing.Point(6, 524)
        Me.lblLeyendaPromocion.Name = "lblLeyendaPromocion"
        Me.lblLeyendaPromocion.Size = New System.Drawing.Size(120, 18)
        Me.lblLeyendaPromocion.TabIndex = 550
        Me.lblLeyendaPromocion.Text = "Leyenda Promocion"
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.DarkRed
        Me.Button9.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.White
        Me.Button9.Location = New System.Drawing.Point(240, 638)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(178, 36)
        Me.Button9.TabIndex = 551
        Me.Button9.Text = "VER ESTADO DE CUENTA"
        Me.Button9.UseVisualStyleBackColor = False
        Me.Button9.Visible = False
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'btnHabilitaFacNormal
        '
        Me.btnHabilitaFacNormal.BackColor = System.Drawing.Color.DarkRed
        Me.btnHabilitaFacNormal.Enabled = False
        Me.btnHabilitaFacNormal.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHabilitaFacNormal.ForeColor = System.Drawing.Color.White
        Me.btnHabilitaFacNormal.Location = New System.Drawing.Point(10, 682)
        Me.btnHabilitaFacNormal.Name = "btnHabilitaFacNormal"
        Me.btnHabilitaFacNormal.Size = New System.Drawing.Size(181, 49)
        Me.btnHabilitaFacNormal.TabIndex = 552
        Me.btnHabilitaFacNormal.Text = "HABILITAR SIN ESTADO DE CUENTA"
        Me.btnHabilitaFacNormal.UseVisualStyleBackColor = False
        Me.btnHabilitaFacNormal.Visible = False
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'btnRoboSeñal
        '
        Me.btnRoboSeñal.BackColor = System.Drawing.Color.DarkRed
        Me.btnRoboSeñal.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRoboSeñal.ForeColor = System.Drawing.Color.White
        Me.btnRoboSeñal.Location = New System.Drawing.Point(468, 638)
        Me.btnRoboSeñal.Name = "btnRoboSeñal"
        Me.btnRoboSeñal.Size = New System.Drawing.Size(180, 36)
        Me.btnRoboSeñal.TabIndex = 560
        Me.btnRoboSeñal.Text = "ROBO DE SEÑAL"
        Me.btnRoboSeñal.UseVisualStyleBackColor = False
        '
        'FrmFACLogitel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1041, 745)
        Me.Controls.Add(Me.btnRoboSeñal)
        Me.Controls.Add(Me.btnHabilitaFacNormal)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.lblLeyendaPromocion)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.ImportePuntosAplicados)
        Me.Controls.Add(Me.LABEL19)
        Me.Controls.Add(Me.ImporteTotalBonificado)
        Me.Controls.Add(Me.lbl_MsgAyudaDobleClic)
        Me.Controls.Add(Me.btnVerDetalle)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.btnVerEdoCta)
        Me.Controls.Add(Me.lblEsEdoCta)
        Me.Controls.Add(Me.btnAdelantarPagos)
        Me.Controls.Add(Me.lblSumaCargos)
        Me.Controls.Add(Me.ButtonPagoAbono)
        Me.Controls.Add(Me.PanelTel)
        Me.Controls.Add(Me.PanelNrm)
        Me.Controls.Add(Me.Clv_SessionTel)
        Me.Controls.Add(Me.TextImporte_Adic)
        Me.Controls.Add(Me.LblImporte_Total)
        Me.Controls.Add(Me.CNOPanel10)
        Me.Controls.Add(Me.LabelSaldoAnterior)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.LblCredito_Apagar)
        Me.Controls.Add(Me.CMBLabel29)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.LabelTotal)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Panel7)
        Me.MaximizeBox = False
        Me.Name = "FrmFACLogitel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Recepción de Pagos"
        CType(Me.SumaDetalleBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorraClv_SessionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameSerDELCliFACBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CobraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameDetalleBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrabaFacturasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AgregarServicioAdicionalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PagosAdelantadosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCLIPORCONTRATOFACBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamedatosUsuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMENOMBRESUCURSALBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.UltimoSERIEYFOLIOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAVENDEDORES2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMETIPOSCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        CType(Me.DAMEUltimo_FOLIOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMETOTALSumaDetalleBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GUARDATIPOPAGOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.QUITARDELDETALLEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.SumaDetalleDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GuardaMotivosBonificacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORCAMDOCFAC_QUITABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamelasOrdenesque_GeneroFacturaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_Impresora_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimesiahiConexBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameServicioAsignadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BuscaBloqueadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BusFacFiscalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Comentario2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Hora_insBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Selecciona_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dime_ContratacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel6.ResumeLayout(False)
        CType(Me.Dime_Si_ProcedePagoParcialBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cobra_PagosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AgregarServicioAdicionales_PPEBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CobraAdeudoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pregunta_Si_Puedo_AdelantarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cobra_VentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Bonificacion_SupervisorBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EricDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EntregaAparatoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CNOPanel10.ResumeLayout(False)
        Me.CNOPanel10.PerformLayout()
        Me.PanelNrm.ResumeLayout(False)
        Me.PanelTel.ResumeLayout(False)
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelServicios.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.panelBasico.ResumeLayout(False)
        Me.panelBasico.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.panelDigital.ResumeLayout(False)
        Me.panelDigital.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.panelInternet.ResumeLayout(False)
        Me.panelInternet.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.panelTelefonia.ResumeLayout(False)
        Me.panelTelefonia.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel8.ResumeLayout(False)
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SumaDetalleBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents BUSCLIPORCONTRATO_FACTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.BUSCLIPORCONTRATO_FACTableAdapter
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents DameDetalleTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameDetalleTableAdapter
    Friend WithEvents BorraClv_SessionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorraClv_SessionTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.BorraClv_SessionTableAdapter
    Friend WithEvents CobraTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.CobraTableAdapter
    Friend WithEvents DameSerDELCliFACBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameSerDELCliFACTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameSerDELCliFACTableAdapter
    Friend WithEvents CobraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents DameDetalleBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AgregarServicioAdicionalesTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.AgregarServicioAdicionalesTableAdapter
    Friend WithEvents GrabaFacturasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GrabaFacturasTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.GrabaFacturasTableAdapter
    Friend WithEvents AgregarServicioAdicionalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SumaDetalleTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.SumaDetalleTableAdapter
    Friend WithEvents PagosAdelantadosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PagosAdelantadosTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.PagosAdelantadosTableAdapter
    Friend WithEvents BUSCLIPORCONTRATOFACBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents DameDatosGeneralesTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameDatosGeneralesTableAdapter
    Friend WithEvents DamedatosUsuarioTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DamedatosUsuarioTableAdapter
    Friend WithEvents DAMENOMBRESUCURSALTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DAMENOMBRESUCURSALTableAdapter
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents LblVersion As System.Windows.Forms.Label
    Friend WithEvents LblFecha As System.Windows.Forms.Label
    Friend WithEvents DameDatosGeneralesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LblUsuario As System.Windows.Forms.Label
    Friend WithEvents DamedatosUsuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LblSucursal As System.Windows.Forms.Label
    Friend WithEvents DAMENOMBRESUCURSALBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LblNomCaja As System.Windows.Forms.Label
    Friend WithEvents LblSistema As System.Windows.Forms.Label
    Friend WithEvents LblNomEmpresa As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents FolioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Clv_Session As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents MUESTRAVENDEDORES2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAVENDEDORES_2TableAdapter As softvFacturacion.DataSetEdgarTableAdapters.MUESTRAVENDEDORES_2TableAdapter
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents UltimoSERIEYFOLIOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Ultimo_SERIEYFOLIOTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.Ultimo_SERIEYFOLIOTableAdapter
    Friend WithEvents DAMEUltimo_FOLIOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMEUltimo_FOLIOTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DAMEUltimo_FOLIOTableAdapter
    Friend WithEvents NewsoftvDataSet1 As softvFacturacion.NewsoftvDataSet1
    Friend WithEvents DAMETOTALSumaDetalleBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMETOTALSumaDetalleTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.DAMETOTALSumaDetalleTableAdapter
    Friend WithEvents GUARDATIPOPAGOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GUARDATIPOPAGOTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.GUARDATIPOPAGOTableAdapter
    Friend WithEvents QUITARDELDETALLEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents QUITARDELDETALLETableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.QUITARDELDETALLETableAdapter
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents DAMETIPOSCLIENTESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMETIPOSCLIENTESTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DAMETIPOSCLIENTESTableAdapter
    Friend WithEvents DESCRIPCIONLabel1 As System.Windows.Forms.Label
    Friend WithEvents CLV_TIPOCLIENTELabel1 As System.Windows.Forms.Label
    Friend WithEvents LABEL19 As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents GuardaMotivosBonificacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GuardaMotivosBonificacionTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.GuardaMotivosBonificacionTableAdapter
    Friend WithEvents SumaDetalleDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents NewsoftvDataSet2 As softvFacturacion.NewsoftvDataSet2
    Friend WithEvents BORCAMDOCFAC_QUITABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORCAMDOCFAC_QUITATableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.BORCAMDOCFAC_QUITATableAdapter
    Friend WithEvents DamelasOrdenesque_GeneroFacturaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DamelasOrdenesque_GeneroFacturaTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DamelasOrdenesque_GeneroFacturaTableAdapter
    Friend WithEvents Dame_Impresora_OrdenesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Impresora_OrdenesTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.Dame_Impresora_OrdenesTableAdapter
    Friend WithEvents DimesiahiConexBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DimesiahiConexTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DimesiahiConexTableAdapter
    Friend WithEvents DameServicioAsignadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BuscaBloqueadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BuscaBloqueadoTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.BuscaBloqueadoTableAdapter
    Friend WithEvents BusFacFiscalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BusFacFiscalTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
    Friend WithEvents Inserta_Comentario2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Comentario2TableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.Inserta_Comentario2TableAdapter
    Friend WithEvents Hora_insBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Hora_insTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.Hora_insTableAdapter
    Friend WithEvents Selecciona_Impresora_SucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Selecciona_Impresora_SucursalTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.Selecciona_Impresora_SucursalTableAdapter
    Friend WithEvents Dime_ContratacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dime_ContratacionTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.Dime_ContratacionTableAdapter
    Friend WithEvents CMBPanel6 As System.Windows.Forms.Panel
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Dime_Si_ProcedePagoParcialBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dime_Si_ProcedePagoParcialTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.Dime_Si_ProcedePagoParcialTableAdapter
    Friend WithEvents Cobra_PagosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Cobra_PagosTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.Cobra_PagosTableAdapter
    Friend WithEvents AgregarServicioAdicionales_PPEBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents AgregarServicioAdicionales_PPETableAdapter1 As softvFacturacion.DataSetEdgarTableAdapters.AgregarServicioAdicionales_PPETableAdapter
    Friend WithEvents ClvSessionDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PosicionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NivelDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents CLV_DETALLETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CobraAdeudoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CobraAdeudoTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.CobraAdeudoTableAdapter
    Friend WithEvents Pregunta_Si_Puedo_AdelantarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Pregunta_Si_Puedo_AdelantarTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.Pregunta_Si_Puedo_AdelantarTableAdapter
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Cobra_VentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Cobra_VentasTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.Cobra_VentasTableAdapter
    Friend WithEvents Fecha_Venta As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents Inserta_Bonificacion_SupervisorBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Bonificacion_SupervisorTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_Bonificacion_SupervisorTableAdapter
    Friend WithEvents REDLabel25 As System.Windows.Forms.Label
    Friend WithEvents Dime_Si_DatosFiscalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EricDataSet2 As softvFacturacion.EricDataSet2
    Friend WithEvents DameGeneralMsjTicketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameGeneralMsjTicketsTableAdapter As softvFacturacion.EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents EntregaAparatoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EntregaAparatoTableAdapter As softvFacturacion.EricDataSet2TableAdapters.EntregaAparatoTableAdapter
    Friend WithEvents REDLabel26 As System.Windows.Forms.Label
    Friend WithEvents EricDataSet As softvFacturacion.EricDataSet
    Friend WithEvents DameServicioAsignadoBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents DameServicioAsignadoTableAdapter As softvFacturacion.EricDataSetTableAdapters.DameServicioAsignadoTableAdapter
    Friend WithEvents LabelSubTotal As System.Windows.Forms.Label
    Friend WithEvents LabelIva As System.Windows.Forms.Label
    Friend WithEvents LabelTotal As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel29 As System.Windows.Forms.Label
    Friend WithEvents LblCredito_Apagar As System.Windows.Forms.Label
    Friend WithEvents LblImporte_Total As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents LabelSaldoAnterior As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel30 As System.Windows.Forms.Label
    Friend WithEvents CNOPanel10 As System.Windows.Forms.Panel
    Friend WithEvents LabelGRan_Total As System.Windows.Forms.Label
    Friend WithEvents ClvSessionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVSERVICIODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvllavedelservicioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvUnicaNetDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLAVEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MACCABLEMODEM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCORTADataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pagos_Adelantados As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents TvAdicDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesCortesiaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesApagarDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PeriodoPagadoIniDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PeriodoPagadoFinDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosAplicadosOtrosDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosAplicadosAntDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescuentoNet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Des_Otr_Ser_Misma_Categoria As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BonificacionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteAdicionalDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnaDetalleDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DiasBonificaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesBonificarDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteBonificaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltimoMesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltimoanioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AdelantadoDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DESCRIPCIONDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLV_DETALLE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TextImporte_Adic As System.Windows.Forms.TextBox
    Friend WithEvents PanelNrm As System.Windows.Forms.Panel
    Friend WithEvents PanelTel As System.Windows.Forms.Panel
    Friend WithEvents Clv_SessionTel As System.Windows.Forms.TextBox
    Friend WithEvents ButtonVentaEq As System.Windows.Forms.Button
    Friend WithEvents ButtonPagoAbono As System.Windows.Forms.Button
    Friend WithEvents panelServicios As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents txt_Puntos_PPE_B As System.Windows.Forms.TextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txt_Puntos_x_Antiguedad_B As System.Windows.Forms.TextBox
    Friend WithEvents txt_Puntos_x_pago_oportuno_B As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents txt_Periodo_Final_B As System.Windows.Forms.TextBox
    Friend WithEvents txt_Periodo_Inicial_B As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents txt_TVs_adicionales_B As System.Windows.Forms.TextBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents panelDigital As System.Windows.Forms.Panel
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents txt_Periodo_Final_I As System.Windows.Forms.TextBox
    Friend WithEvents txt_Periodo_Inicial_I As System.Windows.Forms.TextBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents txt_Puntos_Desc_T As System.Windows.Forms.TextBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents txt_Incluidas_LDN_T As System.Windows.Forms.TextBox
    Friend WithEvents txt_Incluidas_Locales As System.Windows.Forms.TextBox
    Friend WithEvents txt_Incluidas_Frcc_T As System.Windows.Forms.TextBox
    Friend WithEvents txt_NumTel_T As System.Windows.Forms.TextBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents lblSumaCargos As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents LabelIEPS As System.Windows.Forms.Label
    Friend WithEvents panelInternet As System.Windows.Forms.Panel
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents panelTelefonia As System.Windows.Forms.Panel
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents panelBasico As System.Windows.Forms.Panel
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents btnAdelantarPagos As System.Windows.Forms.Button
    Friend WithEvents txtMsjError As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView3 As System.Windows.Forms.DataGridView
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents lblEsEdoCta As System.Windows.Forms.Label
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents ImportePuntosAplicados As System.Windows.Forms.Label
    Friend WithEvents ImporteTotalBonificado As System.Windows.Forms.Label
    Friend WithEvents DataGridView4 As System.Windows.Forms.DataGridView
    Friend WithEvents btnVerEdoCta As System.Windows.Forms.Button
    Friend WithEvents btnVerDetalle As System.Windows.Forms.Button
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents lbl_MsgAyudaDobleClic As System.Windows.Forms.Label
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblLeyendaPromocion As System.Windows.Forms.Label
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents btnHabilitaFacNormal As System.Windows.Forms.Button
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents btnRoboSeñal As System.Windows.Forms.Button
    'Friend WithEvents Dime_Si_DatosFiscalesTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.Dime_Si_DatosFiscalesTableAdapter
End Class
