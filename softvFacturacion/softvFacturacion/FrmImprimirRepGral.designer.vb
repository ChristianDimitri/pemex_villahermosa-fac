<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmImprimirRepGral
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.SumaArqueoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SumaArqueoTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.SumaArqueoTableAdapter()
        Me.Dame_base_datosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_base_datosTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Dame_base_datosTableAdapter()
        Me.Panel1.SuspendLayout()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SumaArqueoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_base_datosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.CrystalReportViewer1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1008, 730)
        Me.Panel1.TabIndex = 0
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.CausesValidation = False
        Me.CrystalReportViewer1.DisplayGroupTree = False
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.EnableToolTips = False
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.SelectionFormula = ""
        Me.CrystalReportViewer1.ShowGroupTreeButton = False
        Me.CrystalReportViewer1.ShowRefreshButton = False
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(1008, 730)
        Me.CrystalReportViewer1.TabIndex = 0
        Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SumaArqueoBindingSource
        '
        Me.SumaArqueoBindingSource.DataMember = "SumaArqueo"
        Me.SumaArqueoBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'SumaArqueoTableAdapter
        '
        Me.SumaArqueoTableAdapter.ClearBeforeFill = True
        '
        'Dame_base_datosBindingSource
        '
        Me.Dame_base_datosBindingSource.DataMember = "Dame_base_datos"
        Me.Dame_base_datosBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Dame_base_datosTableAdapter
        '
        Me.Dame_base_datosTableAdapter.ClearBeforeFill = True
        '
        'FrmImprimirRepGral
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmImprimirRepGral"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Imprimir Reporte General"
        Me.Panel1.ResumeLayout(False)
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SumaArqueoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_base_datosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents SumaArqueoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SumaArqueoTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.SumaArqueoTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents Dame_base_datosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_base_datosTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Dame_base_datosTableAdapter
End Class
