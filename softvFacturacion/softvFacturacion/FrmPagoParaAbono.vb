Imports System.Data.SqlClient
Public Class FrmPagoParaAbono

    Private Sub InsertaPagoParaAbonoACuenta(ByVal Clv_Session As Long, ByVal Importe As Double)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaPagoParaAbonarACuenta", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Importe", SqlDbType.Decimal)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Importe
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "InsertaPagoParaAbonarACuenta"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub
    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "L")))
        If Asc(e.KeyChar) = 13 Then
            Boton_Aceptar()
        End If
    End Sub

    Private Sub Boton_Aceptar()
        If IsNumeric(Me.TextBox1.Text) = False Then
            MsgBox("Captura el Importe.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        InsertaPagoParaAbonoACuenta(gloClv_Session, Me.TextBox1.Text)
        GloAbonoACuenta = True
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If GloImporteTotalEstadoDeCuenta >= CType(Me.TextBox1.Text, Double) Then

            Boton_Aceptar()

        Else

            MsgBox("El Importe para abonar al Estado de Cuenta debe ser menor al total a Pagar.", MsgBoxStyle.Information, "IMPORTE NO V�LIDO")

        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Me.Close()
    End Sub

    Private Sub FrmPagoParaAbono_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)

        Me.lblImporteTotal.ForeColor = Color.Red
        Me.lblMsjImporte.ForeColor = Color.Black
        Me.lblTitulo.ForeColor = Color.Black
        Me.Label1.ForeColor = Color.Black

        Me.lblImporteTotal.Text = CType(GloImporteTotalEstadoDeCuenta, Double)
        Me.lblImporteTotal.Text = "$" & Me.lblImporteTotal.Text

    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub pbAyuda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbAyuda.Click
        MsgBox("Coloca en la caja de tetxo el Importe que quieres abonar al Estado de Cuenta.", MsgBoxStyle.Information)
    End Sub
End Class