Imports System.Data.SqlClient

Public Class FrmPagosAdelantados
    Dim HabilitaAdelantar_ServiciosUnicos As Boolean = True
    Dim Mensualidades_Restantes As Integer = 0


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        GloBnd = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If Mensualidades_Restantes < CType(Me.NumericUpDown1.Value, Integer) And Mensualidades_Restantes <> -1 Then

            MsgBox("El n�mero de meses a adelantar( " & Me.NumericUpDown1.Value.ToString & " ) sobrepasa el n�mero de mensualidades restantes de su promoci�n que es: " & Mensualidades_Restantes.ToString & " , solo puedes adelantar las mensualidades restantes de su promoci�n y despues puedes adelantar meses Normalmente.", MsgBoxStyle.Information, "Mensualidades restantes de la promoci�n: " & Mensualidades_Restantes.ToString)
            GloBnd = False
            Exit Sub

        End If

        Try
            'If (Me.Panel1.Visible = True Or Me.Panel1.Visible = False) And (Me.NumericUpDown1.Value >= 6 And Me.NumericUpDown1.Value <= 12) And IdSistema = "SA" Then
            '    Dim CON As New SqlConnection(MiConexion)
            '    If (Me.NumericUpDown1.Value >= 6 And Me.NumericUpDown1.Value <= 11) And IdSistema = "SA" Then

            '        CON.Open()
            '        Me.ASIGNA_PrePorcentaje_DescuentoTableAdapter.Connection = CON
            '        Me.ASIGNA_PrePorcentaje_DescuentoTableAdapter.Fill(Me.DataSetEdgar.ASIGNA_PrePorcentaje_Descuento, gloClv_Session, 5)
            '        CON.Close()
            '    ElseIf Me.NumericUpDown2.Value = 10 Or Me.NumericUpDown2.Value = 15 Or Me.NumericUpDown2.Value = 20 Or Me.NumericUpDown2.Value = 25 Or Me.NumericUpDown2.Value = 40 Or Me.NumericUpDown2.Value = 50 Or Me.NumericUpDown2.Value = 100 Then

            '        CON.Open()
            '        Me.ASIGNA_PrePorcentaje_DescuentoTableAdapter.Connection = CON
            '        Me.ASIGNA_PrePorcentaje_DescuentoTableAdapter.Fill(Me.DataSetEdgar.ASIGNA_PrePorcentaje_Descuento, gloClv_Session, Me.NumericUpDown2.Value)
            '        CON.Close()
            '    Else
            '        MsgBox("Los Descuentos solo puede ser de 10,15,20,25,40,50 y 100")
            '        Exit Sub
            '    End If

            'End If



            'If (Me.Panel1.Visible = True Or Me.Panel2.Visible = True) And (Me.NumericUpDown1.Value >= 6 And Me.NumericUpDown1.Value <= 12) And IdSistema = "VA" Then
            '    If Me.RadioButtonDescuento.Checked = True Then
            '        Dim CON2 As New SqlConnection(MiConexion)
            '        CON2.Open()
            '        Me.ASIGNA_PrePorcentaje_DescuentoTableAdapter.Connection = CON2
            '        Me.ASIGNA_PrePorcentaje_DescuentoTableAdapter.Fill(Me.DataSetEdgar.ASIGNA_PrePorcentaje_Descuento, gloClv_Session, Me.NumericUpDown2.Value)
            '        CON2.Close()
            '    ElseIf Me.RadioButtonMeses.Checked = True Then
            '        AsigaMesesDeRegalo(gloClv_Session, Me.NumericUpDown3.Value)
            '    End If
            'End If


            Glo_Apli_Pnt_Ade = False
            If Me.CheckBox1.CheckState = CheckState.Checked Then
                Glo_Apli_Pnt_Ade = True
            End If
            GloAdelantados = Me.NumericUpDown1.Value
            GloBnd = True
            Me.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub FrmPagosAdelantados_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

    End Sub

    Private Sub FrmPagosAdelantados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Me.NumericUpDown2.Value = 0

        HabilitaAdelantar_ServiciosUnicos = True

        'Verificamos si es que el Cliente Aplicara para la Promoci�n por COMBO
        'Si el cliente aplica promoci�n COMBO debe de adelantar sus meses a la par, no puede adelantar solo un servicio

        If DimeSiPuedeAdelantarPorCombo_PromocionMayo2010() = False Then

            'Si el resultado es True, significa que SI podemos habilitar la palomita de solo un servicio a la vez
            HabilitaAdelantar_ServiciosUnicos = False

        End If

        If HabilitaAdelantar_ServiciosUnicos = True Then

            Me.CheckBox1.Checked = True
            Me.CheckBox1.Enabled = False
            MsgBox("El cliente cuenta con una promoci�n activa por COMBO, solo puedes adelantar ambos servicios a la vez. N�mero de Mensualidades restantes con Promoci�n: " & Mensualidades_Restantes.ToString, MsgBoxStyle.Information, "Solo puedes adelantar los 2 servicios a la vez || Promoci�n COMBO")

        End If

        Me.Panel1.Visible = False
        Me.Panel4.Visible = False
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

    End Sub

    Private Sub NumericUpDown1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NumericUpDown1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.NumericUpDown1, Asc(LCase(e.KeyChar)), "N")))
    End Sub


    Private Sub NumericUpDown1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown1.ValueChanged
        Me.Panel1.Visible = False
        Me.Panel2.Visible = False
        Me.Panel3.Visible = False
        Me.Panel4.Visible = False
        Me.NumericUpDown2.Value = 0
        Me.NumericUpDown3.Value = 0

        'If (Me.NumericUpDown1.Value >= 12 And Me.NumericUpDown1.Value <= 12) And IdSistema = "SA" Then
        '    Me.Panel1.Visible = True
        '    Me.Panel4.Visible = True
        'ElseIf (Me.NumericUpDown1.Value >= 6 And Me.NumericUpDown1.Value <= 12) And IdSistema = "VA" Then
        '    Me.Panel1.Visible = True
        '    Me.Panel2.Visible = True
        '    Me.Panel3.Visible = True
        'End If
    End Sub


 



    Private Sub RadioButtonDescuento_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonDescuento.CheckedChanged
        If Me.RadioButtonDescuento.Checked = True Then
            Me.Panel1.Enabled = True
            Me.Panel2.Enabled = False
        End If
    End Sub

    Private Sub RadioButtonMeses_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonMeses.CheckedChanged
        If Me.RadioButtonMeses.Checked = True Then
            Me.Panel1.Enabled = False
            Me.Panel2.Enabled = True
        End If
    End Sub

    Private Sub AsigaMesesDeRegalo(ByVal Clv_Session As Long, ByVal Meses As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("AsigaMesesDeRegalo", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Meses", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = Meses
        comando.Parameters.Add(parametro1)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Function DimeSiPuedeAdelantarPorCombo_PromocionMayo2010() As Boolean

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "DimeSiPuedeAdelantarPorCombo_PromocionMayo2010"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 8

                Dim prmSession As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                prmSession.Value = GloTelClv_Session
                .Parameters.Add(prmSession)

                Dim prmContrato As New SqlParameter("@Contrato ", SqlDbType.BigInt)
                prmContrato.Value = GloContrato
                .Parameters.Add(prmContrato)

                Dim prmPudeAdelantar As New SqlParameter("@PudeAdelantar ", SqlDbType.Bit)
                prmPudeAdelantar.Direction = ParameterDirection.Output
                prmPudeAdelantar.Value = 0
                .Parameters.Add(prmPudeAdelantar)

                Dim prmMensualidades_Restantes As New SqlParameter("@Mensualidades_Restantes ", SqlDbType.Int)
                prmMensualidades_Restantes.Direction = ParameterDirection.Output
                prmMensualidades_Restantes.Value = 0
                .Parameters.Add(prmMensualidades_Restantes)

                Dim Result As Integer = .ExecuteNonQuery()

                Mensualidades_Restantes = prmMensualidades_Restantes.Value

                Return prmPudeAdelantar.Value


            End With
            CON80.Dispose()
            CON80.Close()
        Catch ex As Exception
            CON80.Dispose()
            CON80.Close()
            MsgBox("Error al verificar si el cliente puede adelantar meses por COMBO", MsgBoxStyle.Exclamation, ":: Advertencia ::")
            MsgBox(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Procedimiento: DimeSiPuedeAdelantarPorCombo_PromocionMayo2010, Funci�n: DimeSiPuedeAdelantarPorCombo_PromocionMayo2010()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try

    End Function

End Class