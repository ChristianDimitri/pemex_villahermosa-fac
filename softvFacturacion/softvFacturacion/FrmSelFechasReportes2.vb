﻿Imports System.Data.SqlClient
Public Class FrmSelFechasReportes2

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        LocBndReporteBonificaciones = True


        'Asignamos los valores de búsqueda para el Listado

        Sucursal_RepCobroAdeudo = Me.cbxSucursal.SelectedValue
        Caja_RepCobroAdeudo = Me.cbxCajera.SelectedValue
        Clv_Servicio_RepCobroAdeudo = Me.cbxServicio.SelectedValue

        If Me.cbxStatusFactura.SelectedItem.ToString = "Activa" Then
            StatusFactura_RepCobroAdeudo = 1
        ElseIf Me.cbxStatusFactura.SelectedItem.ToString = "Activa" Then
            StatusFactura_RepCobroAdeudo = 0
        Else
            'TODAS
            StatusFactura_RepCobroAdeudo = 2
        End If

        If Me.cbxStatusServAnt.SelectedItem.ToString = "   --- Selecciona un Status de Servicio ---" Then
            StatusServicio_RepCobroAdeudo = ""
        Else
            'TODOS
            StatusServicio_RepCobroAdeudo = Me.cbxStatusServAnt.SelectedItem.ToString
        End If

        FechaInicial_RepCobroAdeudo = Me.dtpFechaInicial.Value
        FechaFinal_RepCobroAdeudo = Me.dtpFechaFinal.Value

        FrmImprimirRepGral.Show()

        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub FrmSelFechasReportes2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me)

        Try

            Dim conn As New SqlConnection(MiConexion)
            conn.Open()

            Dim comando As New SqlClient.SqlCommand("DameSucursales_CBX", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Dim Dataset As New DataSet
            Dim Bs As New BindingSource
            Adaptador.Fill(Dataset, "resultado_Sucursales")
            Bs.DataSource = Dataset.Tables("resultado_Periodos")

            Me.cbxSucursal.DisplayMember = "Nombre"
            Me.cbxSucursal.ValueMember = "Clv_Sucursal"
            Me.cbxSucursal.DataSource = Dataset.Tables("resultado_Sucursales")

            '-------------------------------------------------------------------------------------------------------------------

            'Con la misma conexión jalamos las Cajas de la Sucursal
            Dim cmdCajas As New SqlClient.SqlCommand("DameCajasPorSucursal_CBX", conn)
            cmdCajas.CommandType = CommandType.StoredProcedure

            Dim AdaptadorCajas As New SqlDataAdapter()
            AdaptadorCajas.SelectCommand = cmdCajas

            Dim DatasetSucursales As New DataSet
            Dim BsCajas As New BindingSource

            AdaptadorCajas.SelectCommand.Parameters.Add("@Clv_Sucursal", SqlDbType.Int).Value = Me.cbxSucursal.SelectedValue

            AdaptadorCajas.Fill(DatasetSucursales, "resultado_Cajas")
            BsCajas.DataSource = DatasetSucursales.Tables("resultado_Cajas")

            Me.cbxCajera.DisplayMember = "Descripcion"
            Me.cbxCajera.ValueMember = "Clave"
            Me.cbxCajera.DataSource = DatasetSucursales.Tables("resultado_Cajas")

            '-------------------------------------------------------------------------------------------------------------------

            '                           TIPO SERVICIOS
            Dim cmdTipoServicios As New SqlClient.SqlCommand("DameTipoServicios_CBX", conn)
            cmdTipoServicios.CommandType = CommandType.StoredProcedure

            Dim AdaptadorTipoServicios As New SqlDataAdapter()
            AdaptadorTipoServicios.SelectCommand = cmdTipoServicios

            Dim DatasetTipoServicios As New DataSet
            Dim BsTipoServicios As New BindingSource

            AdaptadorTipoServicios.Fill(DatasetTipoServicios, "resultado_TipoServicios")
            BsTipoServicios.DataSource = DatasetTipoServicios.Tables("resultado_TipoServicios")

            Me.cbxTipSer.DisplayMember = "Concepto"
            Me.cbxTipSer.ValueMember = "Clv_TipSer"
            Me.cbxTipSer.DataSource = DatasetTipoServicios.Tables("resultado_TipoServicios")

            '-------------------------------------------------------------------------------------------------------------------

            '                           SERVICIOS TARIFADOS
            Dim cmdServiciosTarifados As New SqlClient.SqlCommand("DameDescripcionPorTipoDeServicio_CBX", conn)
            cmdServiciosTarifados.CommandType = CommandType.StoredProcedure

            Dim AdaptadorServiciosTarifados As New SqlDataAdapter()
            AdaptadorServiciosTarifados.SelectCommand = cmdServiciosTarifados

            Dim DatasetServiciosTarifados As New DataSet
            Dim BsServiciosTarifados As New BindingSource

            AdaptadorServiciosTarifados.SelectCommand.Parameters.Add("@Clv_TipSer", SqlDbType.Int).Value = Me.cbxTipSer.SelectedValue

            AdaptadorServiciosTarifados.Fill(DatasetServiciosTarifados, "resultado_ServiciosTarifados")
            BsServiciosTarifados.DataSource = DatasetServiciosTarifados.Tables("resultado_ServiciosTarifados")

            Me.cbxServicio.DisplayMember = "Descripcion"
            Me.cbxServicio.ValueMember = "Clv_Servicio"
            Me.cbxServicio.DataSource = DatasetServiciosTarifados.Tables("resultado_ServiciosTarifados")

            '-------------------------------------------------------------------------------------------------------------------
            conn.Close()
            conn.Dispose()
        Catch ex As Exception

        End Try

    End Sub
    Public Sub RefrescaCajas()

        Try
            Dim conn As New SqlConnection(MiConexion)

            'Con la misma conexión jalamos las Cajas de la Sucursal
            Dim cmdCajas As New SqlClient.SqlCommand("DameCajasPorSucursal_CBX", conn)
            cmdCajas.CommandType = CommandType.StoredProcedure

            Dim AdaptadorCajas As New SqlDataAdapter()
            AdaptadorCajas.SelectCommand = cmdCajas

            Dim DatasetSucursales As New DataSet
            Dim BsCajas As New BindingSource

            AdaptadorCajas.SelectCommand.Parameters.Add("@Clv_Sucursal", SqlDbType.Int).Value = Me.cbxSucursal.SelectedValue

            AdaptadorCajas.Fill(DatasetSucursales, "resultado_Cajas")
            BsCajas.DataSource = DatasetSucursales.Tables("resultado_Cajas")

            Me.cbxCajera.DisplayMember = "Descripcion"
            Me.cbxCajera.ValueMember = "Clave"
            Me.cbxCajera.DataSource = DatasetSucursales.Tables("resultado_Cajas")

            conn.Open()
        Catch ex As Exception

        End Try

       
    End Sub
    Public Sub RefrescaServiciosTarifados()

        Try
            Dim conn As New SqlConnection(MiConexion)

            '                           SERVICIOS TARIFADOS
            Dim cmdServiciosTarifados As New SqlClient.SqlCommand("DameDescripcionPorTipoDeServicio_CBX", conn)
            cmdServiciosTarifados.CommandType = CommandType.StoredProcedure

            Dim AdaptadorServiciosTarifados As New SqlDataAdapter()
            AdaptadorServiciosTarifados.SelectCommand = cmdServiciosTarifados

            Dim DatasetServiciosTarifados As New DataSet
            Dim BsServiciosTarifados As New BindingSource

            AdaptadorServiciosTarifados.SelectCommand.Parameters.Add("@Clv_TipSer", SqlDbType.Int).Value = Me.cbxTipSer.SelectedValue

            AdaptadorServiciosTarifados.Fill(DatasetServiciosTarifados, "resultado_ServiciosTarifados")
            BsServiciosTarifados.DataSource = DatasetServiciosTarifados.Tables("resultado_ServiciosTarifados")

            Me.cbxServicio.DisplayMember = "Descripcion"
            Me.cbxServicio.ValueMember = "Clv_Servicio"
            Me.cbxServicio.DataSource = DatasetServiciosTarifados.Tables("resultado_ServiciosTarifados")

            conn.Open()
            conn.Dispose()
        Catch ex As Exception

        End Try


    End Sub

    Private Sub cbxSucursal_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxSucursal.SelectedValueChanged
        RefrescaCajas()
    End Sub

    Private Sub cbxTipSer_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipSer.SelectedValueChanged
        RefrescaServiciosTarifados()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
        Me.Dispose()
    End Sub
End Class