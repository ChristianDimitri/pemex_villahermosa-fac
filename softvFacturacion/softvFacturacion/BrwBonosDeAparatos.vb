﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Public Class BrwBonosDeAparatos

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BrwBonosDeAparatos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Op_Devolucion = ""
        colorea(Me)
        BuscaBonosDeGarantia()

        '-------------------------------------------------
        Me.chBoxPorFecha.CheckState = CheckState.Unchecked
        dtpFechaGeneracion.Enabled = False
        dtpFechaGeneracionFinal.Enabled = False
        Me.PanelFechas.BackColor = Color.Gainsboro

        If Me.dgvResultados.RowCount <= 0 Then
            Me.btnGenerar.Enabled = False
            Me.btnConsultar.Enabled = False
            Me.btnCancelar.Enabled = False
            Me.btnReImprimir.Enabled = False
        End If


    End Sub

    Private Sub btnGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerar.Click

        If Me.dgvResultados.CurrentRow.Cells(5).Value.ToString = "PENDIENTE" Then

            Dim Opcion As Integer = VerificaSiAplicaDevolcionPorDeBonoGarantia(Me.dgvResultados.CurrentRow.Cells(1).Value)

            Select Case Opcion
                Case 0
                    MsgBox("No se puede generar la devolución ya que el Cliente aún cuenta con el aparato y con el servicio. Primero debe generarse su Cobro de Adeudo.", MsgBoxStyle.Information)
                Case 1

                    '
                    Op_Devolucion = "N"
                    Id_Devolucion = CType(Me.dgvResultados.CurrentRow.Cells(0).Value, Integer)
                    Contrato_Devolucion = CType(Me.dgvResultados.CurrentRow.Cells(1).Value, Integer)
                    FrmDevoluciones.Show()

                Case 2
                    Dim Resp = MsgBox("El Cliente tiene su servicio en baja, pero NO HA SALIDO POR COBRO DE ADEUDO. Debe traer consigo el aparato. ¿Aún así deseas generar la devolución?", MsgBoxStyle.YesNoCancel, "EL CLIENTE DEBE TRAER EL APARATO")

                    If Resp = MsgBoxResult.Yes Then

                        Op_Devolucion = "N"
                        Id_Devolucion = CType(Me.dgvResultados.CurrentRow.Cells(0).Value, Integer)
                        Contrato_Devolucion = CType(Me.dgvResultados.CurrentRow.Cells(1).Value, Integer)
                        FrmDevoluciones.Show()

                    End If

            End Select

        Else
            MsgBox("No se puede generar la devolución por que se encuentra: " & Me.dgvResultados.CurrentRow.Cells(5).Value.ToString, MsgBoxStyle.Information)
        End If

    End Sub

    'Mostramos de Inicio los últimos registros que han entrado
    Public Sub BuscaBonosDeGarantia()


        Try

            Dim conn As New SqlConnection(MiConexion)
            conn.Open()
            Dim comando As New SqlClient.SqlCommand("BuscaBonosDeGarantia", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Dim Dataset As New DataSet
            Dim Bs As New BindingSource

            Adaptador.SelectCommand.Parameters.Add("@busquedaIncial", SqlDbType.Bit).Value = 1

            Adaptador.Fill(Dataset, "resultado_BonosDeGarantia")
            Bs.DataSource = Dataset.Tables(0)

            Me.dgvResultados.DataSource = Bs
            MarcaCanceladas()
            conn.Dispose()
            conn.Close()

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de realizar labúsqueda de Bonos de Garantía.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Porcedimeinto: BuscaBonosDeGarantia, Función: BuscaBonosDeGarantia(), Formulario: BrwBonosDeAparatos"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try

    End Sub
    Public Sub BuscaBonosDeGarantiaPorFiltros()


        Try

            Dim conn As New SqlConnection(MiConexion)
            conn.Open()
            Dim comando As New SqlClient.SqlCommand("BuscaBonosDeGarantiaPorFiltro", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Dim Dataset As New DataSet
            Dim Bs As New BindingSource

            Adaptador.SelectCommand.Parameters.Add("@IdDevolucion", SqlDbType.VarChar, 50).Value = Me.txtIdDevolucion.Text

            If Len(Me.txtContrato.Text) > 0 And IsNumeric(Me.txtContrato.Text) = True Then
                Adaptador.SelectCommand.Parameters.Add("@Contrato", SqlDbType.BigInt).Value = Me.txtContrato.Text
            Else
                Adaptador.SelectCommand.Parameters.Add("@Contrato", SqlDbType.BigInt).Value = 0
            End If

            Adaptador.SelectCommand.Parameters.Add("@FechaGeneracion_Inicial", SqlDbType.DateTime).Value = Me.dtpFechaGeneracion.Value
            Adaptador.SelectCommand.Parameters.Add("@FechaGeneracion_Final", SqlDbType.DateTime).Value = Me.dtpFechaGeneracionFinal.Value

            If Me.chBoxPorFecha.CheckState = CheckState.Checked Then
                Adaptador.SelectCommand.Parameters.Add("@por_fecha", SqlDbType.Int).Value = 1
            Else
                Adaptador.SelectCommand.Parameters.Add("@por_fecha", SqlDbType.Int).Value = 0
            End If


            Adaptador.SelectCommand.Parameters.Add("@orderby", SqlDbType.VarChar).Value = "Desc"

            If Len(Me.txtRegMax.Text) > 0 And IsNumeric(Me.txtRegMax.Text) = True Then
                Adaptador.SelectCommand.Parameters.Add("@registros_maximos", SqlDbType.Int).Value = CType(Me.txtRegMax.Text, Integer)
            Else
                Adaptador.SelectCommand.Parameters.Add("@registros_maximos", SqlDbType.Int).Value = 100
            End If

            If Me.cmbxStatus.Text = "" Then
                Adaptador.SelectCommand.Parameters.Add("@Status", SqlDbType.VarChar).Value = ""
            Else
                Select Case Me.cmbxStatus.Text

                    Case "PENDIENTE"
                        Adaptador.SelectCommand.Parameters.Add("@Status", SqlDbType.VarChar).Value = "A"

                    Case "GENERADA"
                        Adaptador.SelectCommand.Parameters.Add("@Status", SqlDbType.VarChar).Value = "S"

                    Case "CANCELADA"
                        Adaptador.SelectCommand.Parameters.Add("@Status", SqlDbType.VarChar).Value = "C"

                End Select

            End If

            Adaptador.Fill(Dataset, "resultado_BuscaBonosDeGarantiaPorFiltro")
            Bs.DataSource = Dataset.Tables("resultado_BuscaBonosDeGarantiaPorFiltro")
            MarcaCanceladas()
            Me.dgvResultados.DataSource = Bs

            Me.Label6.Text = "RESULTADO DE LA BÚSQUEDA"

            conn.Dispose()
            conn.Close()

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de realizar la búsqueda de Bonos de Garantía.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Porcedimeinto: BuscaBonosDeGarantia, Función: BuscaBonosDeGarantia(), Formulario: BrwBonosDeAparatos"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try

    End Sub



    Private Sub dgvResultados_CellMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvResultados.CellMouseDoubleClick
        Id_Devolucion = CType(Me.dgvResultados.CurrentRow.Cells(0).Value, Integer)
        FrmDetDevolucionAparato.Show()
    End Sub

    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click

        Id_Devolucion = CType(Me.dgvResultados.CurrentRow.Cells(0).Value, Integer)
        FrmDetDevolucionAparato.Show()

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        Op_Devolucion = ""
        colorea(Me)

        If Me.txtContrato.Text = "" And Me.txtIdDevolucion.Text = "" And Me.txtRegMax.Text = "" And Me.chBoxPorFecha.CheckState = CheckState.Unchecked And Me.cmbxStatus.Text = "" Then
            BuscaBonosDeGarantia()
        Else
            BuscaBonosDeGarantiaPorFiltros()
        End If


    End Sub

    Private Sub chBoxPorFecha_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chBoxPorFecha.CheckedChanged

        If chBoxPorFecha.Checked = True Then

            dtpFechaGeneracion.Enabled = True
            dtpFechaGeneracionFinal.Enabled = True
            Me.PanelFechas.BackColor = Color.WhiteSmoke

        Else
            dtpFechaGeneracion.Enabled = False
            dtpFechaGeneracionFinal.Enabled = False
            Me.PanelFechas.BackColor = Color.Gainsboro

        End If

    End Sub
    Private Sub cmbxStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbxStatus.SelectedIndexChanged

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Dim decision = MsgBox("¿Deseas Cancelar la Devolución del Bono de Aparato?", MsgBoxStyle.YesNo)

        If decision = MsgBoxResult.Yes Then

            If Me.dgvResultados.CurrentRow.Cells(5).Value.ToString = "GENERADA" Then
                Dim FechaDevolcuion As DateTime
                Dim FechaServidor As DateTime

                FechaDevolcuion = FormatDateTime(Me.dgvResultados.CurrentRow.Cells(6).Value, DateFormat.ShortDate)
                FechaServidor = FormatDateTime(Now(), DateFormat.ShortDate)

                If FechaDevolcuion = FechaServidor Then
                    CancelaDevolucion(Me.dgvResultados.CurrentRow.Cells(0).Value)
                Else
                    MsgBox("La Fecha de Generación de la Devolución NO es del día actual. No se puede cancelar la devolución.", MsgBoxStyle.Information, "LA FECHA NO ES DEL DÍA ACTUAL")
                End If
            Else
                MsgBox("No se puede cancelar la Devolución por que esta pendiente o ya ha sido cancelada.", MsgBoxStyle.Exclamation)
            End If

        End If

    End Sub

    'Procedimiento para Cancelar la Devolución
    Public Sub CancelaDevolucion(ByVal ID_Devolucion As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim Comando As New SqlCommand()

        Try

            Dim prmID_Dev As New SqlParameter("@ID_Dev", SqlDbType.VarChar, 400)
            Dim prmMotivoCancelacion_Dev As New SqlParameter("@MotivoCancelacion_Dev", SqlDbType.VarChar, 400)
            Dim prmError As New SqlParameter("@Error", SqlDbType.Int)
            Dim prmMsgError As New SqlParameter("@MsgError", SqlDbType.VarChar, 400)

            prmID_Dev.Value = ID_Devolucion
            prmMotivoCancelacion_Dev.Value = "CANCELADA DESDE EL CATÁLOGO DE BONOS DE GARANTÍA"
            prmError.Direction = ParameterDirection.Output
            prmMsgError.Direction = ParameterDirection.Output

            With Comando
                .Parameters.Add(prmID_Dev)
                .Parameters.Add(prmMotivoCancelacion_Dev)
                .Parameters.Add(prmError)
                .Parameters.Add(prmMsgError)

                .CommandText = "CancelaDevolucion"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                .Connection = conexion

            End With

            conexion.Open()
            Comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()

            If prmError.Value = 1 Then
                MsgBox(prmMsgError.Value.ToString, MsgBoxStyle.Exclamation)
            Else
                MsgBox("Devolución Cancelada correctamente.", MsgBoxStyle.Information)
                'REFRESCAMOS LOS DATOS
                BuscaBonosDeGarantia()
            End If



        Catch ex As Exception

            MsgBox("Ocurrió un error al cancelar la Devolución.", MsgBoxStyle.Critical)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Porcedimeinto: CancelaDevolucion, Función: CancelaDevolucion(), Formulario: BrwBonosDeAparatos"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

            conexion.Close()
            conexion.Dispose()

        Finally

            conexion.Close()
            conexion.Dispose()

        End Try

    End Sub

    'Marcamos las Cancelaciones
    Public Sub MarcaCanceladas()

        Dim i As Integer
        Try
            For i = 0 To Me.dgvResultados.RowCount - 1

                If Me.dgvResultados.Rows(i).Cells(5).Value.ToString = "CANCELADA" Then

                    Me.dgvResultados.Rows(i).DefaultCellStyle.ForeColor = Color.Red

                End If
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btnReImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReImprimir.Click

        'Checamos que existan Registros
        If Me.dgvResultados.CurrentRow.Cells(5).Value.ToString = "PENDIENTE" Then

            MsgBox("No se puede Reimprimir el comprobante ya que aún no se ha generado la devolución.", MsgBoxStyle.Information, "LA DEVOLUCIÓN AÚN NO SE HA GENERADO")

        Else

            'Cargamos el Reporte
            CargaReporteDevolucion(Me.dgvResultados.CurrentRow.Cells(0).Value, Me.dgvResultados.CurrentRow.Cells(1).Value, "Este comprobante es una COPIA del Original. ")

        End If

    End Sub

    Public Sub CargaReporteDevolucion(ByVal IDDevolcion As Integer, ByVal Contrato As Integer, ByVal Original_o_Copia As String)

        'Mandamos a Imprimir el Reporte del Ticket de la devoluacion
        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet

        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("ReporteDevoluciones", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Adaptador.SelectCommand.Parameters.Add("@ID_DEVOLUCION", SqlDbType.Int).Value = IDDevolcion
            Adaptador.SelectCommand.Parameters.Add("@CONTRATO", SqlDbType.BigInt).Value = Contrato

            Dim Bs As New BindingSource
            Adaptador.Fill(Dataset)

            Dataset.Tables(0).TableName = "DetFactura"
            Dataset.Tables(1).TableName = "DetCliente"

            conn.Close()
            conn.Dispose()

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de enlazar los Datos Principales, los resultados de las búsquedas pueden ser inconsistentes.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Exit Sub

        Finally

            conn.Close()
            conn.Dispose()

        End Try

        'Cargamos el Informe en base al procediemiento
        Try

            'Asignamos el Reporte al Objeto Global
            rDocument = New ReportDocument()

            'Ruta del Reporte
            rDocument.Load("E:\ProyectosTeamFundation\Developer\Sahuayo\Facturación\softvFacturacion\softvFacturacion\ReporteDevoluciones.rpt")

            'Formulas
            rDocument.DataDefinition.FormulaFields("EMPRESA").Text = "'" & GloEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("DIRECCION_EMPRESA").Text = "'" & GloDireccionEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("COLONIA DE LA EMPRESA").Text = "'" & GloColonia_CpEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("CIUDAD DE LA EMPRESA").Text = "'" & GloCiudadEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("RFC EMPRESA").Text = "'" & GloRfcEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("TELEFONO EMPRESA").Text = "'" & GloTelefonoEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("Original O Copia").Text = "'" & Original_o_Copia & "'"

            'Origen de Datos
            rDocument.SetDataSource(Dataset)

            'Lo mandamos al visor de Reportes
            FrmVisorDeReportes.Show()

        Catch ex As Exception
            MsgBox("Ocurrió un error al momento de generar la reimpresión de la devolución.", MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Function VerificaSiAplicaDevolcionPorDeBonoGarantia(ByVal Contrato As Integer) As Integer

        'Procedimiento que nos dice si es que el Cliente ya ha pagado su cobro de adeudo 
        'por el servicio que se entregó el aparato. La devolución de efectivo debe ser del mismo díaque pasó a baja.

        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet

        Try
            Dim comando As New SqlClient.SqlCommand("VerificaSiAplicaDevolcionPorDeBonoGarantia", conn)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@Contrato", SqlDbType.Int)
            comando.Parameters(0).Value = Contrato

            comando.Parameters.Add("@DevolucionAprobada", SqlDbType.TinyInt)
            comando.Parameters(1).Direction = ParameterDirection.Output
            comando.Parameters(1).Value = 0

            conn.Open()

            comando.ExecuteNonQuery()

            conn.Close()
            conn.Dispose()

            Return comando.Parameters(1).Value

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de verificar si el cliente ya pago su cobro de adeudo.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            conn.Close()
            conn.Dispose()

            Return False

        Finally

            conn.Close()
            conn.Dispose()

        End Try



    End Function


End Class