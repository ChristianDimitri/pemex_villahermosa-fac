﻿Public Class NumRevistas

    Private Sub NumRevistas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
    End Sub

    Private Sub CancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelButton.Click
        NoRevistas = 0
        Me.Close()
    End Sub

    Private Sub AceptarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AceptarButton.Click
        FrmFAC.CHECASIHAYREVISTAS(2)

        If TotalRevistas < RevistasNumeric.Value Then
            MsgBox("El Número de Revistas Solicitadas Excede El Número De Revistas En Existencia")
            MsgBox("Revistas En Existencia: " + CStr(TotalRevistas))
            Exit Sub
        End If
        NoRevistas = RevistasNumeric.Value
        Revista = True
        Me.Close()
    End Sub
End Class