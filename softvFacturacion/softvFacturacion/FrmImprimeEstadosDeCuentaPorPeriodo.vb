﻿Imports System.Net
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class FrmImprimeEstadosDeCuentaPorPeriodo

    Dim PeriodoDeCobroSeleccionado As Integer
    Dim TotalDeContratosPorPeriodo As Integer
    Dim ContratoInicial As Integer
    Dim ContratoFinal As Integer
    Dim UltimoContratoImpreso As Integer
    Dim StatusDeImpresion As String = ""
    Dim listaDeContratos As New List(Of Integer)
    Dim ProcesoIniciado As Boolean = False


    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        If ProcesoIniciado = False Then

            Me.Close()
            Me.Dispose()
        Else

            Dim Respuesta = MsgBox("El Proceso de Impresión aún NO ha terminado. ¿Deseas salir y cancelar la Impresión?")
            If Respuesta = MsgBoxResult.Yes Then

                Me.Close()
                Me.Dispose()

            End If

        End If

        
    End Sub


    Private Sub FrmImprimeEstadosDeCuentaPorPeriodo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Me.chbxImprimirTodosLosContratos.Checked = True
        Me.chbxImprimirTodosLosContratos.CheckState = CheckState.Checked
        Me.txtContratoInicial.Enabled = False
        Me.txtContratoFinal.Enabled = False

        colorea(Me)
        Me.lblTitulo.ForeColor = Color.Black
        Me.lblStatusDeImpresion.Text = "PROCESO INACTIVO"
        Me.lblStatusDeImpresion.ForeColor = Color.Black
        Me.lblNumeroDeContratoImprimiendo.ForeColor = Color.Black
        Me.lblPeriodoDeCobroSeleccionado.ForeColor = Color.Black
        Me.Label1.ForeColor = Color.Black
        Me.Label2.ForeColor = Color.Black

        'Cargamos los Periodos Disponibles
        Dim dsPeriodos As New DataSet
        dsPeriodos = Nothing
        dsPeriodos = CargaGeneralesDeImpresionEstadosDeCuenta()

        If (dsPeriodos.Tables.Count > 0) Then
            dgvPeriodosDeCobro.DataSource = dsPeriodos.Tables(0)

            Me.dgvPeriodosDeCobro.Columns(1).Width = 160
            Me.dgvPeriodosDeCobro.Columns(2).Width = 160
            Me.dgvPeriodosDeCobro.Columns(3).Width = 160
            Me.dgvPeriodosDeCobro.Columns(4).Width = 100
            Me.dgvPeriodosDeCobro.Columns(5).Width = 100

            Me.dgvPeriodosDeCobro.Columns(0).HeaderText = "Número de Periodo de Cobro"
            Me.dgvPeriodosDeCobro.Columns(1).HeaderText = "Fecha de Generación"
            Me.dgvPeriodosDeCobro.Columns(2).HeaderText = "Fecha INICIAL"
            Me.dgvPeriodosDeCobro.Columns(3).HeaderText = "Fecha FINAL"
            Me.dgvPeriodosDeCobro.Columns(4).HeaderText = "Usuario que lo generó"
            Me.dgvPeriodosDeCobro.Columns(5).HeaderText = "Total Edos. de Cuenta Generados"

            Me.dgvPeriodosDeCobro.Columns(4).Visible = False

            btnCancelar.Enabled = True
            btnImprimir.Enabled = True
            btnCargarPeriodo.Enabled = True

        Else

            btnCancelar.Enabled = False
            btnImprimir.Enabled = False
            btnCargarPeriodo.Enabled = False

        End If


    End Sub

    Private Sub txtContratoInicial_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtContratoInicial.KeyPress
        If e.KeyChar = Chr(13) Then

            If Len(Me.txtContratoInicial.Text) > 0 Then
                If IsNumeric(Me.txtContratoInicial.Text) Then
                    Me.txtContratoInicial.Text.Trim(e.KeyChar)
                End If
            End If

        End If
    End Sub

    Private Sub txtContratoFinal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtContratoFinal.KeyPress
        If e.KeyChar = Chr(13) Then

            If Len(Me.txtContratoFinal.Text) > 0 Then
                If IsNumeric(Me.txtContratoFinal.Text) Then
                    Me.txtContratoFinal.Text.Trim(e.KeyChar)
                End If
            End If

        End If
    End Sub
    Private Function CargaGeneralesDeImpresionEstadosDeCuenta() As DataSet
        Dim conn As New SqlConnection(MiConexion)
        Dim dsPeriodos As New DataSet


        Try


            conn.Open()
            Dim comando As New SqlClient.SqlCommand("CargaGeneralesDeImpresionEstadosDeCuenta", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Adaptador.Fill(dsPeriodos)

            dsPeriodos.Tables(0).TableName = "PeriodosDeCobro"


        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de cargar los Periodos de Cobro.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        Finally

            conn.Dispose()
            conn.Close()

        End Try

        Return dsPeriodos


    End Function

    Private Sub btnCargarPeriodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCargarPeriodo.Click

        If dgvPeriodosDeCobro.RowCount <= 0 Then
            MsgBox("Aún no se ha generado ningún Periodo de Cobro.", MsgBoxStyle.Information)
            Exit Sub
        End If

        PeriodoDeCobroSeleccionado = Me.dgvPeriodosDeCobro.SelectedCells(0).Value
        TotalDeContratosPorPeriodo = Me.dgvPeriodosDeCobro.SelectedCells(5).Value

        CargaContratosPorPeriodosDeCobro()
        Me.lblPeriodoDeCobroSeleccionado.Text = "Periodo de Cobro Seleccionado: " & PeriodoDeCobroSeleccionado.ToString & " ~ Total de Estados de Cuenta: " & TotalDeContratosPorPeriodo.ToString

    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click

        If PeriodoDeCobroSeleccionado <= 0 Then
            MsgBox("Primero debes seleccionar un Periodo de Cobro")
            Exit Sub
        End If

        Dim Respuesta = MsgBox("Se van a imprimir los " & TotalDeContratosPorPeriodo.ToString & " Estados de Cuenta. ¿Está la impresora lista y deseas continuar?", MsgBoxStyle.YesNoCancel)

        If Respuesta <> MsgBoxResult.Yes Then
            Exit Sub
        End If

        Dim Contrato As Integer = 0
        Dim TotalContratosImpresos As Integer = 0

        If PeriodoDeCobroSeleccionado > 0 Then

            ProcesoIniciado = True
            Me.pgProgresoDeImpresion.Maximum = TotalDeContratosPorPeriodo
            Me.pgProgresoDeImpresion.Style = ProgressBarStyle.Blocks

            Me.lblStatusDeImpresion.Text = "PROCESO INICIADO..."

            For Each Contrato In listaDeContratos

                TotalContratosImpresos = TotalContratosImpresos + 1
                Me.lblNumeroDeContratoImprimiendo.Text = "Imprimiendo Contrato " & TotalContratosImpresos.ToString & " de " & TotalDeContratosPorPeriodo
                Imprimir(Contrato, PeriodoDeCobroSeleccionado)
                Me.pgProgresoDeImpresion.Value = TotalContratosImpresos

            Next

            Me.lblStatusDeImpresion.Text = "¡PROCESO TERMINADO!"
            ProcesoIniciado = False
            MsgBox("La Impresión de Estados de Cuenta ha finalizado.", MsgBoxStyle.Information)

        End If

        

        

    End Sub


    Private Sub CargaContratosPorPeriodosDeCobro()

        Dim I As Integer = 0
        Dim Fila2 As DataRow
        Dim conexion As New SqlConnection(MiConexion)
        Dim oDataSet As New DataSet

        Try


            Dim cmd As New SqlCommand("CargaContratosAImprimirPorPeriodo", conexion)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("@PERIODO", PeriodoDeCobroSeleccionado)

            cmd.Parameters.AddWithValue("@TODOS", Me.chbxImprimirTodosLosContratos.Checked)

            If chbxImprimirTodosLosContratos.Checked = False Then

                If IsNumeric(txtContratoInicial.Text) And IsNumeric(txtContratoFinal.Text) Then

                    If CType(txtContratoInicial.Text, Integer) <= CType(txtContratoFinal.Text, Integer) Then

                        cmd.Parameters.AddWithValue("@CONTRATO_INICIAL", CType(txtContratoInicial.Text, Integer))
                        cmd.Parameters.AddWithValue("@CONTRATO_FINAL", CType(txtContratoFinal.Text, Integer))

                    Else

                        MsgBox("El Contrato Inicial debe ser menor o igual que el contrato Final")
                        Exit Sub
                    End If

                Else
                    MsgBox("Debes especificar tanto el Contrato Inicial como el Contrato Final.")
                    Exit Sub
                End If

            Else
                cmd.Parameters.AddWithValue("@CONTRATO_INICIAL", 0)
                cmd.Parameters.AddWithValue("@CONTRATO_FINAL", 0)
            End If

            cmd.Parameters.Add("@TOTALCONTRATOS", SqlDbType.Int)
            cmd.Parameters("@TOTALCONTRATOS").Value = 0
            cmd.Parameters("@TOTALCONTRATOS").Direction = ParameterDirection.Output


            Dim dr As SqlDataReader
            conexion.Open()
            dr = cmd.ExecuteReader()
            listaDeContratos.Clear()

            Using dr

                While dr.Read
                    listaDeContratos.Add(dr("CONTRATO").ToString())
                End While
            End Using

            TotalDeContratosPorPeriodo = cmd.Parameters("@TOTALCONTRATOS").Value

        Catch ex As Exception

            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        Finally

            conexion.Close()
            conexion.Dispose()

        End Try

    End Sub

    Private Sub Imprimir(ByVal Contrato As Integer, ByVal PeriodoDeCobro As Integer)

        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet
        Dim ContratoActual As Integer = Contrato


        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("MuestraEstadoDeCuentaPorPeriodo", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Adaptador.SelectCommand.Parameters.Add("@CONTRATO", SqlDbType.BigInt).Value = Contrato
            Adaptador.SelectCommand.Parameters.Add("@PERIODO", SqlDbType.BigInt).Value = PeriodoDeCobro

            Dim Bs As New BindingSource
            Adaptador.Fill(Dataset)

            '               DATOS GENERALES

            Dataset.Tables(0).TableName = "tbl_DatosCliente"
            Dataset.Tables(1).TableName = "tbl_CargosDelCliente"
            Dataset.Tables(2).TableName = "tbl_Saldos"

            '               CONSUMOS

            Dataset.Tables(3).TableName = "tbl_044"
            Dataset.Tables(4).TableName = "tbl_045"
            Dataset.Tables(5).TableName = "tbl_LDN"
            Dataset.Tables(6).TableName = "tbl_EUC"
            Dataset.Tables(7).TableName = "tbl_RESTO"

            '               RESUMEN DE LLAMADAS

            Dataset.Tables(8).TableName = "tbl_TitLlamadas_044"
            Dataset.Tables(9).TableName = "tbl_TitLlamadas_045"
            Dataset.Tables(10).TableName = "tbl_TitLlamadas_LDN"
            Dataset.Tables(11).TableName = "tbl_TitLlamadas_EUC"
            Dataset.Tables(12).TableName = "tbl_TitLlamadas_RESTO"

            Dataset.Tables(13).TableName = "tbl_TitLlamadas_EUC_LDN"

            Dataset.Tables(14).TableName = "Tel_Tit_Resumen_Locales"
            Dataset.Tables(15).TableName = "Detalle_Llamadas_Locales"
            Dataset.Tables(16).TableName = "Detalle_Llamadas_EntreClientes"

            Dataset.Tables(17).TableName = "tbl_Impuestos"
            Dataset.Tables(18).TableName = "tbl_GeneralesEdoCta"



            conn.Close()
            conn.Dispose()

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de enlazar los Datos Principales, los resultados de las búsquedas pueden ser inconsistentes.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)


        Finally

            conn.Close()
            conn.Dispose()

        End Try

        'IMPRIMIMOS EL ESTADO DE CUENTA
        Try

            'Asignamos el Reporte al Objeto Global
            rDocument = New ReportDocument()

            'Ruta del Reporte
            rDocument.Load("\\192.168.60.200\Exes\SofTv\Reportes\rptEstadoDeCuenta.rpt")
            'rDocument.Load("E:\ProyectosTeamFundation\Developer\Sahuayo\Facturación\softvFacturacion\softvFacturacion\rptEstadoDeCuenta.rpt")

            'Origen de Datos
            rDocument.SetDataSource(Dataset)
            rDocument.PrintToPrinter(1, True, 1, 0)
            'Lo mandamos al visor de Reportes
            'FrmVisorDeReportes.Show()

        Catch ex As Exception

            MsgBox("Ocurrió un error." & ex.Message.ToString, MsgBoxStyle.Critical)

        End Try


    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click

    End Sub

    Private Sub chbxImprimirTodosLosContratos_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chbxImprimirTodosLosContratos.CheckedChanged

        If Me.chbxImprimirTodosLosContratos.CheckState = CheckState.Checked Then

            Me.txtContratoInicial.Enabled = False
            Me.txtContratoFinal.Enabled = False

            Me.txtContratoInicial.Text = ""
            Me.txtContratoFinal.Text = ""

        Else

            Me.txtContratoInicial.Enabled = True
            Me.txtContratoFinal.Enabled = True

        End If
        


    End Sub
End Class