Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FrmSelCiuSucUsu

    Private Sub FrmSelCiuSucUsu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.cbPoblaciones.DataSource = Me.obtenCiudades()
    End Sub

    Private Sub cbPoblaciones_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPoblaciones.SelectedIndexChanged
        If Me.cbPoblaciones.SelectedValue > 0 Then
            Me.cbSucursal.DataSource = Me.obtenSucursales(Me.cbPoblaciones.SelectedValue)
            Me.cbSucursal.Enabled = True
        Else
            Me.cbSucursal.Enabled = False
        End If
    End Sub

    Private Sub cbSucursal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSucursal.SelectedIndexChanged
        If Me.cbSucursal.SelectedValue > 0 Then
            Me.cbUsuario.DataSource = Me.obtenUsuarios(Me.cbPoblaciones.SelectedValue)
            Me.cbUsuario.Enabled = True
        Else
            Me.cbUsuario.Enabled = False
        End If
    End Sub

    Private Sub cbUsuario_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbUsuario.SelectedIndexChanged
        If Me.cbUsuario.SelectedValue > 0 Then
            Me.txtContrasena.Enabled = True
        Else
            Me.txtContrasena.Enabled = False
        End If
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Function obtenCiudades() As BindingSource
        Dim query As String = "SELECT 0 AS Clv_Id,'SELECCIONA' AS Ciudad UNION "
        query = query + "SELECT Clv_Id,Ciudad FROM Estadistica.dbo.RelBaseDatosCiudad "
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand(query, con)
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        Dim tabla As DataTable = New DataTable()
        Dim bs As BindingSource = New BindingSource()
        Try
            con.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            con.Close()
        End Try
        Return bs
    End Function

    Private Function obtenSucursales(ByVal clvId As Integer) As BindingSource
        Dim query As String = "llenaCombosDesgloseSucursales"
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand(query, con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@Clv_Id", clvId))
        com.Parameters.Add(New SqlParameter("@Message", SqlDbType.VarChar, 250))
        com.Parameters("@Message").Direction = ParameterDirection.Output
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        Dim tabla As DataTable = New DataTable()
        Dim bs As BindingSource = New BindingSource()
        Try
            con.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            con.Close()
        End Try
        Return bs
    End Function

    Private Function obtenUsuarios(ByVal clvId As Integer) As BindingSource
        Dim query As String = "llenaCombosDesgloseUsuarios"
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand(query, con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@Clv_Id", clvId))
        com.Parameters.Add(New SqlParameter("@Message", SqlDbType.VarChar, 250))
        com.Parameters("@Message").Direction = ParameterDirection.Output
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        Dim tabla As DataTable = New DataTable()
        Dim bs As BindingSource = New BindingSource()
        Try
            con.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            con.Close()
        End Try
        Return bs
    End Function

End Class