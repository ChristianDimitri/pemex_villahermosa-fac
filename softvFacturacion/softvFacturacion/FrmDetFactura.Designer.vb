﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDetFactura
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtTVExtras = New System.Windows.Forms.TextBox()
        Me.txtMesesAPagar = New System.Windows.Forms.TextBox()
        Me.txtPeriodoInical = New System.Windows.Forms.TextBox()
        Me.txtPeriodoFinal = New System.Windows.Forms.TextBox()
        Me.txtPuntosPagoOportuno = New System.Windows.Forms.TextBox()
        Me.txtPuntosPorAntiguedad = New System.Windows.Forms.TextBox()
        Me.txtPuntosPagoAdelantado = New System.Windows.Forms.TextBox()
        Me.txtPuntosCombo = New System.Windows.Forms.TextBox()
        Me.txtPuntosPPE = New System.Windows.Forms.TextBox()
        Me.txtImporteBonificar = New System.Windows.Forms.TextBox()
        Me.txtDiasBonificados = New System.Windows.Forms.TextBox()
        Me.txtImporteFinal = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblAparato = New System.Windows.Forms.Label()
        Me.txtAparato = New System.Windows.Forms.TextBox()
        Me.lbl_Ult_Anio = New System.Windows.Forms.Label()
        Me.txtUltAnioPagado = New System.Windows.Forms.TextBox()
        Me.lbl_Ult_Mes = New System.Windows.Forms.Label()
        Me.txtUltMesPagado = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblDiasBonificado = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtContrato = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtConcepto = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtTVExtras
        '
        Me.txtTVExtras.BackColor = System.Drawing.Color.White
        Me.txtTVExtras.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTVExtras.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtTVExtras.Location = New System.Drawing.Point(689, 47)
        Me.txtTVExtras.Multiline = True
        Me.txtTVExtras.Name = "txtTVExtras"
        Me.txtTVExtras.ReadOnly = True
        Me.txtTVExtras.Size = New System.Drawing.Size(156, 50)
        Me.txtTVExtras.TabIndex = 0
        Me.txtTVExtras.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtMesesAPagar
        '
        Me.txtMesesAPagar.BackColor = System.Drawing.Color.White
        Me.txtMesesAPagar.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMesesAPagar.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtMesesAPagar.Location = New System.Drawing.Point(26, 47)
        Me.txtMesesAPagar.Multiline = True
        Me.txtMesesAPagar.Name = "txtMesesAPagar"
        Me.txtMesesAPagar.ReadOnly = True
        Me.txtMesesAPagar.Size = New System.Drawing.Size(170, 50)
        Me.txtMesesAPagar.TabIndex = 1
        Me.txtMesesAPagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPeriodoInical
        '
        Me.txtPeriodoInical.BackColor = System.Drawing.Color.White
        Me.txtPeriodoInical.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeriodoInical.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtPeriodoInical.Location = New System.Drawing.Point(256, 47)
        Me.txtPeriodoInical.Multiline = True
        Me.txtPeriodoInical.Name = "txtPeriodoInical"
        Me.txtPeriodoInical.ReadOnly = True
        Me.txtPeriodoInical.Size = New System.Drawing.Size(166, 50)
        Me.txtPeriodoInical.TabIndex = 2
        Me.txtPeriodoInical.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPeriodoFinal
        '
        Me.txtPeriodoFinal.BackColor = System.Drawing.Color.White
        Me.txtPeriodoFinal.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeriodoFinal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtPeriodoFinal.Location = New System.Drawing.Point(483, 47)
        Me.txtPeriodoFinal.Multiline = True
        Me.txtPeriodoFinal.Name = "txtPeriodoFinal"
        Me.txtPeriodoFinal.ReadOnly = True
        Me.txtPeriodoFinal.Size = New System.Drawing.Size(164, 50)
        Me.txtPeriodoFinal.TabIndex = 3
        Me.txtPeriodoFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPuntosPagoOportuno
        '
        Me.txtPuntosPagoOportuno.BackColor = System.Drawing.Color.White
        Me.txtPuntosPagoOportuno.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPuntosPagoOportuno.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtPuntosPagoOportuno.Location = New System.Drawing.Point(24, 54)
        Me.txtPuntosPagoOportuno.Multiline = True
        Me.txtPuntosPagoOportuno.Name = "txtPuntosPagoOportuno"
        Me.txtPuntosPagoOportuno.ReadOnly = True
        Me.txtPuntosPagoOportuno.Size = New System.Drawing.Size(155, 51)
        Me.txtPuntosPagoOportuno.TabIndex = 4
        Me.txtPuntosPagoOportuno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPuntosPorAntiguedad
        '
        Me.txtPuntosPorAntiguedad.BackColor = System.Drawing.Color.White
        Me.txtPuntosPorAntiguedad.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPuntosPorAntiguedad.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtPuntosPorAntiguedad.Location = New System.Drawing.Point(254, 54)
        Me.txtPuntosPorAntiguedad.Multiline = True
        Me.txtPuntosPorAntiguedad.Name = "txtPuntosPorAntiguedad"
        Me.txtPuntosPorAntiguedad.ReadOnly = True
        Me.txtPuntosPorAntiguedad.Size = New System.Drawing.Size(173, 51)
        Me.txtPuntosPorAntiguedad.TabIndex = 5
        Me.txtPuntosPorAntiguedad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPuntosPagoAdelantado
        '
        Me.txtPuntosPagoAdelantado.BackColor = System.Drawing.Color.White
        Me.txtPuntosPagoAdelantado.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPuntosPagoAdelantado.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtPuntosPagoAdelantado.Location = New System.Drawing.Point(472, 54)
        Me.txtPuntosPagoAdelantado.Multiline = True
        Me.txtPuntosPagoAdelantado.Name = "txtPuntosPagoAdelantado"
        Me.txtPuntosPagoAdelantado.ReadOnly = True
        Me.txtPuntosPagoAdelantado.Size = New System.Drawing.Size(173, 51)
        Me.txtPuntosPagoAdelantado.TabIndex = 6
        Me.txtPuntosPagoAdelantado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPuntosCombo
        '
        Me.txtPuntosCombo.BackColor = System.Drawing.Color.White
        Me.txtPuntosCombo.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPuntosCombo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtPuntosCombo.Location = New System.Drawing.Point(687, 54)
        Me.txtPuntosCombo.Multiline = True
        Me.txtPuntosCombo.Name = "txtPuntosCombo"
        Me.txtPuntosCombo.ReadOnly = True
        Me.txtPuntosCombo.Size = New System.Drawing.Size(158, 51)
        Me.txtPuntosCombo.TabIndex = 7
        Me.txtPuntosCombo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPuntosPPE
        '
        Me.txtPuntosPPE.BackColor = System.Drawing.Color.White
        Me.txtPuntosPPE.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPuntosPPE.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtPuntosPPE.Location = New System.Drawing.Point(472, 142)
        Me.txtPuntosPPE.Multiline = True
        Me.txtPuntosPPE.Name = "txtPuntosPPE"
        Me.txtPuntosPPE.ReadOnly = True
        Me.txtPuntosPPE.Size = New System.Drawing.Size(172, 51)
        Me.txtPuntosPPE.TabIndex = 8
        Me.txtPuntosPPE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtImporteBonificar
        '
        Me.txtImporteBonificar.BackColor = System.Drawing.Color.White
        Me.txtImporteBonificar.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteBonificar.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtImporteBonificar.Location = New System.Drawing.Point(24, 142)
        Me.txtImporteBonificar.Multiline = True
        Me.txtImporteBonificar.Name = "txtImporteBonificar"
        Me.txtImporteBonificar.ReadOnly = True
        Me.txtImporteBonificar.Size = New System.Drawing.Size(155, 51)
        Me.txtImporteBonificar.TabIndex = 9
        Me.txtImporteBonificar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtDiasBonificados
        '
        Me.txtDiasBonificados.BackColor = System.Drawing.Color.White
        Me.txtDiasBonificados.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiasBonificados.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtDiasBonificados.Location = New System.Drawing.Point(254, 142)
        Me.txtDiasBonificados.Multiline = True
        Me.txtDiasBonificados.Name = "txtDiasBonificados"
        Me.txtDiasBonificados.ReadOnly = True
        Me.txtDiasBonificados.Size = New System.Drawing.Size(173, 51)
        Me.txtDiasBonificados.TabIndex = 10
        Me.txtDiasBonificados.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtImporteFinal
        '
        Me.txtImporteFinal.BackColor = System.Drawing.Color.White
        Me.txtImporteFinal.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteFinal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtImporteFinal.Location = New System.Drawing.Point(28, 611)
        Me.txtImporteFinal.Multiline = True
        Me.txtImporteFinal.Name = "txtImporteFinal"
        Me.txtImporteFinal.ReadOnly = True
        Me.txtImporteFinal.Size = New System.Drawing.Size(295, 56)
        Me.txtImporteFinal.TabIndex = 20
        Me.txtImporteFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblAparato)
        Me.GroupBox1.Controls.Add(Me.txtAparato)
        Me.GroupBox1.Controls.Add(Me.lbl_Ult_Anio)
        Me.GroupBox1.Controls.Add(Me.txtUltAnioPagado)
        Me.GroupBox1.Controls.Add(Me.lbl_Ult_Mes)
        Me.GroupBox1.Controls.Add(Me.txtUltMesPagado)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtTVExtras)
        Me.GroupBox1.Controls.Add(Me.txtMesesAPagar)
        Me.GroupBox1.Controls.Add(Me.txtPeriodoInical)
        Me.GroupBox1.Controls.Add(Me.txtPeriodoFinal)
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(26, 152)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(863, 202)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Pagos y Periodos"
        '
        'lblAparato
        '
        Me.lblAparato.AutoSize = True
        Me.lblAparato.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAparato.Location = New System.Drawing.Point(626, 111)
        Me.lblAparato.Name = "lblAparato"
        Me.lblAparato.Size = New System.Drawing.Size(57, 14)
        Me.lblAparato.TabIndex = 22
        Me.lblAparato.Text = "Aparato"
        '
        'txtAparato
        '
        Me.txtAparato.BackColor = System.Drawing.Color.White
        Me.txtAparato.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAparato.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtAparato.Location = New System.Drawing.Point(483, 130)
        Me.txtAparato.Multiline = True
        Me.txtAparato.Name = "txtAparato"
        Me.txtAparato.ReadOnly = True
        Me.txtAparato.Size = New System.Drawing.Size(362, 50)
        Me.txtAparato.TabIndex = 21
        Me.txtAparato.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbl_Ult_Anio
        '
        Me.lbl_Ult_Anio.AutoSize = True
        Me.lbl_Ult_Anio.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Ult_Anio.Location = New System.Drawing.Point(277, 111)
        Me.lbl_Ult_Anio.Name = "lbl_Ult_Anio"
        Me.lbl_Ult_Anio.Size = New System.Drawing.Size(125, 14)
        Me.lbl_Ult_Anio.TabIndex = 20
        Me.lbl_Ult_Anio.Text = "Ultimo Año Pagado"
        '
        'txtUltAnioPagado
        '
        Me.txtUltAnioPagado.BackColor = System.Drawing.Color.White
        Me.txtUltAnioPagado.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUltAnioPagado.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtUltAnioPagado.Location = New System.Drawing.Point(256, 130)
        Me.txtUltAnioPagado.Multiline = True
        Me.txtUltAnioPagado.Name = "txtUltAnioPagado"
        Me.txtUltAnioPagado.ReadOnly = True
        Me.txtUltAnioPagado.Size = New System.Drawing.Size(166, 50)
        Me.txtUltAnioPagado.TabIndex = 19
        Me.txtUltAnioPagado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbl_Ult_Mes
        '
        Me.lbl_Ult_Mes.AutoSize = True
        Me.lbl_Ult_Mes.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Ult_Mes.Location = New System.Drawing.Point(48, 111)
        Me.lbl_Ult_Mes.Name = "lbl_Ult_Mes"
        Me.lbl_Ult_Mes.Size = New System.Drawing.Size(124, 14)
        Me.lbl_Ult_Mes.TabIndex = 18
        Me.lbl_Ult_Mes.Text = "Ultimo Mes Pagado"
        '
        'txtUltMesPagado
        '
        Me.txtUltMesPagado.BackColor = System.Drawing.Color.White
        Me.txtUltMesPagado.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUltMesPagado.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtUltMesPagado.Location = New System.Drawing.Point(28, 130)
        Me.txtUltMesPagado.Multiline = True
        Me.txtUltMesPagado.Name = "txtUltMesPagado"
        Me.txtUltMesPagado.ReadOnly = True
        Me.txtUltMesPagado.Size = New System.Drawing.Size(168, 50)
        Me.txtUltMesPagado.TabIndex = 17
        Me.txtUltMesPagado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(497, 28)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(135, 14)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Periodo Pagado Final"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(264, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(140, 14)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Periodo Pagado Incial"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(68, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 14)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Meses a Pagar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(717, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 14)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "T.V.s Extras"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblDiasBonificado)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.txtPuntosPagoOportuno)
        Me.GroupBox2.Controls.Add(Me.txtPuntosPorAntiguedad)
        Me.GroupBox2.Controls.Add(Me.txtDiasBonificados)
        Me.GroupBox2.Controls.Add(Me.txtPuntosPagoAdelantado)
        Me.GroupBox2.Controls.Add(Me.txtPuntosCombo)
        Me.GroupBox2.Controls.Add(Me.txtImporteBonificar)
        Me.GroupBox2.Controls.Add(Me.txtPuntosPPE)
        Me.GroupBox2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(28, 369)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(863, 209)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Puntos Aplicados y Bonificaciones"
        '
        'lblDiasBonificado
        '
        Me.lblDiasBonificado.AutoSize = True
        Me.lblDiasBonificado.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDiasBonificado.Location = New System.Drawing.Point(300, 123)
        Me.lblDiasBonificado.Name = "lblDiasBonificado"
        Me.lblDiasBonificado.Size = New System.Drawing.Size(100, 14)
        Me.lblDiasBonificado.TabIndex = 20
        Me.lblDiasBonificado.Text = "Dias bonificado"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(35, 123)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(125, 14)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Importe a Bonificar"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(515, 123)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(78, 14)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Puntos PPE"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(716, 35)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(98, 14)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Puntos Combo"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(469, 35)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(186, 14)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Puntos por Pago Adelantado"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(260, 35)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(152, 14)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Puntos por Antigüedad"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(21, 35)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(175, 14)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Puntos por Pago Oportuno"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Trebuchet MS", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(89, 581)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(153, 27)
        Me.Label11.TabIndex = 15
        Me.Label11.Text = "Importe Total:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(294, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(320, 23)
        Me.Label12.TabIndex = 17
        Me.Label12.Text = "Detalles del Concepto a Facturar"
        '
        'txtContrato
        '
        Me.txtContrato.BackColor = System.Drawing.Color.White
        Me.txtContrato.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContrato.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtContrato.Location = New System.Drawing.Point(80, 38)
        Me.txtContrato.Multiline = True
        Me.txtContrato.Name = "txtContrato"
        Me.txtContrato.ReadOnly = True
        Me.txtContrato.Size = New System.Drawing.Size(145, 45)
        Me.txtContrato.TabIndex = 1
        Me.txtContrato.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.txtConcepto)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.txtContrato)
        Me.GroupBox3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(28, 37)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(861, 100)
        Me.GroupBox3.TabIndex = 18
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Generales"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(495, 19)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(131, 14)
        Me.Label14.TabIndex = 16
        Me.Label14.Text = "Concepto a Facturar"
        '
        'txtConcepto
        '
        Me.txtConcepto.BackColor = System.Drawing.Color.White
        Me.txtConcepto.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConcepto.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtConcepto.Location = New System.Drawing.Point(282, 38)
        Me.txtConcepto.Multiline = True
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.ReadOnly = True
        Me.txtConcepto.Size = New System.Drawing.Size(547, 45)
        Me.txtConcepto.TabIndex = 15
        Me.txtConcepto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(108, 19)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(63, 14)
        Me.Label13.TabIndex = 14
        Me.Label13.Text = "Contrato"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(715, 594)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(174, 59)
        Me.Button1.TabIndex = 19
        Me.Button1.Text = "SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FrmDetFactura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(918, 679)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtImporteFinal)
        Me.MaximizeBox = False
        Me.Name = "FrmDetFactura"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Detalles de Factura"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtTVExtras As System.Windows.Forms.TextBox
    Friend WithEvents txtMesesAPagar As System.Windows.Forms.TextBox
    Friend WithEvents txtPeriodoInical As System.Windows.Forms.TextBox
    Friend WithEvents txtPeriodoFinal As System.Windows.Forms.TextBox
    Friend WithEvents txtPuntosPagoOportuno As System.Windows.Forms.TextBox
    Friend WithEvents txtPuntosPorAntiguedad As System.Windows.Forms.TextBox
    Friend WithEvents txtPuntosPagoAdelantado As System.Windows.Forms.TextBox
    Friend WithEvents txtPuntosCombo As System.Windows.Forms.TextBox
    Friend WithEvents txtPuntosPPE As System.Windows.Forms.TextBox
    Friend WithEvents txtImporteBonificar As System.Windows.Forms.TextBox
    Friend WithEvents txtDiasBonificados As System.Windows.Forms.TextBox
    Friend WithEvents txtImporteFinal As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtContrato As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtConcepto As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lbl_Ult_Anio As System.Windows.Forms.Label
    Friend WithEvents txtUltAnioPagado As System.Windows.Forms.TextBox
    Friend WithEvents lbl_Ult_Mes As System.Windows.Forms.Label
    Friend WithEvents txtUltMesPagado As System.Windows.Forms.TextBox
    Friend WithEvents lblDiasBonificado As System.Windows.Forms.Label
    Friend WithEvents lblAparato As System.Windows.Forms.Label
    Friend WithEvents txtAparato As System.Windows.Forms.TextBox
End Class
