Imports System.Data.SqlClient
Public Class FrmDevoluciones

    Dim monto As Double
    Dim factura As Integer
    Dim factura_inicila As Integer
    Dim clave_txt As String
    'Variables biracora

    Private suc_aplica As String = Nothing
    Private caja As String = Nothing
    Private cajero As String = Nothing
    Private fecha_caducidad As String = Nothing
    Private Locactura As String = Nothing
    Private Observa As String = Nothing
    Private Locmonto As String = Nothing
    ' Private Locsaldo As String = nothing
    Private conGlo As New SqlConnection(MiConexion)
    Private bndDevolucionValida As Boolean
    Private ContratoObtenido As Integer

    Private tbl_DetCliente As DataTable
    Private tbl_DetFactura As DataTable
    Private tbl_DetConceptosFactura As DataTable
    Private tbl_DetConceptoDevolcion As DataTable


    Private Sub damedatosbitacora()
        Try
            suc_aplica = Me.ComboBox7.Text
            caja = Me.ComboBox8.Text
            cajero = Me.ComboBox5.Text
            fecha_caducidad = Me.Fecha_CaducidadDateTimePicker.Text
            Locactura = Me.ComboBox3.Text
            Observa = Me.ObservacionesTextBox.Text
            Locmonto = Me.MontoTextBox.Text
            'Locsaldo = Me.TextBox1.Text
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora()
        Try
            If OPCION = "M" Then
                'suc_aplica = Me.ComboBox7.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Sucural Aplica" + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), suc_aplica, Me.ComboBox7.Text, SubCiudad)
                'caja = Me.ComboBox8.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Caja Aplica" + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), caja, Me.ComboBox8.Text, SubCiudad)
                'cajero = Me.ComboBox5.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Cajero Aplica" + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), cajero, Me.ComboBox5.Text, SubCiudad)
                'fecha_caducidad = Me.Fecha_CaducidadDateTimePicker.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, Me.Fecha_CaducidadDateTimePicker.Name + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), fecha_caducidad, Me.Fecha_CaducidadDateTimePicker.Text, SubCiudad)
                'Locactura = Me.ComboBox3.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, "Factura Genera" + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), Locactura, Me.ComboBox3.Text, SubCiudad)
                'Observa = Me.ObservacionesTextBox.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, Me.ObservacionesTextBox.Name + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), Observa, Me.ObservacionesTextBox.Text, SubCiudad)
                'Locmonto = Me.MontoTextBox.Text
                bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, Me.MontoTextBox.Name + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), Locmonto, Me.MontoTextBox.Text, SubCiudad)
                ''Locsaldo = Me.TextBox1.Text
                'bitsist(GloUsuario, CLng(Me.ContratoTextBox.Text), GloSistema, Me.Name, Me.MontoTextBox.Name + ", Nota: " + CStr(Me.Clv_NotadeCreditoTextBox.Text), Locsaldo, Me.TextBox1.Text, SubCiudad)
                damedatosbitacora()
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub busca()
       
    End Sub
    Private Sub FrmNotasdeCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)

        bndDevolucionValida = False
        btnGenerarDevolucion.Enabled = False

        If IdSistema = "SA" Then

            Me.TextBox1.Visible = False
            Me.CMBLabel1.Visible = False
            Me.Text = "Cat�logo de Devoluciones de Dep�sito"
            Me.Label2.Text = "Pagos Realizados con la Devoluci�n:"
            Me.REDLabel3.Text = "Devoluci�n en Efectivo"
            Me.Label4.Text = "Sucursal donde se Aplica la Devoluci�n:"
            Me.Usuario_AutorizoLabel.Text = "Usuario que realiza la Devoluci�n:"
            Me.Clv_sucursalLabel.Text = "Sucursal que Realiza la Devoluci�n:"
            Me.CMBClv_NotadeCreditoLabel.Text = "Devoluci�n:"
            Me.Button4.Visible = False

            ComboBox7.SelectedValue = GloSucursal
            ComboBox8.SelectedValue = 0
            ComboBox8.SelectedValue = GloCaja
            ComboBox5.SelectedValue = 0
            ComboBox5.SelectedValue = GloUsuario
            ComboBox7.Enabled = False
            ComboBox8.Enabled = False
            ComboBox5.Enabled = False

            'Cargar la devoluci�n a generar si va por Generaci�n
            If Op_Devolucion = "N" Then


                CargaGeneralesDevolucion()

                Me.dgvConceptosFacturaInicial.Columns(0).Width = 380
                Me.dgvConceptosFacturaInicial.Columns(1).Width = 80

            End If

        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.ComboBox3.SelectedValue) = True Then
            borra_Predetalle(Me.ComboBox3.SelectedValue)
        End If
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Consulta_NotaCreditoBindingSource.CancelEdit()
        Me.Close()
    End Sub

    ' ContratoTextBox_KeyPress
    Private Sub ContratoTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ContratoTextBox.KeyPress

        'Evitamos Letras en el campo de Contrato
        If e.KeyChar.IsDigit(e.KeyChar) Then

            e.Handled = False

        ElseIf e.KeyChar.IsControl(e.KeyChar) Then

            e.Handled = False

        Else
            e.Handled = True
        End If

        If IsNumeric(Me.ContratoTextBox.Text) Then
            If e.KeyChar = Chr(13) And CType(Me.ContratoTextBox.Text, Integer) > 0 Then

                'Para cuando sea el Enter
                Try
                    Dim Contrato As Integer = 0
                    Contrato = CType(Me.ContratoTextBox.Text, Integer)

                    'Nuevo c�digo para Dep�sito de Aparatos
                    If IsNumeric(Me.ContratoTextBox.Text) Then
                        If Me.ContratoTextBox.Text > 0 Then

                            'Buscamos si es que el Contrato Cuenta con un Factura por Cobro de dep�sito
                            If DameServiciosAplicanDevolucionPorCliente(Me.ContratoTextBox.Text) = True Then

                                'Cargamos los datos del Cliente
                                Dim conn As New SqlConnection(MiConexion)
                                conn.Open()

                                Me.BUSCLIPORCONTRATOTableAdapter.Connection = conn
                                Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.DataSetLydia.BUSCLIPORCONTRATO, Me.ContratoTextBox.Text, "", "", "", "", 0, 0)

                                'Cargar los servicios del Cliente
                                CREAARBOL()

                                'Cerrar y limpiar conexiones
                                conn.Close()
                                conn.Dispose()

                                'Habilitamos el Panel para agregar el servicio por el cual aplicar� la Devoluci�n del Aparato
                                Me.Panel5.Enabled = True
                                Me.btnGenerarDevolucion.Enabled = True

                                Me.ContratoTextBox.Text = Contrato.ToString

                            Else
                                MsgBox("El Cliente no cuenta con ninguna Factura (activa) por concepto de: 'Bono de Aparato'. No se puede generar una devoluci�n.", MsgBoxStyle.Exclamation, "ESTE CLIENTE NO APLICA PARA DEVOLUCI�N DE EFECTIVO")
                                Me.btnGenerarDevolucion.Enabled = False

                                'Asignamos los datos a la Interfaz
                                LimpiaInterfaz()

                            End If

                        End If
                    End If

                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            End If
        End If

    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged

        If Me.ContratoTextBox.Text = "" Then

            LimpiaInterfaz()

            'Asignamos los datos a la Interfaz
            Me.txtTipoServicioPrincipal.Text = ""
            Me.txtServicioDeposito.Text = ""
            Me.TextBox9.Text = ""

            Me.NOMBRELabel1.Text = ""
            Me.CALLELabel1.Text = ""
            Me.NUMEROLabel1.Text = ""
            Me.COLONIALabel1.Text = ""

            'Detalles de la Factura Inicial
            Me.txtFactura.Text = ""
            Me.txtSucursal.Text = ""
            Me.txtMontoDelDeposito.Text = ""
            Me.txtFechaFactura.Text = ""

            Me.dgvConceptosFacturaInicial.DataSource = ""

            Me.TreeView1.Nodes.Clear()

            bndDevolucionValida = False

        End If


    End Sub
    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)

            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
         
            CON.Open()
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.DataSetLydia.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.DataSetLydia.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            CON.Close()
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow

            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.DataSetLydia.dameSerDELCli.Rows


                X = 0
             
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next

            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        FrmSelCliente.Show()
    End Sub
    Private Sub Muestra_Predetalle(ByVal factura As Integer)
       
    End Sub
    Private Sub borra_Predetalle(ByVal factura1 As Integer)
      
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarDevolucion.Click

        Dim RESP = MsgBox("�Deseas generar la Devoluci�n del Bono de Barant�a ahora?", MsgBoxStyle.YesNo)

        If RESP = MsgBoxResult.Yes Then

            GeneraDevolucionBonoAparato()
            LimpiaInterfaz()

            Me.Close()
            Me.Dispose()

        Else
            MsgBox("No se ha generado a�n la Devoluci�n.", MsgBoxStyle.Information)
        End If

    End Sub
    Private Sub borra_detalle()
      
    End Sub
    Private Sub modifica_PreDetalle(ByVal factura2 As Integer, ByVal value As Integer)
     
    End Sub
  

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged

    End Sub

    Private Sub Detalle_NotasdeCreditoDataGridView_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Detalle_NotasdeCreditoDataGridView.CellClick
      
    End Sub

    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox4.TextChanged
        factura = Me.TextBox4.Text
    End Sub
    Private Sub ComboBox7_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
      
    End Sub

    Private Sub ComboBox8_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
     
    End Sub


    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged
     
    End Sub


    Private Sub TextBox6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox6.TextChanged
       
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Panel1.Enabled = True
        Me.Panel3.Enabled = True
        Me.Button5.Enabled = True
        Me.Panel3.Visible = True

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Panel1.Enabled = False
        Me.Panel3.Visible = False
    End Sub

    Private Sub ComboBox9_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox9.SelectedIndexChanged
        Dim conFact As New SqlConnection(MiConexion)

        If IsNumeric(Me.ComboBox9.SelectedValue) = True Then
            conFact.Open()
            Me.MuestraServicios_por_TipoTableAdapter.Connection = conFact
            Me.MuestraServicios_por_TipoTableAdapter.Fill(Me.DataSetLydia.MuestraServicios_por_Tipo, Me.ComboBox9.SelectedValue)
            conFact.Close()
        End If
    End Sub

    Private Sub ComboBox10_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox10.SelectedIndexChanged
        If IsNumeric(Me.ComboBox10.SelectedValue) = True Then
            Dame_Datos_Servicio(Me.ComboBox10.SelectedValue)
        End If
    End Sub
    Private Sub Dame_Datos_Servicio(ByVal clv_serv As Integer)
       

    End Sub
    Private Sub busca_detalleNota()
        Dim ConFact4 As New SqlConnection(MiConexion)
        Dim Cmd4 As New SqlCommand
        ConFact4.Open()
        Me.MuestraDetalleNota_por_ConceptoTableAdapter.Connection = ConFact4
        Me.MuestraDetalleNota_por_ConceptoTableAdapter.Fill(Me.DataSetLydia.MuestraDetalleNota_por_Concepto, Me.Clv_NotadeCreditoTextBox.Text)
        ConFact4.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
       
    End Sub
    Private Sub Agregar_Conceptos(ByVal opc As Integer)
     

    End Sub
    Private Sub Borra_conceptos()
      
    End Sub
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
      
    End Sub

    Private Sub TextBox9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox9.KeyPress
      
    End Sub

    'Buscamos y obtenemos los cobros por dep�sito de aparato que tenga el cliente
    Private Function DameServiciosAplicanDevolucionPorCliente(ByVal Contrato As Integer) As Boolean

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "DameServiciosAplicanDevolucionPorCliente"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 8

                Dim prmContrato As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prmContrato.Direction = ParameterDirection.Output
                prmContrato.Value = Contrato
                .Parameters.Add(prmContrato)

                Dim prmError As New SqlParameter("@Error", SqlDbType.Bit)
                prmError.Direction = ParameterDirection.Output
                prmError.Value = False
                .Parameters.Add(prmError)

                Dim prmMsgError As New SqlParameter("@MsgError", SqlDbType.VarChar, 250)
                prmMsgError.Direction = ParameterDirection.Output
                prmMsgError.Value = 0
                .Parameters.Add(prmMsgError)

                Dim prmDescripcionServPrinicpal As New SqlParameter("@DescripcionServPrinicpal", SqlDbType.VarChar, 250)
                prmDescripcionServPrinicpal.Direction = ParameterDirection.Output
                prmDescripcionServPrinicpal.Value = 0
                .Parameters.Add(prmDescripcionServPrinicpal)

                Dim prmDescripcionConcepto As New SqlParameter("@DescripcionConcepto", SqlDbType.VarChar, 250)
                prmDescripcionConcepto.Direction = ParameterDirection.Output
                prmDescripcionConcepto.Value = 0
                .Parameters.Add(prmDescripcionConcepto)

                Dim prmCosto As New SqlParameter("@Costo", SqlDbType.Money)
                prmCosto.Direction = ParameterDirection.Output
                prmCosto.Value = 0
                .Parameters.Add(prmCosto)

                Dim prmFactura As New SqlParameter("@Factura", SqlDbType.VarChar, 250)
                prmFactura.Direction = ParameterDirection.Output
                prmFactura.Value = ""
                .Parameters.Add(prmFactura)

                Dim prmNombreSucursal As New SqlParameter("@NombreSucursal", SqlDbType.VarChar, 250)
                prmNombreSucursal.Direction = ParameterDirection.Output
                prmNombreSucursal.Value = ""
                .Parameters.Add(prmNombreSucursal)

                Dim prmFecha As New SqlParameter("@FechaStr", SqlDbType.VarChar, 250)
                prmFecha.Direction = ParameterDirection.Output
                prmFecha.Value = ""
                .Parameters.Add(prmFecha)

                Dim prmMontoIncial As New SqlParameter("@MontoIncial", SqlDbType.Money)
                prmMontoIncial.Direction = ParameterDirection.Output
                prmMontoIncial.Value = 0
                .Parameters.Add(prmMontoIncial)

                Dim prmClvFactura As New SqlParameter("@ClvFactura", SqlDbType.Money)
                prmClvFactura.Direction = ParameterDirection.Output
                prmClvFactura.Value = 0
                .Parameters.Add(prmClvFactura)


                Dim prmPorId As New SqlParameter("@PorId", SqlDbType.Bit)
                prmPorId.Value = 1
                .Parameters.Add(prmPorId)

                Dim prmIdDevolucion As New SqlParameter("@IdDevolucion", SqlDbType.BigInt)
                prmIdDevolucion.Value = Id_Devolucion
                .Parameters.Add(prmIdDevolucion)


                Dim Result As Integer = .ExecuteNonQuery()

                ContratoObtenido = prmContrato.Value
                Me.ContratoTextBox.Text = ContratoObtenido

                If prmError.Value = True Then
                    'El Cliente no cuenta con un Dep�sito de Aparato

                    'Bloqueamos el la interfaz para que no pueda agregar servicios/conceptos a la lista
                    Me.Panel5.Enabled = False

                    'Lipiamos la Interfaz
                    LimpiaInterfaz()

                    Return False

                Else

                    'Asignamos los datos a la Interfaz
                    Me.txtTipoServicioPrincipal.Text = prmDescripcionServPrinicpal.Value.ToString
                    Me.txtServicioDeposito.Text = prmDescripcionConcepto.Value.ToString
                    Me.TextBox9.Text = prmCosto.Value.ToString

                    'Detalles de la Factura Inicial
                    Me.txtFactura.Text = prmFactura.Value.ToString
                    Me.txtSucursal.Text = prmNombreSucursal.Value.ToString
                    Me.txtMontoDelDeposito.Text = prmMontoIncial.Value.ToString
                    Me.txtFechaFactura.Text = prmFecha.Value.ToString

                    'Cargamos los canceptos facturados con la factura inicial
                    DameConceptosFacturadosPorDeposito(prmClvFactura.Value)

                    Me.dgvConceptosFacturaInicial.Columns(0).Width = 380
                    Me.dgvConceptosFacturaInicial.Columns(1).Width = 80

                    Return True

                End If

            End With
            CON80.Dispose()
            CON80.Close()
        Catch ex As Exception

            CON80.Dispose()
            CON80.Close()
            MsgBox("Error al verificar si el cliente cuenta con un Cobro por Deop�sito de Aparato.", MsgBoxStyle.Exclamation, ":: Advertencia ::")
            MsgBox(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Porcedimeinto: DameServiciosAplicanDevolucionPorCliente, Funci�n: DameServiciosAplicanDevolucionPorCliente()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try

    End Function

    'Cargamos los Conceptos facturados con la misma factura que se pago el 'DEP�SITO DE APARATOS'
    Public Sub DameConceptosFacturadosPorDeposito(ByVal Factura As Integer)

        Try

            Dim conn As New SqlConnection(MiConexion)
            conn.Open()
            Dim comando As New SqlClient.SqlCommand("DameConceptosFacturadosConDeposito", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Dim Dataset As New DataSet
            Dim Bs As New BindingSource

            Adaptador.SelectCommand.Parameters.Add("@ClvFactura", SqlDbType.BigInt).Value = Factura

            Adaptador.Fill(Dataset, "resultado_ConceptosFactura")
            Bs.DataSource = Dataset.Tables("resultado_ConceptosFactura")

            Me.dgvConceptosFacturaInicial.DataSource = Bs
            bndDevolucionValida = True

            conn.Dispose()
            conn.Close()

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de enlazar los Periodos de Cobro, los resultados del Reporte ser�n inconcistentes.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Porcedimeinto: DameConceptosFacturadosConDeposito, Funci�n: DameConceptosFacturadosPorDeposito(), Factura: "
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try

    End Sub
  
    Private Sub btnLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click

        LimpiaInterfaz()

    End Sub

    'Limpiamos los controles de la interfaz
    Public Sub LimpiaInterfaz()

        'Asignamos los datos a la Interfaz
        Me.txtTipoServicioPrincipal.Text = ""
        Me.txtServicioDeposito.Text = ""
        Me.TextBox9.Text = ""

        Me.NOMBRELabel1.Text = ""
        Me.CALLELabel1.Text = ""
        Me.NUMEROLabel1.Text = ""
        Me.COLONIALabel1.Text = ""

        'Detalles de la Factura Inicial
        Me.txtFactura.Text = ""
        Me.txtSucursal.Text = ""
        Me.txtMontoDelDeposito.Text = ""
        Me.txtFechaFactura.Text = ""

        Me.dgvConceptosFacturaInicial.DataSource = ""

        Me.TreeView1.Nodes.Clear()

        bndDevolucionValida = False
        ObservacionesTextBox.Text = ""

        Me.ContratoTextBox.Text = ""

    End Sub

    Public Sub CargaDevolucion()

        'Para cuando sea el Enter
        Try
            Dim Contrato As Integer = 0
            'Contrato = CType(Me.ContratoTextBox.Text, Integer)

            'Nuevo c�digo para Dep�sito de Aparatos
            If IsNumeric(Contrato) Then
                'If Me.ContratoTextBox.Text > 0 Then

                'Buscamos si es que el Contrato Cuenta con un Factura por Cobro de dep�sito
                If DameServiciosAplicanDevolucionPorCliente(Contrato) = True Then

                    'Cargamos los datos del Cliente
                    Dim conn As New SqlConnection(MiConexion)
                    conn.Open()

                    Me.BUSCLIPORCONTRATOTableAdapter.Connection = conn
                    Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.DataSetLydia.BUSCLIPORCONTRATO, ContratoObtenido, "", "", "", "", 0, 0)

                    Me.ContratoTextBox.Text = ContratoObtenido
                    Me.Clv_NotadeCreditoTextBox.Text = Id_Devolucion


                    'Cargar los servicios del Cliente
                    CREAARBOL()

                    'Cerrar y limpiar conexiones
                    conn.Close()
                    conn.Dispose()

                    'Habilitamos el Panel para agregar el servicio por el cual aplicar� la Devoluci�n del Aparato
                    Me.Panel5.Enabled = True
                    Me.btnGenerarDevolucion.Enabled = True

                    Me.ContratoTextBox.Text = Contrato.ToString

                Else
                    MsgBox("El Cliente no cuenta con ninguna Factura (activa) por concepto de: 'Bono de Aparato'. No se puede generar una devoluci�n.", MsgBoxStyle.Exclamation, "ESTE CLIENTE NO APLICA PARA DEVOLUCI�N DE EFECTIVO")
                    Me.btnGenerarDevolucion.Enabled = False

                    'Asignamos los datos a la Interfaz
                    LimpiaInterfaz()

                End If

            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    'Procedimiento para Generar la Devoluci�n
    Public Sub GeneraDevolucionBonoAparato()

        Dim Con As New SqlConnection(MiConexion)
        Dim Comando As New SqlCommand()

        Try

            Con.Open()
            With Comando

                ' Comando
                .CommandText = "GeneraDevolucionPorBonoAparato"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = Con

                'Par�metros
                Dim prmIdDevolucion As New SqlParameter("@idDevolucion", SqlDbType.BigInt)
                Dim prmCaja As New SqlParameter("@Caja", SqlDbType.Int)
                Dim prmSucursal As New SqlParameter("@Sucursal", SqlDbType.Int)
                Dim prmObs As New SqlParameter("@Obs", SqlDbType.NText)
                Dim prmError As New SqlParameter("@Error", SqlDbType.Int)
                Dim prmMsgError As New SqlParameter("@MsgError", SqlDbType.VarChar, 250)


                With prmIdDevolucion
                    .Value = Id_Devolucion
                    .Direction = ParameterDirection.Input
                End With

                With prmCaja
                    .Value = GloCaja
                    .Direction = ParameterDirection.Input
                End With

                With prmSucursal
                    .Value = GloSucursal
                    .Direction = ParameterDirection.Input
                End With

                With prmObs
                    .Value = Me.ObservacionesTextBox.Text
                    .Direction = ParameterDirection.Input
                End With

                With prmError
                    .Value = 0
                    .Direction = ParameterDirection.Output
                End With

                With prmMsgError
                    .Value = ""
                    .Direction = ParameterDirection.Output
                End With

                'Agregamos los Par�metros al Comando
                .Parameters.Add(prmIdDevolucion)
                .Parameters.Add(prmCaja)
                .Parameters.Add(prmSucursal)
                .Parameters.Add(prmObs)
                .Parameters.Add(prmError)
                .Parameters.Add(prmMsgError)

                Dim Result As Integer = .ExecuteNonQuery()

                If prmError.Value = 0 Then
                    MsgBox("Se gener� la Devoluci�n correctamente.", MsgBoxStyle.Information, "DEVOLUCI�N GENERADA CORRECTAMENTE")

                    'Mandamos Imprimir el reporte
                    BrwBonosDeAparatos.CargaReporteDevolucion(Id_Devolucion, 0, "Original")

                Else

                    MsgBox(prmMsgError.Value.ToString, MsgBoxStyle.Exclamation, "DEVOLUCI�N NO GENERADA")

                End If

            End With

        Catch ex As Exception

            Con.Dispose()
            Con.Close()
            MsgBox("Error al Generar la Devoluci�n del Bono de Garant�a.", MsgBoxStyle.Exclamation, ":: Advertencia ::")
            MsgBox(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Porcedimeinto: GeneraDevolucionPorBonoAparato, Funci�n: GeneraDevolucionPorBonoAparato()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        Finally

            Con.Dispose()
            Con.Close()

        End Try

    End Sub

    Public Sub CargaGeneralesDevolucion()


        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet

        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("CargaGeneralesDevolucion", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Adaptador.SelectCommand.Parameters.Add("@ID_DEVOLUCION", SqlDbType.Int).Value = Id_Devolucion
            Adaptador.SelectCommand.Parameters.Add("@CONTRATO", SqlDbType.BigInt).Value = Contrato_Devolucion

            Dim Bs As New BindingSource
            Adaptador.Fill(Dataset)

            Dataset.Tables(0).TableName = "tbl_Cliente"
            Dataset.Tables(1).TableName = "tbl_Factura"
            Dataset.Tables(2).TableName = "tbl_ConceptosFactura"
            Dataset.Tables(3).TableName = "tbl_DetConceptoDevolcion"

            tbl_DetCliente = Dataset.Tables("tbl_Cliente")
            tbl_DetFactura = Dataset.Tables("tbl_Factura")
            tbl_DetConceptosFactura = Dataset.Tables("tbl_ConceptosFactura")
            tbl_DetConceptoDevolcion = Dataset.Tables("tbl_DetConceptoDevolcion")

            Me.dgvConceptosFacturaInicial.DataSource = tbl_DetConceptosFactura

            Me.ContratoTextBox.Text = Contrato_Devolucion
            Me.Clv_NotadeCreditoTextBox.Text = Id_Devolucion

            'DATOS DE LA FACTURA
            Me.txtFactura.Text = tbl_DetFactura.Rows(0).Item(0).ToString
            Me.txtFechaFactura.Text = tbl_DetFactura.Rows(0).Item(1).ToString
            Me.txtMontoDelDeposito.Text = tbl_DetFactura.Rows(0).Item(2).ToString
            Me.txtSucursal.Text = tbl_DetFactura.Rows(0).Item(3).ToString

            'DATOS DEL SERVICIO DE DEVOLCUCI�N
            Me.txtTipoServicioPrincipal.Text = tbl_DetConceptoDevolcion.Rows(0).Item(0).ToString
            Me.txtServicioDeposito.Text = tbl_DetConceptoDevolcion.Rows(0).Item(1).ToString
            Me.TextBox9.Text = tbl_DetConceptoDevolcion.Rows(0).Item(2).ToString

            'DATOS DEL CLIENTE
            Me.NOMBRELabel1.Text = tbl_DetCliente.Rows(0).Item(0).ToString
            Me.CALLELabel1.Text = tbl_DetCliente.Rows(0).Item(1).ToString
            Me.NUMEROLabel1.Text = tbl_DetCliente.Rows(0).Item(2).ToString


            conn.Close()
            conn.Dispose()

            'Id_Devolucion = 0
            'Contrato_Devolucion = 0

            Me.btnGenerarDevolucion.Enabled = True

            Me.CREAARBOL()

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de enlazar los Datos Principales.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            Id_Devolucion = 0
            Contrato_Devolucion = 0

            Exit Sub

        Finally

            conn.Close()
            conn.Dispose()

        End Try

    End Sub

End Class