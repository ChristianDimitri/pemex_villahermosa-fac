﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmImprimeEstadosDeCuentaPorPeriodo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtContratoInicial = New System.Windows.Forms.TextBox()
        Me.txtContratoFinal = New System.Windows.Forms.TextBox()
        Me.pgProgresoDeImpresion = New System.Windows.Forms.ProgressBar()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chbxImprimirTodosLosContratos = New System.Windows.Forms.CheckBox()
        Me.lblPeriodoDeCobroSeleccionado = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvPeriodosDeCobro = New System.Windows.Forms.DataGridView()
        Me.btnCargarPeriodo = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblStatusDeImpresion = New System.Windows.Forms.Label()
        Me.lblNumeroDeContratoImprimiendo = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvPeriodosDeCobro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtContratoInicial
        '
        Me.txtContratoInicial.Font = New System.Drawing.Font("Trebuchet MS", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContratoInicial.Location = New System.Drawing.Point(425, 34)
        Me.txtContratoInicial.Name = "txtContratoInicial"
        Me.txtContratoInicial.Size = New System.Drawing.Size(163, 35)
        Me.txtContratoInicial.TabIndex = 0
        Me.txtContratoInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtContratoFinal
        '
        Me.txtContratoFinal.Font = New System.Drawing.Font("Trebuchet MS", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContratoFinal.Location = New System.Drawing.Point(650, 34)
        Me.txtContratoFinal.Name = "txtContratoFinal"
        Me.txtContratoFinal.Size = New System.Drawing.Size(163, 35)
        Me.txtContratoFinal.TabIndex = 1
        Me.txtContratoFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'pgProgresoDeImpresion
        '
        Me.pgProgresoDeImpresion.Location = New System.Drawing.Point(32, 65)
        Me.pgProgresoDeImpresion.Name = "pgProgresoDeImpresion"
        Me.pgProgresoDeImpresion.Size = New System.Drawing.Size(752, 55)
        Me.pgProgresoDeImpresion.TabIndex = 3
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.Color.Black
        Me.btnCancelar.Location = New System.Drawing.Point(338, 417)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(150, 51)
        Me.btnCancelar.TabIndex = 54
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = False
        Me.btnCancelar.Visible = False
        '
        'btnImprimir
        '
        Me.btnImprimir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.ForeColor = System.Drawing.Color.Black
        Me.btnImprimir.Location = New System.Drawing.Point(654, 417)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(182, 51)
        Me.btnImprimir.TabIndex = 53
        Me.btnImprimir.Text = "Comenzar Impresión"
        Me.btnImprimir.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chbxImprimirTodosLosContratos)
        Me.GroupBox1.Controls.Add(Me.lblPeriodoDeCobroSeleccionado)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.dgvPeriodosDeCobro)
        Me.GroupBox1.Controls.Add(Me.txtContratoFinal)
        Me.GroupBox1.Controls.Add(Me.txtContratoInicial)
        Me.GroupBox1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(23, 54)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(828, 357)
        Me.GroupBox1.TabIndex = 55
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Impresión de Estados de Cuenta por Periodo"
        '
        'chbxImprimirTodosLosContratos
        '
        Me.chbxImprimirTodosLosContratos.AutoSize = True
        Me.chbxImprimirTodosLosContratos.Location = New System.Drawing.Point(419, 75)
        Me.chbxImprimirTodosLosContratos.Name = "chbxImprimirTodosLosContratos"
        Me.chbxImprimirTodosLosContratos.Size = New System.Drawing.Size(394, 22)
        Me.chbxImprimirTodosLosContratos.TabIndex = 7
        Me.chbxImprimirTodosLosContratos.Text = "Imprimir todos los Estados de Cuenta del Periodo Seleccionado"
        Me.chbxImprimirTodosLosContratos.UseVisualStyleBackColor = True
        '
        'lblPeriodoDeCobroSeleccionado
        '
        Me.lblPeriodoDeCobroSeleccionado.AutoSize = True
        Me.lblPeriodoDeCobroSeleccionado.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodoDeCobroSeleccionado.ForeColor = System.Drawing.Color.Black
        Me.lblPeriodoDeCobroSeleccionado.Location = New System.Drawing.Point(28, 325)
        Me.lblPeriodoDeCobroSeleccionado.Name = "lblPeriodoDeCobroSeleccionado"
        Me.lblPeriodoDeCobroSeleccionado.Size = New System.Drawing.Size(243, 18)
        Me.lblPeriodoDeCobroSeleccionado.TabIndex = 6
        Me.lblPeriodoDeCobroSeleccionado.Text = "Periodo de Cobro Seleccionado: Ninguno"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(660, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 18)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Contrato Final:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(433, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 18)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Contrato Inicial:"
        '
        'dgvPeriodosDeCobro
        '
        Me.dgvPeriodosDeCobro.AllowDrop = True
        Me.dgvPeriodosDeCobro.AllowUserToAddRows = False
        Me.dgvPeriodosDeCobro.AllowUserToDeleteRows = False
        Me.dgvPeriodosDeCobro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPeriodosDeCobro.Location = New System.Drawing.Point(32, 107)
        Me.dgvPeriodosDeCobro.Name = "dgvPeriodosDeCobro"
        Me.dgvPeriodosDeCobro.ReadOnly = True
        Me.dgvPeriodosDeCobro.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPeriodosDeCobro.Size = New System.Drawing.Size(781, 215)
        Me.dgvPeriodosDeCobro.TabIndex = 0
        '
        'btnCargarPeriodo
        '
        Me.btnCargarPeriodo.BackColor = System.Drawing.Color.DarkOrange
        Me.btnCargarPeriodo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCargarPeriodo.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCargarPeriodo.ForeColor = System.Drawing.Color.Black
        Me.btnCargarPeriodo.Location = New System.Drawing.Point(23, 417)
        Me.btnCargarPeriodo.Name = "btnCargarPeriodo"
        Me.btnCargarPeriodo.Size = New System.Drawing.Size(170, 51)
        Me.btnCargarPeriodo.TabIndex = 55
        Me.btnCargarPeriodo.Text = "Cargar Periodo"
        Me.btnCargarPeriodo.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblStatusDeImpresion)
        Me.GroupBox2.Controls.Add(Me.lblNumeroDeContratoImprimiendo)
        Me.GroupBox2.Controls.Add(Me.pgProgresoDeImpresion)
        Me.GroupBox2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(23, 504)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(828, 147)
        Me.GroupBox2.TabIndex = 56
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Proceso de Impresión de los Estados de Cuenta"
        '
        'lblStatusDeImpresion
        '
        Me.lblStatusDeImpresion.AutoSize = True
        Me.lblStatusDeImpresion.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatusDeImpresion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblStatusDeImpresion.Location = New System.Drawing.Point(28, 40)
        Me.lblStatusDeImpresion.Name = "lblStatusDeImpresion"
        Me.lblStatusDeImpresion.Size = New System.Drawing.Size(205, 22)
        Me.lblStatusDeImpresion.TabIndex = 5
        Me.lblStatusDeImpresion.Text = "Comenzando Impresión..."
        '
        'lblNumeroDeContratoImprimiendo
        '
        Me.lblNumeroDeContratoImprimiendo.AutoSize = True
        Me.lblNumeroDeContratoImprimiendo.Location = New System.Drawing.Point(579, 40)
        Me.lblNumeroDeContratoImprimiendo.Name = "lblNumeroDeContratoImprimiendo"
        Me.lblNumeroDeContratoImprimiendo.Size = New System.Drawing.Size(196, 18)
        Me.lblNumeroDeContratoImprimiendo.TabIndex = 4
        Me.lblNumeroDeContratoImprimiendo.Text = "Imprimiendo el Contrato # de #:"
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(705, 657)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(146, 51)
        Me.btnSalir.TabIndex = 57
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'lblTitulo
        '
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.Font = New System.Drawing.Font("Trebuchet MS", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.Location = New System.Drawing.Point(268, 3)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(291, 27)
        Me.lblTitulo.TabIndex = 58
        Me.lblTitulo.Text = "Impresión Estados de Cuenta"
        '
        'FrmImprimeEstadosDeCuentaPorPeriodo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(865, 715)
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCargarPeriodo)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.Name = "FrmImprimeEstadosDeCuentaPorPeriodo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Impresión de Estados de Cuenta por Periodo"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvPeriodosDeCobro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtContratoInicial As System.Windows.Forms.TextBox
    Friend WithEvents txtContratoFinal As System.Windows.Forms.TextBox
    Friend WithEvents pgProgresoDeImpresion As System.Windows.Forms.ProgressBar
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnCargarPeriodo As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvPeriodosDeCobro As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblNumeroDeContratoImprimiendo As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents lblStatusDeImpresion As System.Windows.Forms.Label
    Friend WithEvents lblPeriodoDeCobroSeleccionado As System.Windows.Forms.Label
    Friend WithEvents chbxImprimirTodosLosContratos As System.Windows.Forms.CheckBox
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
End Class
