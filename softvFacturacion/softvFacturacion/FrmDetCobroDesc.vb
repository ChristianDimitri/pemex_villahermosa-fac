Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Public Class FrmDetCobroDesc

    Dim diferiencia As Decimal = 0
    Dim montoTotal As Decimal = 0
    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub

    Private Sub FrmDetCobroDesc_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        suma()
    End Sub

    Private Sub FrmDetCobroDesc_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim clv_orden As Integer = 0
        Dim clv_tipo As Integer = 0
        Me.Muestra_Descr_CoDescTableAdapter.Connection = CON
        Me.Muestra_Descr_CoDescTableAdapter.Fill(Me.Procedimientos_arnoldo.Muestra_Descr_CoDesc, gloClv_UnicaNet, clv_orden, clv_tipo)
        Me.TextBox1.Text = clv_orden.ToString
        Me.TextBox2.Text = clv_tipo.ToString
        Me.Valida_tipser_ordenTableAdapter.Connection = CON
        Me.Valida_tipser_ordenTableAdapter.Fill(Me.Procedimientos_arnoldo.Valida_tipser_orden, clv_orden, clv_tipo)
        CON.Close()
        If IsNumeric(Me.TextBox2.Text) = True Then
            If CInt(Me.TextBox2.Text) = 1 Then
                Me.CMBLabel1.Text = "No. de Orden que se est� cobrando:"
            ElseIf CInt(Me.TextBox2.Text) = 2 Then
                Me.CMBLabel1.Text = "No. de Queja que se est� cobrando:"
            End If
        End If
        suma()
        Muestra_RelCobraMaterialProp()
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click

        If IsNumeric(Me.TextBox5.Text) = True Then

            'If CDec(Me.TextBox4.Text) > 0 Then
            '    If CDec(Me.TextBox5.Text) > CDec(Me.TextBox4.Text) Then
            '        MsgBox("El total a pagar no debe se mayor a la cantidad restante")
            '        Exit Sub
            '    Else
            '        Inserta_RelCobraMaterialProp()
            '        opcionClientes = True
            '        Me.Close()
            '    End If
            'Else
            If CDec(Me.TextBox5.Text) > CDec(Me.TextBox3.Text) Then
                MsgBox("El total a pagar no debe se mayor al monto total")
                Exit Sub
            Else
                Inserta_RelCobraMaterialProp()
                opcionClientes = True
                bnd2pardep1 = True
                Me.Close()
            End If
            'End If

        End If



    End Sub
    Private Sub suma()

        Dim sumar As Decimal = 0
        If montoTotal = 0 Then
            For i As Integer = 0 To Muestra_Descr_CoDescDataGridView.RowCount - 1

                sumar = sumar + Me.Muestra_Descr_CoDescDataGridView.Item(2, i).Value

            Next
            sumar = Microsoft.VisualBasic.FormatCurrency(sumar, TriState.True, TriState.True, TriState.True)
            Me.TextBox3.Text = CStr(sumar)
        Else
            Me.TextBox3.Text = CStr(montoTotal)
        End If

        

    End Sub
    Private Sub Muestra_RelCobraMaterialProp()

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Muestra_RelCobraMaterialProp", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@contrato", SqlDbType.BigInt)
        Dim par2 As New SqlParameter("@clv_orden", SqlDbType.BigInt)
        Dim par3 As New SqlParameter("@diferiencia", SqlDbType.Money)
        Dim par4 As New SqlParameter("@montoTotal", SqlDbType.Money)


        par1.Direction = ParameterDirection.Input
        par2.Direction = ParameterDirection.Input
        par3.Direction = ParameterDirection.Output
        par4.Direction = ParameterDirection.Output

        par1.Value = CLng(GloContrato)
        par2.Value = gloClv_UnicaNet

        com.Parameters.Add(par1)
        com.Parameters.Add(par2)
        com.Parameters.Add(par3)
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()
            diferiencia = Microsoft.VisualBasic.FormatCurrency(par3.Value, TriState.True, TriState.True, TriState.True)
            Me.TextBox4.Text = CStr(diferiencia)
            montoTotal = Microsoft.VisualBasic.FormatCurrency(par4.Value, TriState.True, TriState.True, TriState.True)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub
    Private Sub Inserta_RelCobraMaterialProp()
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Inserta_TmpRelCobraMaterialProp", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@contrato", SqlDbType.BigInt)
        Dim par2 As New SqlParameter("@montoTotal", SqlDbType.Decimal)
        Dim par3 As New SqlParameter("@montoPagar", SqlDbType.Decimal)
        Dim par4 As New SqlParameter("@diferiencia", SqlDbType.Decimal)
        Dim par5 As New SqlParameter("@clv_orden", SqlDbType.BigInt)


        par1.Direction = ParameterDirection.Input
        par2.Direction = ParameterDirection.Input
        par3.Direction = ParameterDirection.Input
        par4.Direction = ParameterDirection.Input
        par5.Direction = ParameterDirection.Input

        par1.Value = CLng(GloContrato)
        par2.Value = CDec(Me.TextBox3.Text)
        par3.Value = CDec(Me.TextBox5.Text)
        par4.Value = CDec(Me.TextBox4.Text)
        par5.Value = gloClv_UnicaNet

        com.Parameters.Add(par1)
        com.Parameters.Add(par2)
        com.Parameters.Add(par3)
        com.Parameters.Add(par4)
        com.Parameters.Add(par5)

        Try
            con.Open()
            com.ExecuteNonQuery()
           
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged

    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click

        Me.Close()
        Me.Dispose()

    End Sub
End Class