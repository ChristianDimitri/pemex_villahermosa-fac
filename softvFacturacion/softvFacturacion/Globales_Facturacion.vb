﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Module Globales_Facturacion

    'Variables que Identifican si hubo error dentro del Cobra
    Public miError_Cobra As Integer
    Public miMsjError_Cobra As String
    Public miClv_Session_Cobra As Integer
    ' Variables para mostrar los detalles de la factura a cograr
    Public DetFac_Clv_Detalle As Integer
    Public DetFac_Clv_Servicio As Integer
    Public DetFac_Descorta As String
    Public DetFac_TVsAdic As Integer
    Public DetFac_Meses_Cortesia As Integer
    Public DetFac_Meses_Apagar As Integer
    Public DetFac_Importe As Integer
    Public DetFac_Periodo_Pagado_Inicial As String
    Public DetFac_Periodo_Pagado_Final As String
    Public DetFac_Puntos_Aplicados_Otros As Integer
    Public DetFac_Puntos_Aplicados_Ant As Integer
    Public DetFac_Puntos_Aplicados_Pago_Oportuno As Integer
    Public DetFac_DescuentoNet As Integer
    Public DetFac_Ultimo_Mes As String
    Public DetFac_Ultimo_Anio As String
    Public DetFac_ImporteBonificacion As String
    Public DetFac_Desc_OtrosServ_Misma_Categoria As String 'Puntos PPE
    Public DetFac_Dias_Bonifica As String
    Public DetFac_Aparato As String
    'Banderas para la Sesión------------------
    Public GloAgregar_a_lista As Boolean
    Public GloQuitar_a_lista As Boolean
    Public GloServiciosCobroAdeudo As Boolean

    '-----------------------------------------
    'Variables de los Tickets para imprimir tanto Estados de Cuenta como factura Normal
    Public Ticket_Fac_Normal As Boolean
    Public Ticket_Fac_EstadoCuenta As Boolean

    'Variables para Guardar el Error
    Public Log_Descripcion As String
    Public Log_Formulario As String
    Public Log_ProcedimientoAlmacenado As String

    'Variables para el Reporte de Cobro de Adeudo
    Public FechaInicial_RepCobroAdeudo As DateTime
    Public FechaFinal_RepCobroAdeudo As DateTime
    '-------------------------------------------------
    Public Sucursal_RepCobroAdeudo As Integer = 0
    Public Caja_RepCobroAdeudo As Integer = 0
    Public Clv_Servicio_RepCobroAdeudo As Integer = 0
    Public StatusFactura_RepCobroAdeudo As Integer = 0
    Public StatusServicio_RepCobroAdeudo As String = ""

    'Variables para la Devolución de Aparatos
    Public Op_Devolucion As String = ""
    Public Id_Devolucion As Integer = 0
    Public Contrato_Devolucion As Integer = 0

    'Estado de Cuenta
    Public GloImporteTotalEstadoDeCuenta As Decimal = 0

    'Reporte Global
    Public rDocument As New ReportDocument()

    'Procemiento que registra las excepciones NO controladas en Facturación
    Public Sub GuardaLogError_Facturacion(ByRef Descripcion As String, ByRef Formulario As String, ByRef ProcedimientoAlmacenado As String)


        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "sp_Guarda_LogError_Facturacion"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prmDescripcion As New SqlParameter("@Descripcion", SqlDbType.Text)
                prmDescripcion.Value = Log_Descripcion
                .Parameters.Add(prmDescripcion)

                Dim prmFormulario As New SqlParameter("@Formulario", SqlDbType.Text)
                prmFormulario.Value = Log_Formulario
                .Parameters.Add(prmFormulario)

                Dim prmProcedimientoAlmacenado As New SqlParameter("@ProcedimientoAlmacenado", SqlDbType.Text)
                prmProcedimientoAlmacenado.Value = Log_ProcedimientoAlmacenado
                .Parameters.Add(prmProcedimientoAlmacenado)

                Dim prmCiudad As New SqlParameter("@Ciudad", SqlDbType.Text)
                prmCiudad.Value = GloEmpresa
                .Parameters.Add(prmCiudad)

                Dim i As Integer = .ExecuteNonQuery()

            End With
            CON80.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

End Module
