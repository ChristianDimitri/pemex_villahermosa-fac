﻿Public Class FrmDetFactura

    Private Sub FrmDetFactura_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       

        Try

            colorea(Me)

            Me.Label11.ForeColor = Color.Black
            Me.Label12.ForeColor = Color.Black

            Me.txtContrato.BackColor = Color.White

            'Cargar los conceptos en pantalla
            Me.txtConcepto.BackColor = Color.White
            Me.txtTVExtras.BackColor = Color.White
            'Me.txtDetFac_Meses_Cortesia 
            Me.txtMesesAPagar.BackColor = Color.White
            Me.txtImporteFinal.BackColor = Color.White
            Me.txtImporteFinal.BackColor = Color.White
            Me.txtPeriodoInical.BackColor = Color.White
            Me.txtPeriodoFinal.BackColor = Color.White
            Me.txtPuntosPagoOportuno.BackColor = Color.White
            Me.txtPuntosPorAntiguedad.BackColor = Color.White
            Me.txtPuntosPagoOportuno.BackColor = Color.White
            Me.txtPuntosCombo.BackColor = Color.White
            Me.txtAparato.BackColor = Color.White

            Me.txtContrato.Text = GloContrato

            'Cargar los conceptos en pantalla
            Me.txtConcepto.Text = DetFac_Descorta
            Me.txtTVExtras.Text = DetFac_TVsAdic
            'Me.txtDetFac_Meses_Cortesia 
            Me.txtMesesAPagar.Text = DetFac_Meses_Apagar
            Me.txtImporteFinal.Text = DetFac_Importe
            Me.txtImporteFinal.Text = "$" & Me.txtImporteFinal.Text
            Me.txtPeriodoInical.Text = DetFac_Periodo_Pagado_Inicial
            Me.txtPeriodoFinal.Text = DetFac_Periodo_Pagado_Final
            Me.txtPuntosPagoOportuno.Text = DetFac_Puntos_Aplicados_Otros
            Me.txtPuntosPorAntiguedad.Text = DetFac_Puntos_Aplicados_Ant
            Me.txtPuntosCombo.Text = DetFac_DescuentoNet
            Me.txtPuntosPagoAdelantado.Text = DetFac_Puntos_Aplicados_Pago_Oportuno ' Pago Adelantado
            Me.txtUltMesPagado.Text = DetFac_Ultimo_Mes

            If DetFac_Ultimo_Anio = "0" Then
                Me.txtUltAnioPagado.Text = " - NINGUNO - "
            Else
                Me.txtUltAnioPagado.Text = DetFac_Ultimo_Anio

            End If

            Me.txtImporteBonificar.Text = DetFac_ImporteBonificacion
            Me.txtPuntosPPE.Text = DetFac_Desc_OtrosServ_Misma_Categoria
            Me.txtDiasBonificados.Text = DetFac_Dias_Bonifica

            If DetFac_Aparato = "0" Then
                Me.txtAparato.Text = " - NINGUNO - "
            Else
                Me.txtAparato.Text = DetFac_Aparato
            End If

            'Visual
            ' Colocar el Mes que le Pertenece
            Select Case DetFac_Ultimo_Mes
                Case 0
                    Me.txtUltMesPagado.Text = " - NINGUNO - "
                Case 1
                    Me.txtUltMesPagado.Text = "ENERO"
                Case 2
                    Me.txtUltMesPagado.Text = "FEBRERO"
                Case 3
                    Me.txtUltMesPagado.Text = "MARZO"
                Case 4
                    Me.txtUltMesPagado.Text = "ABRIL"
                Case 5
                    Me.txtUltMesPagado.Text = "MAYO"
                Case 6
                    Me.txtUltMesPagado.Text = "JUNIO"
                Case 7
                    Me.txtUltMesPagado.Text = "JULIO"
                Case 8
                    Me.txtUltMesPagado.Text = "AGOSTO"
                Case 9
                    Me.txtUltMesPagado.Text = "SEPTIEMBRE"
                Case 10
                    Me.txtUltMesPagado.Text = "OCTUBRE"
                Case 11
                    Me.txtUltMesPagado.Text = "NOVIEMBRE"
                Case 12
                    Me.txtUltMesPagado.Text = "DICIEMBRE"
            End Select

            Me.Text = "Detalles del Concepto a pagar: " & DetFac_Descorta.ToString


            DetFac_Clv_Detalle = "0"
            DetFac_Clv_Servicio = "0"
            DetFac_Descorta = "0"
            DetFac_TVsAdic = "0"
            DetFac_Meses_Cortesia = "0"
            DetFac_Meses_Apagar = "0"
            DetFac_Importe = "0"
            DetFac_Periodo_Pagado_Inicial = "0"
            DetFac_Periodo_Pagado_Final = "0"
            DetFac_Puntos_Aplicados_Otros = "0"
            DetFac_Puntos_Aplicados_Ant = "0"
            DetFac_Puntos_Aplicados_Pago_Oportuno = "0"
            DetFac_DescuentoNet = "0"
            DetFac_Ultimo_Mes = "0"
            DetFac_Ultimo_Anio = "0"
            DetFac_ImporteBonificacion = "0"
            DetFac_Aparato = "0"

        Catch ex As Exception
            MsgBox("No se ha podido cargar los detalles de este concepto a facturar:" & DetFac_Descorta.ToString, MsgBoxStyle.Exclamation, " :: Atención :: ")
        End Try

       

    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class