﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data.OleDb

Imports MizarCFD.DAL
Imports MizarCFD.BRL
Imports MizarCFD.Reportes


Module Module2
    Public GloTxtUlt4DigCheque As String = Nothing
    Public GloFacGlobalclvcompania As Long = 0
    Public GloNomReporteFiscal As String = Nothing
    Public GloNomReporteFiscalGlobal As String = Nothing
   
    Public GloIDFactura As Long = 0
    Public GloClv_FacturaCFD As Long = 0
    Public GloMotivoCancelacionCFD As String = Nothing
    Public Locop As Integer = 0
    Public mIva As Decimal = 0
    Public mipes As Decimal = 0
    Public msubtotal As Decimal = 0
    Public mdetalle1 As String = Nothing
    Public locID_Compania_Mizart As String = Nothing
    Public locID_Sucursal_Mizart As String = Nothing
    Public GloFORMADEPAGO As String = Nothing
    Public GloCONDICIONESDEPAGO As String = Nothing
    Public GloMOTIVODESCUENTO As String = Nothing
    Public GloMETODODEPAGO As String = Nothing
    Public GloTIPODECOMPROBANTE As String = Nothing
    Public GloPAGOENPARCIALIDADES As String = Nothing
    Public GloLUGAREXPEDICION As String = Nothing
    Public GloREGIMEN As String = Nothing
    Public GloMONEDA As String = Nothing
    Public GloVERSON As String = Nothing

    'Public Sub Imprime_Factura_Digital(ByVal oid_CFD As Integer)
    '    '    Using Cnx As New DAConexion("HL", "sa", "sa")
    '    '        Dim oRreportes As New MizarCFD.Reportes.CFD
    '    '        Dim oCFD As New MizarCFD.BRL.CFD
    '    '        oCFD.IdCFD = "1"
    '    '        oCFD.Consultar(Cnx)
    '    '        Dim document As New System.Xml.XmlDocument
    '    '        oCFD.ObtenerXML()
    '    '        document.Value = oCFD.CadenaOriginal
    '    '    End Using

    'End Sub



    Public Function Existe_Checalo(ByVal oSql As String) As Boolean
        Existe_Checalo = False
        Using Cnx As New DAConexion("HL", "sa", "sa")
            Cnx.DbCommand.CommandText = oSql
            Cnx.DbCommand.CommandTimeout = 0
            Cnx.DbCommand.CommandType = CommandType.Text
            'Cnx.DbConnection.Open()
            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
            'COMANDO.CommandType = CommandType.Text
            'Try
            '    CONEXION.Open()
            Dim reader As SqlDataReader = Cnx.DbCommand.ExecuteReader
            '    'Recorremos los Cargos obtenidos desde el Procedimiento
            While (reader.Read())
                If Len(reader(0).ToString) > 0 Then
                    Existe_Checalo = True
                End If
            End While
            'Cnx.DbConnection.Close()
            'Catch ex As Exception
            '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            'Finally
            '    CONEXION.Close()
            '    CONEXION.Dispose()
            'End Try
        End Using
    End Function

    Public Sub Alta_Datosfiscales(ByVal oRazonSocial As String,
        ByVal oRFC As String,
        ByVal oCalle As String,
        ByVal oClaveAsociado As String,
        ByVal oCodigoPostal As String,
        ByVal oColonia As String,
        ByVal oEMail As String,
        ByVal oEntreCalles As String,
        ByVal oEstado As String,
        ByVal oIdAsociado As String,
        ByVal oLocalidad As String,
        ByVal oMunicipio As String,
        ByVal oNumeroExterior As String,
            ByVal oPais As String)



        If String.IsNullOrEmpty(oRazonSocial) = True Then oRazonSocial = ""
        If String.IsNullOrEmpty(oRFC) = True Then oRFC = ""
        If String.IsNullOrEmpty(oCalle) = True Then oCalle = ""
        If String.IsNullOrEmpty(oClaveAsociado) = True Then oClaveAsociado = ""
        If String.IsNullOrEmpty(oCodigoPostal) = True Then oCodigoPostal = ""
        If String.IsNullOrEmpty(oColonia) = True Then oColonia = ""
        If String.IsNullOrEmpty(oEMail) = True Then oEMail = ""
        If String.IsNullOrEmpty(oEntreCalles) = True Then oEntreCalles = ""
        If String.IsNullOrEmpty(oEstado) = True Then oEstado = ""
        If String.IsNullOrEmpty(oIdAsociado) = True Then oIdAsociado = ""
        If String.IsNullOrEmpty(oLocalidad) = True Then oLocalidad = ""
        If String.IsNullOrEmpty(oMunicipio) = True Then oMunicipio = ""
        If String.IsNullOrEmpty(oNumeroExterior) = True Then oNumeroExterior = ""
        If String.IsNullOrEmpty(oPais) = True Then oPais = ""

        If Existe_Checalo(" Select * from asociados  where id_asociado = " + oIdAsociado) = False Then

            Dim oAsociados As MizarCFD.BRL.Asociados
            Using Cnx As New DAConexion("HL", "sa", "sa")

                oAsociados = New MizarCFD.BRL.Asociados
                oAsociados.Inicializar()
                oAsociados.RazonSocial = oRazonSocial
                oAsociados.RFC = oRFC
                'oAsociados.Calle = oCalle
                oAsociados.ClaveAsociado = oIdAsociado
                'oAsociados.CodigoPostal = oCodigoPostal
                'oAsociados.Colonia = oColonia
                'oAsociados.EMail = oEMail
                'oAsociados.EntreCalles = oEntreCalles
                'oAsociados.Estado = oEstado
                oAsociados.IdAsociado = oIdAsociado
                'oAsociados.Localidad = oLocalidad
                'oAsociados.Municipio = oMunicipio
                'oAsociados.NumeroExterior = oNumeroExterior
                'oAsociados.Pais = oPais
                'oAsociados.IdTipoAsociado = Asociados.TipoAsociado.Cliente
                'If oAsociados.Validar(Cnx, TipoAfectacionBD.Insertar) = True Then
                '    If Not oAsociados.Insertar(Cnx) Then
                '        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                '            MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                '        Next
                '        Return
                '    End If
                'Else
                '    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
                '        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                '    Next
                '    Return
                'End If





            End Using
        Else

        End If

    End Sub

    Public Sub Graba_Factura_Digital_Global(ByVal oClv_Factura As Long, ByVal oId_Compania As String, ByVal oId_Sucursal As String)
        Dim oTotalConPuntos As String = Nothing
        Dim oSubTotal As String = Nothing
        Dim oTotalSinPuntos As String = Nothing
        Dim oDescuento As String = Nothing
        Dim oiva As String = Nothing
        Dim oieps As String = Nothing
        Dim oTasaIva As String = Nothing
        Dim oTasaIeps As String = Nothing
        Dim oFecha As String = Nothing
        Dim oTotalImpuestos As String = Nothing

        Dim mfecha As DateTime

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
        BaseII.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL")
        mfecha = DateTime.Parse(BaseII.dicoPar("@FECHA").ToString())

        Dim CONEXION As New SqlConnection(MiConexion)
        Dim COMANDO As New SqlCommand("DameFacDig_Parte_1_Global", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Factura
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = 0
        COMANDO.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        COMANDO.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = 0
        COMANDO.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
        parametro5.Direction = ParameterDirection.Output
        parametro5.Value = 0
        COMANDO.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
        parametro6.Direction = ParameterDirection.Output
        parametro6.Value = 0
        COMANDO.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
        parametro7.Direction = ParameterDirection.Output
        parametro7.Value = 0
        COMANDO.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
        parametro8.Direction = ParameterDirection.Output
        parametro8.Value = 0
        COMANDO.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
        parametro9.Direction = ParameterDirection.Output
        parametro9.Value = 0
        COMANDO.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
        parametro10.Direction = ParameterDirection.Output
        parametro10.Value = 0
        COMANDO.Parameters.Add(parametro10)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            oTotalConPuntos = CStr(parametro1.Value)
            oSubTotal = CStr(parametro2.Value)
            oTotalSinPuntos = CStr(parametro3.Value)
            oDescuento = CStr(parametro4.Value)
            oiva = CStr(parametro5.Value)
            oieps = CStr(parametro6.Value)
            oTasaIva = CStr(parametro7.Value)
            oTasaIeps = CStr(parametro8.Value)
            oFecha = CStr(parametro9.Value)
            oTotalImpuestos = CStr(parametro10.Value)
            'Cnx.DbConnection.Close()
        Catch ex As Exception
            MsgBox(ex.Message & " Parte 1 " & ex.Source, MsgBoxStyle.Exclamation)
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try

        Usp_Ed_DameDatosFacDigGlobal(oClv_Factura, oId_Compania)

        Dim oId_AsociadoLlave As Long = 0
        oId_AsociadoLlave = Existe_DevuelveValor(" Select top 1 id_asociado from asociados where RFC ='XAXX010101000' order by id_asociado ")

        Dim oCFD As MizarCFD.BRL.CFD

        Using Cnx As New DAConexion("HL", "sa", "sa")
            Try


                oCFD = New MizarCFD.BRL.CFD
                oCFD.Inicializar()
                oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                If oId_Sucursal = "0" Then oId_Sucursal = ""

                'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                'oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oIden), 1, 0)

                'oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania, oId_Sucursal)
                'Comente lo de compañia

                If oId_Sucursal.Length > 0 And oId_Sucursal <> "0" Then
                    oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania, oId_Sucursal)
                Else
                    oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania)
                End If

                oCFD.PrepararDatosReceptorPorId(Cnx, oId_AsociadoLlave, 1, 0)


                oCFD.EsTimbrePrueba = False
                oCFD.Timbrador = CFD.PAC.Mizar

                oCFD.Version = GloVERSON
                oCFD.IdMoneda = GloMONEDA  '//1 = Pesos , Pesos = 2 USD
                'oFecha
                ' oCFD.Fecha = mfecha
                oCFD.FormaDePago = GloFORMADEPAGO
                'oCFD.Certificado = ""
                oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                oCFD.SubTotal = oSubTotal
                oCFD.Descuento = oDescuento
                oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                oCFD.Total = oTotalSinPuntos
                oCFD.MetodoDePago = GloMETODODEPAGO
                oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE

                oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                oCFD.LugarExpedicion = GloLUGAREXPEDICION


                Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                oRegimen.IdRegimen = 0
                oRegimen.IdCFD = 0
                oRegimen.Regimen = GloREGIMEN
                'oRegimen.Insertar(Cnx)

                oCFD.RegimenesFiscales.Add(oRegimen)

                'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
                'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

                oCFD.Impuestos.TotalImpuestosRetenidos = 0
                oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
                'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
                'oimpuestosRetenciones = New CFDImpuestosRetenciones()
                'oimpuestosRetenciones.Impuesto = "IVA"
                'oimpuestosRetenciones.Importe = oiva
                'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

                Dim oImpuestosTraslados As CFDImpuestosTraslados



                oImpuestosTraslados = New CFDImpuestosTraslados()
                oImpuestosTraslados.Impuesto = "IVA"
                oImpuestosTraslados.Importe = oiva
                oImpuestosTraslados.Tasa = oTasaIva

                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                oImpuestosTraslados = New CFDImpuestosTraslados()
                oImpuestosTraslados.Impuesto = "IEPS"
                oImpuestosTraslados.Importe = oieps
                oImpuestosTraslados.Tasa = oTasaIeps

                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                Dim oConcepto As CFDConceptos

                Dim CONEXION2 As New SqlConnection(MiConexion)
                Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2_Global " + CStr(oClv_Factura), CONEXION2)
                COMANDO2.CommandType = CommandType.Text
                Try
                    CONEXION2.Open()
                    Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                    '    'Recorremos los Cargos obtenidos desde el Procedimiento
                    While (reader.Read())
                        oConcepto = New CFDConceptos()
                        oConcepto.Cantidad = reader(0).ToString
                        oConcepto.UnidadMedida = "Servicio"
                        oConcepto.NumeroIdentificacion = reader(1).ToString
                        oConcepto.Descripcion = reader(2).ToString
                        oConcepto.ValorUnitario = reader(3).ToString
                        oConcepto.Importe = reader(3).ToString
                        'oConcepto.CausaIVA = "1"
                        oConcepto.TrasladarIVA = "1"
                        oCFD.Conceptos.Add(oConcepto)
                    End While
                    'Cnx.DbConnection.Close()
                Catch ex As Exception
                    MsgBox(ex.Message & " Parte 2 " & ex.Source, MsgBoxStyle.Exclamation)
                Finally
                    CONEXION2.Close()
                    CONEXION2.Dispose()
                End Try

                If Not oCFD.Insertar(Cnx, True) Then
                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                    Next
                    Return
                End If


                GloClv_FacturaCFD = oCFD.IdCFD
                UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "G")

            Catch ex As Exception
                MsgBox(ex.Message & " Parte 3 " & ex.Source, MsgBoxStyle.Exclamation)
            Finally
                CONEXION.Close()
                CONEXION.Dispose()
            End Try

        End Using
    End Sub



    Public Sub Graba_Factura_Digital(ByVal oClv_Factura As Long, ByVal oIden As Integer)
        Dim oTotalConPuntos As String = Nothing
        Dim oSubTotal As String = Nothing
        Dim oTotalSinPuntos As String = Nothing
        Dim oDescuento As String = Nothing
        Dim oiva As String = Nothing
        Dim oieps As String = Nothing
        Dim oTasaIva As String = Nothing
        Dim oTasaIeps As String = Nothing
        Dim oFecha As String = Nothing
        Dim oTotalImpuestos As String = Nothing

        ''
        Dim mfecha As DateTime
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("DAMEFECHADELSERVIDORFACTURADIGITAL", con)
        com.CommandType = CommandType.StoredProcedure
        'Dame la fecha del Servidor
        Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
        prmFechaObtebida.Direction = ParameterDirection.Output
        prmFechaObtebida.Value = ""
        com.Parameters.Add(prmFechaObtebida)
        Try
            con.Open()
            com.ExecuteNonQuery()
            mfecha = prmFechaObtebida.Value
        Catch ex As Exception
            MsgBox("Error: No se pudo obtener la fecha válida del Servidor.", MsgBoxStyle.Critical)
            MsgBox("Descripción:  Procedimiento [DAMEFECHADELSERVIDOR] // Función [DameFechaServidor] // Form: FrmAdelantarCargos", MsgBoxStyle.Information)
            MsgBox(ex.Message)
        Finally
            con.Close()
        End Try
        ''

        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
        'BaseII.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL")
        'mfecha = DateTime.Parse(BaseII.dicoPar("@FECHA").ToString())


        Dim CONEXION As New SqlConnection(MiConexion)
        Dim COMANDO As New SqlCommand("DameFacDig_Parte_1", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Factura
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = 0
        COMANDO.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        COMANDO.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = 0
        COMANDO.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
        parametro5.Direction = ParameterDirection.Output
        parametro5.Value = 0
        COMANDO.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
        parametro6.Direction = ParameterDirection.Output
        parametro6.Value = 0
        COMANDO.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
        parametro7.Direction = ParameterDirection.Output
        parametro7.Value = 0
        COMANDO.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
        parametro8.Direction = ParameterDirection.Output
        parametro8.Value = 0
        COMANDO.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
        parametro9.Direction = ParameterDirection.Output
        parametro9.Value = 0
        COMANDO.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
        parametro10.Direction = ParameterDirection.Output
        parametro10.Value = 0
        COMANDO.Parameters.Add(parametro10)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            oTotalConPuntos = CStr(parametro1.Value)
            oSubTotal = CStr(parametro2.Value)
            oTotalSinPuntos = CStr(parametro3.Value)
            oDescuento = CStr(parametro4.Value)
            oiva = CStr(parametro5.Value)
            oieps = CStr(parametro6.Value)
            oTasaIva = CStr(parametro7.Value)
            oTasaIeps = CStr(parametro8.Value)
            oFecha = CStr(parametro9.Value)
            oTotalImpuestos = CStr(parametro10.Value)
            'Cnx.DbConnection.Close()
        Catch ex As Exception
            MsgBox(ex.Message & " Parte 1 " & ex.Source, MsgBoxStyle.Exclamation)
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try

        Usp_Ed_DameDatosFacDig(oClv_Factura, locID_Compania_Mizart)

        Dim oCFD As MizarCFD.BRL.CFD

        Dim oId_AsociadoLlave As Long = 0
        oId_AsociadoLlave = oIden 'Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + CStr(oIden))


        Using Cnx As New DAConexion("HL", "sa", "sa")
            Try
                'oIden = "1"

                oCFD = New MizarCFD.BRL.CFD
                oCFD.Inicializar()
                oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
                ''Ojo 
                'locID_Sucursal_Mizart = 0
                'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                If locID_Sucursal_Mizart.Length > 0 And locID_Sucursal_Mizart <> "0" Then
                    oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
                Else
                    oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
                End If
                oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oId_AsociadoLlave), 1, 0)


                oCFD.EsTimbrePrueba = False
                oCFD.Timbrador = CFD.PAC.Mizar

                oCFD.Version = GloVERSON
                oCFD.IdMoneda = GloMONEDA  '//1 = Pesos , Pesos = 2 USD
                'oFecha
                ' oCFD.Fecha = mfecha '//Date.Now
                oCFD.FormaDePago = GloFORMADEPAGO
                'oCFD.Certificado = ""
                oCFD.CondicionesDePago = GloCONDICIONESDEPAGO
                oCFD.SubTotal = oSubTotal
                oCFD.Descuento = oDescuento
                oCFD.MotivoDescuento = GloMOTIVODESCUENTO
                oCFD.Total = oTotalSinPuntos
                oCFD.MetodoDePago = GloMETODODEPAGO
                oCFD.TipoDeComprobante = GloTIPODECOMPROBANTE
                oCFD.PagoEnParcialiades = GloPAGOENPARCIALIDADES
                oCFD.LugarExpedicion = GloPAGOENPARCIALIDADES
                If Len(GloTxtUlt4DigCheque) > 0 Then
                    oCFD.CuentaPago = GloTxtUlt4DigCheque
                End If




                Dim oRegimen As New MizarCFD.BRL.CFDRegimenesFiscales
                oRegimen.IdRegimen = 0
                oRegimen.IdCFD = 0
                oRegimen.Regimen = GloREGIMEN
                'oRegimen.Insertar(Cnx)

                oCFD.RegimenesFiscales.Add(oRegimen)

                'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
                'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

                oCFD.Impuestos.TotalImpuestosRetenidos = 0
                oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
                'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
                'oimpuestosRetenciones = New CFDImpuestosRetenciones()
                'oimpuestosRetenciones.Impuesto = "IVA"
                'oimpuestosRetenciones.Importe = oiva
                'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

                Dim oImpuestosTraslados As CFDImpuestosTraslados

                oImpuestosTraslados = New CFDImpuestosTraslados()
                oImpuestosTraslados.Impuesto = "IVA"
                oImpuestosTraslados.Importe = oiva
                oImpuestosTraslados.Tasa = oTasaIva

                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                oImpuestosTraslados = New CFDImpuestosTraslados()
                oImpuestosTraslados.Impuesto = "IEPS"
                oImpuestosTraslados.Importe = oieps
                oImpuestosTraslados.Tasa = oTasaIeps

                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

                Dim oConcepto As CFDConceptos

                Dim CONEXION2 As New SqlConnection(MiConexion)
                Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2 " + CStr(oClv_Factura), CONEXION2)
                COMANDO2.CommandType = CommandType.Text
                Try
                    CONEXION2.Open()
                    Dim reader As SqlDataReader = COMANDO2.ExecuteReader
                    '    'Recorremos los Cargos obtenidos desde el Procedimiento
                    While (reader.Read())
                        oConcepto = New CFDConceptos()
                        oConcepto.Cantidad = reader(0).ToString
                        oConcepto.UnidadMedida = "Servicio"
                        oConcepto.NumeroIdentificacion = reader(1).ToString
                        oConcepto.Descripcion = reader(2).ToString
                        oConcepto.ValorUnitario = reader(3).ToString
                        oConcepto.Importe = reader(3).ToString
                        oConcepto.TrasladarIVA = "1"
                        oCFD.Conceptos.Add(oConcepto)
                    End While
                    'Cnx.DbConnection.Close()
                Catch ex As Exception
                    MsgBox(ex.Message & " Parte 2 " & ex.Source, MsgBoxStyle.Exclamation)
                Finally
                    CONEXION2.Close()
                    CONEXION2.Dispose()
                End Try

                If Not oCFD.Insertar(Cnx, True) Then
                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                    Next
                    Return
                End If
                'If Not oCFD.Insertar(Cnx, True) Then
                '    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                '        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                '    Next
                '    Return
                'End If




                GloClv_FacturaCFD = oCFD.IdCFD
                UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "N")
                GloTxtUlt4DigCheque = ""
            Catch ex As Exception
                MsgBox(ex.Message & " Parte 3 " & ex.Source, MsgBoxStyle.Exclamation)
            Finally
                CONEXION.Close()
                CONEXION.Dispose()
            End Try

        End Using
    End Sub

    Public Sub Cancelacion_FacturaCFD(ByVal oClv_FacturaCFD As Long)
        Dim ofecha As Date
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("DAMEFECHADELSERVIDOR", con)
        com.CommandType = CommandType.StoredProcedure

        'Dame la fecha del Servidor
        Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
        prmFechaObtebida.Direction = ParameterDirection.Output
        prmFechaObtebida.Value = ""
        com.Parameters.Add(prmFechaObtebida)

        Try
            con.Open()
            com.ExecuteNonQuery()

            ofecha = prmFechaObtebida.Value

        Catch ex As Exception
            MsgBox("Error: No se pudo obtener la fecha válida del Servidor.", MsgBoxStyle.Critical)
            MsgBox("Descripción:  Procedimiento [DAMEFECHADELSERVIDOR] // Función [DameFechaServidor] // Form: FrmAdelantarCargos", MsgBoxStyle.Information)
            MsgBox(ex.Message)
        Finally
            con.Close()
        End Try
        'Dim oEFAC As New Miza

        Dim oCFD As MizarCFD.BRL.CFD
        Using Cnx As New DAConexion("HL", "sa", "sa")
            oCFD = New MizarCFD.BRL.CFD
            oCFD.Inicializar()
            oCFD.IdCFD = CStr(oClv_FacturaCFD)
            If Len(GloMotivoCancelacionCFD) = 0 Then GloMotivoCancelacionCFD = "CANCELACION"
            oCFD.MotivoCancelacion = GloMotivoCancelacionCFD
            oCFD.FechaCancelacion = ofecha
            oCFD.EstaCancelado = 1
            If Not oCFD.ModificarEstatusCancelado(Cnx) = False Then
                For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
                    MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
                Next
                Return
            End If
        End Using
    End Sub

    Public Sub Dame_Detalle1Global(ByVal oClv_FacturaCFD As Long)
        mIva = 0
        mipes = 0
        msubtotal = 0
        mdetalle1 = ""

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Dame_Detalle1", con)
        com.CommandType = CommandType.StoredProcedure
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oClv_FacturaCFD
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@Iva", SqlDbType.Money)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@Ieps", SqlDbType.Money)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)

        Dim prm4 As New SqlParameter("@Subtotal", SqlDbType.Money)
        prm4.Direction = ParameterDirection.Output
        prm4.Value = ""
        com.Parameters.Add(prm4)

        Dim prm5 As New SqlParameter("@Detalle1", SqlDbType.VarChar, 8000)
        prm5.Direction = ParameterDirection.Output
        prm5.Value = ""
        com.Parameters.Add(prm5)

        Try
            con.Open()
            com.ExecuteNonQuery()
            mIva = prm2.Value
            mipes = prm3.Value
            msubtotal = prm4.Value
            mdetalle1 = prm5.Value

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Close()
        End Try
    End Sub

    Public Sub Dime_Aque_Compania_Facturarle(ByVal oClv_Factura As Long)
        locID_Compania_Mizart = ""
        locID_Sucursal_Mizart = ""

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Dime_Aque_Compania_Facturarle", con)
        com.CommandType = CommandType.StoredProcedure
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oClv_Factura
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)


        Try
            con.Open()
            com.ExecuteNonQuery()
            locID_Compania_Mizart = prm2.Value
            locID_Sucursal_Mizart = prm3.Value

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Close()
        End Try
    End Sub

    Public Sub Dime_Aque_Compania_FacturarleGlobal(ByVal oClv_Factura As Long)
        locID_Compania_Mizart = ""
        locID_Sucursal_Mizart = ""

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Dime_Aque_Compania_FacturarleGlobal", con)
        com.CommandType = CommandType.StoredProcedure
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        Dim prm1 As New SqlParameter("@Clv_Facturaglobal", SqlDbType.BigInt)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oClv_Factura
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50)
        prm2.Direction = ParameterDirection.Output
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)


        Try
            con.Open()
            com.ExecuteNonQuery()
            locID_Compania_Mizart = prm2.Value
            locID_Sucursal_Mizart = prm3.Value

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Close()
        End Try
    End Sub

    Public Function Dame_Cantidad_Con_Letra(ByVal oCantidad As Long) As String
        Dame_Cantidad_Con_Letra = ""

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("DameCantidadALetra", con)
        com.CommandType = CommandType.StoredProcedure
        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
        'Dame la fecha del Servidor
        If IsNumeric(oCantidad) = False Then oCantidad = 0
        Dim prm1 As New SqlParameter("@Numero", SqlDbType.Decimal)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = oCantidad
        com.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@moneda", SqlDbType.Int)
        prm2.Direction = ParameterDirection.Input
        prm2.Value = 0
        com.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@letra", SqlDbType.VarChar, 400)
        prm3.Direction = ParameterDirection.Output
        prm3.Value = ""
        com.Parameters.Add(prm3)

        Try
            con.Open()
            com.ExecuteNonQuery()

            Dame_Cantidad_Con_Letra = prm3.Value

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Close()
        End Try
    End Function

    Public Sub DameId_FacturaCDF(ByVal oClv_Factura As Long, ByVal oTipo As String)
        GloClv_FacturaCFD = 0
        Dim CONEXION As New SqlConnection(MiConexion)
        Dim COMANDO As New SqlCommand("upsDameFacDig_Clv_FacturaCDF", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Factura
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Clv_FacturaCDF", SqlDbType.Money)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@tipo", SqlDbType.VarChar, 1)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = oTipo
        COMANDO.Parameters.Add(parametro2)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloClv_FacturaCFD = CStr(parametro1.Value)

            'Cnx.DbConnection.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Function Existe_DevuelveValor(ByVal oSql As String) As Long
        Existe_DevuelveValor = 0

        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
            Cnx.DbCommand.CommandText = oSql
            Cnx.DbCommand.CommandTimeout = 0
            Cnx.DbCommand.CommandType = CommandType.Text
            'Cnx.DbConnection.Open()
            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
            'COMANDO.CommandType = CommandType.Text
            'Try
            '    CONEXION.Open()
            Dim reader As SqlDataReader = Cnx.DbCommand.ExecuteReader
            '    'Recorremos los Cargos obtenidos desde el Procedimiento
            While (reader.Read())
                If Len(reader(0).ToString) > 0 Then
                    Existe_DevuelveValor = CLng(reader(0).ToString)
                End If
            End While
            'Cnx.DbConnection.Close()
            'Catch ex As Exception
            '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            'Finally
            '    CONEXION.Close()
            '    CONEXION.Dispose()
            'End Try
        End Using
    End Function

    Public Function BusFacFiscalOledb(ByVal oClv_Factura As Long) As Integer
        BusFacFiscalOledb = 0
        Dim CONEXION As New SqlConnection(MiConexion)
        Dim COMANDO As New SqlCommand("BusFacFiscal", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oClv_Factura
        COMANDO.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@ident", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = 0
        COMANDO.Parameters.Add(parametro1)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            BusFacFiscalOledb = CStr(parametro1.Value)

            'Cnx.DbConnection.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Function





    'Public Sub Graba_Factura_Digital(ByVal oClv_Factura As Long)
    '    Dim oTotalConPuntos As String = Nothing
    '    Dim oSubTotal As String = Nothing
    '    Dim oTotalSinPuntos As String = Nothing
    '    Dim oDescuento As String = Nothing
    '    Dim oiva As String = Nothing
    '    Dim oieps As String = Nothing
    '    Dim oTasaIva As String = Nothing
    '    Dim oTasaIeps As String = Nothing
    '    Dim oFecha As String = Nothing

    '    Dim CONEXION As New SqlConnection(MiConexion)
    '    Dim COMANDO As New SqlCommand("DameFacDig_Parte_1", CONEXION)
    '    COMANDO.CommandType = CommandType.StoredProcedure

    '    Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
    '    parametro.Direction = ParameterDirection.Input
    '    parametro.Value = oClv_Factura
    '    COMANDO.Parameters.Add(parametro)

    '    Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
    '    parametro1.Direction = ParameterDirection.Output
    '    parametro1.Value = 0
    '    COMANDO.Parameters.Add(parametro1)

    '    Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
    '    parametro2.Direction = ParameterDirection.Output
    '    parametro2.Value = 0
    '    COMANDO.Parameters.Add(parametro2)

    '    Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
    '    parametro3.Direction = ParameterDirection.Output
    '    parametro3.Value = 0
    '    COMANDO.Parameters.Add(parametro3)

    '    Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
    '    parametro4.Direction = ParameterDirection.Output
    '    parametro4.Value = 0
    '    COMANDO.Parameters.Add(parametro4)

    '    Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
    '    parametro5.Direction = ParameterDirection.Output
    '    parametro5.Value = 0
    '    COMANDO.Parameters.Add(parametro5)

    '    Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
    '    parametro6.Direction = ParameterDirection.Output
    '    parametro6.Value = 0
    '    COMANDO.Parameters.Add(parametro6)

    '    Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
    '    parametro7.Direction = ParameterDirection.Output
    '    parametro7.Value = 0
    '    COMANDO.Parameters.Add(parametro7)

    '    Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
    '    parametro8.Direction = ParameterDirection.Output
    '    parametro8.Value = 0
    '    COMANDO.Parameters.Add(parametro8)

    '    Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
    '    parametro9.Direction = ParameterDirection.Output
    '    parametro9.Value = 0
    '    COMANDO.Parameters.Add(parametro9)


    '    Try
    '        CONEXION.Open()
    '        Dim i As Integer = COMANDO.ExecuteNonQuery
    '        oTotalConPuntos = CStr(parametro1.Value)
    '        oSubTotal = CStr(parametro2.Value)
    '        oTotalSinPuntos = CStr(parametro3.Value)
    '        oDescuento = CStr(parametro4.Value)
    '        oiva = CStr(parametro5.Value)
    '        oieps = CStr(parametro6.Value)
    '        oTasaIva = CStr(parametro7.Value)
    '        oTasaIeps = CStr(parametro8.Value)
    '        oFecha = CStr(parametro9.Value)
    '        'Cnx.DbConnection.Close()
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
    '    Finally
    '        CONEXION.Close()
    '        CONEXION.Dispose()
    '    End Try


    '    Dim oCFD As MizarCFD.BRL.CFD

    '    Using Cnx As New DAConexion("HL", "sa", "sa")
    '        oCFD = New MizarCFD.BRL.CFD
    '        oCFD.Inicializar()
    '        oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision

    '        oCFD.PrepararDatosEmisorPorId(Cnx, "1", "1")
    '        oCFD.PrepararDatosReceptorPorId(Cnx, "3")

    '        oCFD.Version = "2.0"
    '        oCFD.IdMoneda = "1" '//1 = Pesos , Pesos = 2 USD
    '        oCFD.Fecha = oFecha
    '        oCFD.FormaDePago = "Forma de Pago"
    '        oCFD.Certificado = ""
    '        oCFD.CondicionesDePago = "Condiciones de Pago"
    '        oCFD.SubTotal = oSubTotal
    '        oCFD.Descuento = oDescuento
    '        oCFD.MotivoDescuento = "Pronto Pago"
    '        oCFD.Total = oTotalSinPuntos
    '        oCFD.MetodoDePago = "Efectivo"
    '        oCFD.TipoDeComprobante = "ingreso"

    '        oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
    '        oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

    '        oCFD.Impuestos.TotalImpuestosRetenidos = oiva
    '        oCFD.Impuestos.TotalImpuestosTrasladados = oieps

    '        Dim oimpuestosRetenciones As CFDImpuestosRetenciones
    '        oimpuestosRetenciones = New CFDImpuestosRetenciones()
    '        oimpuestosRetenciones.Impuesto = "IVA"
    '        oimpuestosRetenciones.Importe = oiva
    '        oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

    '        Dim oImpuestosTraslados As CFDImpuestosTraslados

    '        oImpuestosTraslados = New CFDImpuestosTraslados()
    '        oImpuestosTraslados.Impuesto = "IEPS"
    '        oImpuestosTraslados.Importe = oieps
    '        oImpuestosTraslados.Tasa = oTasaIeps

    '        oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

    '        oImpuestosTraslados = New CFDImpuestosTraslados()
    '        oImpuestosTraslados.Impuesto = "IVA"
    '        oImpuestosTraslados.Importe = oiva
    '        oImpuestosTraslados.Tasa = oTasaIva

    '        oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

    '        Dim oConcepto As CFDConceptos
    '        oConcepto = New CFDConceptos()
    '        oConcepto.Cantidad = "10"
    '        oConcepto.UnidadMedida = "Pieza"
    '        oConcepto.NumeroIdentificacion = "ART 350"
    '        oConcepto.Descripcion = "Escoba"
    '        oConcepto.ValorUnitario = "10.50"
    '        oConcepto.Importe = "105.000"
    '        oConcepto.CausaIVA = "1"
    '        oCFD.Conceptos.Add(oConcepto)


    '        Dim oConceptoComplemento As CFDConceptosComplemento
    '        oConceptoComplemento = New CFDConceptosComplemento()
    '        oConceptoComplemento.XMLComplemento = "<rootita>Cogito Ergo Sum</rootita>"
    '        oConcepto.Complemento = oConceptoComplemento

    '        oConcepto = New CFDConceptos()
    '        oConcepto.Cantidad = "5"
    '        oConcepto.UnidadMedida = "Pieza"
    '        oConcepto.NumeroIdentificacion = "ART 5"
    '        oConcepto.Descripcion = "Escoba"
    '        oConcepto.ValorUnitario = "5.50"
    '        oConcepto.Importe = "27.50"
    '        oConcepto.CausaIVA = "1"
    '        oCFD.Conceptos.Add(oConcepto)

    '        Dim oConceptoAduanas As CFDConceptosAduanas

    '        oConceptoAduanas = New CFDConceptosAduanas()
    '        oConceptoAduanas.Numero = "Num"
    '        oConceptoAduanas.Fecha = DateTime.Now
    '        oConceptoAduanas.Aduana = "Nuevo Laredo"

    '        oConcepto.Aduanas.Add(oConceptoAduanas)

    '        oConcepto = New CFDConceptos()
    '        oConcepto.Cantidad = "20"
    '        oConcepto.UnidadMedida = "Pieza"
    '        oConcepto.NumeroIdentificacion = "ART 352"
    '        oConcepto.Descripcion = "Trapeador"
    '        oConcepto.ValorUnitario = "11.50"
    '        oConcepto.Importe = "230.0000"
    '        oConcepto.CausaIVA = "1"

    '        Dim oConceptoParte As CFDConceptosPartes

    '        oConceptoParte = New CFDConceptosPartes()
    '        oConceptoParte.Cantidad = "10"
    '        oConceptoParte.UnidadMedida = "Componente"
    '        oConceptoParte.Descripcion = "Palo"
    '        oConceptoParte.ValorUnitario = "10"
    '        oConceptoParte.NumeroIdentificacion = "N1"
    '        oConceptoParte.Importe = "100.00"

    '        oConcepto.Partes.Add(oConceptoParte)

    '        oConceptoParte = New CFDConceptosPartes()
    '        oConceptoParte.Cantidad = "10"
    '        oConceptoParte.UnidadMedida = "Componente"
    '        oConceptoParte.Descripcion = "Estopa"
    '        oConceptoParte.ValorUnitario = "13"
    '        oConceptoParte.NumeroIdentificacion = "N2"
    '        oConceptoParte.Importe = "130.00"


    '        Dim oConceptoParteAduana As CFDConceptosPartesAduanas

    '        oConceptoParteAduana = New CFDConceptosPartesAduanas()
    '        oConceptoParteAduana.Numero = "Num"
    '        oConceptoParteAduana.Fecha = DateTime.Now
    '        oConceptoParteAduana.Aduana = "King Nosa"

    '        oConceptoParte.Aduanas.Add(oConceptoParteAduana)

    '        oConcepto.Partes.Add(oConceptoParte)

    '        oCFD.Conceptos.Add(oConcepto)

    '        If Not oCFD.Insertar(Cnx, True) Then
    '            For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
    '                MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
    '            Next
    '            Return
    '        End If


    '        MessageBox.Show("El número de cfd generadio fue : " + oCFD.IdCFD)



    '    End Using
    'End Sub

    Public Sub Usp_Ed_DameDatosFacDig(ByVal oCLV_FActura As Long, ByVal oCompania As Integer)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""

        Dim CONEXION As New SqlConnection(MiConexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDig", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@CLV_FACTURA", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_FActura
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)
        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMADEPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Function ups_NUEVAFACTGLOBALporcompania(ByVal oclvcompania As Long, ByVal oFecha As Date, ByVal oCajera As String, ByVal oSucursal As Integer) As Long
        ups_NUEVAFACTGLOBALporcompania = 0
        '@clvcompania int,@Fecha datetime,@Cajera varchar(11),@IdFactura bigint output
        oclvcompania = 1
        GloNomReporteFiscal = ""
        GloNomReporteFiscalGlobal = ""
        Dim CONEXION As New SqlConnection(MiConexion)
        Dim COMANDO As New SqlCommand("ups_NUEVAFACTGLOBALporcompania", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)
        Dim PARAMETRO1 As New SqlParameter("@clvcompania", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oclvcompania
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@Fecha", SqlDbType.DateTime)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oFecha
        COMANDO.Parameters.Add(PARAMETRO2)

        Dim PARAMETRO3 As New SqlParameter("@Cajera", SqlDbType.VarChar, 11)
        PARAMETRO3.Direction = ParameterDirection.Input
        PARAMETRO3.Value = oCajera
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@IdFactura", SqlDbType.BigInt)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@Sucursal", SqlDbType.Int)
        PARAMETRO5.Direction = ParameterDirection.Input
        PARAMETRO5.Value = oSucursal
        COMANDO.Parameters.Add(PARAMETRO5)


        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            ups_NUEVAFACTGLOBALporcompania = PARAMETRO4.Value

        Catch ex As Exception
            ups_NUEVAFACTGLOBALporcompania = 0
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Function

    Public Function USP_DameImportePorCompania(ByVal oFecha As Date, ByVal oclvcompania As Long, oSucursal As Long) As Double
        USP_DameImportePorCompania = 0
        Dim CON01 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "USP_DameImportePorCompania"
                .CommandTimeout = 0
                .Connection = CON01
                .CommandType = CommandType.StoredProcedure

                Dim PRM As New SqlParameter("@Fecha", SqlDbType.DateTime)
                PRM.Direction = ParameterDirection.Input
                PRM.Value = oFecha
                .Parameters.Add(PRM)
                '@clvcompania
                Dim PRM2 As New SqlParameter("@clvcompania", SqlDbType.BigInt)
                PRM2.Direction = ParameterDirection.Input
                PRM2.Value = oclvcompania
                .Parameters.Add(PRM2)

                '@sucursal
                Dim PRM3 As New SqlParameter("@sucursal", SqlDbType.BigInt)
                PRM3.Direction = ParameterDirection.Input
                PRM3.Value = oSucursal
                .Parameters.Add(PRM3)

                Dim reader As SqlDataReader = .ExecuteReader()

                While (reader.Read)
                    USP_DameImportePorCompania = reader.GetValue(0)
                    'EMail_TextBox.Text = reader.GetValue(2)
                End While
            End With

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CON01.Close()
        End Try
    End Function

    Public Sub Usp_Ed_DameDatosFacDigGlobal(ByVal oCLV_FActura As Long, ByVal oCompania As Integer)
        GloFORMADEPAGO = ""
        GloCONDICIONESDEPAGO = ""
        GloMOTIVODESCUENTO = ""
        GloMETODODEPAGO = ""
        GloTIPODECOMPROBANTE = ""
        GloPAGOENPARCIALIDADES = ""
        GloLUGAREXPEDICION = ""
        GloREGIMEN = ""
        GloMONEDA = ""
        GloVERSON = ""

        Dim CONEXION As New SqlConnection(MiConexion)
        Dim COMANDO As New SqlCommand("Usp_Ed_DameDatosFacDigGlobal", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)

        Dim PARAMETRO1 As New SqlParameter("@CLV_FACTURA", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_FActura
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@IDCOMPANIA", SqlDbType.Int)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oCompania
        COMANDO.Parameters.Add(PARAMETRO2)

        '@FORMADEPAGO VARCHAR(250) OUTPUT
        ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
        '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
        ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON

        Dim PARAMETRO3 As New SqlParameter("@FORMADEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO3.Direction = ParameterDirection.Output
        PARAMETRO3.Value = ""
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@CONDICIONESDEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@MOTIVODESCUENTO", SqlDbType.VarChar, 250)
        PARAMETRO5.Direction = ParameterDirection.Output
        PARAMETRO5.Value = ""
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@METODODEPAGO", SqlDbType.VarChar, 250)
        PARAMETRO6.Direction = ParameterDirection.Output
        PARAMETRO6.Value = ""
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@TIPODECOMPROBANTE", SqlDbType.VarChar, 250)
        PARAMETRO7.Direction = ParameterDirection.Output
        PARAMETRO7.Value = ""
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@PAGOENPARCIALIDADES", SqlDbType.VarChar, 250)
        PARAMETRO8.Direction = ParameterDirection.Output
        PARAMETRO8.Value = ""
        COMANDO.Parameters.Add(PARAMETRO8)

        Dim PARAMETRO9 As New SqlParameter("@LUGAREXPEDICION", SqlDbType.VarChar, 250)
        PARAMETRO9.Direction = ParameterDirection.Output
        PARAMETRO9.Value = ""
        COMANDO.Parameters.Add(PARAMETRO9)

        Dim PARAMETRO10 As New SqlParameter("@REGIMEN", SqlDbType.VarChar, 400)
        PARAMETRO10.Direction = ParameterDirection.Output
        PARAMETRO10.Value = ""
        COMANDO.Parameters.Add(PARAMETRO10)

        Dim PARAMETRO11 As New SqlParameter("@MONEDA", SqlDbType.VarChar, 15)
        PARAMETRO11.Direction = ParameterDirection.Output
        PARAMETRO11.Value = ""
        COMANDO.Parameters.Add(PARAMETRO11)

        Dim PARAMETRO12 As New SqlParameter("@VERSON", SqlDbType.VarChar, 15)
        PARAMETRO12.Direction = ParameterDirection.Output
        PARAMETRO12.Value = ""
        COMANDO.Parameters.Add(PARAMETRO12)
        Try
            '@FORMADEPAGO VARCHAR(250) OUTPUT
            ',@CONDICIONESDEPAGO VARCHAR(250)OUTPUT,@MOTIVODESCUENTO VARCHAR(250)OUTPUT,@METODODEPAGO VARCHAR(250)OUTPUT,
            '@TIPODECOMPROBANTE VARCHAR(50) OUTPUT,@PAGOENPARCIALIDADES VARCHAR(50) OUTPUT,@LUGAREXPEDICION VARCHAR(250) OUTPUT
            ',@REGIMEN VARCHAR(400) OUTPUT,@MONEDA VARCHAR(15) OUTPUT,@VERSON  

            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            GloFORMADEPAGO = PARAMETRO3.Value
            GloCONDICIONESDEPAGO = PARAMETRO4.Value
            GloMOTIVODESCUENTO = PARAMETRO5.Value
            GloMETODODEPAGO = PARAMETRO6.Value
            GloTIPODECOMPROBANTE = PARAMETRO7.Value
            GloPAGOENPARCIALIDADES = PARAMETRO8.Value
            GloLUGAREXPEDICION = PARAMETRO9.Value
            GloREGIMEN = PARAMETRO10.Value
            GloMONEDA = PARAMETRO11.Value
            GloVERSON = PARAMETRO12.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub

    Public Sub UPS_Inserta_Rel_FacturasCFD(ByVal oCLV_FActura As Long, ByVal oClv_FacturaCFD As Long, ByVal oSerie As String, ByVal oTipo As String)
        Dim CONEXION As New SqlConnection(MiConexion)
        Dim COMANDO As New SqlCommand("UPS_Inserta_Rel_FacturasCFD", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)
        Dim PARAMETRO1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oCLV_FActura
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oClv_FacturaCFD
        COMANDO.Parameters.Add(PARAMETRO2)

        Dim PARAMETRO3 As New SqlParameter("@Serie", SqlDbType.VarChar, 5)
        PARAMETRO3.Direction = ParameterDirection.Input
        PARAMETRO3.Value = oSerie
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@tipo", SqlDbType.VarChar, 1)
        PARAMETRO4.Direction = ParameterDirection.Input
        PARAMETRO4.Value = oTipo
        COMANDO.Parameters.Add(PARAMETRO4)

        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Sub


End Module

'Imports System
'Imports System.Collections.Generic
'Imports System.ComponentModel
'Imports System.Data
'Imports System.Drawing
'Imports System.Text
'Imports System.Windows.Forms
'Imports System.Xml
'Imports System.Data.SqlClient
'Imports System.Data.OleDb

'Imports MizarCFD.DAL
'Imports MizarCFD.BRL
'Imports MizarCFD.Reportes

'Module Module2

'    Public GloIDFactura As Long = 0
'    Public GloClv_FacturaCFD As Long = 0
'    Public GloMotivoCancelacionCFD As String = Nothing
'    Public Locop As Integer = 0
'    Public mIva As Decimal = 0
'    Public mipes As Decimal = 0
'    Public msubtotal As Decimal = 0
'    Public mdetalle1 As String = Nothing
'    Public locID_Compania_Mizart As String = Nothing
'    Public locID_Sucursal_Mizart As String = Nothing
'    'Public Sub Imprime_Factura_Digital(ByVal oid_CFD As Integer)
'    '    '    Using Cnx As New DAConexion("HL", "sa", "sa")
'    '    '        Dim oRreportes As New MizarCFD.Reportes.CFD
'    '    '        Dim oCFD As New MizarCFD.BRL.CFD
'    '    '        oCFD.IdCFD = "1"
'    '    '        oCFD.Consultar(Cnx)
'    '    '        Dim document As New System.Xml.XmlDocument
'    '    '        oCFD.ObtenerXML()
'    '    '        document.Value = oCFD.CadenaOriginal
'    '    '    End Using

'    'End Sub

'    Public Function Existe_Checalo(ByVal oSql As String) As Boolean
'        Existe_Checalo = False
'        Using Cnx As New DAConexion("HL", "sa", "sa")
'            Cnx.DbCommand.CommandText = oSql
'            Cnx.DbCommand.CommandTimeout = 0
'            Cnx.DbCommand.CommandType = CommandType.Text
'            'Cnx.DbConnection.Open()
'            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
'            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
'            'COMANDO.CommandType = CommandType.Text
'            'Try
'            '    CONEXION.Open()
'            Dim reader As OleDbDataReader = Cnx.DbCommand.ExecuteReader
'            '    'Recorremos los Cargos obtenidos desde el Procedimiento
'            While (reader.Read())
'                If Len(reader(0).ToString) > 0 Then
'                    Existe_Checalo = True
'                End If
'            End While
'            'Cnx.DbConnection.Close()
'            'Catch ex As Exception
'            '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'            'Finally
'            '    CONEXION.Close()
'            '    CONEXION.Dispose()
'            'End Try
'        End Using
'    End Function

'    Public Sub Alta_Datosfiscales(ByVal oRazonSocial As String,
'        ByVal oRFC As String,
'        ByVal oCalle As String,
'        ByVal oClaveAsociado As String,
'        ByVal oCodigoPostal As String,
'        ByVal oColonia As String,
'        ByVal oEMail As String,
'        ByVal oEntreCalles As String,
'        ByVal oEstado As String,
'        ByVal oIdAsociado As String,
'        ByVal oLocalidad As String,
'        ByVal oMunicipio As String,
'        ByVal oNumeroExterior As String,
'            ByVal oPais As String)



'        If String.IsNullOrEmpty(oRazonSocial) = True Then oRazonSocial = ""
'        If String.IsNullOrEmpty(oRFC) = True Then oRFC = ""
'        If String.IsNullOrEmpty(oCalle) = True Then oCalle = ""
'        If String.IsNullOrEmpty(oClaveAsociado) = True Then oClaveAsociado = ""
'        If String.IsNullOrEmpty(oCodigoPostal) = True Then oCodigoPostal = ""
'        If String.IsNullOrEmpty(oColonia) = True Then oColonia = ""
'        If String.IsNullOrEmpty(oEMail) = True Then oEMail = ""
'        If String.IsNullOrEmpty(oEntreCalles) = True Then oEntreCalles = ""
'        If String.IsNullOrEmpty(oEstado) = True Then oEstado = ""
'        If String.IsNullOrEmpty(oIdAsociado) = True Then oIdAsociado = ""
'        If String.IsNullOrEmpty(oLocalidad) = True Then oLocalidad = ""
'        If String.IsNullOrEmpty(oMunicipio) = True Then oMunicipio = ""
'        If String.IsNullOrEmpty(oNumeroExterior) = True Then oNumeroExterior = ""
'        If String.IsNullOrEmpty(oPais) = True Then oPais = ""

'        If Existe_Checalo(" Select * from asociados  where id_asociado = " + oIdAsociado) = False Then

'            Dim oAsociados As MizarCFD.BRL.Asociados
'            Using Cnx As New DAConexion("HL", "sa", "sa")

'                oAsociados = New MizarCFD.BRL.Asociados
'                oAsociados.Inicializar()
'                oAsociados.RazonSocial = oRazonSocial
'                oAsociados.RFC = oRFC

'                oAsociados.ClaveAsociado = oIdAsociado

'                oAsociados.IdAsociado = oIdAsociado

'                'oAsociados.Calle = oCalle
'                'oAsociados.CodigoPostal = oCodigoPostal
'                'oAsociados.Colonia = oColonia
'                'oAsociados.EMail = oEMail
'                'oAsociados.EntreCalles = oEntreCalles
'                'oAsociados.Estado = oEstado
'                'oAsociados.Localidad = oLocalidad
'                'oAsociados.Municipio = oMunicipio
'                'oAsociados.NumeroExterior = oNumeroExterior
'                'oAsociados.Pais = oPais
'                oAsociados.IdTipoAsociado = Asociados.TipoAsociado.Cliente
'                If oAsociados.Validar(Cnx, TipoAfectacionBD.Insertar) = True Then
'                    If Not oAsociados.Insertar(Cnx) Then
'                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
'                            MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                        Next
'                        Return
'                    End If
'                Else
'                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
'                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                    Next
'                    Return
'                End If





'            End Using
'        Else

'        End If

'    End Sub

'    Public Sub Graba_Factura_Digital_Global(ByVal oClv_Factura As Long, ByVal oId_Compania As String, ByVal oId_Sucursal As String)
'        Dim oTotalConPuntos As String = Nothing
'        Dim oSubTotal As String = Nothing
'        Dim oTotalSinPuntos As String = Nothing
'        Dim oDescuento As String = Nothing
'        Dim oiva As String = Nothing
'        Dim oieps As String = Nothing
'        Dim oTasaIva As String = Nothing
'        Dim oTasaIeps As String = Nothing
'        Dim oFecha As String = Nothing
'        Dim oTotalImpuestos As String = Nothing

'        Dim mfecha As DateTime

'        BaseII.limpiaParametros()
'        BaseII.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
'        BaseII.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL")
'        mfecha = DateTime.Parse(BaseII.dicoPar("@FECHA").ToString())

'        Dim CONEXION As New SqlConnection(MiConexion)
'        Dim COMANDO As New SqlCommand("DameFacDig_Parte_1_Global", CONEXION)
'        COMANDO.CommandType = CommandType.StoredProcedure

'        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'        parametro.Direction = ParameterDirection.Input
'        parametro.Value = oClv_Factura
'        COMANDO.Parameters.Add(parametro)

'        Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
'        parametro1.Direction = ParameterDirection.Output
'        parametro1.Value = 0
'        COMANDO.Parameters.Add(parametro1)

'        Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
'        parametro2.Direction = ParameterDirection.Output
'        parametro2.Value = 0
'        COMANDO.Parameters.Add(parametro2)

'        Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
'        parametro3.Direction = ParameterDirection.Output
'        parametro3.Value = 0
'        COMANDO.Parameters.Add(parametro3)

'        Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
'        parametro4.Direction = ParameterDirection.Output
'        parametro4.Value = 0
'        COMANDO.Parameters.Add(parametro4)

'        Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
'        parametro5.Direction = ParameterDirection.Output
'        parametro5.Value = 0
'        COMANDO.Parameters.Add(parametro5)

'        Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
'        parametro6.Direction = ParameterDirection.Output
'        parametro6.Value = 0
'        COMANDO.Parameters.Add(parametro6)

'        Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
'        parametro7.Direction = ParameterDirection.Output
'        parametro7.Value = 0
'        COMANDO.Parameters.Add(parametro7)

'        Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
'        parametro8.Direction = ParameterDirection.Output
'        parametro8.Value = 0
'        COMANDO.Parameters.Add(parametro8)

'        Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
'        parametro9.Direction = ParameterDirection.Output
'        parametro9.Value = 0
'        COMANDO.Parameters.Add(parametro9)

'        Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
'        parametro10.Direction = ParameterDirection.Output
'        parametro10.Value = 0
'        COMANDO.Parameters.Add(parametro10)

'        Try
'            CONEXION.Open()
'            Dim i As Integer = COMANDO.ExecuteNonQuery
'            oTotalConPuntos = CStr(parametro1.Value)
'            oSubTotal = CStr(parametro2.Value)
'            oTotalSinPuntos = CStr(parametro3.Value)
'            oDescuento = CStr(parametro4.Value)
'            oiva = CStr(parametro5.Value)
'            oieps = CStr(parametro6.Value)
'            oTasaIva = CStr(parametro7.Value)
'            oTasaIeps = CStr(parametro8.Value)
'            oFecha = CStr(parametro9.Value)
'            oTotalImpuestos = CStr(parametro10.Value)
'            'Cnx.DbConnection.Close()
'        Catch ex As Exception
'            MsgBox(ex.Message & " Parte 1 " & ex.Source, MsgBoxStyle.Exclamation)
'        Finally
'            CONEXION.Close()
'            CONEXION.Dispose()
'        End Try


'        Dim oCFD As MizarCFD.BRL.CFD

'        Using Cnx As New DAConexion("HL", "sa", "sa")
'            Try


'                oCFD = New MizarCFD.BRL.CFD
'                oCFD.Inicializar()
'                oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
'                If oId_Sucursal = "0" Then oId_Sucursal = ""
'                oCFD.PrepararDatosEmisorPorId(Cnx, oId_Compania, oId_Sucursal)
'                oCFD.PrepararDatosReceptorPorId(Cnx, "22995", 1, 0)

'                oCFD.Version = "2.0"
'                oCFD.IdMoneda = "1" '//1 = Pesos , Pesos = 2 USD
'                'oFecha
'                oCFD.Fecha = mfecha
'                oCFD.FormaDePago = "Forma de Pago"
'                'oCFD.Certificado = ""
'                oCFD.CondicionesDePago = "Condiciones de Pago"
'                oCFD.SubTotal = oSubTotal
'                oCFD.Descuento = oDescuento
'                oCFD.MotivoDescuento = "Pronto Pago"
'                oCFD.Total = oTotalSinPuntos
'                oCFD.MetodoDePago = "Efectivo"
'                oCFD.TipoDeComprobante = "ingreso"

'                'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
'                'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

'                oCFD.Impuestos.TotalImpuestosRetenidos = 0
'                oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
'                'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
'                'oimpuestosRetenciones = New CFDImpuestosRetenciones()
'                'oimpuestosRetenciones.Impuesto = "IVA"
'                'oimpuestosRetenciones.Importe = oiva
'                'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

'                Dim oImpuestosTraslados As CFDImpuestosTraslados

'                oImpuestosTraslados = New CFDImpuestosTraslados()
'                oImpuestosTraslados.Impuesto = "IEPS"
'                oImpuestosTraslados.Importe = oieps
'                oImpuestosTraslados.Tasa = oTasaIeps

'                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'                oImpuestosTraslados = New CFDImpuestosTraslados()
'                oImpuestosTraslados.Impuesto = "IVA"
'                oImpuestosTraslados.Importe = oiva
'                oImpuestosTraslados.Tasa = oTasaIva

'                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'                Dim oConcepto As CFDConceptos

'                Dim CONEXION2 As New SqlConnection(MiConexion)
'                Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2_Global " + CStr(oClv_Factura), CONEXION2)
'                COMANDO2.CommandType = CommandType.Text
'                Try
'                    CONEXION2.Open()
'                    Dim reader As SqlDataReader = COMANDO2.ExecuteReader
'                    '    'Recorremos los Cargos obtenidos desde el Procedimiento
'                    While (reader.Read())
'                        oConcepto = New CFDConceptos()
'                        oConcepto.Cantidad = reader(0).ToString
'                        oConcepto.UnidadMedida = ""
'                        oConcepto.NumeroIdentificacion = reader(1).ToString
'                        oConcepto.Descripcion = reader(2).ToString
'                        oConcepto.ValorUnitario = reader(3).ToString
'                        oConcepto.Importe = reader(3).ToString
'                        'oConcepto.CausaIVA = "1"
'                        oConcepto.TrasladarIVA = "1"
'                        oCFD.Conceptos.Add(oConcepto)
'                    End While
'                    'Cnx.DbConnection.Close()
'                Catch ex As Exception
'                    MsgBox(ex.Message & " Parte 2 " & ex.Source, MsgBoxStyle.Exclamation)
'                Finally
'                    CONEXION2.Close()
'                    CONEXION2.Dispose()
'                End Try

'                If Not oCFD.Insertar(Cnx, True) Then
'                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
'                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                    Next
'                    Return
'                End If


'                GloClv_FacturaCFD = oCFD.IdCFD
'                UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "G")

'            Catch ex As Exception
'                MsgBox(ex.Message & " Parte 3 " & ex.Source, MsgBoxStyle.Exclamation)
'            Finally
'                CONEXION.Close()
'                CONEXION.Dispose()
'            End Try

'        End Using
'    End Sub

'    Public Sub Graba_Factura_Digital(ByVal oClv_Factura As Long, ByVal oIden As Integer)
'        Dim oTotalConPuntos As String = Nothing
'        Dim oSubTotal As String = Nothing
'        Dim oTotalSinPuntos As String = Nothing
'        Dim oDescuento As String = Nothing
'        Dim oiva As String = Nothing
'        Dim oieps As String = Nothing
'        Dim oTasaIva As String = Nothing
'        Dim oTasaIeps As String = Nothing
'        Dim oFecha As String = Nothing
'        Dim oTotalImpuestos As String = Nothing

'        ''
'        Dim mfecha As DateTime
'        Dim con As New SqlConnection(MiConexion)
'        Dim com As New SqlCommand("DAMEFECHADELSERVIDOR", con)
'        com.CommandType = CommandType.StoredProcedure
'        'Dame la fecha del Servidor
'        Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
'        prmFechaObtebida.Direction = ParameterDirection.Output
'        prmFechaObtebida.Value = ""
'        com.Parameters.Add(prmFechaObtebida)
'        Try
'            con.Open()
'            com.ExecuteNonQuery()
'            mfecha = prmFechaObtebida.Value
'        Catch ex As Exception
'            MsgBox("Error: No se pudo obtener la fecha válida del Servidor.", MsgBoxStyle.Critical)
'            MsgBox("Descripción:  Procedimiento [DAMEFECHADELSERVIDOR] // Función [DameFechaServidor] // Form: FrmAdelantarCargos", MsgBoxStyle.Information)
'            MsgBox(ex.Message)
'        Finally
'            con.Close()
'        End Try
'        ''

'        BaseII.limpiaParametros()
'        BaseII.CreateMyParameter("@FECHA", ParameterDirection.Output, SqlDbType.DateTime)
'        BaseII.ProcedimientoOutPut("DAMEFECHADELSERVIDORFACTURADIGITAL")
'        mfecha = DateTime.Parse(BaseII.dicoPar("@FECHA").ToString())


'        Dim CONEXION As New SqlConnection(MiConexion)
'        Dim COMANDO As New SqlCommand("DameFacDig_Parte_1", CONEXION)
'        COMANDO.CommandType = CommandType.StoredProcedure

'        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'        parametro.Direction = ParameterDirection.Input
'        parametro.Value = oClv_Factura
'        COMANDO.Parameters.Add(parametro)

'        Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
'        parametro1.Direction = ParameterDirection.Output
'        parametro1.Value = 0
'        COMANDO.Parameters.Add(parametro1)

'        Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
'        parametro2.Direction = ParameterDirection.Output
'        parametro2.Value = 0
'        COMANDO.Parameters.Add(parametro2)

'        Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
'        parametro3.Direction = ParameterDirection.Output
'        parametro3.Value = 0
'        COMANDO.Parameters.Add(parametro3)

'        Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
'        parametro4.Direction = ParameterDirection.Output
'        parametro4.Value = 0
'        COMANDO.Parameters.Add(parametro4)

'        Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
'        parametro5.Direction = ParameterDirection.Output
'        parametro5.Value = 0
'        COMANDO.Parameters.Add(parametro5)

'        Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
'        parametro6.Direction = ParameterDirection.Output
'        parametro6.Value = 0
'        COMANDO.Parameters.Add(parametro6)

'        Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
'        parametro7.Direction = ParameterDirection.Output
'        parametro7.Value = 0
'        COMANDO.Parameters.Add(parametro7)

'        Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
'        parametro8.Direction = ParameterDirection.Output
'        parametro8.Value = 0
'        COMANDO.Parameters.Add(parametro8)

'        Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
'        parametro9.Direction = ParameterDirection.Output
'        parametro9.Value = 0
'        COMANDO.Parameters.Add(parametro9)

'        Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
'        parametro10.Direction = ParameterDirection.Output
'        parametro10.Value = 0
'        COMANDO.Parameters.Add(parametro10)

'        Try
'            CONEXION.Open()
'            Dim i As Integer = COMANDO.ExecuteNonQuery
'            oTotalConPuntos = CStr(parametro1.Value)
'            oSubTotal = CStr(parametro2.Value)
'            oTotalSinPuntos = CStr(parametro3.Value)
'            oDescuento = CStr(parametro4.Value)
'            oiva = CStr(parametro5.Value)
'            oieps = CStr(parametro6.Value)
'            oTasaIva = CStr(parametro7.Value)
'            oTasaIeps = CStr(parametro8.Value)
'            oFecha = CStr(parametro9.Value)
'            oTotalImpuestos = CStr(parametro10.Value)
'            'Cnx.DbConnection.Close()
'        Catch ex As Exception
'            MsgBox(ex.Message & " Parte 1 " & ex.Source, MsgBoxStyle.Exclamation)
'        Finally
'            CONEXION.Close()
'            CONEXION.Dispose()
'        End Try


'        Dim oCFD As MizarCFD.BRL.CFD

'        Using Cnx As New DAConexion("HL", "sa", "sa")
'            Try
'                'oIden = "1"

'                oCFD = New MizarCFD.BRL.CFD
'                oCFD.Inicializar()
'                oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision

'                oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
'                oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oIden), 1, 0)

'                oCFD.Version = "2.0"
'                oCFD.IdMoneda = "1" '//1 = Pesos , Pesos = 2 USD
'                'oFecha
'                oCFD.Fecha = mfecha '//Date.Now
'                oCFD.FormaDePago = "Forma de Pago"
'                'oCFD.Certificado = ""
'                oCFD.CondicionesDePago = "Condiciones de Pago"
'                oCFD.SubTotal = oSubTotal
'                oCFD.Descuento = oDescuento
'                oCFD.MotivoDescuento = "Pronto Pago"
'                oCFD.Total = oTotalSinPuntos
'                oCFD.MetodoDePago = "Efectivo"
'                oCFD.TipoDeComprobante = "ingreso"

'                'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
'                'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

'                oCFD.Impuestos.TotalImpuestosRetenidos = 0
'                oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
'                'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
'                'oimpuestosRetenciones = New CFDImpuestosRetenciones()
'                'oimpuestosRetenciones.Impuesto = "IVA"
'                'oimpuestosRetenciones.Importe = oiva
'                'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

'                Dim oImpuestosTraslados As CFDImpuestosTraslados

'                oImpuestosTraslados = New CFDImpuestosTraslados()
'                oImpuestosTraslados.Impuesto = "IEPS"
'                oImpuestosTraslados.Importe = oieps
'                oImpuestosTraslados.Tasa = oTasaIeps

'                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'                oImpuestosTraslados = New CFDImpuestosTraslados()
'                oImpuestosTraslados.Impuesto = "IVA"
'                oImpuestosTraslados.Importe = oiva
'                oImpuestosTraslados.Tasa = oTasaIva

'                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'                Dim oConcepto As CFDConceptos

'                Dim CONEXION2 As New SqlConnection(MiConexion)
'                Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2 " + CStr(oClv_Factura), CONEXION2)
'                COMANDO2.CommandType = CommandType.Text
'                Try
'                    CONEXION2.Open()
'                    Dim reader As SqlDataReader = COMANDO2.ExecuteReader
'                    '    'Recorremos los Cargos obtenidos desde el Procedimiento
'                    While (reader.Read())
'                        oConcepto = New CFDConceptos()
'                        oConcepto.Cantidad = reader(0).ToString
'                        oConcepto.UnidadMedida = ""
'                        oConcepto.NumeroIdentificacion = reader(1).ToString
'                        oConcepto.Descripcion = reader(2).ToString
'                        oConcepto.ValorUnitario = reader(3).ToString
'                        oConcepto.Importe = reader(3).ToString
'                        oConcepto.TrasladarIVA = "1"
'                        oCFD.Conceptos.Add(oConcepto)
'                    End While
'                    'Cnx.DbConnection.Close()
'                Catch ex As Exception
'                    MsgBox(ex.Message & " Parte 2 " & ex.Source, MsgBoxStyle.Exclamation)
'                Finally
'                    CONEXION2.Close()
'                    CONEXION2.Dispose()
'                End Try

'                'If Not oCFD.Insertar(Cnx, True) Then
'                '    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
'                '        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                '    Next
'                '    Return
'                'End If
'                If Not oCFD.Insertar(Cnx, True) Then
'                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
'                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                    Next
'                    Return
'                End If




'                GloClv_FacturaCFD = oCFD.IdCFD
'                UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "N")

'            Catch ex As Exception
'                MsgBox(ex.Message & " Parte 3 " & ex.Source, MsgBoxStyle.Exclamation)
'            Finally
'                CONEXION.Close()
'                CONEXION.Dispose()
'            End Try

'        End Using
'    End Sub

'    Public Sub Cancelacion_FacturaCFD(ByVal oClv_FacturaCFD As Long)
'        Dim ofecha As Date
'        Dim con As New SqlConnection(MiConexion)
'        Dim com As New SqlCommand("DAMEFECHADELSERVIDOR", con)
'        com.CommandType = CommandType.StoredProcedure

'        'Dame la fecha del Servidor
'        Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
'        prmFechaObtebida.Direction = ParameterDirection.Output
'        prmFechaObtebida.Value = ""
'        com.Parameters.Add(prmFechaObtebida)

'        Try
'            con.Open()
'            com.ExecuteNonQuery()

'            ofecha = prmFechaObtebida.Value

'        Catch ex As Exception
'            MsgBox("Error: No se pudo obtener la fecha válida del Servidor.", MsgBoxStyle.Critical)
'            MsgBox("Descripción:  Procedimiento [DAMEFECHADELSERVIDOR] // Función [DameFechaServidor] // Form: FrmAdelantarCargos", MsgBoxStyle.Information)
'            MsgBox(ex.Message)
'        Finally
'            con.Close()
'        End Try

'        Dim oCFD As MizarCFD.BRL.CFD
'        Using Cnx As New DAConexion("HL", "sa", "sa")
'            oCFD = New MizarCFD.BRL.CFD
'            oCFD.Inicializar()
'            oCFD.IdCFD = CStr(oClv_FacturaCFD)
'            If Len(GloMotivoCancelacionCFD) = 0 Then GloMotivoCancelacionCFD = "CANCELACION"
'            oCFD.MotivoCancelacion = GloMotivoCancelacionCFD
'            oCFD.FechaCancelacion = ofecha
'            oCFD.EstaCancelado = 1
'            If Not oCFD.ModificarEstatusCancelado(Cnx) = False Then
'                For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
'                    MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                Next
'                Return
'            End If
'        End Using
'    End Sub

'    Public Sub Dame_Detalle1Global(ByVal oClv_FacturaCFD As Long)
'        mIva = 0
'        mipes = 0
'        msubtotal = 0
'        mdetalle1 = ""

'        Dim con As New SqlConnection(MiConexion)
'        Dim com As New SqlCommand("Dame_Detalle1", con)
'        com.CommandType = CommandType.StoredProcedure
'        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
'        'Dame la fecha del Servidor
'        Dim prm1 As New SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt)
'        prm1.Direction = ParameterDirection.Input
'        prm1.Value = oClv_FacturaCFD
'        com.Parameters.Add(prm1)

'        Dim prm2 As New SqlParameter("@Iva", SqlDbType.Money)
'        prm2.Direction = ParameterDirection.Output
'        prm2.Value = 0
'        com.Parameters.Add(prm2)

'        Dim prm3 As New SqlParameter("@Ieps", SqlDbType.Money)
'        prm3.Direction = ParameterDirection.Output
'        prm3.Value = ""
'        com.Parameters.Add(prm3)

'        Dim prm4 As New SqlParameter("@Subtotal", SqlDbType.Money)
'        prm4.Direction = ParameterDirection.Output
'        prm4.Value = ""
'        com.Parameters.Add(prm4)

'        Dim prm5 As New SqlParameter("@Detalle1", SqlDbType.VarChar, 8000)
'        prm5.Direction = ParameterDirection.Output
'        prm5.Value = ""
'        com.Parameters.Add(prm5)

'        Try
'            con.Open()
'            com.ExecuteNonQuery()
'            mIva = prm2.Value
'            mipes = prm3.Value
'            msubtotal = prm4.Value
'            mdetalle1 = prm5.Value

'        Catch ex As Exception
'            MsgBox(ex.Message)
'        Finally
'            con.Close()
'        End Try
'    End Sub

'    Public Sub Dime_Aque_Compania_Facturarle(ByVal oClv_Factura As Long)
'       locID_Compania_Mizart = ""
'        locID_Sucursal_Mizart = ""

'        Dim con As New SqlConnection(MiConexion)
'        Dim com As New SqlCommand("Dime_Aque_Compania_Facturarle", con)
'        com.CommandType = CommandType.StoredProcedure
'        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
'        'Dame la fecha del Servidor
'        Dim prm1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'        prm1.Direction = ParameterDirection.Input
'        prm1.Value = oClv_Factura
'        com.Parameters.Add(prm1)

'        Dim prm2 As New SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50)
'        prm2.Direction = ParameterDirection.Output
'        prm2.Value = 0
'        com.Parameters.Add(prm2)

'        Dim prm3 As New SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50)
'        prm3.Direction = ParameterDirection.Output
'        prm3.Value = ""
'        com.Parameters.Add(prm3)


'        Try
'            con.Open()
'            com.ExecuteNonQuery()
'            locID_Compania_Mizart = prm2.Value
'            locID_Sucursal_Mizart = prm3.Value

'        Catch ex As Exception
'            MsgBox(ex.Message)
'        Finally
'            con.Close()
'        End Try
'    End Sub

'    Public Function Dame_Cantidad_Con_Letra(ByVal oCantidad As Long) As String
'        Dame_Cantidad_Con_Letra = ""

'        Dim con As New SqlConnection(MiConexion)
'        Dim com As New SqlCommand("DameCantidadALetra", con)
'        com.CommandType = CommandType.StoredProcedure
'        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
'        'Dame la fecha del Servidor
'        If IsNumeric(oCantidad) = False Then oCantidad = 0
'        Dim prm1 As New SqlParameter("@Numero", SqlDbType.Decimal)
'        prm1.Direction = ParameterDirection.Input
'        prm1.Value = oCantidad
'        com.Parameters.Add(prm1)

'        Dim prm2 As New SqlParameter("@moneda", SqlDbType.Int)
'        prm2.Direction = ParameterDirection.Input
'        prm2.Value = 0
'        com.Parameters.Add(prm2)

'        Dim prm3 As New SqlParameter("@letra", SqlDbType.VarChar, 400)
'        prm3.Direction = ParameterDirection.Output
'        prm3.Value = ""
'        com.Parameters.Add(prm3)

'        Try
'            con.Open()
'            com.ExecuteNonQuery()

'            Dame_Cantidad_Con_Letra = prm3.Value

'        Catch ex As Exception
'            MsgBox(ex.Message)
'        Finally
'            con.Close()
'        End Try
'    End Function

'    Public Sub DameId_FacturaCDF(ByVal oClv_Factura As Long, ByVal oTipo As String)
'        GloClv_FacturaCFD = 0
'        Dim CONEXION As New SqlConnection(MiConexion)
'        Dim COMANDO As New SqlCommand("upsDameFacDig_Clv_FacturaCDF", CONEXION)
'        COMANDO.CommandType = CommandType.StoredProcedure

'        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'        parametro.Direction = ParameterDirection.Input
'        parametro.Value = oClv_Factura
'        COMANDO.Parameters.Add(parametro)

'        Dim parametro1 As New SqlParameter("@Clv_FacturaCDF", SqlDbType.Money)
'        parametro1.Direction = ParameterDirection.Output
'        parametro1.Value = 0
'        COMANDO.Parameters.Add(parametro1)

'        Dim parametro2 As New SqlParameter("@tipo", SqlDbType.VarChar, 1)
'        parametro2.Direction = ParameterDirection.Input
'        parametro2.Value = oTipo
'        COMANDO.Parameters.Add(parametro2)

'        Try
'            CONEXION.Open()
'            Dim i As Integer = COMANDO.ExecuteNonQuery
'            GloClv_FacturaCFD = CStr(parametro1.Value)

'            'Cnx.DbConnection.Close()
'        Catch ex As Exception
'            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'        Finally
'            CONEXION.Close()
'            CONEXION.Dispose()
'        End Try
'    End Sub

'    Public Function BusFacFiscalOledb(ByVal oClv_Factura As Long) As Integer
'        BusFacFiscalOledb = 0
'        Dim CONEXION As New SqlConnection(MiConexion)
'        Dim COMANDO As New SqlCommand("BusFacFiscal", CONEXION)
'        COMANDO.CommandType = CommandType.StoredProcedure

'        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'        parametro.Direction = ParameterDirection.Input
'        parametro.Value = oClv_Factura
'        COMANDO.Parameters.Add(parametro)

'        Dim parametro1 As New SqlParameter("@ident", SqlDbType.Int)
'        parametro1.Direction = ParameterDirection.Output
'        parametro1.Value = 0
'        COMANDO.Parameters.Add(parametro1)

'        Try
'            CONEXION.Open()
'            Dim i As Integer = COMANDO.ExecuteNonQuery
'            BusFacFiscalOledb = CStr(parametro1.Value)

'            'Cnx.DbConnection.Close()
'        Catch ex As Exception
'            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'        Finally
'            CONEXION.Close()
'            CONEXION.Dispose()
'        End Try
'    End Function





'    'Public Sub Graba_Factura_Digital(ByVal oClv_Factura As Long)
'    '    Dim oTotalConPuntos As String = Nothing
'    '    Dim oSubTotal As String = Nothing
'    '    Dim oTotalSinPuntos As String = Nothing
'    '    Dim oDescuento As String = Nothing
'    '    Dim oiva As String = Nothing
'    '    Dim oieps As String = Nothing
'    '    Dim oTasaIva As String = Nothing
'    '    Dim oTasaIeps As String = Nothing
'    '    Dim oFecha As String = Nothing

'    '    Dim CONEXION As New SqlConnection(MiConexion)
'    '    Dim COMANDO As New SqlCommand("DameFacDig_Parte_1", CONEXION)
'    '    COMANDO.CommandType = CommandType.StoredProcedure

'    '    Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'    '    parametro.Direction = ParameterDirection.Input
'    '    parametro.Value = oClv_Factura
'    '    COMANDO.Parameters.Add(parametro)

'    '    Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
'    '    parametro1.Direction = ParameterDirection.Output
'    '    parametro1.Value = 0
'    '    COMANDO.Parameters.Add(parametro1)

'    '    Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
'    '    parametro2.Direction = ParameterDirection.Output
'    '    parametro2.Value = 0
'    '    COMANDO.Parameters.Add(parametro2)

'    '    Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
'    '    parametro3.Direction = ParameterDirection.Output
'    '    parametro3.Value = 0
'    '    COMANDO.Parameters.Add(parametro3)

'    '    Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
'    '    parametro4.Direction = ParameterDirection.Output
'    '    parametro4.Value = 0
'    '    COMANDO.Parameters.Add(parametro4)

'    '    Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
'    '    parametro5.Direction = ParameterDirection.Output
'    '    parametro5.Value = 0
'    '    COMANDO.Parameters.Add(parametro5)

'    '    Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
'    '    parametro6.Direction = ParameterDirection.Output
'    '    parametro6.Value = 0
'    '    COMANDO.Parameters.Add(parametro6)

'    '    Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
'    '    parametro7.Direction = ParameterDirection.Output
'    '    parametro7.Value = 0
'    '    COMANDO.Parameters.Add(parametro7)

'    '    Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
'    '    parametro8.Direction = ParameterDirection.Output
'    '    parametro8.Value = 0
'    '    COMANDO.Parameters.Add(parametro8)

'    '    Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
'    '    parametro9.Direction = ParameterDirection.Output
'    '    parametro9.Value = 0
'    '    COMANDO.Parameters.Add(parametro9)


'    '    Try
'    '        CONEXION.Open()
'    '        Dim i As Integer = COMANDO.ExecuteNonQuery
'    '        oTotalConPuntos = CStr(parametro1.Value)
'    '        oSubTotal = CStr(parametro2.Value)
'    '        oTotalSinPuntos = CStr(parametro3.Value)
'    '        oDescuento = CStr(parametro4.Value)
'    '        oiva = CStr(parametro5.Value)
'    '        oieps = CStr(parametro6.Value)
'    '        oTasaIva = CStr(parametro7.Value)
'    '        oTasaIeps = CStr(parametro8.Value)
'    '        oFecha = CStr(parametro9.Value)
'    '        'Cnx.DbConnection.Close()
'    '    Catch ex As Exception
'    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'    '    Finally
'    '        CONEXION.Close()
'    '        CONEXION.Dispose()
'    '    End Try


'    '    Dim oCFD As MizarCFD.BRL.CFD

'    '    Using Cnx As New DAConexion("HL", "sa", "sa")
'    '        oCFD = New MizarCFD.BRL.CFD
'    '        oCFD.Inicializar()
'    '        oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision

'    '        oCFD.PrepararDatosEmisorPorId(Cnx, "1", "1")
'    '        oCFD.PrepararDatosReceptorPorId(Cnx, "3")

'    '        oCFD.Version = "2.0"
'    '        oCFD.IdMoneda = "1" '//1 = Pesos , Pesos = 2 USD
'    '        oCFD.Fecha = oFecha
'    '        oCFD.FormaDePago = "Forma de Pago"
'    '        oCFD.Certificado = ""
'    '        oCFD.CondicionesDePago = "Condiciones de Pago"
'    '        oCFD.SubTotal = oSubTotal
'    '        oCFD.Descuento = oDescuento
'    '        oCFD.MotivoDescuento = "Pronto Pago"
'    '        oCFD.Total = oTotalSinPuntos
'    '        oCFD.MetodoDePago = "Efectivo"
'    '        oCFD.TipoDeComprobante = "ingreso"

'    '        oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
'    '        oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

'    '        oCFD.Impuestos.TotalImpuestosRetenidos = oiva
'    '        oCFD.Impuestos.TotalImpuestosTrasladados = oieps

'    '        Dim oimpuestosRetenciones As CFDImpuestosRetenciones
'    '        oimpuestosRetenciones = New CFDImpuestosRetenciones()
'    '        oimpuestosRetenciones.Impuesto = "IVA"
'    '        oimpuestosRetenciones.Importe = oiva
'    '        oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

'    '        Dim oImpuestosTraslados As CFDImpuestosTraslados

'    '        oImpuestosTraslados = New CFDImpuestosTraslados()
'    '        oImpuestosTraslados.Impuesto = "IEPS"
'    '        oImpuestosTraslados.Importe = oieps
'    '        oImpuestosTraslados.Tasa = oTasaIeps

'    '        oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'    '        oImpuestosTraslados = New CFDImpuestosTraslados()
'    '        oImpuestosTraslados.Impuesto = "IVA"
'    '        oImpuestosTraslados.Importe = oiva
'    '        oImpuestosTraslados.Tasa = oTasaIva

'    '        oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'    '        Dim oConcepto As CFDConceptos
'    '        oConcepto = New CFDConceptos()
'    '        oConcepto.Cantidad = "10"
'    '        oConcepto.UnidadMedida = "Pieza"
'    '        oConcepto.NumeroIdentificacion = "ART 350"
'    '        oConcepto.Descripcion = "Escoba"
'    '        oConcepto.ValorUnitario = "10.50"
'    '        oConcepto.Importe = "105.000"
'    '        oConcepto.CausaIVA = "1"
'    '        oCFD.Conceptos.Add(oConcepto)


'    '        Dim oConceptoComplemento As CFDConceptosComplemento
'    '        oConceptoComplemento = New CFDConceptosComplemento()
'    '        oConceptoComplemento.XMLComplemento = "<rootita>Cogito Ergo Sum</rootita>"
'    '        oConcepto.Complemento = oConceptoComplemento

'    '        oConcepto = New CFDConceptos()
'    '        oConcepto.Cantidad = "5"
'    '        oConcepto.UnidadMedida = "Pieza"
'    '        oConcepto.NumeroIdentificacion = "ART 5"
'    '        oConcepto.Descripcion = "Escoba"
'    '        oConcepto.ValorUnitario = "5.50"
'    '        oConcepto.Importe = "27.50"
'    '        oConcepto.CausaIVA = "1"
'    '        oCFD.Conceptos.Add(oConcepto)

'    '        Dim oConceptoAduanas As CFDConceptosAduanas

'    '        oConceptoAduanas = New CFDConceptosAduanas()
'    '        oConceptoAduanas.Numero = "Num"
'    '        oConceptoAduanas.Fecha = DateTime.Now
'    '        oConceptoAduanas.Aduana = "Nuevo Laredo"

'    '        oConcepto.Aduanas.Add(oConceptoAduanas)

'    '        oConcepto = New CFDConceptos()
'    '        oConcepto.Cantidad = "20"
'    '        oConcepto.UnidadMedida = "Pieza"
'    '        oConcepto.NumeroIdentificacion = "ART 352"
'    '        oConcepto.Descripcion = "Trapeador"
'    '        oConcepto.ValorUnitario = "11.50"
'    '        oConcepto.Importe = "230.0000"
'    '        oConcepto.CausaIVA = "1"

'    '        Dim oConceptoParte As CFDConceptosPartes

'    '        oConceptoParte = New CFDConceptosPartes()
'    '        oConceptoParte.Cantidad = "10"
'    '        oConceptoParte.UnidadMedida = "Componente"
'    '        oConceptoParte.Descripcion = "Palo"
'    '        oConceptoParte.ValorUnitario = "10"
'    '        oConceptoParte.NumeroIdentificacion = "N1"
'    '        oConceptoParte.Importe = "100.00"

'    '        oConcepto.Partes.Add(oConceptoParte)

'    '        oConceptoParte = New CFDConceptosPartes()
'    '        oConceptoParte.Cantidad = "10"
'    '        oConceptoParte.UnidadMedida = "Componente"
'    '        oConceptoParte.Descripcion = "Estopa"
'    '        oConceptoParte.ValorUnitario = "13"
'    '        oConceptoParte.NumeroIdentificacion = "N2"
'    '        oConceptoParte.Importe = "130.00"


'    '        Dim oConceptoParteAduana As CFDConceptosPartesAduanas

'    '        oConceptoParteAduana = New CFDConceptosPartesAduanas()
'    '        oConceptoParteAduana.Numero = "Num"
'    '        oConceptoParteAduana.Fecha = DateTime.Now
'    '        oConceptoParteAduana.Aduana = "King Nosa"

'    '        oConceptoParte.Aduanas.Add(oConceptoParteAduana)

'    '        oConcepto.Partes.Add(oConceptoParte)

'    '        oCFD.Conceptos.Add(oConcepto)

'    '        If Not oCFD.Insertar(Cnx, True) Then
'    '            For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
'    '                MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'    '            Next
'    '            Return
'    '        End If


'    '        MessageBox.Show("El número de cfd generadio fue : " + oCFD.IdCFD)



'    '    End Using
'    'End Sub

'    Public Sub UPS_Inserta_Rel_FacturasCFD(ByVal oCLV_FActura As Long, ByVal oClv_FacturaCFD As Long, ByVal oSerie As String, ByVal oTipo As String)
'        Dim CONEXION As New SqlConnection(MiConexion)
'        Dim COMANDO As New SqlCommand("UPS_Inserta_Rel_FacturasCFD", CONEXION)
'        COMANDO.CommandType = CommandType.StoredProcedure
'        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)
'        Dim PARAMETRO1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'        PARAMETRO1.Direction = ParameterDirection.Input
'        PARAMETRO1.Value = oCLV_FActura
'        COMANDO.Parameters.Add(PARAMETRO1)

'        Dim PARAMETRO2 As New SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt)
'        PARAMETRO2.Direction = ParameterDirection.Input
'        PARAMETRO2.Value = oClv_FacturaCFD
'        COMANDO.Parameters.Add(PARAMETRO2)

'        Dim PARAMETRO3 As New SqlParameter("@Serie", SqlDbType.VarChar, 5)
'        PARAMETRO3.Direction = ParameterDirection.Input
'        PARAMETRO3.Value = oSerie
'        COMANDO.Parameters.Add(PARAMETRO3)

'        Dim PARAMETRO4 As New SqlParameter("@tipo", SqlDbType.VarChar, 1)
'        PARAMETRO4.Direction = ParameterDirection.Input
'        PARAMETRO4.Value = oTipo
'        COMANDO.Parameters.Add(PARAMETRO4)

'        Try
'            CONEXION.Open()
'            Dim i As Integer = COMANDO.ExecuteNonQuery
'        Catch ex As Exception
'            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'        Finally
'            CONEXION.Close()
'            CONEXION.Dispose()
'        End Try
'    End Sub


'End Module

'Imports System
'Imports System.Collections.Generic
'Imports System.ComponentModel
'Imports System.Data
'Imports System.Drawing
'Imports System.Text
'Imports System.Windows.Forms
'Imports System.Xml
'Imports System.Data.SqlClient
'Imports System.Data.OleDb

'Imports MizarCFD.DAL
'Imports MizarCFD.BRL
'Imports MizarCFD.Reportes

'Imports CrystalDecisions.CrystalReports.Engine
'Imports System.IO

'Module ModuleCFDI

'    Public GloFacGlobalclvcompania As Long = 0
'    Public GloNomReporteFiscal As String = Nothing
'    Public GloNomReporteFiscalGlobal As String = Nothing
'    Public GloIDFactura As Long = 0
'    Public GloClv_FacturaCFD As Long = 0
'    Public GloMotivoCancelacionCFD As String = Nothing
'    Public Locop As Integer = 0
'    Public mIva As Decimal = 0
'    Public mipes As Decimal = 0
'    Public msubtotal As Decimal = 0
'    Public mdetalle1 As String = Nothing
'    Public locID_Compania_Mizart As String = Nothing
'    Public locID_Sucursal_Mizart As String = Nothing
'    'Public Sub Imprime_Factura_Digital(ByVal oid_CFD As Integer)
'    '    '    Using Cnx As New DAConexion("HL", "sa", "sa")
'    '    '        Dim oRreportes As New MizarCFD.Reportes.CFD
'    '    '        Dim oCFD As New MizarCFD.BRL.CFD
'    '    '        oCFD.IdCFD = "1"
'    '    '        oCFD.Consultar(Cnx)
'    '    '        Dim document As New System.Xml.XmlDocument
'    '    '        oCFD.ObtenerXML()
'    '    '        document.Value = oCFD.CadenaOriginal
'    '    '    End Using

'    'End Sub

'    Public Function Existe_Checalo(ByVal oSql As String) As Boolean
'        Existe_Checalo = False

'        Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
'            Cnx.DbCommand.CommandText = oSql
'            Cnx.DbCommand.CommandTimeout = 0
'            Cnx.DbCommand.CommandType = CommandType.Text
'            'Cnx.DbConnection.Open()
'            'Dim CONEXION As New SqlConnection(Cnx.DbConnection.Database)
'            'Dim COMANDO As New SqlCommand(oSql, CONEXION)
'            'COMANDO.CommandType = CommandType.Text
'            'Try
'            '    CONEXION.Open()
'            Dim reader As OleDbDataReader = Cnx.DbCommand.ExecuteReader
'            '    'Recorremos los Cargos obtenidos desde el Procedimiento
'            While (reader.Read())
'                If Len(reader(0).ToString) > 0 Then
'                    Existe_Checalo = True
'                End If
'            End While
'            'Cnx.DbConnection.Close()
'            'Catch ex As Exception
'            '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'            'Finally
'            '    CONEXION.Close()
'            '    CONEXION.Dispose()
'            'End Try
'        End Using
'    End Function

'    Public Sub Alta_Datosfiscales(ByVal oRazonSocial As String,
'        ByVal oRFC As String,
'        ByVal oCalle As String,
'        ByVal oClaveAsociado As String,
'        ByVal oCodigoPostal As String,
'        ByVal oColonia As String,
'        ByVal oEMail As String,
'        ByVal oEntreCalles As String,
'        ByVal oEstado As String,
'        ByVal oIdAsociado As String,
'        ByVal oLocalidad As String,
'        ByVal oMunicipio As String,
'        ByVal oNumeroExterior As String,
'            ByVal oPais As String)



'        If String.IsNullOrEmpty(oRazonSocial) = True Then oRazonSocial = ""
'        If String.IsNullOrEmpty(oRFC) = True Then oRFC = ""
'        If String.IsNullOrEmpty(oCalle) = True Then oCalle = ""
'        If String.IsNullOrEmpty(oClaveAsociado) = True Then oClaveAsociado = ""
'        If String.IsNullOrEmpty(oCodigoPostal) = True Then oCodigoPostal = ""
'        If String.IsNullOrEmpty(oColonia) = True Then oColonia = ""
'        If String.IsNullOrEmpty(oEMail) = True Then oEMail = ""
'        If String.IsNullOrEmpty(oEntreCalles) = True Then oEntreCalles = ""
'        If String.IsNullOrEmpty(oEstado) = True Then oEstado = ""
'        If String.IsNullOrEmpty(oIdAsociado) = True Then oIdAsociado = ""
'        If String.IsNullOrEmpty(oLocalidad) = True Then oLocalidad = ""
'        If String.IsNullOrEmpty(oMunicipio) = True Then oMunicipio = ""
'        If String.IsNullOrEmpty(oNumeroExterior) = True Then oNumeroExterior = ""
'        If String.IsNullOrEmpty(oPais) = True Then oPais = ""

'        If Existe_Checalo(" Select * from asociados  where id_asociado = " + oIdAsociado) = False Then

'            Dim oAsociados As MizarCFD.BRL.Asociados
'            Using Cnx As New DAConexion("HL", "sa", "sa")

'                oAsociados = New MizarCFD.BRL.Asociados
'                oAsociados.Inicializar()
'                oAsociados.RazonSocial = oRazonSocial
'                oAsociados.RFC = oRFC
'                oAsociados.Calle = oCalle
'                oAsociados.ClaveAsociado = oIdAsociado
'                oAsociados.CodigoPostal = oCodigoPostal
'                oAsociados.Colonia = oColonia
'                oAsociados.EMail = oEMail
'                oAsociados.EntreCalles = oEntreCalles
'                oAsociados.Estado = oEstado
'                oAsociados.IdAsociado = oIdAsociado
'                oAsociados.Localidad = oLocalidad
'                oAsociados.Municipio = oMunicipio
'                oAsociados.NumeroExterior = oNumeroExterior
'                oAsociados.Pais = oPais
'                oAsociados.IdTipoAsociado = Asociados.TipoAsociado.Cliente
'                If oAsociados.Validar(Cnx, TipoAfectacionBD.Insertar) = True Then
'                    If Not oAsociados.Insertar(Cnx) Then
'                        For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
'                            MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                        Next
'                        Return
'                    End If
'                Else
'                    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
'                        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                    Next
'                    Return
'                End If





'            End Using
'        Else

'        End If

'    End Sub

'    Public Sub Graba_Factura_Digital_Global(ByVal oClv_Factura As Long, ByVal oId_Compania As String)
'        Dim oTotalConPuntos As String = Nothing
'        Dim oSubTotal As String = Nothing
'        Dim oTotalSinPuntos As String = Nothing
'        Dim oDescuento As String = Nothing
'        Dim oiva As String = Nothing
'        Dim oieps As String = Nothing
'        Dim oTasaIva As String = Nothing
'        Dim oTasaIeps As String = Nothing
'        Dim oFecha As String = Nothing
'        Dim oTotalImpuestos As String = Nothing
'        Dim IdCompaniaMizar As Long = 0
'        Dim IdAsociadoPublicoGeneral As Long = 0

'        Dim CONEXION As New SqlConnection(MiConexion)
'        Dim COMANDO As New SqlCommand("DameFacDig_Parte_1_Global", CONEXION)
'        COMANDO.CommandType = CommandType.StoredProcedure

'        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'        parametro.Direction = ParameterDirection.Input
'        parametro.Value = oClv_Factura
'        COMANDO.Parameters.Add(parametro)

'        Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
'        parametro1.Direction = ParameterDirection.Output
'        parametro1.Value = 0
'        COMANDO.Parameters.Add(parametro1)

'        Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
'        parametro2.Direction = ParameterDirection.Output
'        parametro2.Value = 0
'        COMANDO.Parameters.Add(parametro2)

'        Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
'        parametro3.Direction = ParameterDirection.Output
'        parametro3.Value = 0
'        COMANDO.Parameters.Add(parametro3)

'        Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
'        parametro4.Direction = ParameterDirection.Output
'        parametro4.Value = 0
'        COMANDO.Parameters.Add(parametro4)

'        Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
'        parametro5.Direction = ParameterDirection.Output
'        parametro5.Value = 0
'        COMANDO.Parameters.Add(parametro5)

'        Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
'        parametro6.Direction = ParameterDirection.Output
'        parametro6.Value = 0
'        COMANDO.Parameters.Add(parametro6)

'        Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
'        parametro7.Direction = ParameterDirection.Output
'        parametro7.Value = 0
'        COMANDO.Parameters.Add(parametro7)

'        Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
'        parametro8.Direction = ParameterDirection.Output
'        parametro8.Value = 0
'        COMANDO.Parameters.Add(parametro8)

'        Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
'        parametro9.Direction = ParameterDirection.Output
'        parametro9.Value = 0
'        COMANDO.Parameters.Add(parametro9)

'        Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
'        parametro10.Direction = ParameterDirection.Output
'        parametro10.Value = 0
'        COMANDO.Parameters.Add(parametro10)
'        '@IdCompania
'        Dim parametro11 As New SqlParameter("@IdCompania", SqlDbType.BigInt)
'        parametro11.Direction = ParameterDirection.Output
'        parametro11.Value = 0
'        COMANDO.Parameters.Add(parametro11)
'        '@IdAsociadoPublicoGeneral
'        Dim parametro12 As New SqlParameter("@IdAsociadoPublicoGeneral", SqlDbType.BigInt)
'        parametro12.Direction = ParameterDirection.Output
'        parametro12.Value = 0
'        COMANDO.Parameters.Add(parametro12)

'        Try
'            CONEXION.Open()
'            Dim i As Integer = COMANDO.ExecuteNonQuery
'            oTotalConPuntos = CStr(parametro1.Value)
'            oSubTotal = CStr(parametro2.Value)
'            oTotalSinPuntos = CStr(parametro3.Value)
'            oDescuento = CStr(parametro4.Value)
'            oiva = CStr(parametro5.Value)
'            oieps = CStr(parametro6.Value)
'            oTasaIva = CStr(parametro7.Value)
'            oTasaIeps = CStr(parametro8.Value)
'            oFecha = CStr(parametro9.Value)
'            oTotalImpuestos = CStr(parametro10.Value)
'            IdCompaniaMizar = parametro11.Value
'            IdAsociadoPublicoGeneral = parametro12.Value
'            'Cnx.DbConnection.Close()
'        Catch ex As Exception
'            MsgBox(ex.Message & " Parte 1 " & ex.Source, MsgBoxStyle.Exclamation)
'        Finally
'            CONEXION.Close()
'            CONEXION.Dispose()
'        End Try
'        Dim pacDefault As String = Nothing

'        Dim oCFD As MizarCFD.BRL.CFD

'        Using Cnx As New DAConexion("HL", "sa", "sa")
'            Try

'                oCFD = New MizarCFD.BRL.CFD
'                oCFD.Inicializar()
'                oCFD.EsTimbrePrueba = True
'                oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
'                oCFD.PrepararDatosEmisorPorId(Cnx, IdCompaniaMizar)
'                oCFD.PrepararDatosReceptorPorId(Cnx, IdAsociadoPublicoGeneral)

'                oCFD.Version = "3.0"
'                pacDefault = DAUtileriasSQL.ObtenerParametro(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart, "PAC")
'                oCFD.Timbrador = CFD.Timbradores.Mizar
'                oCFD.IdMoneda = "1" '//1 = Pesos , Pesos = 2 USD
'                'oFecha
'                oCFD.Fecha = Date.Now
'                oCFD.FormaDePago = "Forma de Pago"
'                'oCFD.Certificado = ""
'                oCFD.CondicionesDePago = "Condiciones de Pago"
'                oCFD.SubTotal = oSubTotal
'                oCFD.Descuento = oDescuento
'                oCFD.MotivoDescuento = "Pronto Pago"
'                oCFD.Total = oTotalSinPuntos
'                oCFD.MetodoDePago = "Efectivo"
'                oCFD.TipoDeComprobante = "ingreso"

'                'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
'                'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

'                oCFD.Impuestos.TotalImpuestosRetenidos = 0
'                oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
'                'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
'                'oimpuestosRetenciones = New CFDImpuestosRetenciones()
'                'oimpuestosRetenciones.Impuesto = "IVA"
'                'oimpuestosRetenciones.Importe = oiva
'                'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

'                Dim oImpuestosTraslados As CFDImpuestosTraslados

'                oImpuestosTraslados = New CFDImpuestosTraslados()
'                oImpuestosTraslados.Impuesto = "IEPS"
'                oImpuestosTraslados.Importe = oieps
'                oImpuestosTraslados.Tasa = oTasaIeps

'                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'                oImpuestosTraslados = New CFDImpuestosTraslados()
'                oImpuestosTraslados.Impuesto = "IVA"
'                oImpuestosTraslados.Importe = oiva
'                oImpuestosTraslados.Tasa = oTasaIva

'                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'                Dim oConcepto As CFDConceptos

'                Dim CONEXION2 As New SqlConnection(MiConexion)
'                Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2_Global " + CStr(oClv_Factura), CONEXION2)
'                COMANDO2.CommandType = CommandType.Text
'                Try
'                    CONEXION2.Open()
'                    Dim reader As SqlDataReader = COMANDO2.ExecuteReader
'                    '    'Recorremos los Cargos obtenidos desde el Procedimiento
'                    While (reader.Read())
'                        oConcepto = New CFDConceptos()
'                        oConcepto.Cantidad = reader(0).ToString
'                        oConcepto.UnidadMedida = ""
'                        oConcepto.NumeroIdentificacion = reader(1).ToString
'                        oConcepto.Descripcion = reader(2).ToString
'                        oConcepto.ValorUnitario = reader(3).ToString
'                        oConcepto.Importe = reader(3).ToString
'                        'oConcepto.CausaIVA = "1"
'                        oConcepto.TrasladarIVA = "1"
'                        oCFD.Conceptos.Add(oConcepto)
'                    End While
'                    'Cnx.DbConnection.Close()
'                Catch ex As Exception
'                    MsgBox(ex.Message & " Parte 2 " & ex.Source, MsgBoxStyle.Exclamation)
'                Finally
'                    CONEXION2.Close()
'                    CONEXION2.Dispose()
'                End Try

'                'If Not oCFD.Insertar(Cnx, True) Then
'                '    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
'                '        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                '    Next
'                '    Return
'                'End If
'                Dim resultado As Boolean = False
'                resultado = oCFD.Insertar(Cnx, True)


'                GloClv_FacturaCFD = oCFD.IdCFD
'                UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "G")

'            Catch ex As Exception
'                MsgBox(ex.Message & " Parte 3 " & ex.Source, MsgBoxStyle.Exclamation)
'            Finally
'                CONEXION.Close()
'                CONEXION.Dispose()
'            End Try

'        End Using
'    End Sub

'    Public Sub Graba_Factura_Digital(ByVal oClv_Factura As Long, ByVal oIden As Integer)
'        Dim oTotalConPuntos As String = Nothing
'        Dim oSubTotal As String = Nothing
'        Dim oTotalSinPuntos As String = Nothing
'        Dim oDescuento As String = Nothing
'        Dim oiva As String = Nothing
'        Dim oieps As String = Nothing
'        Dim oTasaIva As String = Nothing
'        Dim oTasaIeps As String = Nothing
'        Dim oFecha As String = Nothing
'        Dim oTotalImpuestos As String = Nothing

'        ''
'        Dim mfecha As Date
'        Dim con As New SqlConnection(MiConexion)
'        Dim com As New SqlCommand("DAMEFECHADELSERVIDOR", con)
'        com.CommandType = CommandType.StoredProcedure
'        'Dame la fecha del Servidor
'        Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
'        prmFechaObtebida.Direction = ParameterDirection.Output
'        prmFechaObtebida.Value = ""
'        com.Parameters.Add(prmFechaObtebida)
'        Try
'            con.Open()
'            com.ExecuteNonQuery()
'            mfecha = prmFechaObtebida.Value
'        Catch ex As Exception
'            MsgBox("Error: No se pudo obtener la fecha válida del Servidor.", MsgBoxStyle.Critical)
'            MsgBox("Descripción:  Procedimiento [DAMEFECHADELSERVIDOR] // Función [DameFechaServidor] // Form: FrmAdelantarCargos", MsgBoxStyle.Information)
'            MsgBox(ex.Message)
'        Finally
'            con.Close()
'        End Try
'        ''




'        Dim CONEXION As New SqlConnection(MiConexion)
'        Dim COMANDO As New SqlCommand("DameFacDig_Parte_1", CONEXION)
'        COMANDO.CommandType = CommandType.StoredProcedure

'        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'        parametro.Direction = ParameterDirection.Input
'        parametro.Value = oClv_Factura
'        COMANDO.Parameters.Add(parametro)

'        Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
'        parametro1.Direction = ParameterDirection.Output
'        parametro1.Value = 0
'        COMANDO.Parameters.Add(parametro1)

'        Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
'        parametro2.Direction = ParameterDirection.Output
'        parametro2.Value = 0
'        COMANDO.Parameters.Add(parametro2)

'        Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
'        parametro3.Direction = ParameterDirection.Output
'        parametro3.Value = 0
'        COMANDO.Parameters.Add(parametro3)

'        Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
'        parametro4.Direction = ParameterDirection.Output
'        parametro4.Value = 0
'        COMANDO.Parameters.Add(parametro4)

'        Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
'        parametro5.Direction = ParameterDirection.Output
'        parametro5.Value = 0
'        COMANDO.Parameters.Add(parametro5)

'        Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
'        parametro6.Direction = ParameterDirection.Output
'        parametro6.Value = 0
'        COMANDO.Parameters.Add(parametro6)

'        Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
'        parametro7.Direction = ParameterDirection.Output
'        parametro7.Value = 0
'        COMANDO.Parameters.Add(parametro7)

'        Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
'        parametro8.Direction = ParameterDirection.Output
'        parametro8.Value = 0
'        COMANDO.Parameters.Add(parametro8)

'        Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
'        parametro9.Direction = ParameterDirection.Output
'        parametro9.Value = 0
'        COMANDO.Parameters.Add(parametro9)

'        Dim parametro10 As New SqlParameter("@TotalImpuestos", SqlDbType.Money)
'        parametro10.Direction = ParameterDirection.Output
'        parametro10.Value = 0
'        COMANDO.Parameters.Add(parametro10)

'        Try
'            CONEXION.Open()
'            Dim i As Integer = COMANDO.ExecuteNonQuery
'            oTotalConPuntos = CStr(parametro1.Value)
'            oSubTotal = CStr(parametro2.Value)
'            oTotalSinPuntos = CStr(parametro3.Value)
'            oDescuento = CStr(parametro4.Value)
'            oiva = CStr(parametro5.Value)
'            oieps = CStr(parametro6.Value)
'            oTasaIva = CStr(parametro7.Value)
'            oTasaIeps = CStr(parametro8.Value)
'            oFecha = CStr(parametro9.Value)
'            oTotalImpuestos = CStr(parametro10.Value)
'            'Cnx.DbConnection.Close()
'        Catch ex As Exception
'            MsgBox(ex.Message & " Parte 1 " & ex.Source, MsgBoxStyle.Exclamation)
'        Finally
'            CONEXION.Close()
'            CONEXION.Dispose()
'        End Try


'        Dim oCFD As MizarCFD.BRL.CFD

'        'pacDefault = DAUtileriasSQL.ObtenerParametro(cnx, cbbCompanias.SelectedValue.ToString(), cbbSucursales.SelectedValue.ToString(), "PAC");
'        Dim pacDefault As String = ""

'        Using Cnx As New DAConexion("HL", "sa", "sa")
'            Try
'                'oIden = "1"

'                oCFD = New MizarCFD.BRL.CFD
'                oCFD.Inicializar()
'                oCFD.EsTimbrePrueba = True
'                oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision
'                oCFD.Version = "3.0"
'                pacDefault = DAUtileriasSQL.ObtenerParametro(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart, "PAC")
'                oCFD.Timbrador = CFD.Timbradores.Mizar

'                'If pacDefault.Contains("1") Then
'                '    oCFD.Timbrador = CFD.Timbradores.TimbreFiscal
'                'ElseIf pacDefault.Contains("2") Then
'                'oCFD.Timbrador = CFD.Timbradores.Mizar
'                'Else
'                'MsgBox("No se puede determinar un PAC, Revisar que el parametro \PAC\ contenga 1 o 2 ", "Aviso")
'                'End If

'                'oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart, locID_Sucursal_Mizart)
'                oCFD.PrepararDatosEmisorPorId(Cnx, locID_Compania_Mizart)
'                'oCFD.PrepararDatosReceptorPorId(Cnx, CStr(oIden))
'                oCFD.PrepararDatosReceptorPorClaveAsociado(Cnx, CStr(oIden))

'                oCFD.IdMoneda = "1" '//1 = Pesos , Pesos = 2 USD
'                'oFecha
'                oCFD.Fecha = mfecha '//Date.Now
'                oCFD.FormaDePago = "Forma de Pago"
'                'oCFD.Certificado = ""
'                oCFD.CondicionesDePago = "Condiciones de Pago"
'                oCFD.SubTotal = oSubTotal
'                oCFD.Descuento = oDescuento
'                oCFD.MotivoDescuento = "Pronto Pago"
'                oCFD.Total = oTotalSinPuntos
'                oCFD.MetodoDePago = "Efectivo"
'                oCFD.TipoDeComprobante = "ingreso"

'                'oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
'                'oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

'                oCFD.Impuestos.TotalImpuestosRetenidos = 0
'                oCFD.Impuestos.TotalImpuestosTrasladados = oTotalImpuestos
'                'Dim oimpuestosRetenciones As CFDImpuestosRetenciones
'                'oimpuestosRetenciones = New CFDImpuestosRetenciones()
'                'oimpuestosRetenciones.Impuesto = "IVA"
'                'oimpuestosRetenciones.Importe = oiva
'                'oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

'                Dim oImpuestosTraslados As CFDImpuestosTraslados

'                oImpuestosTraslados = New CFDImpuestosTraslados()
'                oImpuestosTraslados.Impuesto = "IEPS"
'                oImpuestosTraslados.Importe = oieps
'                oImpuestosTraslados.Tasa = oTasaIeps

'                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'                oImpuestosTraslados = New CFDImpuestosTraslados()
'                oImpuestosTraslados.Impuesto = "IVA"
'                oImpuestosTraslados.Importe = oiva
'                oImpuestosTraslados.Tasa = oTasaIva

'                oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'                Dim oConcepto As CFDConceptos

'                Dim CONEXION2 As New SqlConnection(MiConexion)
'                Dim COMANDO2 As New SqlCommand("exec DameFacDig_Parte_2 " + CStr(oClv_Factura), CONEXION2)
'                COMANDO2.CommandType = CommandType.Text
'                Try
'                    CONEXION2.Open()
'                    Dim reader As SqlDataReader = COMANDO2.ExecuteReader
'                    '    'Recorremos los Cargos obtenidos desde el Procedimiento
'                    While (reader.Read())
'                        oConcepto = New CFDConceptos()
'                        oConcepto.Cantidad = reader(0).ToString
'                        oConcepto.UnidadMedida = ""
'                        oConcepto.NumeroIdentificacion = reader(1).ToString
'                        oConcepto.Descripcion = reader(2).ToString
'                        oConcepto.ValorUnitario = reader(3).ToString
'                        oConcepto.Importe = reader(3).ToString
'                        oConcepto.TrasladarIVA = "1"
'                        oCFD.Conceptos.Add(oConcepto)
'                    End While
'                    'Cnx.DbConnection.Close()
'                Catch ex As Exception
'                    MsgBox(ex.Message & " Parte 2 " & ex.Source, MsgBoxStyle.Exclamation)
'                Finally
'                    CONEXION2.Close()
'                    CONEXION2.Dispose()
'                End Try

'                'If Not oCFD.Insertar(Cnx, True) Then
'                '    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
'                '        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                '    Next
'                '    Return
'                'End If
'                Dim Resultado As Boolean = False
'                Resultado = oCFD.Insertar(Cnx, True)
'                'If Not oCFD.Insertar(Cnx, True) Then
'                '    For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
'                '        MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                '    Next
'                '    Return
'                'End If




'                GloClv_FacturaCFD = oCFD.IdCFD
'                UPS_Inserta_Rel_FacturasCFD(oClv_Factura, GloClv_FacturaCFD, "", "N")

'            Catch ex As Exception
'                MsgBox(ex.Message & " Parte 3 " & ex.Source, MsgBoxStyle.Exclamation)
'            Finally
'                CONEXION.Close()
'                CONEXION.Dispose()
'            End Try

'        End Using
'    End Sub

'    Public Sub Cancelacion_FacturaCFD(ByVal oClv_FacturaCFD As Long)
'        Dim ofecha As Date
'        Dim con As New SqlConnection(MiConexion)
'        Dim com As New SqlCommand("DAMEFECHADELSERVIDOR", con)
'        com.CommandType = CommandType.StoredProcedure

'        'Dame la fecha del Servidor
'        Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
'        prmFechaObtebida.Direction = ParameterDirection.Output
'        prmFechaObtebida.Value = ""
'        com.Parameters.Add(prmFechaObtebida)

'        Try
'            con.Open()
'            com.ExecuteNonQuery()

'            ofecha = prmFechaObtebida.Value

'        Catch ex As Exception
'            MsgBox("Error: No se pudo obtener la fecha válida del Servidor.", MsgBoxStyle.Critical)
'            MsgBox("Descripción:  Procedimiento [DAMEFECHADELSERVIDOR] // Función [DameFechaServidor] // Form: FrmAdelantarCargos", MsgBoxStyle.Information)
'            MsgBox(ex.Message)
'        Finally
'            con.Close()
'        End Try

'        Dim oCFD As MizarCFD.BRL.CFD
'        Using Cnx As New DAConexion("HL", "sa", "sa")
'            oCFD = New MizarCFD.BRL.CFD
'            oCFD.Inicializar()
'            oCFD.IdCFD = CStr(oClv_FacturaCFD)
'            If Len(GloMotivoCancelacionCFD) = 0 Then GloMotivoCancelacionCFD = "CANCELACION"
'            oCFD.MotivoCancelacion = GloMotivoCancelacionCFD
'            oCFD.FechaCancelacion = ofecha
'            oCFD.EstaCancelado = 1
'            If Not oCFD.ModificarEstatusCancelado(Cnx) = False Then
'                For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
'                    MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'                Next
'                Return
'            End If
'        End Using
'    End Sub

'    Public Sub SetDBReport(ByVal ds As DataSet, ByVal myReportDocument As ReportDocument)
'        Try
'            Dim myTables As Tables = myReportDocument.Database.Tables
'            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
'            For Each myTable In myTables
'                'myTable.Location
'                myTable.SetDataSource(ds)
'            Next
'        Catch ex As System.Exception
'            System.Windows.Forms.MessageBox.Show(ex.Message)
'        End Try
'    End Sub

'    Public Sub Dame_Detalle1Global(ByVal oClv_FacturaCFD As Long)
'        mIva = 0
'        mipes = 0
'        msubtotal = 0
'        mdetalle1 = ""

'        Dim con As New SqlConnection(MiConexion)
'        Dim com As New SqlCommand("Dame_Detalle1", con)
'        com.CommandType = CommandType.StoredProcedure
'        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
'        'Dame la fecha del Servidor
'        Dim prm1 As New SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt)
'        prm1.Direction = ParameterDirection.Input
'        prm1.Value = oClv_FacturaCFD
'        com.Parameters.Add(prm1)

'        Dim prm2 As New SqlParameter("@Iva", SqlDbType.Money)
'        prm2.Direction = ParameterDirection.Output
'        prm2.Value = 0
'        com.Parameters.Add(prm2)

'        Dim prm3 As New SqlParameter("@Ieps", SqlDbType.Money)
'        prm3.Direction = ParameterDirection.Output
'        prm3.Value = ""
'        com.Parameters.Add(prm3)

'        Dim prm4 As New SqlParameter("@Subtotal", SqlDbType.Money)
'        prm4.Direction = ParameterDirection.Output
'        prm4.Value = ""
'        com.Parameters.Add(prm4)

'        Dim prm5 As New SqlParameter("@Detalle1", SqlDbType.VarChar, 8000)
'        prm5.Direction = ParameterDirection.Output
'        prm5.Value = ""
'        com.Parameters.Add(prm5)

'        Try
'            con.Open()
'            com.ExecuteNonQuery()
'            mIva = prm2.Value
'            mipes = prm3.Value
'            msubtotal = prm4.Value
'            mdetalle1 = prm5.Value

'        Catch ex As Exception
'            MsgBox(ex.Message)
'        Finally
'            con.Close()
'        End Try
'    End Sub

'    Public Sub Dime_Aque_Compania_Facturarle(ByVal oClv_Factura As Long)
'        locID_Compania_Mizart = ""
'        locID_Sucursal_Mizart = ""

'        Dim con As New SqlConnection(MiConexion)
'        Dim com As New SqlCommand("Dime_Aque_Compania_Facturarle", con)
'        com.CommandType = CommandType.StoredProcedure
'        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
'        'Dame la fecha del Servidor
'        Dim prm1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'        prm1.Direction = ParameterDirection.Input
'        prm1.Value = oClv_Factura
'        com.Parameters.Add(prm1)

'        Dim prm2 As New SqlParameter("@ID_Compania_Mizart", SqlDbType.VarChar, 50)
'        prm2.Direction = ParameterDirection.Output
'        prm2.Value = 0
'        com.Parameters.Add(prm2)

'        Dim prm3 As New SqlParameter("@ID_Sucursal_Mizart", SqlDbType.VarChar, 50)
'        prm3.Direction = ParameterDirection.Output
'        prm3.Value = ""
'        com.Parameters.Add(prm3)


'        Try
'            con.Open()
'            com.ExecuteNonQuery()
'            locID_Compania_Mizart = prm2.Value
'            locID_Sucursal_Mizart = prm3.Value

'        Catch ex As Exception
'            MsgBox(ex.Message)
'        Finally
'            con.Close()
'        End Try
'    End Sub

'    Public Function Dame_Cantidad_Con_Letra(ByVal oCantidad As Double) As String
'        Dame_Cantidad_Con_Letra = ""

'        Dim con As New SqlConnection(MiConexion)
'        Dim com As New SqlCommand("DameCantidadALetra", con)
'        com.CommandType = CommandType.StoredProcedure
'        '@Numero numeric(20,2),@moneda int,@letra varchar(max) output
'        'Dame la fecha del Servidor
'        If IsNumeric(oCantidad) = False Then oCantidad = 0
'        Dim prm1 As New SqlParameter("@Numero", SqlDbType.Decimal)
'        prm1.Direction = ParameterDirection.Input
'        prm1.Value = oCantidad
'        com.Parameters.Add(prm1)

'        Dim prm2 As New SqlParameter("@moneda", SqlDbType.Int)
'        prm2.Direction = ParameterDirection.Input
'        prm2.Value = 0
'        com.Parameters.Add(prm2)

'        Dim prm3 As New SqlParameter("@letra", SqlDbType.VarChar, 400)
'        prm3.Direction = ParameterDirection.Output
'        prm3.Value = ""
'        com.Parameters.Add(prm3)

'        Try
'            con.Open()
'            com.ExecuteNonQuery()

'            Dame_Cantidad_Con_Letra = prm3.Value

'        Catch ex As Exception
'            MsgBox(ex.Message)
'        Finally
'            con.Close()
'        End Try
'    End Function

'    Public Sub DameId_FacturaCDF(ByVal oClv_Factura As Long, ByVal oTipo As String)
'        GloClv_FacturaCFD = 0
'        Dim CONEXION As New SqlConnection(MiConexion)
'        Dim COMANDO As New SqlCommand("upsDameFacDig_Clv_FacturaCDF", CONEXION)
'        COMANDO.CommandType = CommandType.StoredProcedure

'        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'        parametro.Direction = ParameterDirection.Input
'        parametro.Value = oClv_Factura
'        COMANDO.Parameters.Add(parametro)

'        Dim parametro1 As New SqlParameter("@Clv_FacturaCDF", SqlDbType.Money)
'        parametro1.Direction = ParameterDirection.Output
'        parametro1.Value = 0
'        COMANDO.Parameters.Add(parametro1)

'        Dim parametro2 As New SqlParameter("@tipo", SqlDbType.VarChar, 1)
'        parametro2.Direction = ParameterDirection.Input
'        parametro2.Value = oTipo
'        COMANDO.Parameters.Add(parametro2)

'        Try
'            CONEXION.Open()
'            Dim i As Integer = COMANDO.ExecuteNonQuery
'            GloClv_FacturaCFD = CStr(parametro1.Value)

'            'Cnx.DbConnection.Close()
'        Catch ex As Exception
'            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'        Finally
'            CONEXION.Close()
'            CONEXION.Dispose()
'        End Try
'    End Sub

'    Public Function BusFacFiscalOledb(ByVal oClv_Factura As Long) As Integer
'        BusFacFiscalOledb = 0
'        Dim CONEXION As New SqlConnection(MiConexion)
'        Dim COMANDO As New SqlCommand("BusFacFiscal", CONEXION)
'        COMANDO.CommandType = CommandType.StoredProcedure

'        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'        parametro.Direction = ParameterDirection.Input
'        parametro.Value = oClv_Factura
'        COMANDO.Parameters.Add(parametro)

'        Dim parametro1 As New SqlParameter("@ident", SqlDbType.Int)
'        parametro1.Direction = ParameterDirection.Output
'        parametro1.Value = 0
'        COMANDO.Parameters.Add(parametro1)

'        Try
'            CONEXION.Open()
'            Dim i As Integer = COMANDO.ExecuteNonQuery
'            BusFacFiscalOledb = CStr(parametro1.Value)

'            'Cnx.DbConnection.Close()
'        Catch ex As Exception
'            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'        Finally
'            CONEXION.Close()
'            CONEXION.Dispose()
'        End Try
'    End Function





'    'Public Sub Graba_Factura_Digital(ByVal oClv_Factura As Long)
'    '    Dim oTotalConPuntos As String = Nothing
'    '    Dim oSubTotal As String = Nothing
'    '    Dim oTotalSinPuntos As String = Nothing
'    '    Dim oDescuento As String = Nothing
'    '    Dim oiva As String = Nothing
'    '    Dim oieps As String = Nothing
'    '    Dim oTasaIva As String = Nothing
'    '    Dim oTasaIeps As String = Nothing
'    '    Dim oFecha As String = Nothing

'    '    Dim CONEXION As New SqlConnection(MiConexion)
'    '    Dim COMANDO As New SqlCommand("DameFacDig_Parte_1", CONEXION)
'    '    COMANDO.CommandType = CommandType.StoredProcedure

'    '    Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'    '    parametro.Direction = ParameterDirection.Input
'    '    parametro.Value = oClv_Factura
'    '    COMANDO.Parameters.Add(parametro)

'    '    Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
'    '    parametro1.Direction = ParameterDirection.Output
'    '    parametro1.Value = 0
'    '    COMANDO.Parameters.Add(parametro1)

'    '    Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
'    '    parametro2.Direction = ParameterDirection.Output
'    '    parametro2.Value = 0
'    '    COMANDO.Parameters.Add(parametro2)

'    '    Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
'    '    parametro3.Direction = ParameterDirection.Output
'    '    parametro3.Value = 0
'    '    COMANDO.Parameters.Add(parametro3)

'    '    Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
'    '    parametro4.Direction = ParameterDirection.Output
'    '    parametro4.Value = 0
'    '    COMANDO.Parameters.Add(parametro4)

'    '    Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
'    '    parametro5.Direction = ParameterDirection.Output
'    '    parametro5.Value = 0
'    '    COMANDO.Parameters.Add(parametro5)

'    '    Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
'    '    parametro6.Direction = ParameterDirection.Output
'    '    parametro6.Value = 0
'    '    COMANDO.Parameters.Add(parametro6)

'    '    Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
'    '    parametro7.Direction = ParameterDirection.Output
'    '    parametro7.Value = 0
'    '    COMANDO.Parameters.Add(parametro7)

'    '    Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
'    '    parametro8.Direction = ParameterDirection.Output
'    '    parametro8.Value = 0
'    '    COMANDO.Parameters.Add(parametro8)

'    '    Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
'    '    parametro9.Direction = ParameterDirection.Output
'    '    parametro9.Value = 0
'    '    COMANDO.Parameters.Add(parametro9)


'    '    Try
'    '        CONEXION.Open()
'    '        Dim i As Integer = COMANDO.ExecuteNonQuery
'    '        oTotalConPuntos = CStr(parametro1.Value)
'    '        oSubTotal = CStr(parametro2.Value)
'    '        oTotalSinPuntos = CStr(parametro3.Value)
'    '        oDescuento = CStr(parametro4.Value)
'    '        oiva = CStr(parametro5.Value)
'    '        oieps = CStr(parametro6.Value)
'    '        oTasaIva = CStr(parametro7.Value)
'    '        oTasaIeps = CStr(parametro8.Value)
'    '        oFecha = CStr(parametro9.Value)
'    '        'Cnx.DbConnection.Close()
'    '    Catch ex As Exception
'    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'    '    Finally
'    '        CONEXION.Close()
'    '        CONEXION.Dispose()
'    '    End Try


'    '    Dim oCFD As MizarCFD.BRL.CFD

'    '    Using Cnx As New DAConexion("HL", "sa", "sa")
'    '        oCFD = New MizarCFD.BRL.CFD
'    '        oCFD.Inicializar()
'    '        oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision

'    '        oCFD.PrepararDatosEmisorPorId(Cnx, "1", "1")
'    '        oCFD.PrepararDatosReceptorPorId(Cnx, "3")

'    '        oCFD.Version = "2.0"
'    '        oCFD.IdMoneda = "1" '//1 = Pesos , Pesos = 2 USD
'    '        oCFD.Fecha = oFecha
'    '        oCFD.FormaDePago = "Forma de Pago"
'    '        oCFD.Certificado = ""
'    '        oCFD.CondicionesDePago = "Condiciones de Pago"
'    '        oCFD.SubTotal = oSubTotal
'    '        oCFD.Descuento = oDescuento
'    '        oCFD.MotivoDescuento = "Pronto Pago"
'    '        oCFD.Total = oTotalSinPuntos
'    '        oCFD.MetodoDePago = "Efectivo"
'    '        oCFD.TipoDeComprobante = "ingreso"

'    '        oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
'    '        oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

'    '        oCFD.Impuestos.TotalImpuestosRetenidos = oiva
'    '        oCFD.Impuestos.TotalImpuestosTrasladados = oieps

'    '        Dim oimpuestosRetenciones As CFDImpuestosRetenciones
'    '        oimpuestosRetenciones = New CFDImpuestosRetenciones()
'    '        oimpuestosRetenciones.Impuesto = "IVA"
'    '        oimpuestosRetenciones.Importe = oiva
'    '        oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

'    '        Dim oImpuestosTraslados As CFDImpuestosTraslados

'    '        oImpuestosTraslados = New CFDImpuestosTraslados()
'    '        oImpuestosTraslados.Impuesto = "IEPS"
'    '        oImpuestosTraslados.Importe = oieps
'    '        oImpuestosTraslados.Tasa = oTasaIeps

'    '        oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'    '        oImpuestosTraslados = New CFDImpuestosTraslados()
'    '        oImpuestosTraslados.Impuesto = "IVA"
'    '        oImpuestosTraslados.Importe = oiva
'    '        oImpuestosTraslados.Tasa = oTasaIva

'    '        oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'    '        Dim oConcepto As CFDConceptos
'    '        oConcepto = New CFDConceptos()
'    '        oConcepto.Cantidad = "10"
'    '        oConcepto.UnidadMedida = "Pieza"
'    '        oConcepto.NumeroIdentificacion = "ART 350"
'    '        oConcepto.Descripcion = "Escoba"
'    '        oConcepto.ValorUnitario = "10.50"
'    '        oConcepto.Importe = "105.000"
'    '        oConcepto.CausaIVA = "1"
'    '        oCFD.Conceptos.Add(oConcepto)


'    '        Dim oConceptoComplemento As CFDConceptosComplemento
'    '        oConceptoComplemento = New CFDConceptosComplemento()
'    '        oConceptoComplemento.XMLComplemento = "<rootita>Cogito Ergo Sum</rootita>"
'    '        oConcepto.Complemento = oConceptoComplemento

'    '        oConcepto = New CFDConceptos()
'    '        oConcepto.Cantidad = "5"
'    '        oConcepto.UnidadMedida = "Pieza"
'    '        oConcepto.NumeroIdentificacion = "ART 5"
'    '        oConcepto.Descripcion = "Escoba"
'    '        oConcepto.ValorUnitario = "5.50"
'    '        oConcepto.Importe = "27.50"
'    '        oConcepto.CausaIVA = "1"
'    '        oCFD.Conceptos.Add(oConcepto)

'    '        Dim oConceptoAduanas As CFDConceptosAduanas

'    '        oConceptoAduanas = New CFDConceptosAduanas()
'    '        oConceptoAduanas.Numero = "Num"
'    '        oConceptoAduanas.Fecha = DateTime.Now
'    '        oConceptoAduanas.Aduana = "Nuevo Laredo"

'    '        oConcepto.Aduanas.Add(oConceptoAduanas)

'    '        oConcepto = New CFDConceptos()
'    '        oConcepto.Cantidad = "20"
'    '        oConcepto.UnidadMedida = "Pieza"
'    '        oConcepto.NumeroIdentificacion = "ART 352"
'    '        oConcepto.Descripcion = "Trapeador"
'    '        oConcepto.ValorUnitario = "11.50"
'    '        oConcepto.Importe = "230.0000"
'    '        oConcepto.CausaIVA = "1"

'    '        Dim oConceptoParte As CFDConceptosPartes

'    '        oConceptoParte = New CFDConceptosPartes()
'    '        oConceptoParte.Cantidad = "10"
'    '        oConceptoParte.UnidadMedida = "Componente"
'    '        oConceptoParte.Descripcion = "Palo"
'    '        oConceptoParte.ValorUnitario = "10"
'    '        oConceptoParte.NumeroIdentificacion = "N1"
'    '        oConceptoParte.Importe = "100.00"

'    '        oConcepto.Partes.Add(oConceptoParte)

'    '        oConceptoParte = New CFDConceptosPartes()
'    '        oConceptoParte.Cantidad = "10"
'    '        oConceptoParte.UnidadMedida = "Componente"
'    '        oConceptoParte.Descripcion = "Estopa"
'    '        oConceptoParte.ValorUnitario = "13"
'    '        oConceptoParte.NumeroIdentificacion = "N2"
'    '        oConceptoParte.Importe = "130.00"


'    '        Dim oConceptoParteAduana As CFDConceptosPartesAduanas

'    '        oConceptoParteAduana = New CFDConceptosPartesAduanas()
'    '        oConceptoParteAduana.Numero = "Num"
'    '        oConceptoParteAduana.Fecha = DateTime.Now
'    '        oConceptoParteAduana.Aduana = "King Nosa"

'    '        oConceptoParte.Aduanas.Add(oConceptoParteAduana)

'    '        oConcepto.Partes.Add(oConceptoParte)

'    '        oCFD.Conceptos.Add(oConcepto)

'    '        If Not oCFD.Insertar(Cnx, True) Then
'    '            For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
'    '                MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'    '            Next
'    '            Return
'    '        End If


'    '        MessageBox.Show("El número de cfd generadio fue : " + oCFD.IdCFD)



'    '    End Using
'    'End Sub

'    Public Sub UPS_Inserta_Rel_FacturasCFD(ByVal oCLV_FActura As Long, ByVal oClv_FacturaCFD As Long, ByVal oSerie As String, ByVal oTipo As String)
'        Dim CONEXION As New SqlConnection(MiConexion)
'        Dim COMANDO As New SqlCommand("UPS_Inserta_Rel_FacturasCFD", CONEXION)
'        COMANDO.CommandType = CommandType.StoredProcedure
'        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)
'        Dim PARAMETRO1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'        PARAMETRO1.Direction = ParameterDirection.Input
'        PARAMETRO1.Value = oCLV_FActura
'        COMANDO.Parameters.Add(PARAMETRO1)

'        Dim PARAMETRO2 As New SqlParameter("@Clv_FacturaCFD", SqlDbType.BigInt)
'        PARAMETRO2.Direction = ParameterDirection.Input
'        PARAMETRO2.Value = oClv_FacturaCFD
'        COMANDO.Parameters.Add(PARAMETRO2)

'        Dim PARAMETRO3 As New SqlParameter("@Serie", SqlDbType.VarChar, 5)
'        PARAMETRO3.Direction = ParameterDirection.Input
'        PARAMETRO3.Value = oSerie
'        COMANDO.Parameters.Add(PARAMETRO3)

'        Dim PARAMETRO4 As New SqlParameter("@tipo", SqlDbType.VarChar, 1)
'        PARAMETRO4.Direction = ParameterDirection.Input
'        PARAMETRO4.Value = oTipo
'        COMANDO.Parameters.Add(PARAMETRO4)

'        Try
'            CONEXION.Open()
'            Dim i As Integer = COMANDO.ExecuteNonQuery
'        Catch ex As Exception
'            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'        Finally
'            CONEXION.Close()
'            CONEXION.Dispose()
'        End Try
'    End Sub




'    'Public Sub Graba_Factura_Digital(ByVal oClv_Factura As Long)
'    '    Dim oTotalConPuntos As String = Nothing
'    '    Dim oSubTotal As String = Nothing
'    '    Dim oTotalSinPuntos As String = Nothing
'    '    Dim oDescuento As String = Nothing
'    '    Dim oiva As String = Nothing
'    '    Dim oieps As String = Nothing
'    '    Dim oTasaIva As String = Nothing
'    '    Dim oTasaIeps As String = Nothing
'    '    Dim oFecha As String = Nothing

'    '    Dim CONEXION As New SqlConnection(MiConexion)
'    '    Dim COMANDO As New SqlCommand("DameFacDig_Parte_1", CONEXION)
'    '    COMANDO.CommandType = CommandType.StoredProcedure

'    '    Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
'    '    parametro.Direction = ParameterDirection.Input
'    '    parametro.Value = oClv_Factura
'    '    COMANDO.Parameters.Add(parametro)

'    '    Dim parametro1 As New SqlParameter("@TotalConPuntos", SqlDbType.Money)
'    '    parametro1.Direction = ParameterDirection.Output
'    '    parametro1.Value = 0
'    '    COMANDO.Parameters.Add(parametro1)

'    '    Dim parametro2 As New SqlParameter("@SubTotal", SqlDbType.Money)
'    '    parametro2.Direction = ParameterDirection.Output
'    '    parametro2.Value = 0
'    '    COMANDO.Parameters.Add(parametro2)

'    '    Dim parametro3 As New SqlParameter("@TotalSinPuntos", SqlDbType.Money)
'    '    parametro3.Direction = ParameterDirection.Output
'    '    parametro3.Value = 0
'    '    COMANDO.Parameters.Add(parametro3)

'    '    Dim parametro4 As New SqlParameter("@Descuento", SqlDbType.Money)
'    '    parametro4.Direction = ParameterDirection.Output
'    '    parametro4.Value = 0
'    '    COMANDO.Parameters.Add(parametro4)

'    '    Dim parametro5 As New SqlParameter("@iva", SqlDbType.Money)
'    '    parametro5.Direction = ParameterDirection.Output
'    '    parametro5.Value = 0
'    '    COMANDO.Parameters.Add(parametro5)

'    '    Dim parametro6 As New SqlParameter("@ieps", SqlDbType.Money)
'    '    parametro6.Direction = ParameterDirection.Output
'    '    parametro6.Value = 0
'    '    COMANDO.Parameters.Add(parametro6)

'    '    Dim parametro7 As New SqlParameter("@TasaIva", SqlDbType.Money)
'    '    parametro7.Direction = ParameterDirection.Output
'    '    parametro7.Value = 0
'    '    COMANDO.Parameters.Add(parametro7)

'    '    Dim parametro8 As New SqlParameter("@TasaIeps", SqlDbType.Money)
'    '    parametro8.Direction = ParameterDirection.Output
'    '    parametro8.Value = 0
'    '    COMANDO.Parameters.Add(parametro8)

'    '    Dim parametro9 As New SqlParameter("@Fecha", SqlDbType.DateTime)
'    '    parametro9.Direction = ParameterDirection.Output
'    '    parametro9.Value = 0
'    '    COMANDO.Parameters.Add(parametro9)


'    '    Try
'    '        CONEXION.Open()
'    '        Dim i As Integer = COMANDO.ExecuteNonQuery
'    '        oTotalConPuntos = CStr(parametro1.Value)
'    '        oSubTotal = CStr(parametro2.Value)
'    '        oTotalSinPuntos = CStr(parametro3.Value)
'    '        oDescuento = CStr(parametro4.Value)
'    '        oiva = CStr(parametro5.Value)
'    '        oieps = CStr(parametro6.Value)
'    '        oTasaIva = CStr(parametro7.Value)
'    '        oTasaIeps = CStr(parametro8.Value)
'    '        oFecha = CStr(parametro9.Value)
'    '        'Cnx.DbConnection.Close()
'    '    Catch ex As Exception
'    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'    '    Finally
'    '        CONEXION.Close()
'    '        CONEXION.Dispose()
'    '    End Try


'    '    Dim oCFD As MizarCFD.BRL.CFD

'    '    Using Cnx As New DAConexion("HL", "sa", "sa")
'    '        oCFD = New MizarCFD.BRL.CFD
'    '        oCFD.Inicializar()
'    '        oCFD.TipoCFD = MizarCFD.BRL.CFD.TiposCFD.Emision

'    '        oCFD.PrepararDatosEmisorPorId(Cnx, "1", "1")
'    '        oCFD.PrepararDatosReceptorPorId(Cnx, "3")

'    '        oCFD.Version = "2.0"
'    '        oCFD.IdMoneda = "1" '//1 = Pesos , Pesos = 2 USD
'    '        oCFD.Fecha = oFecha
'    '        oCFD.FormaDePago = "Forma de Pago"
'    '        oCFD.Certificado = ""
'    '        oCFD.CondicionesDePago = "Condiciones de Pago"
'    '        oCFD.SubTotal = oSubTotal
'    '        oCFD.Descuento = oDescuento
'    '        oCFD.MotivoDescuento = "Pronto Pago"
'    '        oCFD.Total = oTotalSinPuntos
'    '        oCFD.MetodoDePago = "Efectivo"
'    '        oCFD.TipoDeComprobante = "ingreso"

'    '        oCFD.Complemento.XMLComplemento = "<raiz>Caelli Enarrant Gloria Dei </raiz>"
'    '        oCFD.Addenda.XMLAddenda = "<edi>Edit dididididididi</edi>"

'    '        oCFD.Impuestos.TotalImpuestosRetenidos = oiva
'    '        oCFD.Impuestos.TotalImpuestosTrasladados = oieps

'    '        Dim oimpuestosRetenciones As CFDImpuestosRetenciones
'    '        oimpuestosRetenciones = New CFDImpuestosRetenciones()
'    '        oimpuestosRetenciones.Impuesto = "IVA"
'    '        oimpuestosRetenciones.Importe = oiva
'    '        oCFD.Impuestos.Retenciones.Add(oimpuestosRetenciones)

'    '        Dim oImpuestosTraslados As CFDImpuestosTraslados

'    '        oImpuestosTraslados = New CFDImpuestosTraslados()
'    '        oImpuestosTraslados.Impuesto = "IEPS"
'    '        oImpuestosTraslados.Importe = oieps
'    '        oImpuestosTraslados.Tasa = oTasaIeps

'    '        oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'    '        oImpuestosTraslados = New CFDImpuestosTraslados()
'    '        oImpuestosTraslados.Impuesto = "IVA"
'    '        oImpuestosTraslados.Importe = oiva
'    '        oImpuestosTraslados.Tasa = oTasaIva

'    '        oCFD.Impuestos.Traslados.Add(oImpuestosTraslados)

'    '        Dim oConcepto As CFDConceptos
'    '        oConcepto = New CFDConceptos()
'    '        oConcepto.Cantidad = "10"
'    '        oConcepto.UnidadMedida = "Pieza"
'    '        oConcepto.NumeroIdentificacion = "ART 350"
'    '        oConcepto.Descripcion = "Escoba"
'    '        oConcepto.ValorUnitario = "10.50"
'    '        oConcepto.Importe = "105.000"
'    '        oConcepto.CausaIVA = "1"
'    '        oCFD.Conceptos.Add(oConcepto)


'    '        Dim oConceptoComplemento As CFDConceptosComplemento
'    '        oConceptoComplemento = New CFDConceptosComplemento()
'    '        oConceptoComplemento.XMLComplemento = "<rootita>Cogito Ergo Sum</rootita>"
'    '        oConcepto.Complemento = oConceptoComplemento

'    '        oConcepto = New CFDConceptos()
'    '        oConcepto.Cantidad = "5"
'    '        oConcepto.UnidadMedida = "Pieza"
'    '        oConcepto.NumeroIdentificacion = "ART 5"
'    '        oConcepto.Descripcion = "Escoba"
'    '        oConcepto.ValorUnitario = "5.50"
'    '        oConcepto.Importe = "27.50"
'    '        oConcepto.CausaIVA = "1"
'    '        oCFD.Conceptos.Add(oConcepto)

'    '        Dim oConceptoAduanas As CFDConceptosAduanas

'    '        oConceptoAduanas = New CFDConceptosAduanas()
'    '        oConceptoAduanas.Numero = "Num"
'    '        oConceptoAduanas.Fecha = DateTime.Now
'    '        oConceptoAduanas.Aduana = "Nuevo Laredo"

'    '        oConcepto.Aduanas.Add(oConceptoAduanas)

'    '        oConcepto = New CFDConceptos()
'    '        oConcepto.Cantidad = "20"
'    '        oConcepto.UnidadMedida = "Pieza"
'    '        oConcepto.NumeroIdentificacion = "ART 352"
'    '        oConcepto.Descripcion = "Trapeador"
'    '        oConcepto.ValorUnitario = "11.50"
'    '        oConcepto.Importe = "230.0000"
'    '        oConcepto.CausaIVA = "1"

'    '        Dim oConceptoParte As CFDConceptosPartes

'    '        oConceptoParte = New CFDConceptosPartes()
'    '        oConceptoParte.Cantidad = "10"
'    '        oConceptoParte.UnidadMedida = "Componente"
'    '        oConceptoParte.Descripcion = "Palo"
'    '        oConceptoParte.ValorUnitario = "10"
'    '        oConceptoParte.NumeroIdentificacion = "N1"
'    '        oConceptoParte.Importe = "100.00"

'    '        oConcepto.Partes.Add(oConceptoParte)

'    '        oConceptoParte = New CFDConceptosPartes()
'    '        oConceptoParte.Cantidad = "10"
'    '        oConceptoParte.UnidadMedida = "Componente"
'    '        oConceptoParte.Descripcion = "Estopa"
'    '        oConceptoParte.ValorUnitario = "13"
'    '        oConceptoParte.NumeroIdentificacion = "N2"
'    '        oConceptoParte.Importe = "130.00"


'    '        Dim oConceptoParteAduana As CFDConceptosPartesAduanas

'    '        oConceptoParteAduana = New CFDConceptosPartesAduanas()
'    '        oConceptoParteAduana.Numero = "Num"
'    '        oConceptoParteAduana.Fecha = DateTime.Now
'    '        oConceptoParteAduana.Aduana = "King Nosa"

'    '        oConceptoParte.Aduanas.Add(oConceptoParteAduana)

'    '        oConcepto.Partes.Add(oConceptoParte)

'    '        oCFD.Conceptos.Add(oConcepto)

'    '        If Not oCFD.Insertar(Cnx, True) Then
'    '            For Each oMensaje As DAMensajesSistema.RegistroMensaje In oCFD.MensajesSistema.ListaMensajes
'    '                MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
'    '            Next
'    '            Return
'    '        End If


'    '        MessageBox.Show("El número de cfd generadio fue : " + oCFD.IdCFD)



'    '    End Using
'    'End Sub

'    Public Sub Usp_DameReporteIdCompania(ByVal oclvEmpresaMizar As Long)
'        GloNomReporteFiscal = ""
'        GloNomReporteFiscalGlobal = ""
'        Dim CONEXION As New SqlConnection(MiConexion)
'        Dim COMANDO As New SqlCommand("Usp_DameReporteIdCompania", CONEXION)
'        COMANDO.CommandType = CommandType.StoredProcedure
'        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)
'        Dim PARAMETRO1 As New SqlParameter("@clvEmpresaMizar", SqlDbType.BigInt)
'        PARAMETRO1.Direction = ParameterDirection.Input
'        PARAMETRO1.Value = oclvEmpresaMizar
'        COMANDO.Parameters.Add(PARAMETRO1)

'        Dim PARAMETRO2 As New SqlParameter("@NomReporteFiscal", SqlDbType.VarChar, 100)
'        PARAMETRO2.Direction = ParameterDirection.Output
'        PARAMETRO2.Value = ""
'        COMANDO.Parameters.Add(PARAMETRO2)

'        Dim PARAMETRO3 As New SqlParameter("@NomReporteFiscalGlobal", SqlDbType.VarChar, 100)
'        PARAMETRO3.Direction = ParameterDirection.Output
'        PARAMETRO3.Value = ""
'        COMANDO.Parameters.Add(PARAMETRO3)


'        Try
'            CONEXION.Open()
'            Dim i As Integer = COMANDO.ExecuteNonQuery
'            GloNomReporteFiscal = PARAMETRO2.Value
'            GloNomReporteFiscalGlobal = PARAMETRO3.Value

'        Catch ex As Exception
'            GloNomReporteFiscal = ""
'            GloNomReporteFiscalGlobal = ""
'            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'        Finally
'            CONEXION.Close()
'            CONEXION.Dispose()
'        End Try
'    End Sub

'    Public Function ups_NUEVAFACTGLOBALporcompania(ByVal oclvcompania As Long, ByVal oFecha As Date, ByVal oCajera As String) As Long
'        ups_NUEVAFACTGLOBALporcompania = 0
'        '@clvcompania int,@Fecha datetime,@Cajera varchar(11),@IdFactura bigint output
'        GloNomReporteFiscal = ""
'        GloNomReporteFiscalGlobal = ""
'        Dim CONEXION As New SqlConnection(MiConexion)
'        Dim COMANDO As New SqlCommand("ups_NUEVAFACTGLOBALporcompania", CONEXION)
'        COMANDO.CommandType = CommandType.StoredProcedure
'        COMANDO.CommandTimeout = 0
'        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)
'        Dim PARAMETRO1 As New SqlParameter("@clvcompania", SqlDbType.BigInt)
'        PARAMETRO1.Direction = ParameterDirection.Input
'        PARAMETRO1.Value = oclvcompania
'        COMANDO.Parameters.Add(PARAMETRO1)

'        Dim PARAMETRO2 As New SqlParameter("@Fecha", SqlDbType.DateTime)
'        PARAMETRO2.Direction = ParameterDirection.Input
'        PARAMETRO2.Value = oFecha
'        COMANDO.Parameters.Add(PARAMETRO2)

'        Dim PARAMETRO3 As New SqlParameter("@Cajera", SqlDbType.VarChar, 11)
'        PARAMETRO3.Direction = ParameterDirection.Input
'        PARAMETRO3.Value = oCajera
'        COMANDO.Parameters.Add(PARAMETRO3)

'        Dim PARAMETRO4 As New SqlParameter("@IdFactura", SqlDbType.BigInt)
'        PARAMETRO4.Direction = ParameterDirection.Output
'        PARAMETRO4.Value = ""
'        COMANDO.Parameters.Add(PARAMETRO4)


'        Try
'            CONEXION.Open()
'            Dim i As Integer = COMANDO.ExecuteNonQuery
'            ups_NUEVAFACTGLOBALporcompania = PARAMETRO4.Value

'        Catch ex As Exception
'            ups_NUEVAFACTGLOBALporcompania = 0
'            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
'        Finally
'            CONEXION.Close()
'            CONEXION.Dispose()
'        End Try
'    End Function

'    Public Function Usp_DameIdCompaniaFacturaGlobal(ByVal oIdFactura As Long) As Long
'        Usp_DameIdCompaniaFacturaGlobal = 0
'        Dim CON01 As New SqlConnection(MiConexion)
'        Dim SQL As New SqlCommand()
'        Try
'            CON01.Open()
'            SQL = New SqlCommand()
'            With SQL
'                .CommandText = "UspEsNuevoRel_Cliente_AsociadoCFD"
'                .CommandTimeout = 0
'                .Connection = CON01
'                .CommandType = CommandType.StoredProcedure

'                Dim PRM As New SqlParameter("@IdFactura", SqlDbType.BigInt)
'                PRM.Direction = ParameterDirection.Input
'                PRM.Value = oIdFactura
'                .Parameters.Add(PRM)


'                Dim reader As SqlDataReader = .ExecuteReader()

'                While (reader.Read)
'                    Usp_DameIdCompaniaFacturaGlobal = reader.GetValue(0)
'                    'EMail_TextBox.Text = reader.GetValue(2)
'                End While
'            End With

'        Catch ex As Exception
'            System.Windows.Forms.MessageBox.Show(ex.Message)
'        Finally
'            CON01.Close()
'        End Try
'    End Function

'    Public Function USP_DameImportePorCompania(ByVal oFecha As Date, ByVal oclvcompania As Long) As Double
'        USP_DameImportePorCompania = 0
'        Dim CON01 As New SqlConnection(MiConexion)
'        Dim SQL As New SqlCommand()
'        Try
'            CON01.Open()
'            SQL = New SqlCommand()
'            With SQL
'                .CommandText = "USP_DameImportePorCompania"
'                .CommandTimeout = 0
'                .Connection = CON01
'                .CommandType = CommandType.StoredProcedure

'                Dim PRM As New SqlParameter("@Fecha", SqlDbType.DateTime)
'                PRM.Direction = ParameterDirection.Input
'                PRM.Value = oFecha
'                .Parameters.Add(PRM)
'                '@clvcompania
'                Dim PRM2 As New SqlParameter("@clvcompania", SqlDbType.BigInt)
'                PRM2.Direction = ParameterDirection.Input
'                PRM2.Value = oclvcompania
'                .Parameters.Add(PRM2)

'                Dim reader As SqlDataReader = .ExecuteReader()

'                While (reader.Read)
'                    USP_DameImportePorCompania = reader.GetValue(0)
'                    'EMail_TextBox.Text = reader.GetValue(2)
'                End While
'            End With

'        Catch ex As Exception
'            System.Windows.Forms.MessageBox.Show(ex.Message)
'        Finally
'            CON01.Close()
'        End Try
'    End Function

'End Module
