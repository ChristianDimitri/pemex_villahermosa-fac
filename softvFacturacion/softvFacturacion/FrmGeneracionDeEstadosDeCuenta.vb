﻿Imports System.Data.SqlClient

Public Class FrmGeneracionDeEstadosDeCuenta
    Dim vError As Boolean = False

    Private Sub btnGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
        FrmPeriodoDeCorteTel.Show()
    End Sub

    Private Sub FrmGeneracionDeEstadosDeCuenta_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        If vError = False Then
            'Refrescar la búsqueda de Periodo Generados
            MuestraGeneralesPeriodosDeCobro()
            ActualizaInterfaz()
        End If

    End Sub

    Private Sub FrmGeneracionDeEstadosDeCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me)
        Me.Label1.ForeColor = Color.Black
        MuestraGeneralesPeriodosDeCobro()

        Me.btnConsultar.Enabled = False
        Me.btnCargarEstadosDeCuenta.Enabled = False

    End Sub


    Private Sub MuestraGeneralesPeriodosDeCobro()

        Dim conn As New SqlConnection(MiConexion)
        Dim ds As New DataSet

        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("MuestraGeneralesPeriodosDeCobro", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            'Adaptador.SelectCommand.Parameters.Add("@CONTRATO", SqlDbType.BigInt).Value = Contrato

            Dim Bs As New BindingSource
            Adaptador.Fill(ds)

            '   DATOS GENERALES

            ds.Tables(0).TableName = "tbl_PeriodosDeCobro"
            ds.Tables(1).TableName = "tbl_EstadosDeCuenta"

            Me.dgvEstadosDeCuenta.DataSource = ds.Tables("tbl_EstadosDeCuenta")
            Me.dgvPeriodosDeCobroGenerados.DataSource = ds.Tables("tbl_PeriodosDeCobro")

            Me.dgvPeriodosDeCobroGenerados.Columns(1).Width = 160
            Me.dgvPeriodosDeCobroGenerados.Columns(2).Width = 160
            Me.dgvPeriodosDeCobroGenerados.Columns(3).Width = 160
            Me.dgvPeriodosDeCobroGenerados.Columns(4).Width = 100
            Me.dgvPeriodosDeCobroGenerados.Columns(5).Width = 100

            Me.dgvPeriodosDeCobroGenerados.Columns(0).HeaderText = "Número de Periodo de Cobro"
            Me.dgvPeriodosDeCobroGenerados.Columns(1).HeaderText = "Fecha de Generación"
            Me.dgvPeriodosDeCobroGenerados.Columns(2).HeaderText = "Fecha INICIAL"
            Me.dgvPeriodosDeCobroGenerados.Columns(3).HeaderText = "Fecha FINAL"
            Me.dgvPeriodosDeCobroGenerados.Columns(4).HeaderText = "Usuario que lo generó"
            Me.dgvPeriodosDeCobroGenerados.Columns(5).HeaderText = "Total Edos. de Cuenta Generados"

            Me.dgvPeriodosDeCobroGenerados.Columns(4).Visible = False

            If Me.dgvEstadosDeCuenta.RowCount > 0 Then

                Me.dgvEstadosDeCuenta.Columns(0).HeaderText = "ID Estado de Cuenta"
                Me.dgvEstadosDeCuenta.Columns(2).Visible = False
                Me.dgvEstadosDeCuenta.Columns(4).HeaderText = "Inicio del Proceso"
                Me.dgvEstadosDeCuenta.Columns(5).HeaderText = "Fin del Proceso"
                Me.dgvEstadosDeCuenta.Columns(7).Visible = False
                Me.dgvEstadosDeCuenta.Columns(8).Visible = False


                Me.dgvEstadosDeCuenta.Columns(4).Width = 350
                Me.dgvEstadosDeCuenta.Columns(4).Width = 200
                Me.dgvEstadosDeCuenta.Columns(5).Width = 200
                Me.dgvEstadosDeCuenta.Columns(3).Width = 250
                Me.dgvEstadosDeCuenta.Columns(1).Width = 160
                Me.dgvEstadosDeCuenta.Columns(6).Width = 100

            End If

            vError = False

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de enlazar los Datos Principales, los resultados de las búsquedas pueden ser inconsistentes.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            vError = True

            Me.Close()
            Me.Dispose()

        Finally

            conn.Close()
            conn.Dispose()

        End Try

    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub dgvEstadosDeCuenta_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEstadosDeCuenta.CellContentDoubleClick

        Dim Contrato As Integer = 0
        Dim PeriodoDeCobro As Integer = 0

        If Me.dgvEstadosDeCuenta.RowCount > 0 Then

            If Me.dgvEstadosDeCuenta.CurrentRow.Cells(1).Value > 0 Then


                Contrato = Me.dgvEstadosDeCuenta.CurrentRow.Cells(1).Value
                PeriodoDeCobro = Me.dgvPeriodosDeCobroGenerados.CurrentRow.Cells(0).Value

                FrmFACLogitel.CargaEstadoDeCuentaPorPeriodo(Contrato, PeriodoDeCobro)

            End If

        End If

    End Sub

    Private Sub dgvEstadosDeCuenta_CellMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvEstadosDeCuenta.CellMouseDoubleClick

        Dim Contrato As Integer = 0
        Dim PeriodoDeCobro As Integer = 0

        If Me.dgvEstadosDeCuenta.RowCount > 0 Then

            If Me.dgvEstadosDeCuenta.CurrentRow.Cells(1).Value > 0 Then


                Contrato = Me.dgvEstadosDeCuenta.CurrentRow.Cells(1).Value
                PeriodoDeCobro = Me.dgvPeriodosDeCobroGenerados.CurrentRow.Cells(0).Value

                FrmFACLogitel.CargaEstadoDeCuentaPorPeriodo(Contrato, PeriodoDeCobro)

            End If

        End If
    End Sub


    Private Sub CargaEstadosDeCuentaPorPeriodo(ByVal IDPeriodo As Integer)

        Dim conn As New SqlConnection(MiConexion)
        Dim ds As New DataSet

        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("CargaEstadosDeCuentaPorPeriodo", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Adaptador.SelectCommand.Parameters.Add("@PERIODO", SqlDbType.BigInt).Value = IDPeriodo

            Dim Bs As New BindingSource
            Adaptador.Fill(ds)

            '   DATOS GENERALES

            ds.Tables(0).TableName = "tbl_EstadosDeCuenta"

            Me.dgvEstadosDeCuenta.DataSource = ds.Tables("tbl_EstadosDeCuenta")
            Me.dgvEstadosDeCuenta.Columns(3).Width = 400


        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de enlazar los Datos Principales, los resultados de las búsquedas pueden ser inconsistentes.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Exit Sub

        Finally

            conn.Close()
            conn.Dispose()

        End Try

    End Sub

    Private Sub btnCargarEstadosDeCuenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCargarEstadosDeCuenta.Click

        Dim IDPeriodoDeCobro As Integer = 0


        IDPeriodoDeCobro = Me.dgvPeriodosDeCobroGenerados.CurrentRow.Cells(0).Value
        CargaEstadosDeCuentaPorPeriodo(IDPeriodoDeCobro)

    End Sub

    Private Sub ActualizaInterfaz()


        If Me.dgvPeriodosDeCobroGenerados.RowCount > 0 Then
            Me.btnConsultar.Enabled = True
            Me.btnCargarEstadosDeCuenta.Enabled = True
            Me.btnEliminarPeriodos.Enabled = True
        Else
            Me.btnConsultar.Enabled = False
            Me.btnCargarEstadosDeCuenta.Enabled = False
            Me.btnEliminarPeriodos.Enabled = False
        End If

    End Sub

    Private Sub btnEliminarPeriodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarPeriodos.Click

        Dim PeriodoAEliminar As Integer = 0
        PeriodoAEliminar = Me.dgvPeriodosDeCobroGenerados.CurrentRow.Cells(0).Value

        Dim Resp = MsgBox("¿Deseas eliminar el Periodo de Cobro:" & PeriodoAEliminar.ToString & " y sus Estados de Cuenta Generados?", MsgBoxStyle.YesNoCancel)
        If Resp = MsgBoxResult.Yes Then

            If ChecaSiPuedeEliminarPeriodoDeCobro(PeriodoAEliminar) = True Then

                Dim conn As New SqlConnection(MiConexion)
                Try

                    Dim comando As New SqlClient.SqlCommand("EliminaPeriodosDeCobro", conn)
                    comando.CommandType = CommandType.StoredProcedure

                    comando.Parameters.Add("@PERIODO", SqlDbType.Int)
                    comando.Parameters(0).Value = PeriodoAEliminar
                    comando.Parameters(0).Direction = ParameterDirection.Input

                    conn.Open()
                    comando.ExecuteNonQuery()

                    bitsist(GloCajera, 0, GloSistema, Me.Name, "", "SE ELIMINÓ UN NUEVO PERIODO DE COBRO", "ID Periodo: " & PeriodoAEliminar, LocClv_Ciudad)

                    MsgBox("El Periodo de Cobro ha sido eliminado correctamente.", MsgBoxStyle.Information)

                    MuestraGeneralesPeriodosDeCobro()
                    ActualizaInterfaz()

                Catch ex As Exception

                    MsgBox("Ocurrio un error al momento de Eliminar los Periodos de Cobro.", MsgBoxStyle.Exclamation)
                    MsgBox(ex.Message, MsgBoxStyle.Exclamation)

                Finally

                    conn.Close()
                    conn.Dispose()

                End Try

            End If


        End If

    End Sub
    Private Function ChecaSiPuedeEliminarPeriodoDeCobro(ByVal Periodo As Integer) As Boolean

        Dim conn As New SqlConnection(MiConexion)
        Dim PuedoEliminarElPeriodo As Boolean = True
        Dim Motivo As Integer = 0

        Try

            Dim comando As New SqlClient.SqlCommand("ChecaSiPuedeEliminarPeriodoDeCobro", conn)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@PUEDOELIMINAR", SqlDbType.Bit)
            comando.Parameters.Add("@PERIODO", SqlDbType.Int)
            comando.Parameters.Add("@MOTIVO", SqlDbType.Int)

            comando.Parameters(0).Value = PuedoEliminarElPeriodo
            comando.Parameters(1).Value = Periodo
            comando.Parameters(2).Value = Motivo

            comando.Parameters(0).Direction = ParameterDirection.Output
            comando.Parameters(2).Direction = ParameterDirection.Output

            conn.Open()
            comando.ExecuteNonQuery()

            PuedoEliminarElPeriodo = comando.Parameters(0).Value
            Motivo = comando.Parameters(2).Value

            If (PuedoEliminarElPeriodo = False) Then

                Select Case Motivo
                    Case 1
                        MsgBox("No se puede eliminar el Periodo de Cobro ya que existen Estados de Cuenta Pagados correspondientes a dicho Periodo", MsgBoxStyle.Information, "NO SE PUEDE ELIMINAR")
                    Case 2
                        MsgBox("Solo se pueden eliminar los Periodos de Cobro de forma descendente. Este NO es el último periodo de Cobro.", MsgBoxStyle.Information, "NO SE PUEDE ELIMINAR")
                End Select


            End If

        Catch ex As Exception

            PuedoEliminarElPeriodo = False
            Motivo = 0
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        Finally

            conn.Close()
            conn.Dispose()

        End Try

        Return PuedoEliminarElPeriodo

    End Function
End Class