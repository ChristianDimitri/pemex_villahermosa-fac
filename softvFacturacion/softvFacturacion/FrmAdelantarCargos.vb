﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Public Class FrmAdelantarCargos
    'Declaraciones básicas
    Public diccionario As Dictionary(Of String, Cargos) = New Dictionary(Of String, Cargos)
    Dim listaI As New List(Of Cargos)
    Dim listaD As New List(Of Cargos)
    Dim servicios As Cargos = Nothing

    Dim listaAuxiliar As New List(Of Cargos)
    Dim listaOriginal As New List(Of Cargos)

    Dim arreglo As New ArrayList

    'Total de Cargos a Pagar
    Dim totalAPagar As Double

    Dim FechaHoy As Date '= Now()
    Dim MesAplica As Integer = 0
    Dim MesActual As Integer = 0
    Dim anterior As Integer = 0
    Dim Mes As Integer = 0
    Dim MesCurrent As Integer '= Month(FechaHoy)
    Dim Anio As Integer = 0

    'Ultimo mes y último año del Servicio Seleccionado
    Dim Ult_Mes_Serv As Integer
    Dim Ult_Anio_Serv As Integer
    Dim Ult_Fecha_de_pago As Date

    'Identificar errores en Procedimientos
    Dim MiError As Integer
    Dim MiMsjError As String

    Private Sub FrmAdelantarCargos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Dispose()
    End Sub

    Private Sub FrmAdelantarCargos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Dame el diseño de la Interfaz
        colorea(Me)

        Me.Label1.ForeColor = Color.Black
        Me.Label2.ForeColor = Color.Black
        Me.Label3.ForeColor = Color.Black
        Me.Label4.ForeColor = Color.Black
        Me.Label5.ForeColor = Color.Black
        Me.Label6.ForeColor = Color.Black
        Me.Label7.ForeColor = Color.Black
        Me.Label8.ForeColor = Color.Black
        Me.Label9.ForeColor = Color.Black

        Me.txtNombreServicio.BackColor = Color.WhiteSmoke

        totalAPagar = 0

        NumericUpDown2.Maximum = 100
        NumericUpDown2.Increment = 10
        NumericUpDown2.Minimum = 10

        'Cargamos los servicios del Cliente (los cuales va a poder adelantar)
        Dame_servicios_Cliente(GloContrato)
        'Asignamos la fecha de último pago del servicio seleccionado
        Dame_Fecha_Ult_Pago()
        'Obtenemos la fecha actual desde el Servidor
        'DameFechaServidor()



    End Sub

  
    Private Sub Dame_servicios_Cliente(ByVal contrato As Integer)

        Dim I As Integer = 0
        Dim Fila2 As DataRow
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        Dim oDataSet As New DataSet

        Try
            'Procedimiento que obtiene los servicios que tiene asignados el cliente
            Dim cmd As New SqlCommand("usp_dameServiciosClienteParaAdelantar", conexion)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Contrato", contrato) ' Le pasamos el Contrato

            'Declaramos la lista de la clase "Cargos"
            Dim listaCargos As New List(Of Cargos)

            Dim dr As SqlDataReader
            conexion.Open()
            dr = cmd.ExecuteReader()
            'diccionario.Clear()
            listaI.Clear()

            Using dr
                While dr.Read
                    If diccionario.ContainsKey(dr("ident").ToString().Trim()) Then
                        If Not listaD.Contains(diccionario(dr("ident").ToString().Trim())) Then
                            listaI.Add(diccionario(dr("ident").ToString().Trim()))
                        End If

                    Else
                        servicios = New Cargos()

                        servicios.ident = dr("ident").ToString().Trim()
                        servicios.clv_Servicio = dr("clv_Servicio").ToString().Trim()
                        servicios.concepto = dr("concepto").ToString().Trim()
                        servicios.precio = dr("precio").ToString.Trim()
                        servicios.clv_llave = dr("clv_llave").ToString.Trim()
                        servicios.clv_unicanet = dr("clv_unicanet").ToString.Trim()
                        servicios.clv_txt = dr("clv_txt").ToString.Trim()
                        servicios.ult_mes = dr("ult_mes").ToString.Trim()
                        servicios.ult_anio = dr("ult_anio").ToString.Trim()
                        servicios.clv_TipSer = dr("clv_TipSer").ToString.Trim()
                        servicios.status_serv = dr("status_serv").ToString.Trim()
                        servicios.esServicioPrincipal = dr("esPrincipal").ToString.Trim()
                        servicios.mesesAPagar = dr("mesesAPagar").ToString.Trim()

                        'Atributos de los periodos de pago
                        servicios.periodoPagadoInicial = DateTime.Parse("01/01/1900")
                        servicios.periodoPagadoFinal = DateTime.Parse("01/01/1900")

                        diccionario.Add(servicios.ident, servicios)

                        listaI.Add(servicios)

                    End If
                End While
            End Using

            Me.ListBox1.DataSource = Nothing
            Me.ListBox1.DataSource = listaI
            Me.ListBox1.DisplayMember = "DescripcionCompleta"
            Me.ListBox1.ValueMember = "ident"

        Catch ex As Exception
            MsgBox("Ocurrió un error. No se han podido obtener los servicios del cliente, no se podrán realizar pagos adelantados.", MsgBoxStyle.Critical)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "sp_Dame_servicios_Cliente"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click

        'Si todo se guarda correctamente el cobro de cargos será desde DameDetalle y SumaDetalle
        GloAdelantarCargos = False

        Me.Dispose()
        Me.Close()

    End Sub
    Private Sub btn_enviar_a_derecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_enviar_a_derecha.Click
        If Me.ListBox1.SelectedValue <> Nothing And esUnServicio(Me.ListBox1.SelectedValue) = True Then


            'Antes de pasar el servicio le pasamos los meses 
            Me.AsignaMesesAAdelantar()



            listaD.Add(diccionario(ListBox1.SelectedValue.ToString()))
            listaI.Remove(diccionario(ListBox1.SelectedValue.ToString()))

            refreshLists()

            'CalculaTotal()

           

        End If
    End Sub
    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_enviar_a_derecha.Click

    '    If Me.ListBox1.SelectedValue <> Nothing Then
    '        listaD.Add(diccionario(ListBox1.SelectedValue.ToString()))
    '        listaI.Remove(diccionario(ListBox1.SelectedValue.ToString()))

    '        'Actualizamos los enlaces
    '        refreshLists()

    '        'Calculamos el total a pagar
    '        CalculaTotal()

    '    End If
    'End Sub

    Public Sub refreshLists()
        ListBox2.DataSource = Nothing
        ListBox2.DataSource = listaD
        ListBox2.DisplayMember = "DescripcionCompleta"
        ListBox2.ValueMember = "ident"
        ListBox1.DataSource = Nothing
        ListBox1.DataSource = listaI
        ListBox1.DisplayMember = "DescripcionCompleta"
        ListBox1.ValueMember = "ident"
        ListBox1.Refresh()
    End Sub
    Private Sub btn_enviar_a_Izquierda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_enviar_a_Izquierda.Click

        If Me.ListBox2.SelectedValue <> Nothing Then

            listaI.Add(diccionario(ListBox2.SelectedValue.ToString()))
            listaD.Remove(diccionario(ListBox2.SelectedValue.ToString()))

            arreglo.Clear()

            For Each servicios In listaI
                arreglo.Add(servicios.ident - 1)
                listaAuxiliar.Add(servicios)
                'listaAuxiliar.
            Next

            'Ordenamos el Arreglo
            'arreglo.Sort()
            'Array.Sort(arreglo.ToArray)
            'Borramos la lista que NO esta Ordenada
            listaI.Clear()

            'Barremos los elementos del arreglo para regresarlos a la lista Izquierda ya Ordenados 
            Dim i As Integer
            Dim j As Integer
            j = 0

            For i = 1 To arreglo.Count
                Minimo()
            Next

            listaAuxiliar.Clear()
            refreshLists()

        End If
    End Sub

    Public Sub Minimo()

        Dim num_minimo As Integer
        'Dim compara As Integer
        Dim i As Integer
        i = 0
        num_minimo = -1

        For Each servicios In listaAuxiliar

            If num_minimo = -1 Then
                num_minimo = servicios.ident
            End If

            If num_minimo > servicios.ident Then
                num_minimo = servicios.ident
            End If

            If listaAuxiliar.Count = 1 Then
                num_minimo = servicios.ident
            End If
        Next

        Dim IndexList As Integer
        'Veces = listaAuxiliar.Count - 1

        For Each servicios In listaAuxiliar

            If num_minimo = servicios.ident Then
                listaI.Add(servicios)
                IndexList = i
            End If
            i = i + 1
        Next

        listaAuxiliar.RemoveAt(IndexList)

    End Sub

    'Private Sub btn_enviar_a_Izquierda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_enviar_a_Izquierda.Click
    '    If Me.ListBox2.SelectedValue <> Nothing Then
    '        listaI.Add(diccionario(ListBox2.SelectedValue.ToString()))
    '        listaD.Remove(diccionario(ListBox2.SelectedValue.ToString()))

    '        refreshLists()

    '        'Calculamos el total a pagar
    '        CalculaTotal()

    '    End If
    'End Sub

    Private Sub btn_agregar_todos_a_derecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_agregar_todos_a_derecha.Click

        If ListBox1.Items.Count > 0 Then

            For Each servicios In listaI
                If servicios.concepto <> "___________    SERVICIOS DIGITALES _____________" And servicios.concepto <> "___________   SERVICIOS DE INTERNET ____________" And servicios.concepto <> "___________ SERVICIOS DE T.V. ANÁLOGA __________" And servicios.concepto <> "_____________ SERVICIOS DE TELEFONIA ___________" Then
                    listaD.Add(servicios)
                End If
            Next
            listaI.Clear()
            refreshLists()

            'Calculamos el total a pagar
            CalculaTotal()

        End If

    End Sub
    '
    'Calcula el total de Cargos a pagar
    Public Sub CalculaTotal()
        '
        Me.txt_total_a_pagar.Text = ""
        totalAPagar = 0

        For Each servicios In listaD
            totalAPagar = totalAPagar + CType(servicios.precio, Double)
        Next

        If Me.NumericUpDown1.Value = Nothing Then
            MsgBox("No sepuede establecer el número de meses a pagar, asegurate de que el total de meses sea un número válido", MsgBoxStyle.Critical)
            'Lo sacamos de la suncion y bloqueamos el control
            Me.btn_Aceptar.Enabled = False
            Exit Sub
        Else
            totalAPagar = (totalAPagar) * CType((Me.NumericUpDown1.Value), Double)
        End If

        Me.txt_total_a_pagar.Text = totalAPagar.ToString()
        Me.txt_total_a_pagar.Text = "$" & Me.txt_total_a_pagar.Text

        'Agregamos los meseas a adelantar a la fecha
        FechaHoy.ToString()
        MesAplica = 1
        MesActual = CType(Month(FechaHoy), Integer)

        If (anterior = 0) Then
            anterior = NumericUpDown1.Value
        End If

        'Obtenemos otra vez la fecha actual
        'FechaHoy = Now()
        Mes = Month(FechaHoy.AddMonths(NumericUpDown1.Value))
        Anio = Year(FechaHoy.AddMonths(NumericUpDown1.Value))

        ' Colocar el Mes que le Pertenece
        Select Case Mes
            Case 1
                Me.TextBox1.Text = "ENERO"
            Case 2
                Me.TextBox1.Text = "FEBRERO"
            Case 3
                Me.TextBox1.Text = "MARZO"
            Case 4
                Me.TextBox1.Text = "ABRIL"
            Case 5
                Me.TextBox1.Text = "MAYO"
            Case 6
                Me.TextBox1.Text = "JUNIO"
            Case 7
                Me.TextBox1.Text = "JULIO"
            Case 8
                Me.TextBox1.Text = "AGOSTO"
            Case 9
                Me.TextBox1.Text = "SEPTIEMBRE"
            Case 10
                Me.TextBox1.Text = "OCTUBRE"
            Case 11
                Me.TextBox1.Text = "NOVIEMBRE"
            Case 12
                Me.TextBox1.Text = "DICIEMBRE"
        End Select

        Me.TextBox2.Text = Anio.ToString

        Try

            'Le asignamos el rango de fechas que cubre el número de meses a adelantar
            servicios.periodoPagadoInicial = FechaHoy 'Esta es igual a la fecha que ya trae como ultimo pago el servicio
            servicios.periodoPagadoFinal = FechaHoy.AddMonths(NumericUpDown1.Value) ' Este es igual a lafecha del último pago del servicio + el número de meses a adelantar
            'Asignamos de igual forma el último mes y año pagados del Cliente
            servicios.ult_anio = Year(FechaHoy.AddMonths(NumericUpDown1.Value))
            servicios.ult_mes = Month(FechaHoy.AddMonths(NumericUpDown1.Value))
            '
        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
        

        If listaD.Count <= 0 Then
            Me.btn_Aceptar.Enabled = False
        Else
            Me.btn_Aceptar.Enabled = True
        End If

    End Sub

    
    Private Sub btn_agregar_todos_a_izquierda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_agregar_todos_a_izquierda.Click

        If ListBox2.Items.Count > 0 Then
            'For Each servicios In listaD
            '    listaI.Add(servicios)
            'Next
            listaI.Clear()
            For Each servicios In listaOriginal
                listaI.Add(servicios)
            Next
            listaD.Clear()
            refreshLists()
        End If

        'If ListBox2.Items.Count > 0 Then

        '    For Each servicios In listaD
        '        listaI.Add(servicios)
        '    Next
        '    listaD.Clear()
        '    refreshLists()

        '    'Calculamos el total a pagar
        '    CalculaTotal()
        'End If
    End Sub

    
    Private Sub NumericUpDown1_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles NumericUpDown1.ValueChanged
        'Checamos si aplica porcentaje de descuento
        If (Me.NumericUpDown1.Value >= 12) Then
            Me.pnlAplicaPorcentajeDescuento.Visible = False
        Else
            Me.pnlAplicaPorcentajeDescuento.Visible = True
        End If

        'Calculamos el total a pagar
        CalculaTotal()
    End Sub
    'Generamos el XML en base a los datos
    Private Sub generaXML()


        Try

            Dim sw As New StringWriter()
            Dim w As New XmlTextWriter(sw)
            Dim i As Integer
            Dim serv As Cargos

            w.Formatting = Formatting.Indented

            w.WriteStartElement("Cargos")
            w.WriteAttributeString("servicio", "Adelantados")

            For Each serv In listaD

                w.WriteStartElement("servicio")

                'w.WriteAttributeString("clv_servicio", serv.descripcion)
                'w.WriteAttributeString("descripcion", serv.costo)
                'w.WriteAttributeString("precio", serv.precio)
                'w.WriteAttributeString("clv_llave", serv.clv_llave)
                'w.WriteAttributeString("clv_unicanet", serv.clv_unicanet)
                'w.WriteAttributeString("clv_txt", serv.clv_txt)

                w.WriteAttributeString("clv_Servicio", serv.clv_Servicio)
                w.WriteAttributeString("concepto", serv.concepto)
                w.WriteAttributeString("precio", serv.precio)
                w.WriteAttributeString("clv_llave", serv.clv_llave)
                w.WriteAttributeString("clv_unicanet", serv.clv_unicanet)
                w.WriteAttributeString("clv_txt", serv.clv_txt)
                w.WriteAttributeString("ult_mes", serv.ult_mes)
                w.WriteAttributeString("ult_anio", serv.ult_anio)
                w.WriteAttributeString("clv_TipSer", serv.clv_TipSer)
                w.WriteAttributeString("status_serv", serv.status_serv)

                'Agregamos por cada servicio el periodo inicial y final hasta cual aplicará el pago.
                'w.WriteAttributeString("periodoPagadoInicial", serv.periodoPagadoInicial)
                'w.WriteAttributeString("periodoPagadoFinal", serv.periodoPagadoFinal)

                w.WriteAttributeString("mesesAPagar", serv.mesesAPagar)
                'w.WriteAttributeString("ult_mes", serv.ult_mes)
                'w.WriteAttributeString("ult_anio", serv.ult_anio)
                w.WriteEndElement()

            Next

            w.WriteEndElement()
            w.Close()
            sw.ToString()

            Guarda_Servicios_Adelantados(sw.ToString())

        Catch ex As Exception

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Private Sub generaXML()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        End Try

      

    End Sub
    'Método para Guardar los servicios a adelantar y guardarlos en PreDetFacturas
    Private Sub Guarda_Servicios_Adelantados(ByVal x As String)

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("sp_Guarda_Servicios_Adelantados_2", con)
        com.CommandType = CommandType.StoredProcedure

        'Le pasamos el conjunto de Serivicio a Adelantar (Clave del servisio, descripción, costo) en XML
        Dim par1 As New SqlParameter("@xml", SqlDbType.Xml)
        par1.Direction = ParameterDirection.Input
        par1.Value = x
        com.Parameters.Add(par1)

        'Le pasamos el Costo total generadopor los servicios
        Dim prmCosto_Total As New SqlParameter("@costo_total", SqlDbType.Money)
        prmCosto_Total.Direction = ParameterDirection.Input
        prmCosto_Total.Value = Me.totalAPagar
        com.Parameters.Add(prmCosto_Total)

        'Número de Meses a Pagar
        Dim prmMeses_a_pagar As New SqlParameter("@meses_a_pagar", SqlDbType.Money)
        prmMeses_a_pagar.Direction = ParameterDirection.Input
        prmMeses_a_pagar.Value = Me.NumericUpDown1.Value
        com.Parameters.Add(prmMeses_a_pagar)

        'Clave de Sesión
        Dim prmClv_Session As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        prmClv_Session.Direction = ParameterDirection.Input
        prmClv_Session.Value = GloTelClv_Session 'gloClv_Session
        com.Parameters.Add(prmClv_Session)

        'Contrato (para buscar contrataciones)
        Dim prmContrato As New SqlParameter("@Contrato", SqlDbType.BigInt)
        prmContrato.Direction = ParameterDirection.Input
        prmContrato.Value = GloContrato
        com.Parameters.Add(prmContrato)

        'Id del Sistema
        Dim prmIdSistema As New SqlParameter("@IdSistema", SqlDbType.VarChar, 10)
        prmIdSistema.Direction = ParameterDirection.Input
        prmIdSistema.Value = IdSistema
        com.Parameters.Add(prmIdSistema)

        'Error
        Dim prmError As New SqlParameter("@Error", SqlDbType.SmallInt)
        prmError.Direction = ParameterDirection.Output
        prmError.Value = 0
        com.Parameters.Add(prmError)

        'Mensaje de Error
        Dim prmMsjError As New SqlParameter("@MSGERROR", SqlDbType.VarChar, 250)
        prmMsjError.Direction = ParameterDirection.Output
        prmMsjError.Value = ""
        com.Parameters.Add(prmMsjError)


        Try
            con.Open()
            com.ExecuteNonQuery()

            'Obtenemos los valores regresados por el Procedimiento para char si hubo un error controlado por nosotros
            'MiError = prmError.Value
            'MiMsjError = prmMsjError.Value

            If (MiError = 1) Then
                'Mandamos el error a la pantalla inicial
                miError_Cobra = 1
                miMsjError_Cobra = MiMsjError

                MsgBox("Ocurrió un error al momento de guardar los servicios a Adelantar. Descripción: " & MiMsjError.ToString)
                MsgBox("Procedimiento: sp_Guarda_Servicios_Adelantados_2", MsgBoxStyle.Exclamation)
                Exit Sub
                Me.Dispose()
                Me.Close()

            End If

            'Si todo se guarda correctamente el cobro de cargos será desde DameDetalle y SumaDetalle
            GloAdelantarCargos = True

            Me.Dispose()
            Me.Close()

        Catch ex As Exception
            MsgBox("Ocurrió un error, no se ha podido guardar la relación de pagos adelantados para los servicios.", MsgBoxStyle.Critical)
            MsgBox(ex.Message)
            Me.Dispose()
            Me.Close()


            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "sp_Guarda_Servicios_Adelantados_2"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)


            Exit Sub
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub

    Private Sub btn_Aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Aceptar.Click
        If listaD.Count > 0 Then
            generaXML()
        Else
            MsgBox("No hay servicios seleccionados, debes seleccionar mínimamente un servicio o dar clic en el botón Salir para no adelantar ninguno.", MsgBoxStyle.Exclamation)
        End If
    End Sub
    'Dame la Fecha del Servidor
    Public Sub DameFechaServidor()

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("DAMEFECHADELSERVIDOR", con)
        com.CommandType = CommandType.StoredProcedure

        'Dame la fecha del Servidor
        Dim prmFechaObtebida As New SqlParameter("@FECHA", SqlDbType.DateTime)
        prmFechaObtebida.Direction = ParameterDirection.Output
        prmFechaObtebida.Value = ""
        com.Parameters.Add(prmFechaObtebida)

        Try
            con.Open()
            com.ExecuteNonQuery()

            FechaHoy = prmFechaObtebida.Value

        Catch ex As Exception
            MsgBox("Error: No se pudo obtener la fecha válida del Servidor.", MsgBoxStyle.Critical)
            MsgBox("Descripción:  Procedimiento [DAMEFECHADELSERVIDOR] // Función [DameFechaServidor] // Form: FrmAdelantarCargos", MsgBoxStyle.Information)
            MsgBox(ex.Message)
        Finally
            con.Close()
        End Try
        
    End Sub
    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        'Identitifamos que servicio es el que tenemos cargado para colocar su ultimo mes y año pagado en la Interfaz
        Try
            If Me.ListBox1.SelectedValue <> Nothing Then
                'MsgBox("SELECCION: " & Me.ListBox1.SelectedIndex.ToString)
                'MsgBox(diccionario(ListBox1.SelectedValue.ToString()))
                Dame_Fecha_Ult_Pago()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Public Sub Dame_Fecha_Ult_Pago()
        Try
            If ListBox1.Items.Count > 0 Then
                'Recorremos la lista en busca del objeto seleccionado
                For Each servicios In listaI
                    If servicios.ident = Me.ListBox1.SelectedValue.ToString And servicios.clv_Servicio > 0 Then

                        'Asignamos la fecha del último pago del servicio seleccionado
                        Ult_Anio_Serv = servicios.ult_anio
                        Ult_Mes_Serv = servicios.ult_mes

                        Dim stDate As String '= "31/12/2006"
                        'El día no nos importa tanto así que quedará en "01" ya que solo es último Mes y año pagados
                        stDate = "01/" & Ult_Mes_Serv.ToString & "/" & Ult_Anio_Serv.ToString

                        Dim dtDate As Date
                        dtDate = DateTime.Parse(stDate)

                        'Asignamos la fecha del último pago del servicio seleccionado
                        FechaHoy = dtDate

                        'Asignamos la fecha del último pago del servicio seleccionado
                        Ult_Anio_Serv = servicios.ult_anio
                        Ult_Mes_Serv = servicios.ult_mes


                        'Mostramos el último mes y año pagados del Servicio
                        Mes = Ult_Mes_Serv 'Month(FechaHoy.AddMonths(NumericUpDown1.Value))
                        Anio = Ult_Anio_Serv 'Year(FechaHoy.AddMonths(NumericUpDown1.Value))

                        'Colocar el Mes que le Pertenece
                        Select Case Mes
                            Case 1
                                Me.TextBox1.Text = "ENERO"
                            Case 2
                                Me.TextBox1.Text = "FEBRERO"
                            Case 3
                                Me.TextBox1.Text = "MARZO"
                            Case 4
                                Me.TextBox1.Text = "ABRIL"
                            Case 5
                                Me.TextBox1.Text = "MAYO"
                            Case 6
                                Me.TextBox1.Text = "JUNIO"
                            Case 7
                                Me.TextBox1.Text = "JULIO"
                            Case 8
                                Me.TextBox1.Text = "AGOSTO"
                            Case 9
                                Me.TextBox1.Text = "SEPTIEMBRE"
                            Case 10
                                Me.TextBox1.Text = "OCTUBRE"
                            Case 11
                                Me.TextBox1.Text = "NOVIEMBRE"
                            Case 12
                                Me.TextBox1.Text = "DICIEMBRE"
                        End Select

                        Me.txtNombreServicio.Text = servicios.concepto

                        'Mostrar el último Año
                        Me.TextBox2.Text = Anio.ToString
                        '
                        'Le asignamos el rango de fechas que cubre el número de meses a adelantar
                        servicios.periodoPagadoInicial = FechaHoy 'Esta es igual a la fecha que ya trae como ultimo pago el servicio
                        servicios.periodoPagadoFinal = FechaHoy.AddMonths(NumericUpDown1.Value) ' Este es igual a lafecha del último pago del servicio + el número de meses a adelantar

                    End If
                Next
            End If
        Catch ex As Exception
            MsgBox("Error al momento de Obtener la fecha del último pago del servicio: " & Me.ListBox1.SelectedValue.ToString)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Public Sub Dame_Fecha_Ult_Pago()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)


        End Try
    End Sub
    Private Function esUnServicio(ByRef identificador As Integer) As Boolean

        'Barremos los diferentes servicios para verificar si el que esta seleccionado es un Servicio válido o si es una leyenda de tipo de servicios
        Try
            If ListBox1.Items.Count > 0 Then
                For Each servicios In listaI
                    If servicios.ident = identificador Then
                        If servicios.concepto = "___________    SERVICIOS DIGITALES _____________" Or servicios.concepto = "___________   SERVICIOS DE INTERNET ____________" Or servicios.concepto = "___________ SERVICIOS DE T.V. ANÁLOGA __________" Or servicios.concepto = "_____________ SERVICIOS DE TELEFONIA ___________" Then
                            Return False
                        End If
                    End If
                Next
                Return True
            End If
        Catch ex As Exception
            MsgBox("Ocurrió un error en la función: esUnServicio() As Boolean" & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Function
    Private Sub AsignaMesesAAdelantar()
        'Recorremos la lista Lista en busca del servicio que es el que se va a adelantar en sus pagos
        For Each servicios In listaI
            'Si es el que se esta enviando a la lista Derecha
            If servicios.ident = Me.ListBox1.SelectedValue Then
                'Le asignamos el número de Meses que se indicaron al pasar el servicio a la lista derecha
                servicios.mesesAPagar = Me.NumericUpDown1.Value
            End If
        Next
    End Sub
    Private Sub DevuelveMesesAAdelantar()
        'Recorremos la lista Lista en busca del servicio que es el que se va a adelantar en sus pagos
        For Each servicios In listaI
            'Si es el que se esta enviando a la lista Derecha
            If servicios.ident = Me.ListBox1.SelectedValue Then
                'Le asignamos el número de Meses que se indicaron al pasar el servicio a la lista derecha
                servicios.mesesAPagar = 0
            End If
        Next
    End Sub
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
Public Class Cargos
    Implements IComparer

    Dim _ident As Integer
    Dim _clv_Servicio As Integer
    Dim _concepto As String
    Dim _precio As Double
    Dim _clv_llave As Integer
    Dim _clv_unicanet As Integer
    Dim _clv_txt As String
    Dim _ult_mes As Integer
    Dim _ult_anio As Integer
    Dim _clv_TipSer As Integer
    Dim _status_serv As String
    Dim _esServicioPrincipal As Integer

    'Atributos Adicionales
    Dim _periodoPagadoInicial As Date
    Dim _periodoPagadoFinal As Date
    Dim _mesesAPagar As Integer


    Public Property ident() As Integer
        Get
            Return _ident
        End Get
        Set(ByVal Value As Integer)
            _ident = Value
        End Set
    End Property
    Public Property clv_Servicio() As Integer
        Get
            Return _clv_Servicio
        End Get
        Set(ByVal Value As Integer)
            _clv_Servicio = Value
        End Set
    End Property
    Public Property concepto() As String
        Get
            Return _concepto
        End Get
        Set(ByVal Value As String)
            _concepto = Value
        End Set
    End Property
    Public Property precio() As Double
        Get
            Return _precio
        End Get
        Set(ByVal Value As Double)
            _precio = Value
        End Set
    End Property
    Public Property clv_llave() As Integer
        Get
            Return _clv_llave
        End Get
        Set(ByVal Value As Integer)
            _clv_llave = Value
        End Set
    End Property
    Public Property clv_unicanet() As Integer
        Get
            Return _clv_unicanet
        End Get
        Set(ByVal Value As Integer)
            _clv_unicanet = Value
        End Set
    End Property
    Public Property clv_txt() As String
        Get
            Return _clv_txt
        End Get
        Set(ByVal Value As String)
            _clv_txt = Value
        End Set
    End Property
    Public Property clv_TipSer() As Integer
        Get
            Return _clv_TipSer
        End Get
        Set(ByVal Value As Integer)
            _clv_TipSer = Value
        End Set
    End Property
    Public Property status_serv() As String
        Get
            Return _status_serv
        End Get
        Set(ByVal Value As String)
            _status_serv = Value
        End Set
    End Property
    Public Property esServicioPrincipal() As Integer
        Get
            Return _esServicioPrincipal
        End Get
        Set(ByVal Value As Integer)
            _esServicioPrincipal = Value
        End Set
    End Property
    Public ReadOnly Property DescripcionCompleta() As String

        Get
            Dim StatusMostrar As String
            StatusMostrar = ""
            If _status_serv = "C" Then
                StatusMostrar = "Contratado"
            ElseIf _status_serv = "I" Then
                StatusMostrar = "Instalado"
            ElseIf _status_serv = "D" Then
                StatusMostrar = "Desconectado"
            ElseIf _status_serv = "S" Then
                StatusMostrar = "Suspendido"
            ElseIf _status_serv = "B" Then
                StatusMostrar = "Baja"
            ElseIf _status_serv = "F" Then
                StatusMostrar = "Fuera de Area"
            End If
            If _clv_Servicio <> 0 Then
                Return _concepto + " || " + CType(StatusMostrar, String)
            Else
                Return _concepto
            End If

        End Get
    End Property
    
    Public Property ult_mes() As Integer
        Get
            Return _ult_mes
        End Get
        Set(ByVal Value As Integer)
            _ult_mes = Value
        End Set
    End Property
    Public Property ult_anio() As Integer
        Get
            Return _ult_anio
        End Get
        Set(ByVal Value As Integer)
            _ult_anio = Value
        End Set
    End Property
    Public Property periodoPagadoInicial() As Date
        Get
            Return _periodoPagadoInicial
        End Get
        Set(ByVal Value As Date)
            _periodoPagadoInicial = Value
        End Set
    End Property
    Public Property periodoPagadoFinal() As Date
        Get
            Return _periodoPagadoFinal
        End Get
        Set(ByVal Value As Date)
            _periodoPagadoFinal = Value
        End Set
    End Property
    Public Property mesesAPagar() As Integer
        Get
            Return _mesesAPagar
        End Get
        Set(ByVal Value As Integer)
            _mesesAPagar = Value
        End Set
    End Property
    Public Sub ServicioCobroAdeudo(ByVal descripcion As String, ByVal costo As String)
        'Constructor de la Clase
        Me.ident = ident
        Me.clv_Servicio = clv_Servicio
        Me.concepto = concepto
        Me.precio = precio
        Me.clv_llave = clv_llave
        Me.clv_unicanet = clv_unicanet
        Me.clv_txt = clv_txt
        Me.ult_mes = ult_mes
        Me.ult_anio = ult_anio
        Me.clv_TipSer = clv_TipSer
        Me.status_serv = status_serv
        Me.esServicioPrincipal = esServicioPrincipal
        Me.mesesAPagar = mesesAPagar
    End Sub


    Function Compare(ByVal a As Object, ByVal b As Object) As Integer Implements IComparer.Compare

        Dim c1 As ServicioCobroAdeudo = CType(a, ServicioCobroAdeudo)
        Dim c2 As ServicioCobroAdeudo = CType(b, ServicioCobroAdeudo)

        If (c1.ident > c2.ident) Then
            Return 1
        End If

        If (c1.ident < c2.ident) Then
            Return -1
        Else
            Return 0
        End If
    End Function


End Class


''Definimos la Clase de los Serivicio/Cargos
'Public Class Cargos
'    Dim _descripcion As String
'    Dim _costo As String
'    Dim _precio As String
'    Dim _clv_llave As Integer
'    Dim _clv_unicanet As Integer
'    Dim _clv_txt As String
'    Dim _ult_mes As Integer
'    Dim _ult_anio As Integer
'    Dim _periodoPagadoInicial As Date
'    Dim _periodoPagadoFinal As Date

'    Public Property descripcion() As String
'        Get
'            Return _descripcion
'        End Get
'        Set(ByVal Value As String)
'            _descripcion = Value
'        End Set
'    End Property
'    Public Property costo() As String
'        Get
'            Return _costo
'        End Get
'        Set(ByVal Value As String)
'            _costo = Value
'        End Set
'    End Property
'    Public Property precio() As String
'        Get
'            Return _precio
'        End Get
'        Set(ByVal Value As String)
'            _precio = Value
'        End Set
'    End Property
'    Public Property clv_llave() As String
'        Get
'            Return _clv_llave
'        End Get
'        Set(ByVal Value As String)
'            _clv_llave = Value
'        End Set
'    End Property
'    Public Property clv_unicanet() As String
'        Get
'            Return _clv_unicanet
'        End Get
'        Set(ByVal Value As String)
'            _clv_unicanet = Value
'        End Set
'    End Property
'    Public Property clv_txt() As String
'        Get
'            Return _clv_txt
'        End Get
'        Set(ByVal Value As String)
'            _clv_txt = Value
'        End Set
'    End Property
'    Public ReadOnly Property DescripcionCompleta() As String
'        Get
'            Return _costo + " || Costo: $" + _precio
'        End Get
'    End Property
'    Public Property ult_mes() As Integer
'        Get
'            Return _ult_mes
'        End Get
'        Set(ByVal Value As Integer)
'            _ult_mes = Value
'        End Set
'    End Property
'    Public Property ult_anio() As Integer
'        Get
'            Return _ult_anio
'        End Get
'        Set(ByVal Value As Integer)
'            _ult_anio = Value
'        End Set
'    End Property
'    Public Property periodoPagadoInicial() As Date
'        Get
'            Return _periodoPagadoInicial
'        End Get
'        Set(ByVal Value As Date)
'            _periodoPagadoInicial = Value
'        End Set
'    End Property
'    Public Property periodoPagadoFinal() As Date
'        Get
'            Return _periodoPagadoFinal
'        End Get
'        Set(ByVal Value As Date)
'            _periodoPagadoFinal = Value
'        End Set
'    End Property

'    Public Sub Cargos(ByVal descripcion As String, ByVal costo As String)
'        'Constructor de la Clase
'        Me.descripcion = descripcion
'        Me.costo = costo
'        Me.precio = precio
'        Me.clv_llave = clv_llave
'        Me.clv_unicanet = clv_unicanet
'        Me.clv_txt = clv_txt
'        Me.ult_mes = ult_mes
'        Me.ult_anio = ult_anio
'    End Sub
'End Class
