Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class CAPTURAFACTURAGLOBAL
    Dim locIdFac As Long
    Dim locerror As Integer
    Private customersByCityReport As ReportDocument
    Dim Mes As Integer
    Dim StrMes As String
    Dim a�o As Integer
    Dim fecha As Date
    Dim glofac As Long = 0
    Dim locSucursal As Long = 1
    Private GloFacGlobalclvcompania As Long = 0
    Public GloNomReporteFiscal As String = ""
    Public GloNomReporteFiscalGlobal As String = ""

    Private Sub CAPTURAFACTURAGLOBAL_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bnd = 3 Then
            If glotipoFacGlo = 0 Then
                Me.DateTimePicker2.Visible = False
                Me.Label1.Visible = False
            ElseIf glotipoFacGlo = 1 Then
                Me.DateTimePicker2.Visible = True
            End If
            bnd = 0
        End If
    End Sub


    Private Sub CAPTURAFACTURAGLOBAL_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        If Gloop = "N" Then
            Dim CON1 As New SqlConnection(MiConexion)
            CON1.Open()
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.DameFechadelServidorHora' Puede moverla o quitarla seg�n sea necesario.
            Me.DameFechadelServidorHoraTableAdapter.Connection = CON1
            Me.DameFechadelServidorHoraTableAdapter.Fill(Me.NewsoftvDataSet2.DameFechadelServidorHora)
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.mueastrasuc' Puede moverla o quitarla seg�n sea necesario.
            Me.MueastrasucTableAdapter.Connection = CON1
            Me.MueastrasucTableAdapter.Fill(Me.NewsoftvDataSet2.mueastrasuc, bec_tipo)
            CON1.Close()
            'ValidaFechas()
            Me.ComboBox1.Text = ""
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.MUESTRASUCURSALES' Puede moverla o quitarla seg�n sea necesario.
            If eAccesoAdmin = True Then
                Me.DateTimePicker1.Enabled = True
                Me.DateTimePicker2.Enabled = True
            Else
                Me.DateTimePicker2.Enabled = False
                Me.DateTimePicker1.Enabled = False
            End If

            If GloTipoUsuario = 2 Or GloTipoUsuario = 21 Then
                Me.pnlOcultaImporte.Visible = True
            Else
                Me.pnlOcultaImporte.Visible = False
            End If
            If bnd = 1 Then
                FrmTipoNota.Show()
            End If
        ElseIf Gloop = "C" Then
            Me.Panel1.Enabled = False
            Me.GroupBox1.Enabled = False
            Me.ComboBox1.Text = Locclv_sucursalglo
            Me.SerieTextBox.Text = LocSerieglo
            Me.NuevafacturaTextBox.Text = LocFacturaGlo
            Me.DateTimePicker2.Text = LocFechaGlo
            Me.DateTimePicker1.Text = LocFechaGlo

            If GloTipoUsuario = 2 Or GloTipoUsuario = 21 Then
                Me.pnlOcultaImporte.Visible = True
            Else
                Me.pnlOcultaImporte.Visible = False
            End If

            'MODIFICADO 29 DIC 2009-------------------------------------------------------------
            'Me.TextBox4.Text = LocImporteGlo
            TextBoxImporte.Text = LocImporteGlo

            Me.TextBox5.Text = LocCajeroGlo
        End If
    End Sub

    Private Sub ConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal Importe As String, ByVal SubTotal As String, ByVal Iva As String, ByVal Ieps As String, _
                                                     ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String, ByVal SubTotalConIeps As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim cliente2 As String = "P�blico en General"
            Dim concepto2 As String = "Ingreso por Pago de Servicios"
            Dim reportPath As String = Nothing

            Select Case Locclv_empresa
                Case "AG"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalGiga.rpt"
                Case "TO"
                    reportPath = RutaReportes + "\ReporteFacturaGlobal.rpt"
                Case "SA"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
                Case "VA"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
            End Select
            customersByCityReport.Load(reportPath)

            If Locclv_empresa = "SA" Or Locclv_empresa = "TO" Or Locclv_empresa = "AG" Or Locclv_empresa = "VA" Then


                customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & Format(CDec(SubTotal.ToString()), "####0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"

                customersByCityReport.DataDefinition.FormulaFields("SubTotal").Text = "'" & Format(CDec(SubTotal.ToString()), "####0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & Format(CDec(Iva.ToString()), "####0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ieps").Text = "'" & Format(CDec(Ieps.ToString()), "####0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & Format(CDec(Importe.ToString()), "####0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTotalConIeps").Text = "'" & Format(CDec(SubTotalConIeps.ToString()), "####0.00") & "'"
            End If

            Select Case Locclv_empresa
                Case "AG"
                    'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
                    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
                Case "TO"
                    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
                Case "SA"
                    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
                Case "VA"
                    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            End Select

            MsgBox("Se va a imprimir una Factura Fiscal. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
            MsgBox("Se va a imprimir la copia de la Factura Fiscal anterior. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    'Private Sub oldConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal importe2 As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String)
    '    customersByCityReport = New ReportDocument
    '    Dim connectionInfo As New ConnectionInfo
    '    '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
    '    connectionInfo.ServerName = GloServerName
    '    connectionInfo.DatabaseName = GloDatabaseName
    '    connectionInfo.UserID = GloUserID
    '    connectionInfo.Password = GloPassword




    '    Dim cliente2 As String = "P�blico en General"
    '    Dim concepto2 As String = "Ingreso por Pago de Servicios"
    '    Dim txtsubtotal As String = Nothing
    '    Dim subtotal2 As Double
    '    Dim iva2 As Double
    '    Dim myString As String = iva2.ToString("00.00")
    '    Dim reportPath As String = Nothing
    '    If Locclv_empresa <> "TO" Then
    '        reportPath = RutaReportes + "\ReporteFacturaGlobalticket.rpt"
    '    Else
    '        reportPath = RutaReportes + "\ReporteFacturaGlobal.rpt"
    '    End If
    '    'MsgBox(reportPath)
    '    customersByCityReport.Load(reportPath)
    '    'SetDBLogonForReport(connectionInfo, customersByCityReport)
    '    'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
    '    'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
    '    subtotal2 = CDec(importe2) / 1.15
    '    txtsubtotal = subtotal2
    '    txtsubtotal = subtotal2.ToString("##0.00")
    '    iva2 = CDec(importe2) / 1.15 * 0.15
    '    myString = iva2.ToString("##0.00")
    '    customersByCityReport.DataDefinition.FormulaFields("Subtotal").Text = "'" & txtsubtotal & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & myString & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & txtsubtotal & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & importe2 & "'"

    '    If Locclv_empresa = "SA" Then
    '        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
    '    ElseIf Locclv_empresa <> "TO" Then
    '        customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
    '    Else
    '        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
    '    End If

    '    customersByCityReport.PrintToPrinter(1, True, 1, 1)

    '    'SetDBLogonForReport(connectionInfo, customersByCityReport)
    '    'CrystalReportViewer1.ReportSource = customersByCityReport
    '    customersByCityReport = Nothing


    'End Sub



    Private Sub Guardar()

        'Try
        '    Dim glofac As Integer = 0
        '    Dim comando As New SqlClient.SqlCommand
        '    Dim Comando2 As New SqlClient.SqlCommand
        '    Dim CON3 As New SqlConnection(MiConexion)
        '    If glotipoFacGlo = 1 Then
        '        CON3.Open()
        '        If Gloop = "N" Then
        '            Me.Llena_Factura_Global_nuevoTableAdapter.Connection = CON3
        '            Me.Llena_Factura_Global_nuevoTableAdapter.Fill(Me.Procedimientos_arnoldo.Llena_Factura_Global_nuevo, CInt(Me.ComboBox1.SelectedValue), locerror)
        '        End If
        '        Me.NUEVAFACTGLOBALTableAdapter.Connection = CON3

        '        'MODIFICADO 29 DIC 09---------------------------------------------------------------
        '        'Me.NUEVAFACTGLOBALTableAdapter.Fill(Me.DataSetLydia.NUEVAFACTGLOBAL, glofac, bec_tipo, Me.SerieTextBox.Text, Me.NuevafacturaTextBox.Text, Me.DateTimePicker2.Text, Me.TextBox5.Text, CDec(Me.TextBox4.Text), 0, CStr(Me.ComboBox1.SelectedValue), CDec(Me.Label6.Tag), bec_consecutivo)
        '        Me.NUEVAFACTGLOBALTableAdapter.Fill(Me.DataSetLydia.NUEVAFACTGLOBAL, glofac, bec_tipo, Me.SerieTextBox.Text, Me.NuevafacturaTextBox.Text, Me.DateTimePicker2.Text, Me.TextBox5.Text, CDec(TextBoxImporte.Text), 0, CStr(Me.ComboBox1.SelectedValue), CDec(Me.Label6.Tag), bec_consecutivo)
        '        NueDetFacturaGlobalImpuestos(glofac, TextBoxIVA.Text, TextBoxIEPS.Text, TextBoxSubTotal.Text, TextBoxImporte.Text)
        '        With Comando2
        '            .CommandText = "GUARDARel_FacturaGlo_Rango"
        '            .CommandTimeout = 0
        '            .CommandType = CommandType.StoredProcedure
        '            .Connection = CON3
        '            Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
        '            prm.Direction = ParameterDirection.Input
        '            prm.Value = glofac
        '            .Parameters.Add(prm)

        '            Dim prm2 As New SqlParameter("@Fecha_Ini", SqlDbType.DateTime)
        '            prm2.Direction = ParameterDirection.Input
        '            prm2.Value = Me.DateTimePicker1.Text
        '            .Parameters.Add(prm2)

        '            Dim prm3 As New SqlParameter("@Fecha_Fin", SqlDbType.DateTime)
        '            prm3.Direction = ParameterDirection.Input
        '            prm3.Value = Me.DateTimePicker2.Text
        '            .Parameters.Add(prm3)

        '            Dim i As Integer = Comando2.ExecuteNonQuery
        '        End With
        '        CON3.Close()
        '    Else
        '        CON3.Open()
        '        If Gloop = "N" Then
        '            Me.Llena_Factura_Global_nuevoTableAdapter.Connection = CON3
        '            Me.Llena_Factura_Global_nuevoTableAdapter.Fill(Me.Procedimientos_arnoldo.Llena_Factura_Global_nuevo, CInt(Me.ComboBox1.SelectedValue), locerror)
        '        End If
        '        Me.NUEVAFACTGLOBALTableAdapter.Connection = CON3
        '        'MODIFICADO 29 DIC 09---------------------------------------------------------------
        '        'Me.NUEVAFACTGLOBALTableAdapter.Fill(Me.DataSetLydia.NUEVAFACTGLOBAL, glofac, bec_tipo, Me.SerieTextBox.Text, Me.NuevafacturaTextBox.Text, Me.DateTimePicker1.Text, Me.TextBox5.Text, CDec(Me.TextBox4.Text), 0, CStr(Me.ComboBox1.SelectedValue), CDec(Me.Label6.Tag), bec_consecutivo)
        '        Me.NUEVAFACTGLOBALTableAdapter.Fill(Me.DataSetLydia.NUEVAFACTGLOBAL, glofac, bec_tipo, Me.SerieTextBox.Text, Me.NuevafacturaTextBox.Text, Me.DateTimePicker1.Text, Me.TextBox5.Text, CDec(TextBoxImporte.Text), 0, CStr(Me.ComboBox1.SelectedValue), CDec(Me.Label6.Tag), bec_consecutivo)
        '        CON3.Close()
        '        NueDetFacturaGlobalImpuestos(glofac, TextBoxIVA.Text, TextBoxIEPS.Text, TextBoxSubTotal.Text, TextBoxImporte.Text)
        '    End If
        '    CON3.Open()
        '    With comando
        '        .CommandText = "Guarda_Desgloce_con_iva"
        '        .CommandTimeout = 0
        '        .CommandType = CommandType.StoredProcedure
        '        .Connection = CON3
        '        Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
        '        prm.Direction = ParameterDirection.Input
        '        prm.Value = glofac
        '        .Parameters.Add(prm)
        '        Dim i As Integer = comando.ExecuteNonQuery
        '    End With
        '    CON3.Close()
        '    'MODIFICADO 29 DIC 09---------------------------------------------------------------
        '    'bitsist(GloUsuario, 0, GloSistema, "Nueva Factura Global", "Sucursal En Donde Se Gener�: " + CStr(GloSucursal) + ", Nombre de la Caja: " + CStr(GlonOMCaja), "Se Gener� la Factura Global del cajero: " + Me.TextBox5.Text, "Del Dia:" + Me.DateTimePicker1.Text + " Por la Cantidad de: " + TEXTbox4.Text, LocNomciudad)
        '    bitsist(GloUsuario, 0, GloSistema, "Nueva Factura Global", "Sucursal En Donde Se Gener�: " + CStr(GloSucursal) + ", Nombre de la Caja: " + CStr(GlonOMCaja), "Se Gener� la Factura Global del cajero: " + Me.TextBox5.Text, "Del Dia:" + Me.DateTimePicker1.Text + " Por la Cantidad de: " + TextBoxImporte.Text, LocNomciudad)

        'Catch ex As System.Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try
        Try

            Dim comando As New SqlClient.SqlCommand
            Dim Comando2 As New SqlClient.SqlCommand
            Dim CON3 As New SqlConnection(MiConexion)
            'Me.NUEVAFACTGLOBALTableAdapter.Connection = CON3
            'Me.NUEVAFACTGLOBALTableAdapter.Fill(Me.DataSetLydia.NUEVAFACTGLOBAL, glofac, bec_tipo, Me.SerieTextBox.Text, Me.NuevafacturaTextBox.Text, Me.DateTimePicker2.Text, Me.TextBox5.Text, CDec(Me.TextBox4.Text), 0, CStr(Me.ComboBox1.SelectedValue), CDec(Me.Label6.Tag), bec_consecutivo)

            'glofac = ups_NUEVAFACTGLOBALporcompania(GloFacGlobalclvcompania, DateTimePicker1.Value.ToShortDateString, GloUsuario, Me.ComboBox1.SelectedValue)

            If DateTimePicker2.Visible = True Then
                glofac = ups_NUEVAFACTGLOBALporcompania(GloFacGlobalclvcompania, DateTimePicker1.Value.ToShortDateString, GloUsuario, Me.ComboBox1.SelectedValue, DateTimePicker2.Value.ToShortDateString)

            Else
                glofac = ups_NUEVAFACTGLOBALporcompania(GloFacGlobalclvcompania, DateTimePicker1.Value.ToShortDateString, GloUsuario, Me.ComboBox1.SelectedValue, DateTimePicker1.Value.ToShortDateString)
            End If
            locIdFac = glofac

            If glofac = 0 Then
                MsgBox("No Se Grabo la Factura Global ", vbInformation)
                Exit Sub
            End If
            bitsist(GloUsuario, 0, GloSistema, "Nueva Factura Global", "Sucursal En Donde Se Gener�: " + CStr(GloSucursal) + ", Nombre de la Caja: " + CStr(GlonOMCaja), "Se Gener� la Factura Global del cajero: " + Me.TextBox5.Text, "Del Dia:" + Me.DateTimePicker1.Text + " Por la Cantidad de: " + Me.TextBox4.Text, LocNomciudad)


        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Function ups_NUEVAFACTGLOBALporcompania(ByVal oclvcompania As Long, ByVal oFecha As Date, ByVal oCajera As String, ByVal oSucursal As Integer, oFechaFin As Date) As Long
        ups_NUEVAFACTGLOBALporcompania = 0
        '@clvcompania int,@Fecha datetime,@Cajera varchar(11),@IdFactura bigint output

        Dim DT As New DataTable
        BaseII.limpiaParametros()
        DT = BaseII.ConsultaDT("UspDameClvCompa�ia")

        'oclvcompania = 1
        oclvcompania = CInt(DT.Rows(0)(0).ToString)
        GloNomReporteFiscal = ""
        GloNomReporteFiscalGlobal = ""
        Dim CONEXION As New SqlConnection(MiConexion)
        Dim COMANDO As New SqlCommand("ups_NUEVAFACTGLOBALporcompania", CONEXION)
        COMANDO.CommandType = CommandType.StoredProcedure
        COMANDO.CommandTimeout = 0
        '@Clv_Factura Bigint,@Clv_FacturaCFD bigint,@Serie varchar(5)
        Dim PARAMETRO1 As New SqlParameter("@clvcompania", SqlDbType.BigInt)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = oclvcompania
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@FechaIn", SqlDbType.DateTime)
        PARAMETRO2.Direction = ParameterDirection.Input
        PARAMETRO2.Value = oFecha
        COMANDO.Parameters.Add(PARAMETRO2)


        Dim PARAMETRO12 As New SqlParameter("@FechaFin", SqlDbType.DateTime)
        PARAMETRO12.Direction = ParameterDirection.Input
        PARAMETRO12.Value = oFechaFin
        COMANDO.Parameters.Add(PARAMETRO12)



        Dim PARAMETRO3 As New SqlParameter("@Cajera", SqlDbType.VarChar, 11)
        PARAMETRO3.Direction = ParameterDirection.Input
        PARAMETRO3.Value = oCajera
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@IdFactura", SqlDbType.BigInt)
        PARAMETRO4.Direction = ParameterDirection.Output
        PARAMETRO4.Value = ""
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@Sucursal", SqlDbType.Int)
        PARAMETRO5.Direction = ParameterDirection.Input
        PARAMETRO5.Value = oSucursal
        COMANDO.Parameters.Add(PARAMETRO5)


        Try
            CONEXION.Open()
            Dim i As Integer = COMANDO.ExecuteNonQuery
            ups_NUEVAFACTGLOBALporcompania = PARAMETRO4.Value

        Catch ex As Exception
            ups_NUEVAFACTGLOBALporcompania = 0
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CONEXION.Close()
            CONEXION.Dispose()
        End Try
    End Function

    Private Sub CantidadaLetra()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameCantidadALetraTableAdapter.Connection = CON
            'MODIFICADO 29 DIC 09---------------------------------------------------------------
            'Me.DameCantidadALetraTableAdapter.Fill(Me.NewsoftvDataSet2.DameCantidadALetra, CDec(Me.TextBox4.Text), 0, bec_letra)
            Me.DameCantidadALetraTableAdapter.Fill(Me.NewsoftvDataSet2.DameCantidadALetra, CDec(TextBoxImporte.Text), 0, bec_letra)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ValidaFechas()
        Mes = CInt(Month(Me.DateTimePicker1.Text))
        a�o = CInt(Year(Me.DateTimePicker1.Text))
        fecha = DateSerial(a�o, Mes + 1, 0)
        StrMes = CStr(Mes)
        StrMes = StrMes.PadLeft(2, "0")
        Me.DateTimePicker1.MinDate = CDate("01/" + StrMes + "/" + CStr(a�o))
        Me.DateTimePicker1.MaxDate = CDate(fecha)
        Me.DateTimePicker2.MinDate = CDate("01/" + StrMes + "/" + CStr(a�o))
        Me.DateTimePicker2.MaxDate = CDate(fecha)
        Me.Label1.Visible = True
    End Sub

    Private Sub HazFacturaDigitalGlobal(ByVal LocClv_FacturaGlobal As Integer)
        FacturacionDigitalSoftv.ClassCFDI.MiConexion = MiConexion
        Dim identi As Integer = 0
        FacturacionDigitalSoftv.ClassCFDI.EsTimbrePrueba = False
        FacturacionDigitalSoftv.ClassCFDI.Locop = 1
        FacturacionDigitalSoftv.ClassCFDI.Dime_Aque_Compania_FacturarleGlobal(LocClv_FacturaGlobal, MiConexion)
        FacturacionDigitalSoftv.ClassCFDI.Graba_Factura_Digital_Global(LocClv_FacturaGlobal, CStr(FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart), CStr(FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart), MiConexion)
        Try
            If FacturacionDigitalSoftv.ClassCFDI.GloClv_FacturaCFD > 0 Then
                Dim frm As New FacturacionDigitalSoftv.FrmImprimir
                frm.ShowDialog()
            Else
                MsgBox("No se genero la factura digital cancele y vuelva a intentar por favor")
            End If
            'FormPruebaDigital.Show()
        Catch ex As Exception
        End Try

        Dim r As New Globalization.CultureInfo("es-MX")
        r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
        System.Threading.Thread.CurrentThread.CurrentCulture = r

        FacturacionDigitalSoftv.ClassCFDI.locID_Compania_Mizart = ""
        FacturacionDigitalSoftv.ClassCFDI.locID_Sucursal_Mizart = ""
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Dim conlidia2 As New SqlClient.SqlConnection(MiConexion)
        Dim num As Integer
        If Gloop = "N" Then

            If (glotipoFacGlo = 1 And (CDate(Me.DateTimePicker1.Text) <= CDate(Me.DateTimePicker2.Text))) Or glotipoFacGlo = 0 Then
                'MODIFICADO 29 DIC 09---------------------------------------------------------------
                'If Me.TextBox4.Text <> "" Then
                'If CInt(Me.TextBox4.Text) <> 0 Then

                If IsNumeric(TextBoxImporte.Text) = False Then
                    MsgBox("Importe no v�lido.", MsgBoxStyle.Exclamation)
                    Exit Sub
                Else
                    If CDec(TextBoxImporte.Text) = 0 Then
                        MsgBox("Importe mayor a cero.", MsgBoxStyle.Exclamation)
                        Exit Sub
                    End If
                End If
                If locerror = 0 Then
                    bec_bandera = 1
                    conlidia2.Open()
                    Dim comando2 As New SqlClient.SqlCommand
                    With comando2
                        .Connection = conlidia2
                        .CommandText = "Dime_FacGlo_Ex"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@sucursal", SqlDbType.Int)
                        Dim prm1 As New SqlParameter("@fecha", SqlDbType.DateTime)
                        Dim prm2 As New SqlParameter("@Fecha_2", SqlDbType.DateTime)
                        Dim prm3 As New SqlParameter("@Opc", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Cont", SqlDbType.Int)

                        prm.Direction = ParameterDirection.Input
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Output
                        prm.Value = Me.ComboBox1.SelectedValue
                        prm1.Value = Me.DateTimePicker1.Text
                        prm2.Value = Me.DateTimePicker2.Text
                        If bec_tipo = "C" Then
                            prm3.Value = glotipoFacGlo + 1
                        ElseIf bec_tipo = "V" Then
                            prm3.Value = 4
                        End If
                        prm4.Value = 0
                        .Parameters.Add(prm)
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        Dim i As Integer = comando2.ExecuteNonQuery()
                        num = prm4.Value

                    End With
                    conlidia2.Close()

                    If num > 0 And glotipoFacGlo = 0 Then
                        MsgBox("La factura Global del dia " + Me.DateTimePicker1.Text + " ya ha sido Generada ", MsgBoxStyle.Information)
                        Exit Sub
                    ElseIf num > 0 And glotipoFacGlo = 1 Then
                        MsgBox("Ya Existe una Factura Global en el Rango del : " + Me.DateTimePicker1.Text + " Al D�a " + Me.DateTimePicker2.Text, MsgBoxStyle.Information)
                        Exit Sub
                    ElseIf num = -3 Then
                        MsgBox("La Factura Global se Genera Cada Mes Autom�ticamente, por lo que no es Posible Generarla Manual", MsgBoxStyle.Information)
                    ElseIf num = -4 Then
                        MsgBox("La Factura Global se Genera Diario Autom�ticamente, por lo que no es Posible Generarla Manual", MsgBoxStyle.Information)
                    ElseIf num = 0 Then
                        Guardar()
                        '    bec_serie = Me.SerieTextBox.Text
                        '    bec_factura = Me.NuevafacturaTextBox.Text
                        '    bec_fecha = Me.DateTimePicker1.Text
                        '    'MODIFICADO 29 DIC 09
                        '    'bec_importe = Me.TextBox4.Text
                        '    bec_importe = Me.TextBoxImporte.Text
                        '    CantidadaLetra()
                    End If
                    'Locop = 1
                    'Dime_Aque_Compania_FacturarleGlobal(glofac)
                    'Graba_Factura_Digital_Global(glofac, CStr(locID_Compania_Mizart), CStr(locID_Sucursal_Mizart))

                    'Try
                    '    FormPruebaDigital.Show()
                    'Catch ex As Exception

                    'End Try

                    If LocImpresoraTickets = "" Then
                        MsgBox("No Se Ha Asigando Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
                    ElseIf LocImpresoraTickets <> "" Then

                        'FacturaFiscalCFD---------------------------------------------------------------------
                        'Impresi�n de Factura Global
                        'facturaFiscalCFD = False
                        'facturaFiscalCFD = ChecaSiEsFacturaFiscal("G", 0)
                        'If facturaFiscalCFD = False Then
                        If GloActivarCFD = 0 Then Me.ConfigureCrystalReportefacturaGlobal(bec_letra, TextBoxImporte.Text, TextBoxSubTotal.Text, TextBoxIVA.Text, TextBoxIEPS.Text, bec_serie, bec_fecha, GloUsuario, bec_factura, locIdFac)
                        'End If
                        '--------------------------------------------------------------------------------------

                    End If
                    If GloActivarCFD = 1 Then
                        HazFacturaDigitalGlobal(locIdFac)
                    End If

                    'If LocImpresoraTickets = "" Then
                    '    MsgBox("No Se Ha Asigando Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
                    'ElseIf LocImpresoraTickets <> "" Then
                    '    Me.ConfigureCrystalReportefacturaGlobal(bec_letra, TextBoxImporte.Text, TextBoxSubTotal.Text, TextBoxIVA.Text, TextBoxIEPS.Text, bec_serie, bec_fecha, GloUsuario, bec_factura, CDec(TextBoxSubTotal.Text) + CDec(TextBoxIEPS.Text))
                    'End If
                    'GloReporte = 5
                    'My.Forms.FrmImprimirRepGral.Show()
                    Me.Close()
                    'End If
                Else
                    MsgBox("La Sucursal no tiene Asignada una Serie Para sus Facturas Globales", MsgBoxStyle.Information)
                End If
                'Else
                '    MsgBox("El Importe debe ser Mayor que Cero", MsgBoxStyle.Information)
                'End If
                'Else
                '    MsgBox("Seleccione una Sucursal", MsgBoxStyle.Information)
                'End If
            ElseIf glotipoFacGlo = 1 And (CDate(Me.DateTimePicker1.Text) > CDate(Me.DateTimePicker2.Text)) Then
                MsgBox("La Fecha de Inicio debe ser Mayor a la Fecha Final", MsgBoxStyle.Information)
            End If
        ElseIf Gloop = "C" Then
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub ComboBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.LostFocus
        If Me.ComboBox1.SelectedIndex = -1 Then
            Me.SerieTextBox.Clear()
            Me.NuevafacturaTextBox.Clear()
            'MODIFICADO 29 DIC 09
            'Me.TextBox4.Clear()
            TextBoxImporte.Clear()
            Me.TextBox5.Clear()
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)

        DameImporteFacturaGlobal(Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, 0, glotipoFacGlo)
        'MODIFICADO 29 DIC 09---------------------------------------------------------------
        'CON.Open()
        'Me.DameImporteSucursalTableAdapter.Connection = CON
        'Me.DameImporteSucursalTableAdapter.Fill(Me.DataSetLydia.DameImporteSucursal, Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, glotipoFacGlo)
        'Me.Llena_Factura_Global_nuevoTableAdapter.Connection = CON
        'Me.Llena_Factura_Global_nuevoTableAdapter.Fill(Me.Procedimientos_arnoldo.Llena_Factura_Global_nuevo, CInt(Me.ComboBox1.SelectedValue), locerror)
        CON.Close()
        Me.TextBox5.Text = GloUsuario
    End Sub



    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Dim CON13 As New SqlConnection(MiConexion)
        If Gloop = "N" Then
            DameImporteFacturaGlobal(Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, 0, glotipoFacGlo)
            'MODIFICADO 29 DIC 09---------------------------------------------------------------
            'Me.DameImporteSucursalTableAdapter.Connection = CON13
            'Me.DameImporteSucursalTableAdapter.Fill(Me.DataSetLydia.DameImporteSucursal, Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, glotipoFacGlo)
            CON13.Open()
            'Me.Llena_Factura_Global_nuevoTableAdapter.Connection = CON13
            'Me.Llena_Factura_Global_nuevoTableAdapter.Fill(Me.Procedimientos_arnoldo.Llena_Factura_Global_nuevo, CInt(Me.ComboBox1.SelectedValue), locerror)
            CON13.Close()
            Me.TextBox5.Text = GloUsuario
            Me.DateTimePicker2.Value = Me.DateTimePicker1.Value
            'ElseIf glotipoFacGlo = 1 Then
            'ValidaFechas()
        End If
    End Sub


    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Dim conLidia As New SqlClient.SqlConnection(MiConexion)
        If glotipoFacGlo = 1 Then
            DameImporteFacturaGlobal(Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, 0, glotipoFacGlo)
            'MODIFICADO 29 DIC 09---------------------------------------------------------------
            'Me.DameImporteSucursalTableAdapter.Connection = conLidia
            'Me.DameImporteSucursalTableAdapter.Fill(Me.DataSetLydia.DameImporteSucursal, Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, glotipoFacGlo)
            '    conLidia.Open()
            'Me.Llena_Factura_Global_nuevoTableAdapter.Connection = conLidia
            'Me.Llena_Factura_Global_nuevoTableAdapter.Fill(Me.Procedimientos_arnoldo.Llena_Factura_Global_nuevo, CInt(Me.ComboBox1.SelectedValue), locerror)
            '   conLidia.Close()
        End If
    End Sub

    Private Sub DameImporteFacturaGlobal(ByVal Fecha As DateTime, ByVal FechaFin As DateTime, ByVal SelSucursal As Integer, ByVal Tipo As Char, ByVal idfactura As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        If ComboBox1.SelectedValue = 9999 Then
            strSQL.Append("EXEC DameImporteFacturaGlobal_Puntoweb ")
        Else
            strSQL.Append("EXEC DameImporteFacturaGlobal ")
        End If
        strSQL.Append("'" & CStr(Fecha) & "', ")
        strSQL.Append("'" & CStr(FechaFin) & "', ")
        If ComboBox1.SelectedValue <> 9999 Then
            strSQL.Append(CStr(SelSucursal) & ", ")
        End If
        strSQL.Append(Tipo & ", ")
        strSQL.Append(CStr(idfactura) & ", ")
        strSQL.Append(CStr(Op))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable

        Try

            dataAdapter.Fill(dataTable)
            TextBoxImporte.Text = Format(CDec(dataTable.Rows(0)(0).ToString()), "#####0.0000")
            TextBoxSubTotal.Text = Format(CDec(dataTable.Rows(0)(1).ToString()), "#####0.0000")
            TextBoxIVA.Text = Format(CDec(dataTable.Rows(0)(2).ToString()), "#####0.0000")
            TextBoxIEPS.Text = Format(CDec(dataTable.Rows(0)(3).ToString()), "#####0.0000")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub NueDetFacturaGlobalImpuestos(ByVal IDFactura As Long, ByVal Iva As Double, ByVal Ieps As Double, ByVal SubTotal As Double, ByVal Total As Double)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetFacturaGlobalImpuestos", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@IDFactura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = IDFactura
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Iva", SqlDbType.Money)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Iva
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Ieps", SqlDbType.Money)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Ieps
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@SubTotal", SqlDbType.Money)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = SubTotal
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Total", SqlDbType.Money)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Total
        comando.Parameters.Add(parametro5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

End Class