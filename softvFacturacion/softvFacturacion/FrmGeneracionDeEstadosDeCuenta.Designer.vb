﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGeneracionDeEstadosDeCuenta
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvEstadosDeCuenta = New System.Windows.Forms.DataGridView()
        Me.btnGenerar = New System.Windows.Forms.Button()
        Me.btnConsultar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblMensajeDobleClicEdosCta = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgvPeriodosDeCobroGenerados = New System.Windows.Forms.DataGridView()
        Me.btnCargarEstadosDeCuenta = New System.Windows.Forms.Button()
        Me.btnEliminarPeriodos = New System.Windows.Forms.Button()
        CType(Me.dgvEstadosDeCuenta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvPeriodosDeCobroGenerados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(227, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(537, 27)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Generación de Periodos de Cobro y Estados de Cuenta"
        '
        'dgvEstadosDeCuenta
        '
        Me.dgvEstadosDeCuenta.AllowUserToAddRows = False
        Me.dgvEstadosDeCuenta.AllowUserToDeleteRows = False
        Me.dgvEstadosDeCuenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEstadosDeCuenta.Location = New System.Drawing.Point(22, 22)
        Me.dgvEstadosDeCuenta.Name = "dgvEstadosDeCuenta"
        Me.dgvEstadosDeCuenta.ReadOnly = True
        Me.dgvEstadosDeCuenta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEstadosDeCuenta.Size = New System.Drawing.Size(936, 277)
        Me.dgvEstadosDeCuenta.TabIndex = 0
        '
        'btnGenerar
        '
        Me.btnGenerar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGenerar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerar.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerar.ForeColor = System.Drawing.Color.Black
        Me.btnGenerar.Location = New System.Drawing.Point(834, 34)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(170, 50)
        Me.btnGenerar.TabIndex = 50
        Me.btnGenerar.Text = "Generar Nuevo"
        Me.btnGenerar.UseVisualStyleBackColor = False
        '
        'btnConsultar
        '
        Me.btnConsultar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConsultar.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsultar.ForeColor = System.Drawing.Color.Black
        Me.btnConsultar.Location = New System.Drawing.Point(834, 152)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(170, 45)
        Me.btnConsultar.TabIndex = 51
        Me.btnConsultar.Text = "Consultar"
        Me.btnConsultar.UseVisualStyleBackColor = False
        Me.btnConsultar.Visible = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(815, 659)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(170, 52)
        Me.btnSalir.TabIndex = 52
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblMensajeDobleClicEdosCta)
        Me.GroupBox1.Controls.Add(Me.dgvEstadosDeCuenta)
        Me.GroupBox1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(27, 323)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(977, 330)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Últimos Estados de Cuenta Generados del Periodo Seleccionado"
        '
        'lblMensajeDobleClicEdosCta
        '
        Me.lblMensajeDobleClicEdosCta.AutoSize = True
        Me.lblMensajeDobleClicEdosCta.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensajeDobleClicEdosCta.Location = New System.Drawing.Point(612, 302)
        Me.lblMensajeDobleClicEdosCta.Name = "lblMensajeDobleClicEdosCta"
        Me.lblMensajeDobleClicEdosCta.Size = New System.Drawing.Size(346, 18)
        Me.lblMensajeDobleClicEdosCta.TabIndex = 53
        Me.lblMensajeDobleClicEdosCta.Text = "Doble Clic sobre algún Registro para ver su Estado de Cuenta"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.dgvPeriodosDeCobroGenerados)
        Me.GroupBox2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(27, 24)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(801, 293)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Periodos de Cobro"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(42, 257)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(745, 18)
        Me.Label2.TabIndex = 54
        Me.Label2.Text = "Selecciona un periodo de cobro y da clic en CARGAR ESTADOS DE CUENTA para ver los" & _
            " Estados de Cuenta Generados de ese periodo"
        '
        'dgvPeriodosDeCobroGenerados
        '
        Me.dgvPeriodosDeCobroGenerados.AllowDrop = True
        Me.dgvPeriodosDeCobroGenerados.AllowUserToAddRows = False
        Me.dgvPeriodosDeCobroGenerados.AllowUserToDeleteRows = False
        Me.dgvPeriodosDeCobroGenerados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPeriodosDeCobroGenerados.Location = New System.Drawing.Point(22, 28)
        Me.dgvPeriodosDeCobroGenerados.Name = "dgvPeriodosDeCobroGenerados"
        Me.dgvPeriodosDeCobroGenerados.ReadOnly = True
        Me.dgvPeriodosDeCobroGenerados.Size = New System.Drawing.Size(765, 227)
        Me.dgvPeriodosDeCobroGenerados.TabIndex = 0
        '
        'btnCargarEstadosDeCuenta
        '
        Me.btnCargarEstadosDeCuenta.BackColor = System.Drawing.Color.DarkOrange
        Me.btnCargarEstadosDeCuenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCargarEstadosDeCuenta.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCargarEstadosDeCuenta.ForeColor = System.Drawing.Color.Black
        Me.btnCargarEstadosDeCuenta.Location = New System.Drawing.Point(834, 90)
        Me.btnCargarEstadosDeCuenta.Name = "btnCargarEstadosDeCuenta"
        Me.btnCargarEstadosDeCuenta.Size = New System.Drawing.Size(170, 56)
        Me.btnCargarEstadosDeCuenta.TabIndex = 52
        Me.btnCargarEstadosDeCuenta.Text = "Cargar Estados de Cuenta"
        Me.btnCargarEstadosDeCuenta.UseVisualStyleBackColor = False
        '
        'btnEliminarPeriodos
        '
        Me.btnEliminarPeriodos.BackColor = System.Drawing.Color.DarkOrange
        Me.btnEliminarPeriodos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminarPeriodos.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminarPeriodos.ForeColor = System.Drawing.Color.Black
        Me.btnEliminarPeriodos.Location = New System.Drawing.Point(834, 260)
        Me.btnEliminarPeriodos.Name = "btnEliminarPeriodos"
        Me.btnEliminarPeriodos.Size = New System.Drawing.Size(170, 57)
        Me.btnEliminarPeriodos.TabIndex = 53
        Me.btnEliminarPeriodos.Text = "Eliminar el Periodo Seleccionado"
        Me.btnEliminarPeriodos.UseVisualStyleBackColor = False
        '
        'FrmGeneracionDeEstadosDeCuenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1012, 731)
        Me.Controls.Add(Me.btnEliminarPeriodos)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCargarEstadosDeCuenta)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnConsultar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnGenerar)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.Name = "FrmGeneracionDeEstadosDeCuenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generación De Estados De Cuenta"
        CType(Me.dgvEstadosDeCuenta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvPeriodosDeCobroGenerados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvEstadosDeCuenta As System.Windows.Forms.DataGridView
    Friend WithEvents btnGenerar As System.Windows.Forms.Button
    Friend WithEvents btnConsultar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblMensajeDobleClicEdosCta As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnCargarEstadosDeCuenta As System.Windows.Forms.Button
    Friend WithEvents dgvPeriodosDeCobroGenerados As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnEliminarPeriodos As System.Windows.Forms.Button
End Class
