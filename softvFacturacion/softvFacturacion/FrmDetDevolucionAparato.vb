﻿Imports System.Data.SqlClient
Public Class FrmDetDevolucionAparato

    Private Sub FrmDetDevolucionAparato_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Me.Label1.ForeColor = Color.Black
        Me.Label2.ForeColor = Color.Black
        Me.Label3.ForeColor = Color.Black
        Me.Label4.ForeColor = Color.Black
        Me.Label5.ForeColor = Color.Black
        Me.Label6.ForeColor = Color.Black
        Me.Label7.ForeColor = Color.Black
        MuestraDetallesDevolucion()
    End Sub

    'Cargamos los Detalles de la Devolución
    Public Sub MuestraDetallesDevolucion()

        Dim Con As New SqlConnection(MiConexion)
        Dim Comando As New SqlCommand()

        Try

            Con.Open()
            With Comando

                ' Comando
                .CommandText = "MuestraDetallesDevolucion"
                .CommandTimeout = 10
                .CommandType = CommandType.StoredProcedure
                .Connection = Con

                'Parámetros
                Dim prmIdDevolucion As New SqlParameter("@IdDevolucion", SqlDbType.BigInt)
                
                With prmIdDevolucion
                    .Value = Id_Devolucion
                    .Direction = ParameterDirection.Input
                End With

                'Agregamos los Parámetros al Comando
                .Parameters.Add(prmIdDevolucion)
            
                Dim reader As SqlDataReader = Comando.ExecuteReader()
                While reader.Read()

                    Me.txtIdDevolucion.Text = reader(0).ToString
                    Me.txtContrato.Text = reader(1)
                    Me.txtNombreCliente.Text = reader(2)
                    Me.txtFactura.Text = reader(3)
                    Me.txtMontoInicial.Text = reader(4)
                    Me.txtFechaGeneracion.Text = reader(5)

                    If Not IsDBNull(reader(6)) Then
                        Me.txtFechaDevolucion.Text = reader(6)
                    Else
                        Me.txtFechaDevolucion.Text = "No se ha Generado aún la Devolución."
                    End If

                End While
                
            End With

        Catch ex As Exception

            Con.Dispose()
            Con.Close()
            MsgBox("Error al ver los detallesde la Devolución.", MsgBoxStyle.Exclamation, ":: Advertencia ::")
            MsgBox(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "Porcedimeinto: MuestraDetallesDevolucion, Función: MuestraDetallesDevolucion()"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)
        Finally
            Con.Dispose()
            Con.Close()
        End Try

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
        Id_Devolucion = 0
    End Sub
End Class