﻿Imports System.Data.SqlClient
Imports System.Math

Public Class FrmRoboDeSeñal

    Dim RoboSeñal As New classRoboDeSeñal
    Dim _DataTable As DataTable = Nothing
    Dim monto As Double
    'Guardar

    'salir
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub FrmRoboDeSeñal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            colorea(Me)
            Me.DescripcionTextBox.Enabled = False
            Me.txtMonto.Enabled = False
            Me.cbxCobrado.Enabled = False
            _DataTable = RoboSeñal.buscaDT(eGloContrato)
            If (_DataTable.Rows.Count > 0) Then
                BindingNavigatorDeleteItem.Enabled = True
                ConRoboDeSeñalBindingNavigatorSaveItem.Enabled = True
                DescripcionTextBox.Text = _DataTable.Rows(0)(1).ToString
                txtMonto.Text = Round(Convert.ToDecimal(_DataTable.Rows(0)(2)), 2)
                If Convert.ToBoolean(_DataTable.Rows(0)(3)) = True Then
                    cbxCobrado.Checked = True
                Else
                    cbxCobrado.Checked = False
                End If
            Else
                DescripcionTextBox.Text = ""
                txtMonto.Text = ""
                cbxCobrado.Checked = False
            End If
            If Me.DescripcionTextBox.Text.Length < 0 Then
                Me.BindingNavigatorDeleteItem.Enabled = False
            End If
        Catch ex As Exception
            Throw ex
        End Try

        'CON.Close()
    End Sub

    'Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    'End Sub
End Class