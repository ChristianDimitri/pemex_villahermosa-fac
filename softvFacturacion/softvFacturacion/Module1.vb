Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine


Module Module1
    Public GLOTRANSFERENCIA As Double = 0
    Public GLOBANCO As Integer = 0
    Public GLONUMEROTRANSFERENCIA As String = ""
    Public GLOAUTORIZACION As String = ""

    'EDGAR
    '***********************
    Public locPregunta As String = ""
    Public MClv_Session As Long = 0
    Public MiConexion As String = Nothing
    '***********************
    ''
    Public opcionClientes As Boolean = False
    'Eric Variables
    Public bnd1pardep1 As Integer = 0
    Public bnd1pardep2 As Integer = 0
    Public bnd2pardep As Boolean = False
    Public bnd2pardep1 As Boolean = False
    Public locclvsessionpardep As Long = 0
    Public Loccontratopardep As Long = 0
    Public locbndborracabiosdep As Boolean = False
    'Edgar me Va servir para SAber si es un Estado de cuenta o un Cobro Normal
    Public GloOpCobro As Integer = 0
    Public GloCEXTTE As Boolean = False
    Public GloComproEquipo As Boolean = False
    Public GloAbonoACuenta As Boolean = False
    Public GloAdelantarCargos As Boolean = False
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    Public LocBndrelingporconceptos As Boolean = False
    Public eBotonGuardar As Boolean
    Public LocBndNotasReporteTick As Boolean = False
    Public locoprepnotas As Integer = 0
    Public Repetido As Boolean
    Public eFechaInicial, eFechaFinal As Date
    Public eFechaTitulo As String
    Public eCveFactura As Long
    Public eEntra As Boolean
    Public eReImprimirF As Integer = 0
    Public eEntraReImprimir As Boolean
    Public eMotivoBonificacion As String = Nothing
    Public eLoginCajera As String = Nothing
    Public ePassCajera As String = Nothing
    Public eConModDesglose As Boolean = False
    Public eCveCajera As String = Nothing
    Public eCorresponde As Boolean = False
    Public eClv_Progra As Long = 0
    Public eClv_Txt As String = 0
    Public eFechaServidor As Date
    Public eRes As Integer = 0
    Public eBndPPE As Boolean = False
    Public eAccesoAdmin As Boolean = False
    Public eClv_Session As Long = 0
    'Public GloClv_Detalle As Long = 0
    '**************************************
    Public RangoFacturasIni As Integer
    Public RangoFacturasFin As Integer
    Public RutaReportes As String
    Public OPCION As Char
    Public BLOQUEA As Boolean = False
    Public band As Boolean = False
    Public locnomsupervisor As String = Nothing
    Public bec_tipo As String = Nothing
    Public bec_impIva As Decimal
    Public bec_serie As String
    Public bec_letra As String
    Public bec_factura As String
    Public bec_fecha As String
    Public bec_importe As String
    Public bec_bandera As Integer = 0
    Public bec_consecutivo As Integer
    Public GLOMENUS As Integer
    Public glomenu As String
    Public GloBnd As Boolean
    Public GloAdelantados As Integer
    Public gloClv_Session As Long = 0
    Public gloClv_Servicio As Long = 0
    Public gloClv_llave As Long = 0
    Public gloClv_UnicaNet As Long = 0
    Public gloClave As Long = 0
    Public GloContrato As Long = 0
    Public IdSistema As String = " "
    Public GloBndExt As Boolean = False
    Public GloExt As Integer = 0
    Public GloClv_Txt As String = Nothing
    Public GloCajera As String = "SISTE"
    Public GLONOMCAJERAARQUEO As String = Nothing
    Public GloSucursal As Integer = 0
    Public GloCaja As Integer = 0
    Public GlonOMCaja As String
    Public GloClv_Factura As Long = 0
    Public GloIpMaquina As String = Nothing
    Public GloHostName As String = Nothing
    Public GloUsuario As String = Nothing
    Public op As String
    Public Fecha_ini As String
    Public Fecha_Fin As String
    Public NomCajera As String
    Public NomCaja As String
    Public NomSucursal As String
    Public BanderaReporte As Boolean = False
    Public ExtraT As String
    Public Resumen As Boolean = False
    Public Resumen1 As Integer
    Public Resumen2 As Integer
    Public Resumen3 As Boolean = False
    Public GloReporte As Integer = 0
    Public GloClv_SessionBancos As Long = 0
    Public GloTitulo As String = Nothing
    Public GloSubTitulo As String = Nothing
    Public GloBndControl As Boolean = False
    Public GloTipo As String = "C"
    Public GloServerName As String = Nothing
    Public GloDatabaseName As String = Nothing
    Public GloUserID As String = Nothing
    Public GloPassword As String = Nothing
    Public SelCajaResumen As Integer
    Public FlagCaja As Boolean = False
    Public Consecutivo As Long = 0
    Public Suma As Double = 0
    Public GloConsecutivo As Long
    Public loctitulo As String = Nothing
    Public gloClv_Detalle As Long = 0
    Public GloDes_Ser As String = Nothing
    Public locband_pant As Integer = 0
    Public GloNomSucursal As String
    Public GloOpFacturas As Integer = 0
    Public GLOIMPTOTAL As Double = 0
    Public GLOSIPAGO As Integer = 0
    Public GloTipoUsuario As Integer
    Public glolec As Integer
    Public gloescr As Integer
    Public gloctr As Integer
    Public GloBonif As Integer = 0
    Public GloImprimeTickets As Boolean = False
    Public LocNomEmpresa As String = Nothing
    '--Variables Generales de Facturacion
    Public GloEmpresa As String = Nothing
    Public GloDireccionEmpresa As String = Nothing
    Public GloColonia_CpEmpresa As String = Nothing
    Public GloCiudadEmpresa As String = Nothing
    Public GloRfcEmpresa As String = Nothing
    Public GloTelefonoEmpresa As String = Nothing
    Public GloFechaPeridoPagado As String = Nothing
    Public GloFechaPeriodoPagadoMes As String = Nothing
    Public GloFechaPeriodoFinal As String = Nothing
    Public GloFechaProximoPago As String = Nothing
    '--Variables para el tipo de Pago
    Public GLOCLV_NOTA As Long = 0
    Public GLONOTA As Double = 0
    Public GLOEFECTIVO As Double = 0
    Public GLOCHEQUE As Double = 0
    Public GLOCLV_BANCOCHEQUE As Integer = 0
    Public NUMEROCHEQUE As String = Nothing
    Public GLOTARJETA As Double = 0
    Public GLOCLV_BANCOTARJETA As Integer = 0
    Public NUMEROTARJETA As String = Nothing
    Public TARJETAAUTORIZACION As String = Nothing
    Public Glocontratosel As Long = 0
    Public Glocontratosel2 As Long = 0
    Public GLOTOTALEFECTIVO As Double = 0
    Public GLOCAMBIO As Double = 0
    ' Fin varirables para el tipo de Pago

    Public Glo_Clv_SessionVer As Long = 0
    Public Glo_BndErrorVer As Integer = 0
    Public Glo_MsgVer As String = Nothing

    Public GloActivarCFD As Integer = 0
    Public GloTvSinPago As Integer = 0
    Public GloTvConpago As Integer = 0
    Public GloImprimeTicket As Boolean = False
    Public GloExportaTicket As Boolean = False
    Public GloInstalaRouter As Boolean = False
    Public GloConPlaca As Boolean = False
    Public GloComboSinDigital As Integer = 0
    Public GloPreguntaBonContProp As Integer = 0
    Public GloImprimirOrdenesPant As Integer = 0

    Public GloOpRepGral As String = Nothing
    Public LocEfectivo As Decimal = 0
    Public LocTarjeta As Decimal = 0
    Public LocCheque As Decimal = 0
    Public LocAuto As Decimal = 0
    Public LocParciales As Decimal = 0
    Public LocDesglose As Decimal = 0
    Public LocGastos As Decimal = 0
    Public LocSaldoAnterior As Decimal = 0
    Public LocClv_session As Integer = 0
    Public Gloop As String = Nothing
    Public Glo_Apli_Pnt_Ade As Boolean = False

    Public GloClv_Periodo_Num As Integer = 1
    Public GloClv_Periodo_Txt As String = " Primer Periodo "
    Public GloActPeriodo As Integer = 0
    Public facnormal As Boolean
    Public facticket As Boolean
    Public impresorafiscal As String
    Public Locclv_sucursalglo As String = Nothing
    Public Tipo As String = Nothing
    Public LocSerieglo As String = Nothing
    Public LocFacturaGlo As Integer = 0
    Public LocFechaGlo As Date
    Public LocImporteGlo As Double = 0
    Public LocCajeroGlo As String = Nothing
    Public Locclv_empresa As String = Nothing
    Public LocSucursal As String = Nothing

    ''VARIABLES PARA ESPECIFICACIONES---------
    Public ColorBut As Integer
    Public ColorLetraBut As Integer
    Public ColorMenu As Integer
    Public ColorMenuLetra As Integer
    Public ColorBwr As Integer
    Public ColorBwrLetra As Integer
    Public ColorGrid As Integer
    Public ColorForm As Integer
    Public ColorLetraForm As Integer
    Public ColorLabel As Integer
    Public ColorLetraLabel As Integer
    Public bytesImg() As Byte
    Public bytesImg2() As Byte
    Public num, num2 As Integer
    Public bfac As softvFacturacion.NewsoftvDataSet2.BusFacFiscalDataTable
    Public busfac As NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
    Public horaini As String
    Public horafin As String
    Public bndcontt As Boolean
    Public res As Integer
    Public LocImpresoraTickets As String = Nothing
    Public LocFecha1 As String = Nothing
    Public LocFecha2 As String = Nothing
    Public LocResumenBon As Boolean = False
    Public LocBndBon As Boolean = False
    Public locBndBon1 As Boolean = False
    Public LocSupBon As String = Nothing
    Public LocLoginUsuario As String = Nothing
    Public LocBanderaRep1 As Integer = 0
    Public LocBndrepfac1 As Boolean = False
    Public Locclv_usuario As String = Nothing
    Public LocNombreusuario As String = Nothing
    Public Factura_inicial As Integer
    Public Factura_final As Integer
    Public Unico As Boolean = True
    Public LocCiudades As String = Nothing
    Public gloMotivoCan As Integer
    Public gloClvNota As Integer
    Public GloPoliza2 As Long
    Public glotipoNota As Short
    Public glotipoFacGlo As Short
    Public SubCiudad As String
    Public bnd As Integer = 0
    Public LiTipo As Integer = 0
    Public GloSistema As String = "Facturaci�n"
    Public LiContrato As Long
    Public LocFechaGloFinal As String
    Public Glo_tipSer As Integer
    'variables de Datos
    Public locclv_id As String
    Public LocBdd As String
    Public LocClv_Ciudad As String
    Public LocNomciudad As String
    Public LIFACTURA As Long
    Public refrescar As Boolean = False
    Public status As String
    Public LocbndNotas As Boolean = False
    Public LocActiva As Boolean = False
    Public LocSaldada As Boolean = False
    Public LocCancelada As Boolean = False
    Public LocUsuariosNotas As String = Nothing
    Public LocClientesPagosAdelantados As Boolean = False
    'Proceso de Bancos
    Public BndPasaBancos As Boolean = False
    Public FechaPasaBancos As String = Nothing
    'Fin Proceso de Bancos
    'proceso de Oxxo
    Public GloClv_Recibo As Long = 0
    'Fin Proceso de Oxxo
    'Polizas
    Public Locbndactualizapoiza As Boolean = True
    Public LocopPoliza As String = "C"
    Public LocGloClv_poliza As Long = 0
    Public LocbndPolizaCiudad As Boolean = False
    'Polizas
    'Reporte_Desgloce_Moneda
    Public GloBnd_Des_Men As Boolean = False
    'Clave Session para Facturacion
    Public GloTelClv_Session As Long = 0
    Public GloTiene_Cancelaciones As Boolean = False


    'Bandera Reporte de Cobro de Adeudo
    Public LocBndReporteBonificaciones As Boolean = False

    '
    Public GloPagorecibido As Double = 0
    'Reporte_Desgloce_Moneda Contratacion
    Public GloBnd_Des_Cont As Boolean = False
    Public Locbndcortedet As Boolean = False

    Public Locbndrepnotas As Integer = 0
    Public locbndrepnotas2 As Boolean = False
    Public Locclv_sucursalnotas As Integer = 0
    Public Verifica As Integer
    Public BndAlerta As Boolean
    Public LocbndDesPagos As Boolean = False
    Public guardabitacora As softvFacturacion.DataSetLydia.Inserta_MovSistDataTable
    Public guardabitacorabuena As softvFacturacion.DataSetLydiaTableAdapters.Inserta_MovSistTableAdapter

    Public opcionCuenta As Integer = 1

    'para revistas sahuayo (Juan Jos�)
    Public Op_Revista As Integer = 0
    Public NoRevistas As Integer = 0
    Public Revista As Boolean = False
    Public BndRevista As Boolean = False
    Public TotalRevistas As Integer = 0

    'LISTADO_CORTEFACTURAS JUANJO
    Public OPLISTADOCORTEFAC As Integer = Nothing
    Public TIPOCAJA As String = Nothing

    'SAUL ROBO DE SE�AL
    Public eGloContrato As Long = 0
    'SAUL ROBO DE SE�AL (FIN)

    Public Sub bitsist(ByVal usuario As String, ByVal contrato As Long, ByVal sistema As String, ByVal pantalla As String, ByVal control As String, ByVal valorant As String, ByVal valornuevo As String, ByVal clv_ciudad As String)
        Dim COn85 As New SqlConnection(MiConexion)
        COn85.Open()
        guardabitacora = New softvFacturacion.DataSetLydia.Inserta_MovSistDataTable
        guardabitacorabuena = New softvFacturacion.DataSetLydiaTableAdapters.Inserta_MovSistTableAdapter
        COn85.Close()
        If valorant <> valornuevo Then
            COn85.Open()
            guardabitacorabuena.Connection = COn85
            guardabitacorabuena.Fill(guardabitacora, usuario, contrato, sistema, pantalla, control, valorant, valornuevo, clv_ciudad)
            COn85.Close()
        End If
    End Sub

    Public Sub SP_Dame_General_sistema_II()
        GloTvSinPago = 0
        GloTvConpago = 0
        GloImprimeTicket = False
        GloExportaTicket = False

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("SP_Dame_General_sistema_II", conexion)
        comando.CommandType = CommandType.StoredProcedure
        '@Tv bit OUTPUT,@Dig bit OUTPUT,@Net bit OUTPUT,@Tel bit OUTPUT

        Dim parametro As New SqlParameter("@TvSinPago", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@TvConPago", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@ComboSinDigital", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@PreguntaBonContProp", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@ImprimirOrdenesPant", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@ImprimeTicket", SqlDbType.Bit)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@ExportaTicket", SqlDbType.Bit)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@InstalaRouter", SqlDbType.Bit)
        parametro7.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@ConPlaca", SqlDbType.Bit)
        parametro8.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro8)


        Dim parametro9 As New SqlParameter("@ActivarCFD", SqlDbType.Int)
        parametro9.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro9)
        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            GloTvSinPago = parametro.Value
            GloTvConpago = parametro1.Value
            GloComboSinDigital = parametro2.Value
            GloPreguntaBonContProp = parametro3.Value
            GloImprimirOrdenesPant = parametro4.Value
            GloImprimeTicket = parametro5.Value
            GloExportaTicket = parametro6.Value
            GloInstalaRouter = parametro7.Value
            GloConPlaca = parametro8.Value
            GloActivarCFD = parametro9.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Public Sub RecorrerEstructuraMenu(ByVal oMenu As MenuStrip)
        Dim menu As ToolStripMenuItem
        For Each oOpcionMenu As ToolStripMenuItem In oMenu.Items
            menu = New ToolStripMenuItem
            menu = oOpcionMenu
            menu.ForeColor = System.Drawing.Color.FromArgb(ColorMenuLetra)
            menu = Nothing
            If oOpcionMenu.DropDownItems.Count > 0 Then
                RecorrerSubmenu(oOpcionMenu.DropDownItems, "----")
            End If
        Next
    End Sub

    Public Sub RecorrerSubmenu(ByVal oSubmenuItems As ToolStripItemCollection, ByVal sGuiones As String)
        Dim submenu As ToolStripItem
        For Each oSubitem As ToolStripItem In oSubmenuItems
            If oSubitem.GetType Is GetType(ToolStripMenuItem) Then
                submenu = New ToolStripMenuItem
                submenu = oSubitem
                submenu.ForeColor = System.Drawing.Color.FromArgb(ColorMenuLetra)
                submenu = Nothing
                If CType(oSubitem, ToolStripMenuItem).DropDownItems.Count > 0 Then
                    RecorrerSubmenu(CType(oSubitem, ToolStripMenuItem).DropDownItems, sGuiones & "----")
                End If
            End If
        Next
    End Sub
    Public Sub bwrpanel(ByVal panel2 As Panel)
        Dim data As DataGridView
        Dim label As Label
        Dim boton As Button
        Dim split As SplitContainer
        Dim panel3 As Panel
        Dim text As TextBox
        Dim GROUP As GroupBox
        Dim var As String
        If panel2.BackColor <> Color.WhiteSmoke Then
            panel2.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
            ' panel2.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
        End If
        For Each ctr As Control In panel2.Controls
            If ctr.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctr
                bwrpanel(split.Panel1)
                bwrpanel(split.Panel2)
            End If
            If ctr.GetType Is GetType(CrystalDecisions.Windows.Forms.CrystalReportViewer) Then
                ctr.BackColor = Color.WhiteSmoke
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                data = New DataGridView
                data = ctr
                data.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                data.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                data.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)
                data = Nothing
            End If
            var = Mid(ctr.Name, 1, 3)
            If ctr.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" And ctr.BackColor <> Color.WhiteSmoke And var <> "RED" Then
                label = New Label
                label = ctr
                label.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                label.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                label = Nothing
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.Label) And var = "CMB" And var <> "RED" Then
                label = New Label
                label = ctr
                label.ForeColor = Color.Black
                label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                label = Nothing
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.Label) And var = "RED" Then
                label = New Label
                label = ctr
                label.ForeColor = Color.Red
                label.BackColor = Color.Yellow
                label = Nothing
            End If

            If ctr.GetType Is GetType(System.Windows.Forms.TextBox) And var = "CMB" Then
                text = New TextBox
                text = ctr
                text.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                text.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                text = Nothing
            End If


            If ctr.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctr
                boton.BackColor = System.Drawing.Color.FromArgb(ColorBut)
                boton.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
                boton = Nothing
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.Panel) And var = "CMB" Then
                panel3 = New Panel
                panel3 = ctr
                panel3.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                bwrpanel(panel3)
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GROUP = New GroupBox
                GROUP = ctr
                GROUP.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                bwrgroup(GROUP)
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.Panel) And var <> "CMB" Then
                bwrpanel(ctr)
            End If
        Next
    End Sub
    Public Sub bwrgroup(ByVal grup As GroupBox)
        Dim panel3 As Panel
        Dim label As Label
        Dim var As String
        For Each ctm As Control In grup.Controls
            var = Mid(ctm.Name, 1, 3)
            If ctm.GetType Is GetType(System.Windows.Forms.Panel) And ctm.BackColor <> Color.WhiteSmoke Then
                panel3 = New Panel
                panel3 = ctm
                panel3.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                bwrpanel(panel3)
            End If
            If ctm.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" And ctm.BackColor <> Color.WhiteSmoke Then
                label = New Label
                label = ctm
                label.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                label.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                label = Nothing
            End If
        Next
    End Sub

    Public Sub colorea(ByVal formulario As Form)
        Dim boton As Button
        Dim panel As Panel
        Dim label As Label
        Dim split As SplitContainer
        Dim var As String
        Dim data As DataGridView
        Dim Menupal As MenuStrip
        Dim GROUP As GroupBox

        For Each ctl As Control In formulario.Controls
            var = Mid(ctl.Name, 1, 3)

            If var <> "CNO" Then

                If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                    boton = New Button
                    boton = ctl
                    boton.BackColor = System.Drawing.Color.FromArgb(ColorBut)
                    boton.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
                    boton = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.MenuStrip) Then
                    Menupal = New MenuStrip
                    Menupal = ctl
                    Menupal.BackColor = System.Drawing.Color.FromArgb(ColorMenu)
                    RecorrerEstructuraMenu(Menupal)
                    Menupal = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                    split = New SplitContainer
                    split = ctl
                    bwrpanel(split.Panel1)
                    bwrpanel(split.Panel2)
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) And ctl.BackColor <> Color.WhiteSmoke Then
                    panel = New Panel
                    panel = ctl
                    panel.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                    panel.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                    bwrpanel(panel)
                    panel = Nothing

                ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                    GROUP = New GroupBox
                    GROUP = ctl
                    GROUP.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                    bwrgroup(GROUP)
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) And ctl.BackColor = Color.WhiteSmoke Then
                    panel = New Panel
                    panel = ctl
                    bwrpanel(panel)
                    panel = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) And var = "CMB" Then
                    panel = New Panel
                    panel = ctl
                    panel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    panel = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" Then
                    label = New Label
                    label = ctl
                    label.ForeColor = System.Drawing.Color.FromArgb(ColorLetraLabel)
                    label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    label = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Label) And var = "RED" Then
                    label = New Label
                    label = ctl
                    label.ForeColor = Color.Red
                    label.BackColor = Color.WhiteSmoke
                    label = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Label) And var = "CMB" Then
                    label = New Label
                    label = ctl
                    label.ForeColor = Color.Black
                    label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    label = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                    data = New DataGridView
                    data = ctl
                    data.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                    data.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                    If ctl.Name = "SumaDetalleDataGridView" Then
                        data.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                        data.BackgroundColor = System.Drawing.Color.FromArgb(ColorForm)
                    Else
                        data.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)
                    End If
                    data = Nothing
                End If
            End If
        Next
        formulario.BackColor = System.Drawing.Color.FromArgb(ColorForm)
        formulario.ForeColor = System.Drawing.Color.FromArgb(ColorLetraForm)
    End Sub

    Public Function ValidaKey(ByVal ctl As Object, ByRef nChar As Integer, ByVal Tipo As String) As Integer
        'Solo Enteros
        If Tipo = "N" Then
            If (nChar >= 48 And nChar <= 57) Or nChar = 8 Or nChar = 13 Then
                ValidaKey = nChar
            Else
                ValidaKey = 0
            End If
            'Con Enteros y Decimales
        ElseIf Tipo = "L" Then
            If (nChar >= 48 And nChar <= 57) Or nChar = 8 Or nChar = 13 Or nChar = 46 Then
                ValidaKey = nChar
            Else
                ValidaKey = 0
            End If
            'String
        ElseIf Tipo = "S" Then
            nChar = Asc(LCase(Chr(nChar)))
            If ctl.SelectionStart = 0 Then
                ' This is the first character, so change to uppercase.
                nChar = Asc(UCase(Chr(nChar)))
            Else
                ' If the previous character is a space, capitalize
                ' the current character.
                'If Mid(ctl, ctl.SelectionStart, 1) = Space(1) Then
                'MsgBox(Mid(ctl.ToString, ctl.SelectionStart, 1))
                'MsgBox(Mid(ctl.text, ctl.SelectionStart, 1))
                If Mid(ctl.text, ctl.SelectionStart, 1) = Space(1) Then
                    nChar = Asc(UCase(Chr(nChar)))
                End If
            End If
            ValidaKey = nChar
        ElseIf Tipo = "M" Then
            nChar = Asc(UCase(Chr(nChar)))
            ValidaKey = nChar
            'ElseIf Tipo = "D" Then
            ' If KeyAscii <> 64 Then
            '     a = Right(Txt, 1)
            '     L = Len(Txt)
            '     If a = " " Or L = 0 Or a = "." Then
            '         ValidaKey = (Asc(UCase(Chr(KeyAscii))))
            '     Else
            '         ValidaKey = (Asc(LCase(Chr(KeyAscii))))
            '     End If
            '  End If
        End If
    End Function
    Public Function DAMESclv_Sessionporfavor() As Long
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            DAMESclv_Sessionporfavor = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "DameClv_Session_Servicios"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm2 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
                DAMESclv_Sessionporfavor = prm2.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
        End Try
    End Function

    Public Function upsmanamensajeticketSuspendidos(ByVal eClv_Factura) As String
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            upsmanamensajeticketSuspendidos = ""
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "upsmanamensajeticketSuspendidos"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x

                Dim prm0 As New SqlParameter("@clv_Factura", SqlDbType.BigInt)
                prm0.Direction = ParameterDirection.Input
                prm0.Value = eClv_Factura
                .Parameters.Add(prm0)

                Dim prm1 As New SqlParameter("@bnd", SqlDbType.Bit)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Msj", SqlDbType.VarChar, 400)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
                If prm1.Value = True Then
                    upsmanamensajeticketSuspendidos = prm2.Value
                End If

            End With

        Catch ex As Exception
        Finally
            cON_x.Close()
        End Try
    End Function


    Function DesEncriptaME(ByVal Pass As String) As String
        Dim Clave As String, i As Integer, Pass2 As String
        Dim CAR As String, Codigo As String
        Dim j As Integer

        Clave = "%��T@#$A_"
        Pass2 = ""
        j = 1
        For i = 1 To Len(Pass) Step 2
            CAR = Mid(Pass, i, 2)
            Codigo = Mid(Clave, ((j - 1) Mod Len(Clave)) + 1, 1)
            Pass2 = Pass2 & Chr(Asc(Codigo) Xor Val("&h" + CAR))
            j = j + 1
        Next i
        DesEncriptaME = Pass2
    End Function


    Function Rellena_Text(ByVal Valor As String, ByVal Longitud As Integer, ByVal Relleno As String) As String
        Dim Contador As Integer = 0
        Dim Total As Integer = 0
        Contador = Len(Valor)
        Rellena_Text = ""
        Total = Longitud - Contador
        Dim i As Integer
        i = 1
        For i = 1 To Total
            Rellena_Text = Rellena_Text + Relleno
            'i = i + 1
        Next i
        Rellena_Text = Rellena_Text + Valor
    End Function

    Function Rellena_Text2(ByVal Valor As String, ByVal Longitud As Integer, ByVal Relleno As String) As String
        Dim Contador As Integer = 0
        Dim Total As Integer = 0
        Contador = Len(Valor)
        Rellena_Text2 = ""
        Total = Longitud - Contador
        Dim i As Integer
        i = 1
        For i = 1 To Total
            Rellena_Text2 = Rellena_Text2 + Relleno
            'i = i + 1
        Next i
        Rellena_Text2 = Valor + Rellena_Text2
    End Function

    Public Function UspHaz_Pregunta(ByVal oContrato As Long, ByVal oOp As Integer) As String
        UspHaz_Pregunta = ""
        Dim CON80 As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand()
        Try
            CMD = New SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "UspHaz_Pregunta"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = oContrato
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Op", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = oOp
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Pregunta", SqlDbType.VarChar, 800)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = ""
                .Parameters.Add(prm2)

                Dim i As Integer = .ExecuteNonQuery()
                UspHaz_Pregunta = prm2.Value
            End With

        Catch ex As Exception

        Finally
            CON80.Close()
        End Try
    End Function

    Public Sub SetDBReport(ByVal ds As DataSet, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                myTable.SetDataSource(ds)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

End Module

