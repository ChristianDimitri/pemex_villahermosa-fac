﻿Imports System.Data.SqlClient
Public Class FrmPeriodoDeCorteTel


    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click

        Me.Close()
        Me.Dispose()

    End Sub
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        Dim resp = MsgBox("Se va a Generar un nuevo Periodo de Cobro con fecha: " & Me.dtpFechaFinal.Value.ToString & " ¿deseas continuar?", MsgBoxStyle.YesNoCancel)

        If resp = MsgBoxResult.Yes Then
            GeneraPeriodoDeCobro()
        End If

    End Sub
    Private Sub GeneraPeriodoDeCobro()
        'PROCEDIMIENTO PARA GENERAR EL PERIODO DE COBRO, GENERAR ESTADOS DE CUENTA Y AFECTAR LAS LLAMADAS QUE SE ENCLUENTREN EN EL RANGO ESTABLECIDO

        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet
        Dim comando As New SqlClient.SqlCommand("GeneraPeriodoDeCorte", conn)

        Try

            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@FECHAFIN", SqlDbType.DateTime)
            comando.Parameters(0).Value = Me.dtpFechaFinal.Value

            comando.Parameters.Add("@Descripcion", SqlDbType.VarChar, 4000)
            comando.Parameters(1).Value = Me.rtbAnotaciones.Text

            comando.Parameters.Add("@TEstadosDeCuentaGenerados", SqlDbType.BigInt)
            comando.Parameters(2).Direction = ParameterDirection.Output
            comando.Parameters(2).Value = 0

            comando.Parameters.Add("@Clv_PeriodoDeCobro", SqlDbType.Int)
            comando.Parameters(3).Direction = ParameterDirection.Output
            comando.Parameters(3).Value = 0

            conn.Open()
            comando.ExecuteNonQuery()

            MsgBox("Se ha creado el Periodo de Cobro Exitosamente, generando un total de: " & comando.Parameters(2).Value.ToString & " Estados de Cuenta.", MsgBoxStyle.Information, "PROCESO CORRECTO")

            'Guardamos en la Bitácora
            bitsist(GloCajera, 0, GloSistema, Me.Name, "", "SE GENERÓ UN NUEVO PERIODO DE COBRO", "ID Periodo: " + comando.Parameters(3).Value.ToString, LocClv_Ciudad)

            MuestraErroresDelPeriodoGenerado(comando.Parameters(3).Value)

        Catch ex As Exception

            MsgBox("Ocurrio un error al momento de Generar el Periodo de Cobro.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            MuestraErroresDelPeriodoGenerado(comando.Parameters(3).Value)

        Finally

            conn.Close()
            conn.Dispose()

        End Try


    End Sub

    Private Sub CargaFechaUltPeriodoDeCorte()

        'Si ya hay periodos tomamos como la fecha límite la fecha final del periodo anterior
        'Si no hay ningún periodo generado tomamos la fecha mínima de los CDRs

        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet

        Try

            Dim comando As New SqlClient.SqlCommand("CargaFechaUltPeriodoDeCorte", conn)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@FECHA_LIMITE", SqlDbType.DateTime)
            comando.Parameters(0).Direction = ParameterDirection.Output
            comando.Parameters(0).Value = Now()

            comando.Parameters.Add("@FECHA_LIMITE_MAYORDEHOY", SqlDbType.DateTime)
            comando.Parameters(1).Direction = ParameterDirection.Output
            comando.Parameters(1).Value = Now()

            conn.Open()
            comando.ExecuteNonQuery()

            If comando.Parameters(0).Value >= comando.Parameters(1).Value Then

                MsgBox("No se puede generar un nuevo Periodo de Cobro ya que el último se generó con Fecha Final del día de hoy: " & comando.Parameters(1).Value, MsgBoxStyle.Information)

                Me.Close()
                Me.Dispose()

            Else

                Me.dtpFechaFinal.MinDate = comando.Parameters(0).Value
                Me.dtpFechaFinal.MaxDate = comando.Parameters(1).Value

            End If


            

        Catch ex As Exception

            MsgBox("Ocurrio un error al momento de Generar el Periodo de Cobro.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        Finally

            conn.Close()
            conn.Dispose()

        End Try


    End Sub

    Private Sub MuestraErroresDelPeriodoGenerado(ByVal Clv_Periodo As Integer)

        'Verificamos si es que el Periodo Arrojó errores

        Dim conn As New SqlConnection(MiConexion)
        Dim ds As New DataSet

        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("MuestraErroresDelPeriodoGenerado", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Adaptador.SelectCommand.Parameters.Add("@PERIODO", SqlDbType.Int).Value = Clv_Periodo
            Adaptador.SelectCommand.Parameters.Add("@NUM_ERRORES", SqlDbType.Int).Value = 0

            Adaptador.SelectCommand.Parameters(1).Direction = ParameterDirection.Output

            Adaptador.Fill(ds)

            ds.Tables(0).TableName = "tbl_ErroresEncontrados"

            If Adaptador.SelectCommand.Parameters(1).Value > 0 Then

                Me.dgvErroresGenerados.DataSource = ds.Tables("tbl_ErroresEncontrados")

                Me.dgvErroresGenerados.Columns(0).Width = 100
                Me.dgvErroresGenerados.Columns(1).Width = 140
                Me.dgvErroresGenerados.Columns(2).Width = 600

                Me.Size = New System.Drawing.Size(806, 740)
                Me.btnGuardar.Enabled = False

                Me.lblSeEncontraronErrores.Text = "Se encontró un total de: " & Adaptador.SelectCommand.Parameters(1).Value.ToString & " error(es) del Periodo: " & " #" & Adaptador.SelectCommand.Parameters(0).Value.ToString

               
            Else

                Me.Close()
                Me.Dispose()

            End If


        Catch ex As Exception

            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        Finally

            conn.Close()
            conn.Dispose()

        End Try

    End Sub

    Private Sub FrmPeriodoDeCorteTel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Size = New System.Drawing.Size(806, 378)
        colorea(Me)
        Me.lblSeEncontraronErrores.ForeColor = Color.Black
        CargaFechaUltPeriodoDeCorte()
    End Sub
End Class